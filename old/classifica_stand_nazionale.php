
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Classifica Stand Nazionale</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Classifica Stand Nazionale</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Classifica Stand Nazionale</h2>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">


<?php
$id_abruzzo		 = 99 ;
$id_basilicata   = 100;
$id_campania	 = 102;
$id_emilia 		 = 103;
$id_friuli		 = 104;
$id_lazio 		 = 105;
$id_liguria 	 = 106;
$id_marche 	 	 = 108;
$id_piemonte	 = 110;
$id_puglia 	 	 = 111;
$id_sardegna 	 = 112;
$id_sicilia 	 = 113;
$id_toscana	 	 = 114;
$id_trento 		 = 116;
$id_aosta 		 = 118;


$campania	 = 0;
$trento 	 = 0;
$abruzzo	 = 0;
$marche 	 = 0;
$sicilia 	 = 0;
$sardegna 	 = 0;
$piemonte	 = 0;
$emilia 	 = 0;
$friuli		 = 0;
$toscana	 = 0;
$lazio 		 = 0;
$puglia 	 = 0;
$aosta 		 = 0;
$basilicata  = 0;
$liguria 	 = 0;

include ("config.inc.php");
echo "<font color=#2B3856 size='2' face='Tahoma'>";
include ("apri_db.php");

$query = "SELECT i.id_comitato_prima AS id_comitato_prima,
				 i.id_comitato_seconda AS id_comitato_seconda,
				 i.id_comitato_terza AS id_comitato_terza
				 FROM iscrizioni AS i";
	$result = mysql_query($query, $db);
	while($row = mysql_fetch_array( $result )) 
	{
		if($row[id_comitato_prima] == 	$id_campania) 	$campania += 10;
		if($row[id_comitato_seconda] == 	$id_campania) 	$campania += 6;
		if($row[id_comitato_terza] == 	$id_campania) 	$campania += 4;
		
		if($row[id_comitato_prima] == 	$id_trento) $trento += 10;
		if($row[id_comitato_seconda] == 	$id_trento) $trento += 6;
		if($row[id_comitato_terza] == 	$id_trento) $trento += 4;
		
		if($row[id_comitato_prima] == 	$id_abruzzo) $abruzzo += 10;
		if($row[id_comitato_seconda] == 	$id_abruzzo) $abruzzo += 6;
		if($row[id_comitato_terza] == 	$id_abruzzo) $abruzzo += 4;
		
		if($row[id_comitato_prima] == 	$id_marche) $marche += 10;
		if($row[id_comitato_seconda] == 	$id_marche) $marche += 6;
		if($row[id_comitato_terza] == 	$id_marche) $marche += 4;
		
		if($row[id_comitato_prima] == 	$id_sicilia) $sicilia += 10;
		if($row[id_comitato_seconda] == 	$id_sicilia) $sicilia += 6;
		if($row[id_comitato_terza] == 	$id_sicilia) $sicilia += 4;
		
		if($row[id_comitato_prima] == 	$id_sardegna) $sardegna += 10;
		if($row[id_comitato_seconda] ==	$id_sardegna) $sardegna += 6;
		if($row[id_comitato_terza] == 	$id_sardegna) $sardegna += 4;
		
		if($row[id_comitato_prima] == 	$id_piemonte) $piemonte += 10;
		if($row[id_comitato_seconda] == 	$id_piemonte) $piemonte += 6;
		if($row[id_comitato_terza] == 	$id_piemonte) $piemonte += 4;
		
		if($row[id_comitato_prima] == 	$id_emilia) $emilia += 10;
		if($row[id_comitato_seconda] == 	$id_emilia) $emilia += 6;
		if($row[id_comitato_terza] == 	$id_emilia) $emilia += 4;
		
		if($row[id_comitato_prima] == 	$id_friuli) $friuli += 10;
		if($row[id_comitato_seconda] == 	$id_friuli) $friuli += 6;
		if($row[id_comitato_terza] == 	$id_friuli) $friuli += 4;
		
		if($row[id_comitato_prima] == 	$id_toscana) $toscana += 10;
		if($row[id_comitato_seconda] == 	$id_toscana) $toscana += 6;
		if($row[id_comitato_terza] == 	$id_toscana) $toscana += 4;
		
		if($row[id_comitato_prima] ==	$id_lazio) $lazio += 10;
		if($row[id_comitato_seconda] == 	$id_lazio) $lazio += 6;
		if($row[id_comitato_terza] == 	$id_lazio) $lazio += 4;
		
		if($row[id_comitato_prima] == 	$id_puglia) $puglia += 10;
		if($row[id_comitato_seconda] == 	$id_puglia) $puglia += 6;
		if($row[id_comitato_terza] == 	$id_puglia) $puglia += 4;
		
		if($row[id_comitato_prima] == 	$id_aosta) $aosta += 10;
		if($row[id_comitato_seconda] == 	$id_aosta) $aosta += 6;
		if($row[id_comitato_terza] == 	$id_aosta) $aosta += 4;
		
		if($row[id_comitato_prima] == 	$id_basilicata) $basilicata += 10;
		if($row[id_comitato_seconda] == 	$id_basilicata) $basilicata += 6;
		if($row[id_comitato_terza] == 	$id_basilicata) $basilicata += 4;
		
		if($row[id_comitato_prima] ==	$id_liguria) $liguria += 10;
		if($row[id_comitato_seconda] == 	$id_liguria) $liguria += 6;
		if($row[id_comitato_terza] == 	$id_liguria) $liguria += 4;
	}

	$classifica = array(
		$id_abruzzo	 =>  $abruzzo,	
		$id_basilicata  =>  $basilicata ,
		$id_campania =>  $campania	,
		$id_emilia =>  $emilia 	,	
		$id_friuli =>  $friuli	,	
		$id_lazio =>  $lazio 		,
		$id_liguria =>  $liguria 	,
		$id_marche 	 =>  $marche 	, 	
		$id_piemonte =>  $piemonte	,
		$id_puglia 	 =>  $puglia 	 ,	
		$id_sardegna =>  $sardegna 	,
		$id_sicilia =>  $sicilia 	,
		$id_toscana 	 =>  $toscana	 ,	
		$id_trento	 =>  $trento 		,
		$id_aosta =>  $aosta 	
	)		;
		
		arsort($classifica);
		
		foreach ($classifica as $key => $val) {
			echo "$key = $val\n";
		}
		

?>
	
</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
