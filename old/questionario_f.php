<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice') && !($check['team'] == 'capitani'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>	<!--IE 7 quirks mode please-->	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">	<head>		<title>Meeting 2016 - Comitato Regionale del Piemonte - Homepage</title>			<!-- Contents -->        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />		<meta http-equiv="Content-Language" content="it" />		<meta http-equiv="last-modified" content="07/01/2009 10.37.23" />		<meta http-equiv="Content-Type-Script" content="text/javascript" />		<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />		<meta name="keywords" content="" />			<!-- Others -->		<meta name="Author" content="Paolo di Toma" />		<meta http-equiv="ImageToolbar" content="False" />		<meta name="MSSmartTagsPreventParsing" content="True" />		<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />			<!-- Parent -->		<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />			<!-- Res -->		<script type="text/javascript" src="res/x5engine.js"></script>		<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />		<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />		<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />		<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->		<link rel="stylesheet" type="text/css" href="res/home.css" media="screen, print" />		<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />		<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />			<!-- Robots -->		<meta http-equiv="Expires" content="0" />		<meta name="Resource-Type" content="document" />		<meta name="Distribution" content="global" />		<meta name="Robots" content="index, follow" />		<meta name="Revisit-After" content="21 days" />		<meta name="Rating" content="general" />	</head>	<body>	<div id="imSite">	<div id="imHeader">

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2015</h1>

		<p><strong>Questionario Foxtrot</strong></p>

        <div data-html="true">

		<?php
		$id_comitato = $check['level'];
		?>
				<fieldset data-role="controlgroup">
					<a href="domanda_f1.php?id_comitato=<?= $id_comitato ?>" class="ui-shadow ui-btn ui-corner-all">Domanda 1</a><br />
					<a href="domanda_f2.php?id_comitato=<?= $id_comitato ?>" class="ui-shadow ui-btn ui-corner-all">Domanda 2</a><br />
					<a href="domanda_f3.php?id_comitato=<?= $id_comitato ?>" class="ui-shadow ui-btn ui-corner-all">Domanda 3</a><br />
					<a href="domanda_f4.php?id_comitato=<?= $id_comitato ?>" class="ui-shadow ui-btn ui-corner-all">Domanda 4</a><br />
					<a href="domanda_f5.php?id_comitato=<?= $id_comitato ?>" class="ui-shadow ui-btn ui-corner-all">Domanda 5</a><br />
					<a href="domanda_f6.php?id_comitato=<?= $id_comitato ?>" class="ui-shadow ui-btn ui-corner-all">Domanda 6</a><br />
					<a href="domanda_f7.php?id_comitato=<?= $id_comitato ?>" class="ui-shadow ui-btn ui-corner-all">Domanda 7</a><br />
					<a href="domanda_f8.php?id_comitato=<?= $id_comitato ?>" class="ui-shadow ui-btn ui-corner-all">Domanda 8</a><br />
					<a href="domanda_f9.php?id_comitato=<?= $id_comitato ?>" class="ui-shadow ui-btn ui-corner-all">Domanda 9</a><br />
					<a href="domanda_f10.php?id_comitato=<?= $id_comitato ?>" class="ui-shadow ui-btn ui-corner-all">Domanda 10</a><br />
				</fieldset>
                <br />

        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>