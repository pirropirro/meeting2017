<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice') && !($check['team'] == 'tutor'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
 
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Registrazione Squadra Osservatori</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/tableExport.js"></script>
	<script type="text/javascript" src="js/jquery.base64.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Registrazione Squadra Osservatori</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Registrazione Squadra Osservatori</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include("config.inc.php");
echo "<font color=#2B3856 size='3' face='Calibri'>";

$id_ritira = $_REQUEST['id_ritira'];
$id_comitato = $_REQUEST['id_comitato'];
$penalita_over = $_REQUEST['penalita_over'];

include ("apri_db.php");

//segno prima gli assenti
$assenze = isset($_POST['assenze']) ? $_POST['assenze'] : array();
foreach($assenze as $assenze)
{
	//Recupero i dati del selezionato
	$query = "SELECT * FROM iscrizioni WHERE id = '$assenze'";
	$result = mysql_query($query, $db);
	$row = mysql_fetch_array( $result );
	
	$nome_selezionato = $row[nome];
	$cognome_selezionato = $row[cognome];

	//Faccio l'update del record in tabella
	$query = "UPDATE iscrizioni SET registrazione = '9' WHERE id = '$assenze'";
	if (mysql_query($query, $db))
	{
		//Espongo il messaggio a video
		echo "Il volontario <b>$nome_selezionato $cognome_selezionato</b> risulta assente.<br>";
	}
	else
	{
		echo "Errore durante la registrazione.<br><br>";
	}
}


$presenze = isset($_POST['presenze']) ? $_POST['presenze'] : array();
foreach($presenze as $presenze)
{
	//Recupero i dati del selezionato
	$query = "SELECT * FROM iscrizioni WHERE id = '$presenze'";
	$result = mysql_query($query, $db);
	$row = mysql_fetch_array( $result );
	
	$nome_selezionato = $row[nome];
	$cognome_selezionato = $row[cognome];

	//Faccio l'update del record in tabella
	$query = "UPDATE iscrizioni SET registrazione = '1' WHERE id = '$presenze'";
	if (mysql_query($query, $db))
	{
		//Espongo il messaggio a video
		echo "Il volontario <b>$nome_selezionato $cognome_selezionato</b> è stato registrato.<br>";
	}
	else
	{
		echo "Errore durante la registrazione.<br><br>";
	}
}
echo "<br><br>";

//Registro con 2 chi effettua la registrazione
$query = "UPDATE iscrizioni SET registrazione = '2' WHERE id = '$id_ritira'";
if (mysql_query($query, $db))
{
	$query = "SELECT * FROM iscrizioni WHERE id = '$id_ritira'";
	$result = mysql_query($query, $db);
	$row = mysql_fetch_array( $result );
	
	$nome_selezionato = $row[nome];
	$cognome_selezionato = $row[cognome];

	//Espongo il messaggio a video
	echo "Il materiale è stato ritirato da <b>$nome_selezionato $cognome_selezionato</b><br>";
}
else
{
	echo "Non ho registrato chi ha ritirato il materiale.<br>";
}


//Registro che la squadra è stata registrata e l'eventuale penalità
$query = "UPDATE preiscrizioni SET accoglienza = '2', penalita_over =  '$penalita_over' WHERE id_comitato = '$id_comitato'";
if (mysql_query($query, $db))
{
	//Espongo il messaggio a video
	echo "La registrazione è terminata <b>correttamente</b>.<br>";
}
else
{
	echo "La registrazione non è terminata <b>correttamente</b>. Riportare questo errore in Control Room<br>";
}

//$query_buonipasto = "SELECT id FROM iscrizioni WHERE id_comitato = '$id_comitato' AND (registrazione = 1 OR registrazione = 2)";
//$result_buonipasto = mysql_query($query_buonipasto, $db);
//$num_buonipasto = mysql_num_rows($result_buonipasto);

//echo "Consegnare: <b>$num_buonipasto</b> ticket per pasto<br><br>";


mysql_close($db);
?>

<br /><br />

<form method="post" action="stampa_registrazione.php?comitato=<?php echo $comitato ?>">
<input type="submit" value="Stampa ricevuta registrazione" />
</form>


</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
