<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato al Back Office.</b></font>";
	exit;	// Stop script execution
}

$stanze_p2 = array ('aula_16','aula_17','aula_18','aula_7','aula_8','aula_9','aula_4');
$stanze_p1 = array ('aula_13','aula_14','aula_15');
$stanze_pt = array ('aula_1','aula_2','aula_3','aula_12');
$stanze_ps = array ('aula_10','aula_11','aula_mensa','aula_liberi');


?>
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Tabellone Aule</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/tableExport.js"></script>
	<script type="text/javascript" src="js/jquery.base64.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body style="background-color: white;">



<!-- Menu Content START -->

<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->
<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />


<?php
include ("config.inc.php");
echo "<font color=#2B3856 size='2' face='Tahoma'>";
include ("apri_db.php");
$ora_attuale = strftime('%H:%M', time());
?>
<table border="0" width=100%>

<tr>
<th align="center" style="border-style:outset" style="font size:100%" style="color:#FFFF99"><div style="height:25px;">Aggiornato alle <?= $ora_attuale ?></th> </tr>
</table>

<table class="bordata" id="tabellone_aule">
<thead>
<tr>
<th align="center" style="order-style:outset" style="font size:100%" style="color:#FFFF99"><div style="width:50px; height:25px;">PIANO</th>
<th colspan="7" align="center" style="order-style:outset" style="font size:100%" style="color:#FFFF99"><div style="width:1750px; height:25px;">AULE</th>
</thead>
<td rowspan="2" align="center" style="font size:90%" bgcolor="#ffffff"><div style="width:44px; height:auto; overflow-y:hidden; overflow-x:hidden;">P2</td>
<td align="center" style="order-style:outset" style="font size:100%"><div style="background-color:#81BEF7; height:21px;"><b>Aula 2 NERO A</b></td>
<td align="center" style="order-style:outset" style="font size:100%"><div style="background-color:#81BEF7; height:21px;"><b>Aula 2 NERO B</b></td> 
<td align="center" style="order-style:outset" style="font size:100%"><div style="background-color:#81BEF7; height:21px;"><b>Aula 2 NERO C</b></td>
<td align="center" style="order-style:outset" style="font size:100%"><div style="background-color:#81BEF7; height:21px;"><b>Aula 2 BLU A</b></td> 
<td align="center" style="order-style:outset" style="font size:100%"><div style="background-color:#81BEF7; height:21px;"><b>Aula 2 BLU B</b></td>
<td align="center" style="order-style:outset" style="font size:100%"><div style="background-color:#81BEF7; height:21px;"><b>Aula 2 BLU C</b></td>
<td align="center" style="order-style:outset" style="font size:100%"><div style="background-color:#81BEF7; height:21px;"><b>Aula 2 GIALLA</b></td>
<tr>
<?php
//Faccio un ciclo da 7
foreach ($stanze_p2 as $stanza)
{
	//Faccio la query sulla tabella iscrizioni per estrarre i dati da visualizzare
	$query = "SELECT		c.nome_comitato AS nome_comitato,
							c.id AS id_comitato,
							t.ora_checkin AS ora_checkin
                            FROM timesheet AS t
                            INNER JOIN comitati AS c
                            ON t.id_comitato = c.id
                            WHERE t.estrarre = 'prossimo'
							AND t.aula = '$stanza'
							AND t.ora_checkin IS NOT NULL
							AND t.ora_checkout IS NULL";
	$result = mysql_query($query, $db);
	$squadre_presenti = mysql_num_rows($result);
	$spazi_vuoti = 4 - $squadre_presenti;
	?>
	<td align=center style="font size:90%"><div style="background-color:<?php if($squadre_presenti != 0) {echo "#F08080";} else{echo "#ffffff";} ?>; width:250px; height:21; overflow-y:hidden; overflow-x:hidden;">
	<?php
	while ($row = mysql_fetch_array($result)) 
	{
		$query_persone = "SELECT id FROM iscrizioni WHERE id_comitato = $row[id_comitato] AND (ruolo = 'capitano' OR ruolo = 'partecipante' OR ruolo = 'osservatore')";
		$result_persone = mysql_query($query_persone, $db);
		$num_persone = mysql_num_rows($result_persone);
		
		$re = "/(|\\s*comitato\\s*|\\s*provinciale\\s*|\\s*regionale\\s*|\\s*locale\\s*|\\s*di\\s*|\\s*delegazione\\s*|\\sa\\s|\\s*valenza\\s*)/i"; 
		$subst = ""; 	 
		$comitato_trunc = preg_replace($re, $subst, $row[nome_comitato]);

		?>
		<font color="#2B3856"><?= $comitato_trunc." (".$num_persone.") - ".$row[ora_checkin] ?></font>
		<br />
		<?php
    }
    for ($i = 1; $i <= $spazi_vuoti; $i++) echo "<br>";
	?>
    
	</td>
	<?php
}
?>

<table id="tabellone_aule">
<thead>
<tr>
<th colspan="5" align="center"style="color:#FFFF99"><div style="width:800px; height:25px;">&nbsp;</th>
</thead>
<td rowspan="2" align="center" style="font size:90%" bgcolor="#ffffff"><div style="width:50px; height:auto; overflow-y:hidden; overflow-x:hidden;">P1</td>
<td align="center" style="font size:100%"><div style="background-color:#81BEF7; height:21px;"><b>Aula 1 LILLA A</b></td>
<td align="center" style="font size:100%"><div style="background-color:#81BEF7; height:21px;"><b>Aula 1 LILLA B</b></td> 
<td align="center" style="font size:100%"><div style="background-color:#81BEF7; height:21px;"><b>Aula 1 LILLA C</b></td>
<tr>
<?php
//Faccio un ciclo da 3
foreach ($stanze_p1 as $stanza)
{
	//Faccio la query sulla tabella iscrizioni per estrarre i dati da visualizzare
	$query = "SELECT		c.nome_comitato AS nome_comitato,
							c.id AS id_comitato,
							t.ora_checkin AS ora_checkin
                            FROM timesheet AS t
                            INNER JOIN comitati AS c
                            ON t.id_comitato = c.id
                            WHERE t.estrarre = 'prossimo'
							AND t.aula = '$stanza'
							AND t.ora_checkin IS NOT NULL
							AND t.ora_checkout IS NULL";
	$result = mysql_query($query, $db);
	$squadre_presenti = mysql_num_rows($result);
	$spazi_vuoti = 4 - $squadre_presenti;
	?>
	<td align=center style="font size:90%"><div style="background-color:<?php if($squadre_presenti != 0) {echo "#F08080";} else{echo "#ffffff";} ?>; width:250px; height:21; overflow-y:hidden; overflow-x:hidden;">
	<?php
	while ($row = mysql_fetch_array($result)) 
	{
		$query_persone = "SELECT id FROM iscrizioni WHERE id_comitato = $row[id_comitato] AND (ruolo = 'capitano' OR ruolo = 'partecipante' OR ruolo = 'osservatore')";
		$result_persone = mysql_query($query_persone, $db);
		$num_persone = mysql_num_rows($result_persone);
		
		$re = "/(|\\s*comitato\\s*|\\s*provinciale\\s*|\\s*regionale\\s*|\\s*locale\\s*|\\s*di\\s*|\\s*delegazione\\s*|\\sa\\s|\\s*valenza\\s*)/i"; 
		$subst = ""; 	 
		$comitato_trunc = preg_replace($re, $subst, $row[nome_comitato]);

		?>
		<font color="#2B3856"><?= $comitato_trunc." (".$num_persone.") - ".$row[ora_checkin] ?></font>
		<br />
		<?php
    }
    for ($i = 1; $i <= $spazi_vuoti; $i++) echo "<br>";
    ?>
	</td>
	<?php
}
?>
</table>

<table id="tabellone_aule">
<thead>
<tr>
<th colspan="5" align="center" style="order-style:outset" style="font size:100%" style="color:#FFFF99"><div style="width:1000px; height:25px;">&nbsp;</th>
</thead>
<td rowspan="2" align="center" style="font size:90%" bgcolor="#ffffff"><div style="width:44px; height:auto; overflow-y:hidden; overflow-x:hidden;">PT</td>
<td align="center" style="order-style:outset" style="font size:100%"><div style="background-color:#81BEF7; height:21px;"><b>TERRA VERDE A</b></td>
<td align="center" style="order-style:outset" style="font size:100%"><div style="background-color:#81BEF7; height:21px;"><b>TERRA VERDE B</b></td> 
<td align="center" style="order-style:outset" style="font size:100%"><div style="background-color:#81BEF7; height:21px;"><b>TERRA VERDE C</b></td>
<td align="center" style="order-style:outset" style="font size:100%"><div style="background-color:#81BEF7; height:21px;"><b>TERRA ROSSA</b></td> 
<tr>
<?php
//Faccio un ciclo da 7
foreach ($stanze_pt as $stanza)
{
	//Faccio la query sulla tabella iscrizioni per estrarre i dati da visualizzare
	$query = "SELECT		c.nome_comitato AS nome_comitato,
							c.id AS id_comitato,
							t.ora_checkin AS ora_checkin
                            FROM timesheet AS t
                            INNER JOIN comitati AS c
                            ON t.id_comitato = c.id
                            WHERE t.estrarre = 'prossimo'
							AND t.aula = '$stanza'
							AND t.ora_checkin IS NOT NULL
							AND t.ora_checkout IS NULL";
	$result = mysql_query($query, $db);
	$squadre_presenti = mysql_num_rows($result);
	$spazi_vuoti = 4 - $squadre_presenti;
	?>
	<td align=center style="font size:90%"><div style="background-color:<?php if($squadre_presenti != 0) {echo "#F08080";} else{echo "#ffffff";} ?>; width:250px; height:110; overflow-y:hidden; overflow-x:hidden;">
	<?php
	while ($row = mysql_fetch_array($result)) 
	{
		$query_persone = "SELECT id FROM iscrizioni WHERE id_comitato = $row[id_comitato] AND (ruolo = 'capitano' OR ruolo = 'partecipante' OR ruolo = 'osservatore')";
		$result_persone = mysql_query($query_persone, $db);
		$num_persone = mysql_num_rows($result_persone);
		
		$re = "/(|\\s*comitato\\s*|\\s*provinciale\\s*|\\s*regionale\\s*|\\s*locale\\s*|\\s*di\\s*|\\s*delegazione\\s*|\\sa\\s|\\s*valenza\\s*)/i"; 
		$subst = ""; 	 
		$comitato_trunc = preg_replace($re, $subst, $row[nome_comitato]);

		?>
		<font color="#2B3856"><?= $comitato_trunc." (".$num_persone.") - ".$row[ora_checkin] ?></font>
		<br />
		<?php
    }
    for ($i = 1; $i <= $spazi_vuoti; $i++) echo "<br>";
    ?>
	</td>
	<?php
}
?>
</table>


<table id="tabellone_aule">
<thead>
<tr>
<th colspan="8" align="center" style="order-style:outset" style="font size:100%" style="color:#FFFF99"><div style="width:1000px; height:25px;">&nbsp;</th>
</thead>
<td rowspan="2" align="center" style="font size:90%" bgcolor="#ffffff"><div style="width:44px; height:auto; overflow-y:hidden; overflow-x:hidden;">PS</td>
<td align="center" style="order-style:outset" style="font size:100%"><div style="background-color:#81BEF7; height:21px;"><b>-1 ROSSA A</b></td>
<td align="center" style="order-style:outset" style="font size:100%"><div style="background-color:#81BEF7; height:21px;"><b>-1 ROSSA B</b></td> 
<td colspan="2" align="center" style="order-style:outset" style="font size:100%"><div style="background-color:#81BEF7; height:21px;"><b>MENSA</b></td> 
<td colspan="2" align="center" style="order-style:outset" style="font size:100%"><div style="background-color:#81BEF7; height:21px;"><b>PAUSA</b></td> 
<tr>
<?php
//Faccio un ciclo da 7
foreach ($stanze_ps as $stanza)
{
	//Faccio la query sulla tabella iscrizioni per estrarre i dati da visualizzare
	$query = "SELECT		c.nome_comitato AS nome_comitato,
							c.id AS id_comitato,
							t.ora_checkin AS ora_checkin
                            FROM timesheet AS t
                            INNER JOIN comitati AS c
                            ON t.id_comitato = c.id
                            WHERE t.estrarre = 'prossimo'
							AND t.aula = '$stanza'
							AND t.ora_checkin IS NOT NULL
							AND t.ora_checkout IS NULL";
	$result = mysql_query($query, $db);
	$squadre_presenti = mysql_num_rows($result);
	$spazi_vuoti = 4 - $squadre_presenti;
	?>
	<td align=center style="font size:90%"><div style="background-color:<?php if($squadre_presenti != 0) {echo "#F08080";} else{echo "#ffffff";} ?>; width:250px; height:110; overflow-y:hidden; overflow-x:hidden;">
	<?php
	while ($row = mysql_fetch_array($result)) 
	{
		$query_persone = "SELECT id FROM iscrizioni WHERE id_comitato = $row[id_comitato] AND (ruolo = 'capitano' OR ruolo = 'partecipante' OR ruolo = 'osservatore')";
		$result_persone = mysql_query($query_persone, $db);
		$num_persone = mysql_num_rows($result_persone);
		
		$re = "/(|\\s*comitato\\s*|\\s*provinciale\\s*|\\s*regionale\\s*|\\s*locale\\s*|\\s*di\\s*|\\s*delegazione\\s*|\\sa\\s|\\s*valenza\\s*)/i"; 
		$subst = ""; 	 
		$comitato_trunc = preg_replace($re, $subst, $row[nome_comitato]);

		?>
		<font color="#2B3856"><?= $comitato_trunc." (".$num_persone.") - ".$row[ora_checkin] ?></font>
		<br />
		<?php
    }
    for ($i = 1; $i <= $spazi_vuoti; $i++) echo "<br>";
    ?>
	</td>
	<?php
}
?>
</table>

<?php
mysql_close($db); 
?>

</div>
</div>

</body>
</html>