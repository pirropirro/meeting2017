<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
 
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Invio Mail Apertura Iscrizioni Senza Squadra</title>

	<!-- Contents xxxxx-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p018.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Invio Mail Apertura Iscrizioni Senza Squadra</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Invio Mail Apertura Iscrizioni Senza Squadra</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include ("config.inc.php");
echo "<font color=#2B3856 size='3' face='Tahoma'>";
$fp = fopen ("log_iscrizioni.txt",a);
include('PHPMailer/class.phpmailer.php');
include ("apri_db.php");

//Estraggo in un array le preiscrizioni non confermate
//$query ="SELECT 	c.nome_comitato AS nome_comitato,
//					c.nome_presidente AS nome_presidente,
//					c.cognome_presidente AS cognome_presidente,
//					c.mail_comitato AS mail_comitato,
//					c.passwd AS passwd_comitato,
//					p.id AS id_preiscrizione,
//					p.nome_delegato AS nome_volontario,
//					p.cognome_delegato AS cognome_volontario,
//					p.mail_delegato AS mail_volontario,
//					DATE_FORMAT(p.data_inserimento, '%d-%m') AS data_inserimento,
//					DATE_FORMAT(p.data_inserimento, '%H:%i') AS ora_inserimento
//					FROM preiscrizioni AS p
//					INNER JOIN comitati AS c
//					ON p.id_comitato = c.id
//					WHERE p.conferma_presidente = '1'
//					AND p.mail_apertura_iscrizioni IS NULL
//					AND p.iscrizione IS NULL
//					ORDER BY p.data_conferma DESC";

$query = "SELECT 	c.nome_comitato AS nome_comitato,
					c.id AS id,
					c.nome_presidente AS nome_presidente,
					c.cognome_presidente AS cognome_presidente,
					c.mail_comitato AS mail_comitato,
					c.passwd AS passwd_comitato,
					c.nome_delegato5 AS nome_volontario,
					c.cognome_delegato5 AS cognome_volontario,
					c.mail_delegato5 AS mail_volontario
					FROM comitati AS c
					WHERE c.id NOT IN (
										SELECT p.id_comitato
										FROM preiscrizioni AS p
										WHERE p.id_comitato = c.id
									  )
					AND c.tipo_meeting!='N'
					AND c.mail_delegato5!=''
					AND c.id !='1'";

//$query = "SELECT * FROM preiscrizioni WHERE conferma_presidente = '0' ORDER BY data_inserimento";
$result = mysql_query($query, $db);
while($row = mysql_fetch_array( $result )) 
{
	//echo "Presidente $row[nome_presidente] $row[cognome_presidente] - Preiscrizione n. $row[id_preiscrizione] del $row[nome_comitato] con mail $row[mail_comitato] - Volontario $row[nome_volontario] $row[cognome_volontario] con mail $row[mail_volontario] - Inserita in data $row[data_inserimento] alle ore $row[ora_inserimento] <br>";
	
	//Invio la mail di sollecito con me in ccn
						
	$mittente = "meeting.2015@cri.it";
	$nomemittente = "Back Office Meeting 2015";
	$destinatario = "$row[mail_volontario]";
	$ServerSMTP = "mailbox.cri.it"; //server SMTP
	$oggetto = "Apertura Iscrizioni Meeting 2015";
	$corpo_messaggio = "Buongiorno $row[nome_volontario],\ncon la presente ti notifichiamo che abbiamo reso possibile da oggi l'iscrizione di Osservatori anche per i comitati che non hanno una squadra partecipante al Meeting 2015 (per il $row[nome_comitato] non risulta essere stata fatta la preiscrizione). Collegati al portale all'indirizzo http://www.itempa.com/meeting2015 e vai nella sezione Iscrizioni, troverai la voce Iscrizione Osservatore senza squadra. Seleziona il tuo comitato ed inserisci la password    $row[passwd_comitato]    e procedi con l'iscrizione.\n\nPer le iscrizioni di ogni volontario (sia esso partecipante che osservatore) dovrai essere in possesso dei seguenti dati:\n\n - nome;\n - cognome;\n - recapito telefonico (meglio se smartphone);\n - indirizzo e-mail (distinto e personale);\n - taglia t-shirt;\n - eventuali esigenze alimentari;\n - eventuali disabilità;\n - codice fiscale (non obbligatorio ma richiesto per probabile gadget)\n - - disponibilità come simulatore;\n - disponibilità come truccatore;\n - disponibilità come giudice.\n\n Ti ricordiamo, se non l'hai già fatto, di prendere visione del Regolamento per i Comitati Locali al Meeting Provinciale-Regionale disponibile al link\n\n   http://www.itempa.com/meeting2015/RegolamentoMeeting2015_Provinciale.pdf\n\no del Regolamento per i Comitati Regionali al Meeting Nazionale disponibile al link\n\n   http://www.itempa.com/meeting2015/RegolamentoMeeting2015_Nazionale.pdf\n\n\nAbbiamo inserito in copia alla presente, il tuo presidente $row[nome_presidente] $row[cognome_presidente] per completezza di informazioni, lo stesso leggerà sempre in cc ogni iscrizione venga da te effettuata.\nPer qualsiasi dubbio, angoscia e/o perplessità, non esitare a contattarci.\n\nCordiali Saluti,\nBack Office\nMeeting 2015\n\n(inviata automaticamente dal Portale Web)";
		
	$msg = new PHPMailer;
	$msg->CharSet = "UTF-8";
	$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
	//$msg->IsHTML(true);
	$msg->SMTPAuth = true; // Autenticazione SMTP
	$msg->SMTPKeepAlive = "true";
	$msg->Host = $ServerSMTP;
	$msg->Username = "meeting.2015@cri.it"; // Nome utente SMTP autenticato
	$msg->Password = "Spaghett!44"; // Password account email con SMTP autenticato
	
	$msg->From = $mittente;
	$msg->FromName = $nomemittente;
	$msg->AddAddress($destinatario); 
	$msg->AddCC("$row[mail_comitato]");
	$msg->AddBCC("paolo.ditoma@libero.it");
	$msg->Subject = $oggetto; 
	$msg->Body = $corpo_messaggio;
	
	if(!$msg->Send())
	{
		echo "Si è verificato un errore nell'invio della mail di notifica apertura iscrizioni:".$msg->ErrorInfo;
		fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE - Invio Mail Apertura Iscrizioni Senza Squadra - Presidente $row[nome_presidente] $row[cognome_presidente] - $row[nome_comitato] con mail $row[mail_comitato] - Volontario $row[nome_volontario] $row[cognome_volontario] con mail $row[mail_volontario] ".$msg->ErrorInfo." <br>"."\r\n");
	}
	else
	{
		$query = "UPDATE preiscrizioni SET mail_apertura_iscrizioni = '1' WHERE id = '$row[id_preiscrizione]'";
        mysql_query($query, $db);
		echo "Mail Apertura Iscrizioni Senza Squadra inviata per $row[nome_comitato] al volontario $row[nome_volontario] $row[cognome_volontario] all'indirizzo $row[mail_volontario] ed in cc al presidente $row[nome_presidente] $row[cognome_presidente] all'indirizzo $row[mail_comitato]. <br>";
		fwrite($fp,date('d/m/Y H:i:s').' - '."Mail Apertura Iscrizioni Senza Squadra inviata per $row[nome_comitato] al volontario $row[nome_volontario] $row[cognome_volontario] all'indirizzo $row[mail_volontario] ed in cc al presidente $row[nome_presidente] $row[cognome_presidente] all'indirizzo $row[mail_comitato]."."\r\n");
	}	

}
	
fclose($fp);
mysql_close($db); 
?>

<br /><br />

</div>
</div>
</div>


<!-- Page Content END -->

	</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
