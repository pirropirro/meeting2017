<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice') && !($check['team'] == 'organigramma') && !($check['team'] == 'nazionale'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato all'organizzazione od al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>

<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Ricerca iscrizioni</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Ricerca iscrizione</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Ricerca Iscrizioni</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include ("config.inc.php");
echo "<font color=#2B3856 size='2' face='Tahoma'>";
include ("apri_db.php");
?>

<form method="post" action="risultato_ricerca_iscrizioni.php">

<br />
<fieldset>
		<legend><b>Squadra del</b></legend>
 
<select name="id_comitato" size="4" >
<option value='%'>Tutti i comitati</option>

<?php
$query = "SELECT 	DISTINCT (i.id_comitato) AS id_comitato,
					c.nome_comitato AS nome_comitato,
					c.tipo_meeting AS meeting
					FROM iscrizioni AS i
					INNER JOIN comitati AS c
					ON i.id_comitato = c.id
					GROUP BY i.id_comitato
					ORDER BY c.tipo_meeting DESC";

$result = mysql_query($query, $db);
while($row = mysql_fetch_array( $result )) 
{
	?>
	<option value='<?php echo $row[id_comitato]; ?>'><?php echo $row[nome_comitato]; ?></option>
	<?php
}
mysql_close($db);
?>
</select>
</fieldset><br />
<br />

<fieldset>
		<legend><b>Ruolo</b></legend>
		<br />
		Capitano <input type="radio" name="ruolo" value="ruolo='capitano'"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Partecipante <input type="radio" name="ruolo" value="(ruolo='capitano' OR ruolo='partecipante' OR ruolo='adulto')"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Osservatore <input type="radio" name="ruolo" value="ruolo='osservatore'"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Simulatore <input type="radio" name="ruolo" value="ruolo='simulatore'"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Truccatore <input type="radio" name="ruolo" value="ruolo='truccatore'"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Giudice <input type="radio" name="ruolo" value="ruolo='giudice'"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Staff <input type="radio" name="ruolo" value="ruolo='staff'"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Coordinamento <input type="radio" name="ruolo" value="ruolo='coordinamento'"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Ospite <input type="radio" name="ruolo" value="ruolo='ospite'"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Tutti <input type="radio" name="ruolo" value="1" checked="checked"/><br />
</fieldset><br />
<br />

<fieldset>
	<legend><b>Dati del volontario</b></legend>
        <br />
        Nome&nbsp&nbsp
        <input type="text" name="nome" size="20" maxlength="40">
        
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        
        Cognome&nbsp&nbsp
        <input type="text" name="cognome" size="20" maxlength="40"><br />
        <br />
        
        Recapito telefonico&nbsp&nbsp
        <input type="tel" name="telefono" size="8" title="prefisso e numero senza spazi o altri caratteri (max 10 cifre)" pattern="[0-9]{10}">
        
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        
        E-mail&nbsp&nbsp
        <input type="email" name="mail" size="28"><br />
        <br />
    
		<legend><b>Esigenze alimentari</b></legend>
		<br />
		Indicate <input type="radio" name="esigenze" value="(esigenze IS NOT NULL AND esigenze!='')"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Nessuna indicazione <input type="radio" name="esigenze" value="(esigenze IS NULL OR esigenze='')"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Indifferente <input type="radio" name="esigenze" value="1=1" checked="checked"/><br />
        <br />

		<legend><b>Eventuali disabilità</b></legend>
		<br />
		Indicate <input type="radio" name="disabilita" value="(disabilita IS NOT NULL AND disabilita!='')"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Nessuna indicazione <input type="radio" name="disabilita" value="(disabilita IS NULL OR disabilita='')"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Indifferente <input type="radio" name="disabilita" value="1=1" checked="checked"/><br />

</fieldset><br />
    <br />
    
<fieldset>
		<legend><b>Disponibilità come simulatore</b></legend>
		<br />
		Qualificato <input type="radio" name="simulatore" value="simulatore='2'"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Aspirante simulatore <input type="radio" name="simulatore" value="simulatore='1'"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Non disponibile <input type="radio" name="simulatore" value="simulatore='0'"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Indifferente <input type="radio" name="simulatore" value="1=1" checked="checked"/><br />
</fieldset><br />
		<br />

<fieldset>
		<legend><b>Disponibilità come truccatore</b></legend>
		<br />
		Qualificato <input type="radio" name="truccatore" value="truccatore='2'"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Non disponibile <input type="radio" name="truccatore" value="truccatore='0'"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Indifferente <input type="radio" name="truccatore" value="1=1" checked="checked"/><br />
</fieldset><br />
		<br />

<fieldset>
        <legend><b>Disponibilità come giudice</b></legend>
        <br />
        <input type="checkbox" name="giudice_1" value="AND giudice_1='1'"/>Animatore di Educazione alla Sessualità e Prevenzione dalle MST<br/><br/>
        <input type="checkbox" name="giudice_2" value="AND giudice_2='1'"/>Animatore di Educazione Alimentare<br/><br/>
        <input type="checkbox" name="giudice_3" value="AND giudice_3='1'"/>Monitore PS<br/><br/>
        <input type="checkbox" name="giudice_4" value="AND giudice_4='1'"/>Operatore Sociale Generico<br/><br/>
        <input type="checkbox" name="giudice_5" value="AND giudice_5='1'"/>OPEM<br/><br/>
        <input type="checkbox" name="giudice_6" value="AND giudice_6='1'"/>Operatore Climate in Action<br/><br/>
        <input type="checkbox" name="giudice_7" value="AND giudice_7='1'"/>Istruttore DIU<br/><br/>
        <input type="checkbox" name="giudice_8" value="AND giudice_8='1'"/>Operatore di Cooperazione Internazionale<br/><br/>
        <input type="checkbox" name="giudice_9" value="AND giudice_9='1'"/>Operatore Giovani in Azione<br/><br/>
        <input type="checkbox" name="giudice_10" value="AND giudice_10='1'"/>Animatore di Attività per la Gioventù<br/><br/>
        <input type="checkbox" name="giudice_11" value="AND giudice_11='1'"/>Istruttore di EducAzione alla Pace<br/><br/>
        <input type="checkbox" name="giudice_12" value="AND giudice_12='1'"/>Operatore Sviluppo<br/><br/>
</fieldset><br />
<br />

<fieldset>
		<legend><b>Indirizzo e-mail</b></legend>
		<br />
		Cofermato <input type="radio" name="mail_confermata" value="mail_confermata='1'"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Non confermato <input type="radio" name="mail_confermata" value="mail_confermata IS NULL"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Indifferente <input type="radio" name="mail_confermata" value="1=1" checked="checked"/><br />
</fieldset><br />
<br />

<br />
<p align="left">
<input type="submit" value="Ricerca" />
</form>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
