<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice') && !($check['team'] == 'tutor'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
 
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Riepilogo accoglienza</title>

	<!-- Contents xxxxx-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Parent -->
	<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p018.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<meta http-equiv="refresh" content="60" >
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Registrazioni in Piazza</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Riepilogo Accoglienza</h2>
<p id="imPathTitle">Simulazione</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?
include("config.inc.php");

echo "<font color=#2B3856 size='2' face='Tahoma'>";
echo "<table border='0' width=100%>";

include ("apri_db.php");

//Conto le squadre accolte, non accolte e registrate
$query_reg = "SELECT		
									COUNT(
									CASE
									WHEN p.accoglienza = '1' THEN 1
									ELSE NULL
									END
									) AS accolte,
									COUNT(
									CASE
									WHEN p.accoglienza = '0' THEN 1
									ELSE NULL
									END
									) AS non_accolte,
									COUNT(
									CASE
									WHEN p.accoglienza = '2' THEN 1
									ELSE NULL
									END
									) AS registrate
						FROM preiscrizioni AS p
						WHERE p.iscrizione = '1' OR p.iscrizione = '3'";
$result_reg = mysql_query($query_reg, $db);
$row_reg = mysql_fetch_array( $result_reg );
?>

<table id='stato_iscrizione' border='0' width=100%>
<thead>
<tr>
<td colspan="7" align="center" style="font size:90%" bgcolor="#81BEF7"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><b><?= $row_reg[accolte] ?></b> SQUADRE PRESENTI IN PIAZZA IN ATTESA DI REGISTRAZIONE</td>
<tr>
<tr>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:200px; height:24px; '>Comitato</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:160px; height:24px; '>Iscrizione fatta da</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:160px; height:24px; '>Capitano della squadra</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:120px; height:24px; '>Telefono</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:140px; height:24px; '>Da registrare</th> 
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:140px; height:24px; '>Ora accoglienza</th> 
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:120px; height:24px; '>AZIONI</th>
</tr>
</thead>

<?php

//Comitati che hanno confermato
$query = "SELECT		c.nome_comitato AS nome_comitato,
									p.nome_delegato AS nome_volontario,
									p.cognome_delegato AS cognome_volontario,
									p.telefono_delegato AS telefono_volontario,
									CASE WHEN i.ruolo = 'capitano' THEN i.nome END AS nome_capitano,
									CASE WHEN i.ruolo = 'capitano' THEN i.cognome END AS cognome_capitano,
									CASE WHEN i.ruolo = 'capitano' THEN i.telefono END AS telefono_capitano,
									COUNT(
									CASE
									WHEN i.ruolo = 'capitano' OR i.ruolo = 'partecipante' OR i.ruolo = 'adulto' THEN 1
									ELSE NULL
									END
									) AS squadra,
									COUNT(
									CASE
									WHEN i.ruolo = 'osservatore' THEN 1
									ELSE NULL
									END
									) AS osservatori,
									DATE_FORMAT(p.ora_accoglienza, '%d-%m-%Y %H:%i') AS ora_accoglienza,
									c.id AS id_comitato
						FROM iscrizioni AS i
						INNER JOIN comitati AS c
						ON c.id = i.id_comitato
						INNER JOIN preiscrizioni AS p
						ON p.id_comitato = i.id_comitato
						WHERE p.accoglienza = '1'
						AND (i.ruolo = 'capitano' OR i.ruolo ='partecipante' OR i.ruolo = 'adulto' OR i.ruolo ='osservatore')
						GROUP BY i.id_comitato
						ORDER BY p.ora_accoglienza";
$result = mysql_query($query, $db);
while($row = mysql_fetch_array( $result )) 
{
	?>
    <tr>
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[nome_comitato] ?></td> 
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[nome_volontario]." ".$row[cognome_volontario] ?></td> 
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[nome_capitano]." ".$row[cognome_capitano] ?></td> 
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[telefono_capitano] ?></td> 
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[squadra]." + ".$row[osservatori] ?></td> 
   	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[ora_accoglienza] ?></td> 
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">
	<a href=registrazione_squadra_osservatori.php?id_comitato=<?= $row[id_comitato] ?>><img src=images/registrazione.jpg width='20' height='20' align='middle' border='' title='Registrazione squadre ed osservatori'></a>&nbsp;&nbsp;&nbsp;&nbsp;
	<a href=stampa_registrazione.php?comitato=<?= $row[id_comitato] ?>><img src=images/ricevuta.ico width='20' height='20' align='middle' border='' title='Ristampa registrazione'></a>
    </td> 
	</tr>
    <?php
}
?>

<tr>
<td colspan="7" align="center" style="font size:90%" bgcolor="#ffffff"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"> </td>
<tr>
<td colspan="7" align="center" style="font size:90%" bgcolor="#F08080"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><b><?= $row_reg[non_accolte] ?></b> SQUADRE NON ANCORA PERVENUTE AL DESK ACCOGLIENZA </td>
<tr>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#F08080" style=color:#FFFF99><div style='width:200px; height:24px; '>Comitato</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#F08080" style=color:#FFFF99><div style='width:160px; height:24px; '>Iscrizione fatta da</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#F08080" style=color:#FFFF99><div style='width:160px; height:24px; '>Capitano della squadra</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#F08080" style=color:#FFFF99><div style='width:120px; height:24px; '>Telefono</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#F08080" style=color:#FFFF99><div style='width:140px; height:24px; '>Da registrare</th> 
<th align=center style=border-style:outset style='font size:100%' bgcolor="#F08080" style=color:#FFFF99><div style='width:140px; height:24px; '>Ora accoglienza</th> 
<th align=center style=border-style:outset style='font size:100%' bgcolor="#F08080" style=color:#FFFF99><div style='width:120px; height:24px; '>AZIONI</th>
</tr>
</thead>

<?php

//Comitati che hanno confermato
$query = "SELECT		c.nome_comitato AS nome_comitato,
									p.nome_delegato AS nome_volontario,
									p.cognome_delegato AS cognome_volontario,
									p.telefono_delegato AS telefono_volontario,
									CASE WHEN i.ruolo = 'capitano' THEN i.nome END AS nome_capitano,
									CASE WHEN i.ruolo = 'capitano' THEN i.cognome END AS cognome_capitano,
									CASE WHEN i.ruolo = 'capitano' THEN i.telefono END AS telefono_capitano,
									COUNT(
									CASE
									WHEN i.ruolo = 'capitano' OR i.ruolo = 'partecipante' OR i.ruolo = 'adulto' THEN 1
									ELSE NULL
									END
									) AS squadra,
									COUNT(
									CASE
									WHEN i.ruolo = 'osservatore' THEN 1
									ELSE NULL
									END
									) AS osservatori,
									DATE_FORMAT(p.ora_accoglienza, '%d-%m-%Y %H:%i') AS ora_accoglienza,
									c.id AS id_comitato
						FROM iscrizioni AS i
						INNER JOIN comitati AS c
						ON c.id = i.id_comitato
						INNER JOIN preiscrizioni AS p
						ON p.id_comitato = i.id_comitato
						WHERE p.accoglienza = '0'
						AND (i.ruolo = 'capitano' OR i.ruolo ='partecipante' OR i.ruolo = 'adulto' OR i.ruolo ='osservatore')
						GROUP BY i.id_comitato
						ORDER BY p.ora_accoglienza";
$result = mysql_query($query, $db);
while($row = mysql_fetch_array( $result )) 
{
	?>
    <tr>
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[nome_comitato] ?></td> 
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[nome_volontario]." ".$row[cognome_volontario] ?></td> 
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[nome_capitano]." ".$row[cognome_capitano] ?></td> 
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[telefono_capitano] ?></td> 
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[squadra]." + ".$row[osservatori] ?></td> 
   	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[ora_accoglienza] ?></td> 
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">
	<a href=registrazione_squadra_osservatori.php?id_comitato=<?= $row[id_comitato] ?>><img src=images/registrazione.jpg width='20' height='20' align='middle' border='' title='Registrazione squadre ed osservatori'></a>&nbsp;&nbsp;&nbsp;&nbsp;
	<a href=stampa_registrazione.php?comitato=<?= $row[id_comitato] ?>><img src=images/ricevuta.ico width='20' height='20' align='middle' border='' title='Ristampa registrazione'></a>
    </td> 
	</tr>
    <?php
}
?>

</table>

<table id='squadre_registrate' border='0' width=100%>
<thead>
<tr>
<td colspan="7" align="center" style="font size:90%" bgcolor="#ffffff"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"> </td>
<tr>
<td colspan="7" align="center" style="font size:90%" bgcolor="#53ac9a"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><b><?= $row_reg[registrate] ?></b> SQUADRE REGISTRATE</td>
<tr>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#53ac9a" style=color:#FFFF99><div style='width:200px; height:24px; '>Comitato</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#53ac9a" style=color:#FFFF99><div style='width:160px; height:24px; '>Iscrizione fatta da</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#53ac9a" style=color:#FFFF99><div style='width:160px; height:24px; '>Capitano della squadra</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#53ac9a" style=color:#FFFF99><div style='width:120px; height:24px; '>Telefono</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#53ac9a" style=color:#FFFF99><div style='width:140px; height:24px; '>Da registrare</th> 
<th align=center style=border-style:outset style='font size:100%' bgcolor="#53ac9a" style=color:#FFFF99><div style='width:140px; height:24px; '>Ora accoglienza</th> 
<th align=center style=border-style:outset style='font size:100%' bgcolor="#53ac9a" style=color:#FFFF99><div style='width:120px; height:24px; '>AZIONI</th>
</tr>
</thead>

<?php

//Comitati che hanno confermato
$query = "SELECT		c.nome_comitato AS nome_comitato,
									p.nome_delegato AS nome_volontario,
									p.cognome_delegato AS cognome_volontario,
									p.telefono_delegato AS telefono_volontario,
									CASE WHEN i.ruolo = 'capitano' THEN i.nome END AS nome_capitano,
									CASE WHEN i.ruolo = 'capitano' THEN i.cognome END AS cognome_capitano,
									CASE WHEN i.ruolo = 'capitano' THEN i.telefono END AS telefono_capitano,
									COUNT(
									CASE
									WHEN i.ruolo = 'capitano' OR i.ruolo = 'partecipante' OR i.ruolo = 'adulto' THEN 1
									ELSE NULL
									END
									) AS squadra,
									COUNT(
									CASE
									WHEN i.ruolo = 'osservatore' THEN 1
									ELSE NULL
									END
									) AS osservatori,
									DATE_FORMAT(p.ora_accoglienza, '%d-%m-%Y %H:%i') AS ora_accoglienza,
									c.id AS id_comitato
						FROM iscrizioni AS i
						INNER JOIN comitati AS c
						ON c.id = i.id_comitato
						INNER JOIN preiscrizioni AS p
						ON p.id_comitato = i.id_comitato
						WHERE p.accoglienza = '2'
						AND (i.ruolo = 'capitano' OR i.ruolo ='partecipante' OR i.ruolo = 'adulto' OR i.ruolo ='osservatore')
						GROUP BY i.id_comitato
						ORDER BY p.ora_accoglienza";
$result = mysql_query($query, $db);
while($row = mysql_fetch_array( $result )) 
{
	?>
    <tr>
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[nome_comitato] ?></td> 
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[nome_volontario]." ".$row[cognome_volontario] ?></td> 
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[nome_capitano]." ".$row[cognome_capitano] ?></td> 
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[telefono_capitano] ?></td> 
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[squadra]." + ".$row[osservatori] ?></td> 
   	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[ora_accoglienza] ?></td> 
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">
	<a href=registrazione_squadra_osservatori.php?id_comitato=<?= $row[id_comitato] ?>><img src=images/registrazione.jpg width='20' height='20' align='middle' border='' title='Registrazione squadre ed osservatori'></a>&nbsp;&nbsp;&nbsp;&nbsp;
	<a href=stampa_registrazione.php?comitato=<?= $row[id_comitato] ?>><img src=images/ricevuta.ico width='20' height='20' align='middle' border='' title='Ristampa registrazione'></a>
    </td> 
	</tr>
    <?php
}
mysql_close($db); 
?>

</table>
<br /><br />

</div>
</div>
</div>


<!-- Page Content END -->

	</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
