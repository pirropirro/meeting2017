<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato al Back Office.</b></font>";
	exit;	// Stop script execution
}

$stanze_pt = array ('Torino','Milano','Roma','Napoli','Palermo','Firenze');

?>
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Tabellone Aule</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/tableExport.js"></script>
	<script type="text/javascript" src="js/jquery.base64.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Tabellone aule</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Consulta Stato Iscrizioni</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include ("config.inc.php");
echo "<font color=#2B3856 size='1' face='Tahoma'>";
include ("apri_db.php");
$ora_attuale = strftime('%H%M', time());
?>
<table border='0' width=100%>

<tr>
<th align=center colspan='9' style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Aggiornato alle <?= $ora_attuale ?></th> </tr>
</table>
<table id='tabellone_aule' border='0' width=100%>

<thead>
<tr>

<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Aula Torino</th>
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Aula Milano</th> 
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Aula Roma</th>
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Aula Napoli</th> 
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Aula Palermo</th>
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Aula Firenze</th>
</thead>	

<?php
//Faccio un ciclo da 6
foreach ($stanze_pt as $stanza)
{
	echo $stanza;
	?>
    <td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:110px; height:auto; overflow-y:hidden; overflow-x:hidden;'>
	<?php
    //Faccio la query sulla tabella iscrizioni per estrarre i dati da visualizzare
	$query = "SELECT		c.nome_comitato AS nome_comitato
                            FROM timesheet AS t
                            INNER JOIN comitati AS c
                            ON t.id_comitato = c.id
                            WHERE t.estrarre = 'prossimo'
							AND t.aula = '$stanza'
							AND t.ora_checkin IS NOT NULL
							AND t.ora_checkout IS NULL";
	$result = mysql_query($query, $db);
	while ($row = mysql_fetch_array($result)) 
	{
	?>
	<font color="#2B3856"><?php echo $row[nome_comitato] ?></font>
	<?php
    }
    ?>
	</td>
	<?php
}
?>
</table>
	
<?php
mysql_close($db); 
?>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>