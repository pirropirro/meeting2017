<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato all'organizzazione od al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Invio massivo ai Consiglieri notifica per conferma mail</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Invio singolo notifica per conferma mail</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Invio singolo notifica per conferma mail</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include ("config.inc.php");
include('PHPMailer/class.phpmailer.php');
echo "<font color=#2B3856 size='2' face='Tahoma'>";
$fp = fopen ("log_iscrizioni.txt",a);
include ("apri_db.php");

$operatore=$check['uname'];

//prendo tutti i comitati che risultano nelle preiscrizioni
$query_delegato = "SELECT 	c.id AS id_comitato,
											c.nome_comitato AS nome_comitato,
											p.nome_delegato AS nome_volontario,
											p.cognome_delegato AS cognome_volontario,
											p.mail_delegato AS mail_volontario
											FROM comitati AS c
											INNER JOIN preiscrizioni AS p
											ON c.id = p.id_comitato";
$result_delegato = mysql_query($query_delegato, $db);
while($row_delegato = mysql_fetch_array( $result_delegato )) 
{
	//Cerco i volontari del comitato che non hanno confermato e non sono assenti
	$query_id = "SELECT i.nome AS nome,
						i.cognome AS cognome,
						i.ruolo AS ruolo,
						i.mail AS mail
						FROM iscrizioni AS i
						WHERE i.id_comitato = $row_delegato[id_comitato]
						AND i.mail_confermata IS NULL
						AND i.registrazione != 9";
	$result_id = mysql_query($query_id, $db);
	if (mysql_num_rows($result_id) > 0)
	{	
		echo "Per il comitato $row_delegato[nome_comitato] ci sono mail non confermate, invio mail... <br>";
		//Invio la mail al delegato
		$mittente = "meeting.2016@piemonte.cri.it";
		$nomemittente = "Back Office Meeting 2016";
		$destinatario = "$row_delegato[mail_volontario]";
//		$destinatario = "paolo.ditoma@libero.it";
		$ServerSMTP = "mailbox.cri.it"; //server SMTP
		$oggetto = "Iscrizioni Meeting 2016 - Sollecito conferma indirizzo mail";
		$corpo_messaggio = "Ciao $row_delegato[nome_volontario],\nper il $row_delegato[nome_comitato] ad oggi non ci risulta ancora essere stato confermato l'indirizzo di posta elettronica per i seguenti volontari, inseriti in fase di iscrizione:\n\n";
		while($row_id = mysql_fetch_array( $result_id )) 
		{
			$corpo_messaggio .= " - $row_id[nome] $row_id[cognome], iscritto come $row_id[ruolo] con indirizzo mail $row_id[mail]\n";
		}
		$corpo_messaggio .= "\n\nAbbiamo già provveduto ad inviare più solleciti senza sortire evidentemente alcun effetto. Ti chiediamo quindi di ricontrollare che gli indirizzi inseriti siano corretti e/o di aiutarci a far si che ognuno ottemperi alla procedura di verifica.\n\nN.B. Ogni altra notifica riguardante il Meeting (compreso il programma, il turno mensa e/o eventuali votazioni) sarà da adesso recapitata solo a coloro i quali abbiano confermato il proprio indirizzo.\n\nPer qualsiasi dubbio, angoscia e/o perplessità, non esitare a contattarci.\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
		
		$msg = new PHPMailer;
		$msg->CharSet = "UTF-8";
		$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
		//$msg->IsHTML(true);
		$msg->SMTPAuth = true; // Autenticazione SMTP
		$msg->SMTPKeepAlive = "true";
		$msg->Host = $ServerSMTP;
		$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
		$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
		
		$msg->From = $mittente;
		$msg->FromName = $nomemittente;
		$msg->AddAddress($destinatario); 
		$msg->AddCC("meeting.2016@piemonte.cri.it");
//		$msg->AddBCC("paolo.ditoma@libero.it");
		$msg->Subject = $oggetto; 
		$msg->Body = $corpo_messaggio;
		
		
		if(!$msg->Send())
		{
			echo "Si è verificato un errore nell'invio della mail di sollecito conferma mail per delegato $row_delegato[nome_volontario] $row_delegato[cognome_volontario] per $row_delegato[nome_comitato] all'indirizzo $row_delegato[mail_volontario] :".$msg->ErrorInfo;
			echo "<br><br>";
			fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Sollecito conferma mail delegato - per delegato $row_delegato[nome_volontario] $row_delegato[cognome_volontario] per $row_delegato[nome_comitato] all'indirizzo $row_delegato[mail_volontario] : ".$msg->ErrorInfo."\r\n");
		}
		else
		{
			echo "Abbiamo inviato una mail di sollecito per la conferma mail al delegato $row_delegato[nome_volontario] $row_delegato[cognome_volontario] per $row_delegato[nome_comitato] all'indirizzo $row_delegato[mail_volontario].<br><br>";
			fwrite($fp,date('d/m/Y H:i:s').' - '."Sollecito conferma mail delegato - Invio mail di sollecito per la conferma mail al delegato $row_delegato[nome_volontario] $row_delegato[cognome_volontario] per $row_delegato[nome_comitato] correttamente effettuato all'indirizzo $row_delegato[mail_volontario]."."\r\n");

		}
	}
	else
	{
		echo "Per il comitato $row_delegato[nome_comitato] non ci sono mail non confermate. OK <br>";
	}
}



mysql_close($db);
fclose($fp);

?>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
