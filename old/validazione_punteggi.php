<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato al Back Office.</b></font>";
	exit;	// Stop script execution
}

?>
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Tabellone iscrizioni</title>

	<!-- Contents xxxxx-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Parent -->
	<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/tableExport.js"></script>
	<script type="text/javascript" src="js/jquery.base64.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p018.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Tabellone iscrizioni</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Validazione Punteggi</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?
include("config.inc.php");
include ("apri_db.php");
$fp = fopen ("log_iscrizioni.txt",a);
?>

<br />

<font color=#2B3856 size='2' face='Tahoma'>

<?php
$query = "SELECT	c.nome_comitato AS nome_comitato,
								p.id_comitato AS id_comitato,
								p.0_g AS 0_g,
								p.0_c AS 0_c,
								p.0_v AS 0_v,
								p.1_g AS 1_g,
								p.1_c AS 1_c,
								p.1_v AS 1_v,
								p.2_g AS 2_g,
								p.2_c AS 2_c,
								p.2_v AS 2_v,
								p.3_g AS 3_g,
								p.3_c AS 3_c,
								p.3_v AS 3_v,
								p.4_g AS 4_g,
								p.4_c AS 4_c,
								p.4_v AS 4_v,
								p.5_g AS 5_g,
								p.5_c AS 5_c,
								p.5_v AS 5_v,
								p.6_g AS 6_g,
								p.6_c AS 6_c,
								p.6_v AS 6_v,
								p.punteggio_finale AS punteggio_finale
								FROM preiscrizioni AS p
								INNER JOIN comitati AS c
								ON p.id_comitato = c.id
								WHERE p.iscrizione='1'
								ORDER BY c.nome_comitato";
$result = mysql_query($query, $db);
while($row = mysql_fetch_array( $result )) 
{
	$re = "/(\\s*comitato locale di\\s*|\\s*comitato provinciale di\\s*|\\s*comitato regionale\\s*|\\s*delegazione di\\s*|\\s*a valenza regionale\\s*|\\s*comitato\\s*)/i"; 
	$subst = ""; 	 
	$comitato_trunc = preg_replace($re, $subst, $row[nome_comitato]);
	$id_comitato = $row[id_comitato];
	
	$punteggio_totale = $row[punteggio_finale];
	
	//Validazione punteggi della Prova Comune
	if ($row['0_v'] == NULL)
	{
		if ($row['0_c'] != NULL)
		{
			if ($row['0_c'] == $row['0_g'])
			{
				//Faccio l'update senza alcun controllo (non c'è modo di avere campi non valorizzati
				$punteggio_valido = $row['0_c'];
				$query_valida = "UPDATE preiscrizioni SET 0_v = '$punteggio_valido' WHERE id_comitato = '$id_comitato'";
				if (mysql_query($query_valida, $db))
				{
					$punteggio_totale += $row['0_c'];
					//Espongo il messaggio a video
					echo "Ho validato il punteggio ".$punteggio_valido." per la prova comune per la squadra di ".$comitato_trunc."<br>";
					fwrite($fp,date('d/m/Y H:i:s').' - '."Validazione PUNTEGGIO PROVA COMUNE per $comitato_trunc di valore $punteggio_valido effettuata da $check[uname]."."\r\n");
				}
				else
				{
					echo "Si è verificato un errore durante la validazione del punteggio, fate qualcosa!!!<br><br>";
					fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Validazione PUNTEGGIO per $comitato_trunc di valore $punteggio_valido effettuata da $check[uname]: ".$msg->ErrorInfo."\r\n");
				}	
			}
			
		}
	}

	//Validazione punteggi della Prova 1
	if ($row['1_v'] == NULL)
	{
		if ($row['1_c'] != NULL)
		{
			if ($row['1_c'] == $row['1_g'])
			{
				$punteggio_valido = $row['1_c'];
				//Faccio l'update senza alcun controllo (non c'è modo di avere campi non valorizzati
				$query_valida = "UPDATE preiscrizioni SET 1_v = '$punteggio_valido' WHERE id_comitato = '$id_comitato'";
				if (mysql_query($query_valida, $db))
				{
					$punteggio_totale += $row['1_c'];
					//Espongo il messaggio a video
					echo "Ho validato il punteggio ".$punteggio_valido." per la prova 1 per la squadra di ".$comitato_trunc."<br>";
					fwrite($fp,date('d/m/Y H:i:s').' - '."Validazione PUNTEGGIO PROVA 1 per $comitato_trunc di valore $punteggio_valido effettuata da $check[uname]."."\r\n");
				}
				else
				{
					echo "Si è verificato un errore durante la validazione del punteggio, fate qualcosa!!!<br><br>";
					fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Validazione PUNTEGGIO per $comitato_trunc di valore $punteggio_valido effettuata da $check[uname]: ".$msg->ErrorInfo."\r\n");
				}	
			}
			
		}
	}

	//Validazione punteggi della Prova 2
	if ($row['2_v'] == NULL)
	{
		if ($row['2_c'] != NULL)
		{
			if ($row['2_c'] == $row['2_g'])
			{
				$punteggio_valido = $row['2_c'];
				//Faccio l'update senza alcun controllo (non c'è modo di avere campi non valorizzati
				$query_valida = "UPDATE preiscrizioni SET 2_v = '$punteggio_valido' WHERE id_comitato = '$id_comitato'";
				if (mysql_query($query_valida, $db))
				{
					$punteggio_totale += $row['2_c'];
					//Espongo il messaggio a video
					echo "Ho validato il punteggio ".$punteggio_valido." per la prova 2 per la squadra di ".$comitato_trunc."<br>";
					fwrite($fp,date('d/m/Y H:i:s').' - '."Validazione PUNTEGGIO PROVA 2 per $comitato_trunc di valore $punteggio_valido effettuata da $check[uname]."."\r\n");
				}
				else
				{
					echo "Si è verificato un errore durante la validazione del punteggio, fate qualcosa!!!<br><br>";
					fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Validazione PUNTEGGIO per $comitato_trunc di valore $punteggio_valido effettuata da $check[uname]: ".$msg->ErrorInfo."\r\n");
				}	
			}
			
		}
	}

	//Validazione punteggi della Prova 3
	if ($row['3_v'] == NULL)
	{
		if ($row['3_c'] != NULL)
		{
			if ($row['3_c'] == $row['3_g'])
			{
				$punteggio_valido = $row['3_c'];
				//Faccio l'update senza alcun controllo (non c'è modo di avere campi non valorizzati
				$query_valida = "UPDATE preiscrizioni SET 3_v = '$punteggio_valido' WHERE id_comitato = '$id_comitato'";
				if (mysql_query($query_valida, $db))
				{
					$punteggio_totale += $row['3_c'];
					//Espongo il messaggio a video
					echo "Ho validato il punteggio ".$punteggio_valido." per la prova 3 per la squadra di ".$comitato_trunc."<br>";
					fwrite($fp,date('d/m/Y H:i:s').' - '."Validazione PUNTEGGIO PROVA 3 per $comitato_trunc di valore $punteggio_valido effettuata da $check[uname]."."\r\n");
				}
				else
				{
					echo "Si è verificato un errore durante la validazione del punteggio, fate qualcosa!!!<br><br>";
					fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Validazione PUNTEGGIO per $comitato_trunc di valore $punteggio_valido effettuata da $check[uname]: ".$msg->ErrorInfo."\r\n");
				}	
			}
			
		}
	}

	//Validazione punteggi della Prova 4
	if ($row['4_v'] == NULL)
	{
		if ($row['4_c'] != NULL)
		{
			if ($row['4_c'] == $row['4_g'])
			{
				$punteggio_valido = $row['4_c'];
				//Faccio l'update senza alcun controllo (non c'è modo di avere campi non valorizzati
				$query_valida = "UPDATE preiscrizioni SET 4_v = '$punteggio_valido' WHERE id_comitato = '$id_comitato'";
				if (mysql_query($query_valida, $db))
				{
					$punteggio_totale += $row['4_c'];
					//Espongo il messaggio a video
					echo "Ho validato il punteggio ".$punteggio_valido." per la prova 4 per la squadra di ".$comitato_trunc."<br>";
					fwrite($fp,date('d/m/Y H:i:s').' - '."Validazione PUNTEGGIO PROVA 4 per $comitato_trunc di valore $punteggio_valido effettuata da $check[uname]."."\r\n");
				}
				else
				{
					echo "Si è verificato un errore durante la validazione del punteggio, fate qualcosa!!!<br><br>";
					fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Validazione PUNTEGGIO per $comitato_trunc di valore $punteggio_valido effettuata da $check[uname]: ".$msg->ErrorInfo."\r\n");
				}	
			}
			
		}
	}

	//Validazione punteggi della Prova 5
	if ($row['5_v'] == NULL)
	{
		if ($row['5_c'] != NULL)
		{
			if ($row['5_c'] == $row['5_g'])
			{
				$punteggio_valido = $row['5_c'];
				//Faccio l'update senza alcun controllo (non c'è modo di avere campi non valorizzati
				$query_valida = "UPDATE preiscrizioni SET 5_v = '$punteggio_valido' WHERE id_comitato = '$id_comitato'";
				if (mysql_query($query_valida, $db))
				{
					$punteggio_totale += $row['5_c'];
					//Espongo il messaggio a video
					echo "Ho validato il punteggio ".$punteggio_valido." per la prova 5 per la squadra di ".$comitato_trunc."<br>";
					fwrite($fp,date('d/m/Y H:i:s').' - '."Validazione PUNTEGGIO PROVA 5 per $comitato_trunc di valore $punteggio_valido effettuata da $check[uname]."."\r\n");
				}
				else
				{
					echo "Si è verificato un errore durante la validazione del punteggio, fate qualcosa!!!<br><br>";
					fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Validazione PUNTEGGIO per $comitato_trunc di valore $punteggio_valido effettuata da $check[uname]: ".$msg->ErrorInfo."\r\n");
				}	
			}
			
		}
	}

	//Validazione punteggi della Prova 6
	if ($row['6_v'] == NULL)
	{
		if ($row['6_c'] != NULL)
		{
			if ($row['6_c'] == $row['6_g'])
			{
				$punteggio_valido = $row['6_c'];
				//Faccio l'update senza alcun controllo (non c'è modo di avere campi non valorizzati
				$query_valida = "UPDATE preiscrizioni SET 6_v = '$punteggio_valido' WHERE id_comitato = '$id_comitato'";
				if (mysql_query($query_valida, $db))
				{
					$punteggio_totale += $row['6_c'];
					//Espongo il messaggio a video
					echo "Ho validato il punteggio ".$punteggio_valido." per la prova 6 per la squadra di ".$comitato_trunc."<br>";
					fwrite($fp,date('d/m/Y H:i:s').' - '."Validazione PUNTEGGIO PROVA 6 per $comitato_trunc di valore $punteggio_valido effettuata da $check[uname]."."\r\n");
				}
				else
				{
					echo "Si è verificato un errore durante la validazione del punteggio, fate qualcosa!!!<br><br>";
					fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Validazione PUNTEGGIO per $comitato_trunc di valore $punteggio_valido effettuata da $check[uname]: ".$msg->ErrorInfo."\r\n");
				}	
			}
			
		}
	}

	
	$query_punteggio = "UPDATE preiscrizioni SET punteggio_finale = '$punteggio_totale' WHERE id_comitato = '$id_comitato'";
	if (mysql_query($query_punteggio, $db))
				{
					//Espongo il messaggio a video
					echo "Il punteggio finale è stato updatato a ".$punteggio_totale." per la squadra di ".$comitato_trunc."<br>";
					fwrite($fp,date('d/m/Y H:i:s').' - '."Il punteggio finale è stato updatato a $punteggio_totale per $comitato_trunc effettuata da $check[uname]."."\r\n");
				}
}
mysql_close($db);
fclose($fp);
?>


<br /><br />


</div>
</div>
</div>


<!-- Page Content END -->

	</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
