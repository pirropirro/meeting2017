<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato al Back Office.</b></font>";
	exit;	// Stop script execution
}

$stanze_p2 = array ('aula_16','aula_17','aula_18','aula_7','aula_8','aula_9','aula_4');
$stanze_p1 = array ('aula_13','aula_14','aula_15');
$stanze_pt = array ('aula_1','aula_2','aula_3','aula_12');
$stanze_ps = array ('aula_10','aula_11','aula_mensa','aula_liberi');


?>
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Tabellone Aule</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/tableExport.js"></script>
	<script type="text/javascript" src="js/jquery.base64.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<meta http-equiv="refresh" content="45" >
<body style="background-color: white;">



<!-- Menu Content START -->

<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->
<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />


<?php
include ("config.inc.php");
echo "<font color=#2B3856 size='2' face='Tahoma'>";
include ("apri_db.php");
$ora_attuale = strftime('%H:%M', time());
?>
<table border="0" width=100%>

<tr>
<th align="center" style="border-style:outset" style="font size:100%" style="color:#FFFF99"><div style="height:25px;">Aggiornato alle <?= $ora_attuale ?></th> </tr>
</table>

<iframe width="50" height="240" src="http://www.itempa.com/meeting2016/res/primo_piano.png" frameborder="0" allowfullscreen></iframe>
<iframe width="1500" height="240" src="http://www.itempa.com/meeting2016/frame_piano_primo.php" frameborder="0" allowfullscreen></iframe>
<iframe width="301" height="240" src="http://www.itempa.com/meeting2016/frame_palestra.php" frameborder="0" allowfullscreen></iframe>
<br />
<iframe width="50" height="240" src="http://www.itempa.com/meeting2016/res/piano_terra.png" frameborder="0" allowfullscreen></iframe>
<iframe width="1500" height="240" src="http://www.itempa.com/meeting2016/frame_piano_terra.php" frameborder="0" allowfullscreen></iframe>
<iframe width="301" height="240" src="http://www.itempa.com/meeting2016/frame_piazza.php" frameborder="0" allowfullscreen></iframe>
<br />
<iframe width="50" height="240" src="http://www.itempa.com/meeting2016/res/cortile.png" frameborder="0" allowfullscreen></iframe>
<iframe width="1500" height="240" src="http://www.itempa.com/meeting2016/frame_cortile.php" frameborder="0" allowfullscreen></iframe>
<iframe width="301" height="240" src="http://www.itempa.com/meeting2016/frame_tensostruttura.php" frameborder="0" allowfullscreen></iframe>
<br />
<iframe width="50" height="240" frameborder="0" allowfullscreen></iframe>
<iframe width="1801" height="240" src="http://www.itempa.com/meeting2016/frame_mensa.php" frameborder="0" allowfullscreen></iframe>




</div>
</div>

</body>
</html>