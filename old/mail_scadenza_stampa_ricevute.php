<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
 
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Invio Mail Apertura Iscrizioni</title>

	<!-- Contents xxxxx-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p018.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Invio Mail Apertura Iscrizioni</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Invio Mail Apertura Iscrizioni</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include ("config.inc.php");
echo "<font color=#2B3856 size='3' face='Tahoma'>";
$fp = fopen ("log_solleciti.txt",a);
include('PHPMailer/class.phpmailer.php');
include ("apri_db.php");

//Estraggo in un array le preiscrizioni non confermate
$query ="SELECT 	c.id AS id,
					c.nome_comitato AS nome_comitato,
					c.nome_presidente AS nome_presidente,
					c.cognome_presidente AS cognome_presidente,
					c.mail_comitato AS mail_comitato,
					p.nome_delegato AS nome_volontario,
					p.cognome_delegato AS cognome_volontario,
					p.mail_delegato AS mail_volontario,
					p.pagamento AS pagamento
					FROM preiscrizioni AS p
					INNER JOIN comitati AS c
					ON p.id_comitato = c.id
					WHERE (p.iscrizione = '1' OR p.iscrizione = '3')
					AND pagamento IS NULL";


//$query = "SELECT * FROM preiscrizioni WHERE conferma_presidente = '0' ORDER BY data_inserimento";
$result = mysql_query($query, $db);
while($row = mysql_fetch_array( $result )) 
{
	//echo "Presidente $row[nome_presidente] $row[cognome_presidente] - Preiscrizione n. $row[id_preiscrizione] del $row[nome_comitato] con mail $row[mail_comitato] - Volontario $row[nome_volontario] $row[cognome_volontario] con mail $row[mail_volontario] - Inserita in data $row[data_inserimento] alle ore $row[ora_inserimento] <br>";
	
	//Invio la mail di sollecito con me in ccn
						
	$mittente = "meeting.2016@piemonte.cri.it";
	$nomemittente = "Back Office Meeting 2016";
	$destinatario = "$row[mail_volontario]";
//	$destinatario = "paolo.ditoma@libero.it";
	$ServerSMTP = "mailbox.cri.it"; //server SMTP
	$oggetto = "Scadenza per stampa ricevuta e pagamento Meeting 2016";
	$corpo_messaggio = "Buongiorno $row[nome_volontario],\ncon la presente ti notifichiamo che ad oggi non ci è ancora pervenuta la ricevuta del bonifico per il pagamento della quota di partecipazione al Meeting 2016. Ti ricordiamo che oggi è la data di scadenza e che in assenza della suddetta ricevuta non sarà possibile per il $row[nome_comitato] partecipare all'evento.\n\nCollegati al portale all'indirizzo http://www.itempa.com/meeting2016 e vai nella sezione Iscrizioni - Consulta Iscrizione potrai vedere il riepilogo dei volontari iscritti e stampare la relativa ricevuta. Il pagamento può avvenire solo con bonifico sul\n\n- c/c intestato a CROCE ROSSA ITALIANA COMITATO LOCALE DI MONCALIERI\n- IBAN IT 35 E 06170 20000 000001548167\n- causale Iscrizione Meeting Griovani CRI 2016 - Codice $row[id] ($row[nome_comitato])\n- entro e non oltre il 19 c.m.\n\nUna volta effettuato il bonifico ti chiediamo di inviarci copia dello stesso via mail (rispondendo alla presente) di modo che possiamo registrare l'avvenuto pagamento.\n\nAbbiamo inserito in copia alla presente, il tuo presidente $row[nome_presidente] $row[cognome_presidente], il referente del Meeting (Maria Teresa Canestro) ed il Consigliere Giovane Regionale (Mattia Di Febbo) per completezza di informazioni.\nPer qualsiasi dubbio, angoscia e/o perplessità, non esitare a contattarci.\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
		
	$msg = new PHPMailer;
	$msg->CharSet = "UTF-8";
	$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
	//$msg->IsHTML(true);
	$msg->SMTPAuth = true; // Autenticazione SMTP
	$msg->SMTPKeepAlive = "true";
	$msg->Host = $ServerSMTP;
	$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
	$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
	
	$msg->From = $mittente;
	$msg->FromName = $nomemittente;
	$msg->AddAddress($destinatario); 
	$msg->AddCC("$row[mail_comitato]");
	$msg->AddCC("mattia.difebbo@piemonte.cri.it");
	$msg->AddCC("mariateresa.canestro@piemonte.cri.it");
	$msg->AddBCC("paolo.ditoma@libero.it");
	$msg->Subject = $oggetto; 
	$msg->Body = $corpo_messaggio;
	
	if(!$msg->Send())
	{
		echo "Si è verificato un errore nell'invio della mail di notifica ricevute:".$msg->ErrorInfo;
		fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE - Scadenza Pagamento - Presidente $row[nome_presidente] $row[cognome_presidente] - $row[nome_comitato] con mail $row[mail_comitato] - Volontario $row[nome_volontario] $row[cognome_volontario] con mail $row[mail_volontario] ".$msg->ErrorInfo." <br>"."\r\n");
	}
	else
	{
		echo "Scadenza Pagamento inviata per $row[nome_comitato] al volontario $row[nome_volontario] $row[cognome_volontario] all'indirizzo $row[mail_volontario] ed in cc al presidente $row[nome_presidente] $row[cognome_presidente] all'indirizzo $row[mail_comitato]. <br>";
		fwrite($fp,date('d/m/Y H:i:s').' - '."Mail Ricevute inviata per $row[nome_comitato] al volontario $row[nome_volontario] $row[cognome_volontario] all'indirizzo $row[mail_volontario] ed in cc al presidente $row[nome_presidente] $row[cognome_presidente] all'indirizzo $row[mail_comitato]."."\r\n");
	}	

}
	
fclose($fp);
mysql_close($db); 
?>

<br /><br />

</div>
</div>
</div>


<!-- Page Content END -->

	</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
