<?php
	// settiamo a zero la variabile mobile_browser 
	
	$mobile_browser = '0';
	
	//  controlliamo se l'user agent corrisponde a un dispositivo mobile
	 
	if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
		$mobile_browser++;
	}
	
	// controlliamo HTTP_ACCEPT corrisponde alla tecnologia WAP per telefonini
	 
	if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0)
	or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
		$mobile_browser++;
	}    
	
	// estraiamo i primi 4 caratteri da USER_AGENT e creiamo un array con tutti i header mobile
	 
	$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
	$mobile_agents = array(
		'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
		'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
		'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
		'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
		'newt','noki','oper','palm','pana','pant','phil','play','port','prox',
		'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
		'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
		'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
		'wapr','webc','winw','winw','xda ','xda-');
	
	// se corrisponde a mobile aumentiamo la nostra variabile di 1 
	 
	if (in_array($mobile_ua,$mobile_agents)) {
		$mobile_browser++;
	}
	 
	if (strpos(strtolower($_SERVER['ALL_HTTP']),'OperaMini') > 0) {
		$mobile_browser++;
	}
	 
	if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'windows') > 0) {
		$mobile_browser = 0;
	}
	
	// eccezione per windows phone
	if (preg_match('/(windows phone|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
		$mobile_browser++;
	}
	
	
	// quindi se mobile_browser � inferiore di 0 vuol dire che un dispositivo mobile
	 
	if ($mobile_browser > 0) {
	
	// reindirizzamento per mobile
	
	header('Location: http://www.itempa.com/meeting2015/mobile/programma_meeting.php'); 
	
	}
	else 
	{
	// altrimenti restiamo dove siamo
	?>
    
    <!--IE 7 quirks mode please-->
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
    <head>
        <title>Programma Meeting</title>
    
        <!-- Contents -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="Content-Language" content="it" />
        <meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
        <meta http-equiv="Content-Type-Script" content="text/javascript" />
        <meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
        <meta name="keywords" content="" />
    
        <!-- Others -->
        <meta name="Author" content="Paolo di Toma" />
        <meta http-equiv="ImageToolbar" content="False" />
        <meta name="MSSmartTagsPreventParsing" content="True" />
        <link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />
    
        <!-- Parent -->
        <link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />
    
        <!-- Res -->
        <script type="text/javascript" src="res/x5engine.js"></script>
        <link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
        <link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
        <!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
        <link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
        <link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
        <link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />
    
        <!-- Robots -->
        <meta http-equiv="Expires" content="0" />
        <meta name="Resource-Type" content="document" />
        <meta name="Distribution" content="global" />
        <meta name="Robots" content="index, follow" />
        <meta name="Revisit-After" content="21 days" />
        <meta name="Rating" content="general" />
    </head>
    
    <script type="text/javascript" src="tabber.js"></script>
    <link rel="stylesheet" href="example.css" TYPE="text/css" MEDIA="screen">
    <link rel="stylesheet" href="example-print.css" TYPE="text/css" MEDIA="print">
    
    <script type="text/javascript">
    
    /* Optional: Temporarily hide the "tabber" class so it does not "flash"
       on the page as plain HTML. After tabber runs, the class is changed
       to "tabberlive" and it will appear. */
    
    document.write('<style type="text/css">.tabber{display:none;}<\/style>');
    </script>
    
    <script type="text/javascript" src="scripts/wysiwyg.js"></script>
    <script type="text/javascript" src="scripts/wysiwyg-settings.js"></script> 
    <script type="text/javascript">
    WYSIWYG.attach('textarea2', small); // full featured setup 
    </script> 
    
    <body>
    <div id="imSite">
    <div id="imHeader">
        
        <h1>Programma</h1>
    </div>
    <div class="imInvisible">
    <hr />
    <a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
    <a name="imGoToMenu"></a>
    </div>
    <div id="imBody">
        <div id="imMenuMain">
      
    <!-- Menu Content START -->
    <p class="imInvisible">Menu principale:</p>
    <div id="imMnMn">
    
    <?php 
    include ("main_menu.php");
    ?>
    
    </div>
    <!-- Menu Content END -->
    
        </div>
    <hr class="imInvisible" />
    <a name="imGoToCont"></a>
        <div id="imContent">
    
    <!-- Page Content START -->
    <div id="imPageSub">
    <br />
    <h2>Programma</h2>
    <p id="imPathTitle">Meeting</p>
    <div id="imToolTip"></div>
    <div id="imBody">
    <div id="imContent">
    
		<font color=#2B3856 size='2' face='Tahoma'>
		
		<br />

		<b>Domenica 23 ottobre</b><br /><br />
		Prima delle 7.00 la struttura è chiusa.<br />
		07.00 - 07.30  	Ingresso in struttura dello staff e degli espositori<br />
		07.45 - 08.30 	Ingresso in struttura dei giudici e simulatori (la registrazione avviene al Desk)<br />
		08.00 - 08.30 	Ingresso in struttura delle squadre ed osservatori (la registrazione avviene in Piazza)<br />
		08.00 - 09.00 	Visite agli stand in <i>Piazza Incontro</i><br />
		09.10 - 09.50 	Cerimonia di apertura del X Meeting Regionale<br />
		10.00 - 12.30	Prove del mattino*<br />
		12.30 - 14.30  	Turni per pausa pranzo*<br />
		14.45 - 16.20	Prove del pomeriggio*<br />
		16.20 - 16.35	Pausa<br />
		16.35 - 18.10	Prove del pomeriggio*<br />
		18.10 - 19.30	Cerimonia di chiusura Meeting Regionale<br />
		20.00 - 22.00	Smontaggio staff <br /><br /><br />

		<b>N.B. </b>Consigliamo caldamente l'arrivo al mattino entro le ore 08.00  e non prima delle 7.30! <a href="come_arrivare.php" title="">Indicazioni per la struttura</a>
		<br /><br />
			
		<b>* </b><i>Gli orari riportati sono puramente indicativi, fanno fede quelli ricevuti personalmente via mail e quelli comunicati dai Tutor</i>
 
		<br /><br />
    
    </div>
    </div>
    </div>
    
    </div>
    <!-- Page Content END -->
    
            </div>
        <div id="imFooter">
            <?php 
            include ("footer.php");
            ?>
        </div>
    </div>
    </div>
    <div class="imInvisible">
    <hr />
    <a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
    </div>
    
    <div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
    </body>
    </html>

    
	<?php
	
	echo "";
	
	}   
?>

