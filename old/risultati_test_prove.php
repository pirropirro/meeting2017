<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice') && !($check['team'] == 'organigramma') && !($check['team'] == 'nazionale'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato all'organizzazione od al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
 
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Riepilogo preiscrizioni</title>

	<!-- Contents xxxxx-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Parent -->
	<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p018.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<meta http-equiv="refresh" content="60" >
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Preiscrizioni - Riepilogo preiscrizioni</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Riepilogo Pre-iscritti</h2>
<p id="imPathTitle">Preiscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?
include("config.inc.php");

echo "<font color=#2B3856 size='2' face='Tahoma'>";
echo "<table border='0' width=100%>";

include ("apri_db.php");

echo "<tr>

<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99><div style='width:200px; height:24px; '>COMITATO</th> 
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99><div style='width:100px; height:24px; '>INDIA</th> 
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99><div style='width:100px; height:24px; '>PAPA</th>
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99><div style='width:100px; height:24px; '>QUEBEC</th>
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99><div style='width:100px; height:24px; '>OSCAR</th> 
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99><div style='width:100px; height:24px; '>TANGO</th>
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99><div style='width:100px; height:24px; '>HOTEL</th>
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99><div style='width:100px; height:24px; '>VICTOR</th>
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99><div style='width:100px; height:24px; '>TOTALE</th>
</tr>";


//Comitati che hanno confermato
$query = "SELECT		c.nome_comitato AS nome_comitato,
						c.tipo_meeting AS tipo_meeting,
                    	p.d1_c AS prova_india,
						p.d2_c AS prova_papa,
						p.d5_g AS prova_oscar,
						p.d6_c AS prova_tango,
						p.d7_g AS prova_hotel,
						p.d8_g AS prova_victor,
						p.Q_P1 AS q1,
						p.Q_P2 AS q2,
						p.Q_P3 AS q3,
						p.Q_P4 AS q4,
						p.Q_P5 AS q5,
						p.Q_P6 AS q6,
						p.Q_P7 AS q7,
						p.Q_P8 AS q8,
						p.Q_P9 AS q9,
						p.Q_P10 AS q10
                    	FROM preiscrizioni AS p
                    	INNER JOIN comitati AS c
                    	ON p.id_comitato = c.id
                    	WHERE p.iscrizione = '1'
						AND c.tipo_meeting != 'N'";
$result = mysql_query($query, $db);
while($row = mysql_fetch_array( $result )) 
{
	$prova_quebec = $row[q1] + $row[q2] + $row[q3] + $row[q4] + $row[q5] + $row[q6] + $row[q7] + $row[q8] + $row[q9] + $row[q10];
	$totale = $row[prova_india] + $row[prova_papa] + $prova_quebec + $row[prova_oscar] + $row[prova_tango] + $row[prova_hotel] + $row[prova_victor];
	echo "<tr>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'> $row[nome_comitato] </td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'> $row[prova_india] </td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'> $row[prova_papa] </td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'> $prova_quebec </td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'> $row[prova_oscar] </td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'> $row[prova_tango] </td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'> $row[prova_hotel] </td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'> $row[prova_victor] </td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'> $totale </td>
	<td align=center style=color:#FFFF99 bgcolor=#ffffff>";  
}
	
echo "</table>";



mysql_close($db); 
?>
<br /><br />

<form method="post" action="esporta_elenco_preiscrizioni.php">
<input type='submit' value='Esporta elenco in excel'></form>


</div>
</div>
</div>


<!-- Page Content END -->

	</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
