<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato all'organizzazione od al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Invio Massivo Mail Pubblicazione Classifica</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Invio Massivo Mail Pubblicazione Classifica</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Invio Massivo Mail Pubblicazione Classifica</h2>
<p id="imPathTitle">Post-Meeting</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include ("config.inc.php");
include('PHPMailer/class.phpmailer.php');
echo "<font color=#2B3856 size='2' face='Tahoma'>";
$fp = fopen ("log_iscrizioni.txt",a);

include ("apri_db.php");

//Cerco le info del volontario tramite id
$query_id = "SELECT c.nome_comitato AS nome_comitato,
					i.nome AS nome,
					i.cognome AS cognome,
					i.ruolo AS ruolo,
					i.id AS id,
					i.telefono AS telefono,
					i.mail AS mail,
					i.token AS token,
					i.mail_reminder AS mail_reminder
					FROM iscrizioni AS i
					INNER JOIN comitati AS c
					ON i.id_comitato = c.id
					WHERE i.mail_confermata IS NOT NULL
					AND (i.registrazione = 1 OR i.registrazione = 2)
					AND mail_classifica = 0
					AND i.id <= 800";
$result_id = mysql_query($query_id, $db);
while($row_id = mysql_fetch_array( $result_id )) 
{
	//Invio la mail di conferma all'iscritto
	$mittente = "meeting.2015@cri.it";
	$nomemittente = "Back Office Meeting 2015";
	$destinatario = "$row_id[mail]";
//	$destinatario = "paolo.ditoma@libero.it";
	$ServerSMTP = "mailbox.cri.it"; //server SMTP
	$oggetto = "Classifica Meeting 2015";
	$corpo_messaggio = "Ciao $row_id[nome],\nti informiamo è stata pubblicata la classifica comprensiva del punteggio totalizzato dalla squadra.\n\nPer consultarla vai sul portale dedicato http://www.itempa.com/meeting2015 nella sezione Meeting. Oppure clicca dui link diretti\n\n\n- Classifica Nazionale http://www.itempa.com/meeting2015/classifica_finale_nazionale.php\n\n- Classifica Provinciale-Regionale http://www.itempa.com/meeting2015/classifica_finale_provinciale-regionale.php\n\n\nCordiali Saluti,\nBack Office\nMeeting 2015\n\n(inviata automaticamente dal Portale Web)";
	
	$msg = new PHPMailer;
	$msg->CharSet = "UTF-8";
	$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
	//$msg->IsHTML(true);
	$msg->SMTPAuth = true; // Autenticazione SMTP
	$msg->SMTPKeepAlive = "true";
	$msg->Host = $ServerSMTP;
	$msg->Username = "meeting.2015@cri.it"; // Nome utente SMTP autenticato
	$msg->Password = "Spaghett!44"; // Password account email con SMTP autenticato
	
	$msg->From = $mittente;
	$msg->FromName = $nomemittente;
	$msg->AddAddress($destinatario); 
//	$msg->AddBCC("paolo.ditoma@libero.it");
	$msg->Subject = $oggetto; 
	$msg->Body = $corpo_messaggio;
	
	
	if(!$msg->Send())
	{
		echo "Si è verificato un errore nell'invio della mail pubblicazione classifica per l'iscritto $row_id[nome] $row_id[cognome] all'indirizzo $row_id[mail] :".$msg->ErrorInfo;
		echo "<br><br>";
		fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Pubblicazione classifica - Invio mail pubblicazione classifica per l'iscritto $row_id[nome] $row_id[cognome] all'indrizzo $row_id[mail] con TOKEN $row_id[token] : ".$msg->ErrorInfo."\r\n");
	}
	else
	{
		echo "Abbiamo inviato una mail di pubblicazione classifica all'iscritto $row_id[nome] $row_id[cognome] all'indirizzo $row_id[mail] con TOKEN $row_id[token].<br><br>";
		fwrite($fp,date('d/m/Y H:i:s').' - '."Pubblicazione classifica massivo - Invio mail all'iscritto $row_id[nome] $row_id[cognome] con TOKEN $row_id[token] correttamente effettuato all'indirizzo $row_id[mail]."."\r\n");

		$query = "UPDATE iscrizioni SET mail_classifica = 1 WHERE id = $row_id[id]";
		$result_update=mysql_query($query, $db);
		
		//Se l'update è andata bene, scrivo nel log
		if ($result_update)
		{
			fwrite($fp,date('d/m/Y H:i:s').' - '."Sollecito conferma mail massivo - Update del mail_classifica a 1 per $row_id[nome] $row_id[cognome] correttamente inserito nel DB."."\r\n");
			echo "Update del mail_classificareminder a 1 per $row_id[nome] $row_id[cognome] correttamente inserito nel DB.<br><br>";

		}
		else
		{
			echo "Update del mail_classifica a 1 per $row_id[nome] $row_id[cognome] in errore:".$msg->ErrorInfo;
			fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Update mail_classifica - Update del mail_classifica a 1 per $row_id[nome] $row_id[cognome]: ".$msg->ErrorInfo."\r\n");
		}
	}
}



mysql_close($db);
fclose($fp);

?>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
