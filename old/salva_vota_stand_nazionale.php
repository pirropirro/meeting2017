<?php
//Recupero i dati
  $id_comitato_prima    = $_POST['id_comitato_prima'];
  $id_comitato_seconda  = $_POST['id_comitato_seconda'];
  $id_comitato_terza    = $_POST['id_comitato_terza'];
  $token 				= $_GET['token'];
  ?>

<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Vota Stand Nazionale</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Vota Stand Nazionale</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Vota Stand Nazionale</h2>
<p id="imPathTitle">Simulazione</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">
 
	<?php
	//Devo controllare se l'utente (token) ha già votato o no..
	include ("config.inc.php");
	echo "<font color=#2B3856 size='2' face='Tahoma'>";
	include ("apri_db.php");
	
	$query = "SELECT 		i.votato AS votato,
							i.nome AS nome,
							i.cognome AS cognome,
							i.token AS token
						FROM iscrizioni AS i
						WHERE i.token = '$token'";
					
	$result = mysql_query($query, $db);
	$row = mysql_fetch_array($result);
	
	
	if ($result){
		if ($row[votato] == 1) {
		//Se ho già votato, il suo voto non conta: 
			$id_comitato_prima    = 0;
			$id_comitato_seconda  = 0;
			$id_comitato_terza    = 0;
			echo "$row[nome] $row[cognome] hai già votato!<br><br>";
			//USCITA CONTROLLATA.
			//	...
			//	...			
		} else if ($row[votato] == 0){
		//Se NON ha ancora votato:
			$votato = 1;
			$query = "UPDATE iscrizioni SET votato = $votato WHERE token = '$token'";
			$result_update = mysql_query($query, $db);
			
			//Controllo se ci son votazioni doppie/triple per lo stesso token
			if($id_comitato_seconda == $id_comitato_prima)		$id_comitato_seconda = -1;
			if($id_comitato_terza == $id_comitato_prima)		$id_comitato_terza = -1;
			else {
				if($id_comitato_terza == $id_comitato_seconda)	$id_comitato_terza = -1;
			}	
			//Aggiugo le votazioni al token specificato (-1 valore per votazioni doppie).
			$query = "UPDATE iscrizioni SET id_comitato_prima = $id_comitato_prima WHERE token = '$token'";
			$result_update = mysql_query($query, $db);
			$query = "UPDATE iscrizioni SET id_comitato_seconda = $id_comitato_seconda WHERE token = '$token'";
			$result_update = mysql_query($query, $db);
			$query = "UPDATE iscrizioni SET id_comitato_terza = $id_comitato_terza WHERE token = '$token'";
			$result_update = mysql_query($query, $db);
			echo "$row[nome] $row[cognome] grazie per aver votato!<br><br>";
		}
	}			
	mysql_close($db);
	?>
	
</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>