<?php
	// settiamo a zero la variabile mobile_browser 
	
	$mobile_browser = '0';
	
	//  controlliamo se l'user agent corrisponde a un dispositivo mobile
	 
	if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
		$mobile_browser++;
	}
	
	// controlliamo HTTP_ACCEPT corrisponde alla tecnologia WAP per telefonini
	 
	if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0)
	or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
		$mobile_browser++;
	}    
	
	// estraiamo i primi 4 caratteri da USER_AGENT e creiamo un array con tutti i header mobile
	 
	$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
	$mobile_agents = array(
		'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
		'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
		'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
		'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
		'newt','noki','oper','palm','pana','pant','phil','play','port','prox',
		'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
		'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
		'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
		'wapr','webc','winw','winw','xda ','xda-');
	
	// se corrisponde a mobile aumentiamo la nostra variabile di 1 
	 
	if (in_array($mobile_ua,$mobile_agents)) {
		$mobile_browser++;
	}
	 
	if (strpos(strtolower($_SERVER['ALL_HTTP']),'OperaMini') > 0) {
		$mobile_browser++;
	}
	 
	if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'windows') > 0) {
		$mobile_browser = 0;
	}
	
	// eccezione per windows phone
	if (preg_match('/(windows phone|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
		$mobile_browser++;
	}
	
	
	// quindi se mobile_browser � inferiore di 0 vuol dire che un dispositivo mobile
	 
	if ($mobile_browser > 0) {
	
	// reindirizzamento per mobile
	
	header('Location: http://www.itempa.com/meeting2015/mobile/classifica_finale_provinciale-regionale.php'); 
	
	}
	else 
	{
		// altrimenti restiamo dove siamo
		?>

		<!--IE 7 quirks mode please-->
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
		<head>
			<title>Classifica Finale Provinciale-Regionale</title>

			<!-- Contents xxxxx-->
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
			<meta http-equiv="Content-Type-Script" content="text/javascript" />
			<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
			<meta name="keywords" content="" />

			<!-- Others -->
			<meta name="Author" content="Paolo di Toma" />
			<meta http-equiv="ImageToolbar" content="False" />
			<meta name="MSSmartTagsPreventParsing" content="True" />
			<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

			<!-- Parent -->
			<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

			<!-- Res -->
			<script type="text/javascript" src="res/x5engine.js"></script>
			<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
			<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
			<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
			<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
			<link rel="stylesheet" type="text/css" href="res/p018.css" media="screen, print" />
			<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
			<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

			<!-- Robots -->
			<meta http-equiv="Expires" content="0" />
			<meta name="Resource-Type" content="document" />
			<meta name="Distribution" content="global" />
			<meta name="Robots" content="index, follow" />
			<meta name="Revisit-After" content="21 days" />
			<meta name="Rating" content="general" />
		</head>
		<body>
		<div id="imSite">
		<div id="imHeader">
			
			<h1>Classifica Finale Provinciale-Regionale</h1>
		</div>
		<div class="imInvisible">
		<hr />
		<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
		<a name="imGoToMenu"></a>
		</div>
		<div id="imBody">
			<div id="imMenuMain">

		<!-- Menu Content START -->
		<p class="imInvisible">Menu principale:</p>
		<div id="imMnMn">

		<?php 
		include ("main_menu.php");
		?>

		</div>
		<!-- Menu Content END -->

			</div>
		<hr class="imInvisible" />
		<a name="imGoToCont"></a>
			<div id="imContent">

		<!-- Page Content START -->
		<div id="imPageSub">
		<br />
		<h2>Classifica Finale</h2>
		<p id="imPathTitle">Meeting</p>
		<div id="imToolTip"></div>
		<div id="imBody">
		<div id="imContent">

		<?
		include("config.inc.php");

		echo "<font color=#2B3856 size='2' face='Tahoma'>";
		echo "<table border='0' width=100%>";

		include ("apri_db.php");
		?>

		<table id='stato_iscrizione' border='0' width=100%>
		<thead>
		<tr>
		<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:120px; height:24px; '>POSIZIONE</th>
		<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:640px; height:24px; '>SQUADRA</th>
		<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:120px; height:24px; '>PUNTEGGIO</th>
		</tr>
		</thead>

		<?php

		//Comitati Regionali che hanno concorso al Meeting
		$query = "SELECT 	ROUND(((p.punteggio_finale + p.Q_punteggio - p.penalita_assenza) * p.penalita_over), 2) AS punteggio_finale,
										c.nome_comitato AS nome_comitato
										FROM preiscrizioni AS p
										INNER JOIN comitati AS c
										ON c.id = p.id_comitato
										WHERE p.iscrizione = '1'
										ORDER BY punteggio_finale DESC";
		$result = mysql_query($query, $db);
		$i = 0;
		$pos = 0;
		$punteggio_precedente = 100;
		while($row = mysql_fetch_array( $result )) 
		{
			$re = "/(\\s*comitato lacale di\\s*|\\s*delegazione di\\s*|\\s*comitato provinciale di\\s*)/i"; 
			$subst = ""; 	 
			$comitato_trunc = preg_replace($re, $subst, $row[nome_comitato]);			
			
			$i = $i + 1;
			if ($row[punteggio_finale] != $punteggio_precedente) $pos = $i;
			?>
			<tr>
			<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $pos ?></td> 
			<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[nome_comitato] ?></td> 
			<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[punteggio_finale] ?></td>
			</tr>
			<?php
			$punteggio_precedente = $row[punteggio_finale];
		}
		mysql_close($db); 
		?>

		</table>
		<br /><br />

		</div>
		</div>
		</div>


		<!-- Page Content END -->

			</div>
			<div id="imFooter">
				<?php 
				include ("footer.php");
				?>
			</div>
		</div>
		</div>
		<div class="imInvisible">
		<hr />
		<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
		</div>

		<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
		</body>
		</html>
        
		<?php
		echo "";
	}   
?>