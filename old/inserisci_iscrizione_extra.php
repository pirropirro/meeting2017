<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>

<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Iscrizione personale extra</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Iscrizione personale extra</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Iscrizione personale extra</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include ("config.inc.php");
echo "<font color=#2B3856 size='3' face='Tahoma'>";
include ("apri_db.php");
?>

<form name="form1" enctype="multipart/form-data" method="post" action="salva_inserisci_iscrizione_extra.php">

	<br />
	<label>Afferente al<br>
 
	<select name="id_comitato" size="8" >

	<?php
	$query = "SELECT 	c.nome_comitato AS nome_comitato,
						c.id AS id_comitato
						FROM comitati AS c
						ORDER BY tipo_meeting DESC";

	$result = mysql_query($query, $db);
	while($row = mysql_fetch_array( $result )) 
	{
		?>
		<option value='<?php echo $row[id_comitato]; ?>'><?php echo $row[nome_comitato]; ?></option>
		<?php
	}
	mysql_close($db);
	?>
	</select>
	</label>

	<br /><br />

	<fieldset>
		<legend><b>Ruolo</b></legend>
		<br />
		Simulatore		<input type="radio" name="ruolo" value="simulatore"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Truccatore		<input type="radio" name="ruolo" value="truccatore"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Giudice			<input type="radio" name="ruolo" value="giudice"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Ospite			<input type="radio" name="ruolo" value="ospite"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Staff			<input type="radio" name="ruolo" value="staff" checked="checked"/>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		Coordinamento	<input type="radio" name="ruolo" value="coordinamento"/><br />
	</fieldset><br />
	<br />
	
	<fieldset>
        <legend><b>Dati del volontario</b></legend>
        <br />
        Nome&nbsp&nbsp
        <input type="text" name="nome" size="20" maxlength="40" required >
        
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        
        Cognome&nbsp&nbsp
        <input type="text" name="cognome" size="20" maxlength="40" required><br />
        <br />
        
        Recapito telefonico&nbsp&nbsp
        <input type="tel" name="telefono" size="8" required title="prefisso e numero senza spazi o altri caratteri (max 10 cifre)" pattern="[0-9]{10}">
        
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        
        E-mail&nbsp&nbsp
        <input type="email" name="mail" size="28" required><br />
        <br />
    
        Esigenze alimentari&nbsp&nbsp
        <input type="text" name="esigenze" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario"><br />
        <br />

        Eventuali disabilità&nbsp&nbsp
        <input type="text" name="disabilita" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario"><br />
        <br />
    
    </fieldset><br />
    <br />

	<br />
	<p align="left">
	<input type="submit" value="Inserisci" />
</form>
<br /><br />

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
