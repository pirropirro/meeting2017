<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato all'organizzazione od al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Invio massivo per conferma mail</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Invio massivo per programma giudici</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Invio singolo notifica per conferma mail</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include ("config.inc.php");
include('PHPMailer/class.phpmailer.php');
echo "<font color=#2B3856 size='2' face='Tahoma'>";
$fp = fopen ("log_iscrizioni.txt",a);

$operatore=$check['uname'];


include ("apri_db.php");

//Cerco le info del giudice facendo join con authuser tramite id
$query_id = "SELECT c.nome_comitato AS nome_comitato,
					i.nome AS nome,
					i.cognome AS cognome,
					i.ruolo AS ruolo,
					i.id AS id,
					i.telefono AS telefono,
					i.mail AS mail,
					i.ora_inizio_mensa AS ora_inizio_mensa,
					i.ora_fine_mensa AS ora_fine_mensa,
					a.level AS level,
					a.team AS team
					FROM iscrizioni AS i
					INNER JOIN comitati AS c
					ON i.id_comitato = c.id
					INNER JOIN authuser AS a
					ON i.id = a.id_iscrizione
					WHERE i.mail_confermata IS NOT NULL
					AND i.ruolo = 'giudice'
					AND i.inviata_mail_programma = '5'";
$result_id = mysql_query($query_id, $db);
while($row_id = mysql_fetch_array( $result_id )) 
{
	$prova_voto = substr($row_id[level], 0, 1);
	//Invio la mail di conferma all'iscritto
	$mittente = "meeting.2016@piemonte.cri.it";
	$nomemittente = "Back Office Meeting 2016";
	$destinatario = "$row_id[mail]";
//	$destinatario = "paolo.ditoma@libero.it";
	$ServerSMTP = "mailbox.cri.it"; //server SMTP
	$oggetto = "Programma Meeting 2016 - Riservato al $row_id[team] - ANNULLA E SOTITUISCE LA PRECEDENTE";
	$apertura_messaggio = "Ciao $row_id[nome],\nmancano pochi giorni al Meeting 2016 dei Giovani della Croce Rossa Italiana.\nTi notifichiamo che il tuo ruolo è il $row_id[team] per la prova $prova_voto e ti riportiamo di seguito il programma dettagliato previsto per te per la giornata\n\n - dalle 07:45 alle 08:30 Ingresso per registrazione (al Desk)\n - dalle 08:00 alle 09:00 Visita degli stand in Piazza Incontro\n - dalle 09:10 alle 09:50 Cerimonia di apertura del X Meeting Regionale\n - dalle 10:00 alle 10:50 Formazione ";
	if ($row_id[team] == 'coordinatore')
	{
		$apertura_messaggio .= "con i giudici\n - i turni per la tua prova si svolgeranno\n";
	}
	else
	{
		$apertura_messaggio .= "con il Coordinatore di prova\n";
	}
	
	
	$turni_messaggio ="";
	//estraggo i turni ed accodo il messaggio
	$query_turni = "SELECT 	DISTINCT t.inizio AS inizio,
											t.fine AS fine,
											t.aula AS aula
									FROM timesheet AS t
									INNER JOIN authuser AS a
									ON t.giudice = a.level
									INNER JOIN iscrizioni AS i
									ON a.id_iscrizione = i.id
									WHERE i.id = $row_id[id]
									ORDER BY t.inizio";
	$result_turni = mysql_query($query_turni, $db);
	while($row_turni = mysql_fetch_array( $result_turni )) 
	{
		$ore_inizio = substr($row_turni[inizio], 0, 2);
		$minuti_inizio = substr($row_turni[inizio], 2, 2);
		$ore_fine = substr($row_turni[fine], 0, 2);
		$minuti_fine = substr($row_turni[fine], 2, 2);
		if ($row_id[team] == 'coordinatore')
		{
			$turni_messaggio .= "     dalle $ore_inizio:$minuti_inizio alle $ore_fine:$minuti_fine \n";
		}
		else
		{
			$turni_messaggio .= " - dalle $ore_inizio:$minuti_inizio alle $ore_fine:$minuti_fine in $row_turni[aula]\n";
		}
	}
	
	$ore_inizio_mensa = substr($row_id[ora_inizio_mensa], 0, 2);
	$minuti_inizio_mensa = substr($row_id[ora_inizio_mensa], 2, 2);
	$ore_fine_mensa = substr($row_id[ora_fine_mensa], 0, 2);
	$minuti_fine_mensa = substr($row_id[ora_fine_mensa], 2, 2);

	$mensa_messaggio = " - dalle 18:10 alle 19:30 Cerimonia di chiusura Meeting Regionale\n\nIl tuo turno per la mensa è previsto dalle ore $ore_inizio_mensa:$minuti_inizio_mensa alle $ore_fine_mensa:$minuti_fine_mensa\n\nTi preghiamo di conservare la seguente mail per facilitarci il compito di coordinamento, eventuali modifiche ai suddetti orari potranno esserti comunicate solo ed esclusivamente dal personale della Control Room e/o tramite whatsapp con messaggio proveniente dal numero 3914616256 (che ti preghiamo di registrare come Control Room).\nConsulta il portale http://www.itempa.com/meeting2016/ per ulteriori informazioni su luogo ed istruzioni per l'ingresso.\nTi aspettiamo al meeting!";
	
	$firma_messaggio = "\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
	
	$msg = new PHPMailer;
	$msg->CharSet = "UTF-8";
	$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
	//$msg->IsHTML(true);
	$msg->SMTPAuth = true; // Autenticazione SMTP
	$msg->SMTPKeepAlive = "true";
	$msg->Host = $ServerSMTP;
	$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
	$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
	
	$msg->From = $mittente;
	$msg->FromName = $nomemittente;
	$msg->AddAddress($destinatario); 
	$msg->AddBCC("paolo.ditoma@libero.it");
	$msg->Subject = $oggetto; 
	$msg->Body = $apertura_messaggio;
	$msg->Body .= $turni_messaggio;	
	$msg->Body .= $mensa_messaggio;	
	$msg->Body .= $firma_messaggio;	
	
	if(!$msg->Send())
	{
		echo "Si è verificato un errore nell'invio della mail del programma per giudici per l'iscritto $row_id[nome] $row_id[cognome] all'indirizzo $row_id[mail] :".$msg->ErrorInfo;
		echo "<br><br>";
		fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Programma giudici - Invio mail del programma per giudici per l'iscritto $row_id[nome] $row_id[cognome] all'indrizzo $row_id[mail] : ".$msg->ErrorInfo."\r\n");
	}
	else
	{
		echo "Abbiamo inviato una mail del programma per giudici all'iscritto $row_id[nome] $row_id[cognome] all'indirizzo $row_id[mail].<br><br>";
		fwrite($fp,date('d/m/Y H:i:s').' - '."Programma giudici mail massivo - Invio mail del programma per giudici all'iscritto $row_id[nome] $row_id[cognome] correttamente effettuato all'indirizzo $row_id[mail]."."\r\n");

		//Faccio l'update del mail_programma

		$query = "UPDATE iscrizioni SET inviata_mail_programma = 6 WHERE id = $row_id[id]";
		$result_update=mysql_query($query, $db);
		
		//Se l'update è andata bene, scrivo nel log
		if ($result_update)
		{
			fwrite($fp,date('d/m/Y H:i:s').' - '."Programma giudici mail massivo - Update del reminder a 1 per $row_id[nome] $row_id[cognome] correttamente inserito nel DB."."\r\n");
			echo "Update del reminder a 1 per $row_id[nome] $row_id[cognome] correttamente inserito nel DB.<br><br>";

		}
		else
		{
			echo "Update del reminder a 1 per $row_id[nome] $row_id[cognome] in errore:".$msg->ErrorInfo;
			fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Programma giudici mail massivo - Update del reminder a 1 per $row_id[nome] $row_id[cognome]: ".$msg->ErrorInfo."\r\n");
		}
	}
}



mysql_close($db);
fclose($fp);

?>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
