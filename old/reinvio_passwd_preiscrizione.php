<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>

<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Reinvia password</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Parent -->
	<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Meeting 2016 - Salva preiscrizione</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<h2>Salva Preiscrizione</h2>
<p id="imPathTitle">Preiscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?
echo "<font color=#2B3856 size='3' face='Tahoma'>";

$id_comitato=$_GET['id_comitato'];

include("config.inc.php");

$db = mysql_connect($db_host, $db_user, $db_password);
if ($db == FALSE)
die ("Errore nella connessione. Verificare i parametri nel file config.inc.php");
mysql_select_db($db_name, $db)
or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");
$query = "SELECT * FROM preiscrizioni WHERE id_comitato='$id_comitato'";
$result = mysql_query($query, $db);
$row = mysql_num_rows($result);
if ( $row == 0)
{
	if (trim($cognome_delegato) == "" ):
	echo "Non hai inserito il Cognome!";
	elseif (trim($nome_delegato) == "") :
		echo "Non hai inserito il Nome!";
		elseif (trim($id_comitato) == "") :
			echo "Non hai selezionato il Comitato!";
			elseif ($telefono_delegato == "") :
				echo "Non hai inserito il tuo recapito telefonico!";
				elseif (trim($mail_delegato) == "") :
					echo "Non hai inserito il tuo indirizzo mail!";
					else:
						$nome_delegato = addslashes(stripslashes($nome_delegato)); 
						$nome_delegato = str_replace("<", "&lt;", $nome_delegato);
						$nome_delegato = str_replace(">", "&gt;", $nome_delegato);
						$cognome_delegato_ins = addslashes(stripslashes($cognome_delegato)); 
						$cognome_delegato_ins = str_replace("<", "&lt;", $cognome_delegato_ins);
						$cognome_delegato_ins = str_replace(">", "&gt;", $cognome_delegato_ins);
						$telefono_delegato = addslashes(stripslashes($telefono_delegato)); 
						$telefono_delegato = str_replace("<", "&lt;", $telefono_delegato);
						$telefono_delegato = str_replace(">", "&gt;", $telefono_delegato);
						$mail_delegato = addslashes(stripslashes($mail_delegato)); 
						$mail_delegato = str_replace("<", "&lt;", $mail_delegato);
						$mail_delegato = str_replace(">", "&gt;", $mail_delegato);
	
						$db = mysql_connect($db_host, $db_user, $db_password);
						if ($db == FALSE)
						die ("Errore nella connessione. Verificare i parametri nel file config.inc.php");
	
						mysql_select_db($db_name, $db)
						or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");
						$query = "INSERT INTO preiscrizioni (id_comitato, nome_delegato, cognome_delegato, telefono_delegato, mail_delegato) VALUES ('$id_comitato', '$nome_delegato', '$cognome_delegato_ins', '$telefono_delegato', '$mail_delegato')";
						if (mysql_query($query, $db))
						echo "La preiscrizione è stata correttamente inserita.<br><br>";
						else
						echo "Errore durante l'inserimento della preiscrizione, rivolgersi all'amministratore";
	
						//ricavo le informazioni del Comitato dalla tabella comitati
						mysql_select_db($db_name, $db)
						or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");
						$query_comitato = "SELECT * FROM comitati WHERE id='$id_comitato'";
						$result_comitato = mysql_query($query_comitato, $db);
						$row_comitato = mysql_fetch_array($result_comitato);
						$nome_comitato = $row_comitato[nome_comitato];
						$mail_comitato = $row_comitato[mail_comitato];
						$nome_presidente = $row_comitato[nome_presidente];
						$cognome_presidente = $row_comitato[cognome_presidente];
						$passwd_comitato = $row_comitato[passwd];

						//ricavo l'id dalla tabella preiscrizioni
						mysql_select_db($db_name, $db)
						or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");
						$query_preiscrizioni = "SELECT id FROM preiscrizioni WHERE id_comitato='$id_comitato'";
						$result_preiscrizioni = mysql_query($query_preiscrizioni, $db);
						$row_preiscrizioni = mysql_fetch_array($result_preiscrizioni);
						$id_preiscrizione = $row_preiscrizioni[id];

						mysql_close($db);
	
						include('PHPMailer/class.phpmailer.php');
						//require_once('PHPMailer/class.smtp.php');  
	
						$mittente = "meeting.2016@piemonte.cri.it";
						$nomemittente = "Back Office Meeting 2016";
						$destinatario = "$mail_comitato";
						$ServerSMTP = "mailbox.cri.it"; //server SMTP
						$oggetto = "Preiscrizione Meeting 2016";
						$corpo_messaggio = "Buongiorno Presidente $nome_presidente $cognome_presidente,\n\nil volontario $nome_delegato $cognome_delegato per conto dei giovani CRI del tuo comitato ha registrato, attraverso il portale dedicato, la preiscrizione della squadra al Meeting 2016.\nPer poter ritenere valida la registrazione è necessaria la tua conferma. Clicca sul seguente link \n\n   http://www.itempa.com/meeting2016/conferma_preiscrizione.php?id=$id_preiscrizione\n\n ed inserisci la password     $passwd_comitato     per confermare la partecipazione della squadra del $nome_comitato al Meeting 2016.\n\nNota bene: confermando la preiscrizione dichiari altresì di aver preso visione del Regolamento per i Comitati Locali al Meeting Regionale disponibile al link\n\n   http://www.itempa.com/meeting2016/RegolamentoMeeting2016_Regionale.pdf\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
	
						$msg = new PHPMailer;
						$msg->CharSet = "UTF-8";
						$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
						//$msg->IsHTML(true);
						$msg->SMTPAuth = true; // Autenticazione SMTP
						$msg->SMTPKeepAlive = "true";
						$msg->Host = $ServerSMTP;
						$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
						$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
	
						$msg->From = $mittente;
						$msg->FromName = $nomemittente;
						$msg->AddAddress($destinatario); 
	//					$msg->AddCC("paolo.ditoma@libero.it");
						$msg->AddBCC("paolo.ditoma@libero.it");
						$msg->Subject = $oggetto; 
						$msg->Body = $corpo_messaggio;
	
	
						if(!$msg->Send()) {
						echo "Si è verificato un errore nell'invio della mail per la conferma da parte del tuo presidente:".$msg->ErrorInfo;
						} else {
						echo "Abbiamo inviato una mail al tuo presidente notificando la registrazione della squadra, all'interno della quale è necessario cliccare sul link per la conferma.";
						}
	
	endif; // chiude la verifica della presenza dei dati
	
	
}
else
{
	echo "La preiscrizione per la squadra del comitato indicato risulta già inserita. Reinvio la mail con la password";
	
	//ricavo le informazioni del Comitato dalla tabella comitati
	mysql_select_db($db_name, $db)
	or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");
	$query_comitato = "SELECT * FROM comitati WHERE id='$id_comitato'";
	$result_comitato = mysql_query($query_comitato, $db);
	$row_comitato = mysql_fetch_array($result_comitato);
	$nome_comitato = $row_comitato[nome_comitato];
	$mail_comitato = $row_comitato[mail_comitato];
	$nome_presidente = $row_comitato[nome_presidente];
	$cognome_presidente = $row_comitato[cognome_presidente];
	$passwd_comitato = $row_comitato[passwd];
    
	//ricavo l'id dalla tabella preiscrizioni
	mysql_select_db($db_name, $db)
	or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");
	$query_preiscrizioni = "SELECT id FROM preiscrizioni WHERE id_comitato='$id_comitato'";
	$result_preiscrizioni = mysql_query($query_preiscrizioni, $db);
	$row_preiscrizioni = mysql_fetch_array($result_preiscrizioni);
	$id_preiscrizione = $row_preiscrizioni[id];
    
	mysql_close($db);
	
	include('PHPMailer/class.phpmailer.php');
	//require_once('PHPMailer/class.smtp.php');  
	
	$mittente = "meeting.2016@piemonte.cri.it";
	$nomemittente = "Back Office Meeting 2016";
	$destinatario = "$mail_comitato";
	$ServerSMTP = "mailbox.cri.it"; //server SMTP
	$oggetto = "Preiscrizione Meeting 2016";
	$corpo_messaggio = "Buongiorno Presidente $nome_presidente $cognome_presidente,\n\nil volontario $nome_delegato $cognome_delegato per conto dei giovani CRI del tuo comitato ha registrato, attraverso il portale dedicato, la preiscrizione della squadra al Meeting 2016.\nPer poter ritenere valida la registrazione è necessaria la tua conferma. Clicca sul seguente link \n\n   http://www.itempa.com/meeting2016/conferma_preiscrizione.php?id=$id_preiscrizione\n\n ed inserisci la password     $passwd_comitato     per confermare la partecipazione della squadra del $nome_comitato al Meeting 2016.\n\nNota bene: confermando la preiscrizione dichiari altresì di aver preso visione del Regolamento per i Comitati Locali al Meeting Regionale disponibile al link\n\n   http://www.itempa.com/meeting2016/RegolamentoMeeting2016_Regionale.pdf\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
	
	$msg = new PHPMailer;
	$msg->CharSet = "UTF-8";
	$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
	//$msg->IsHTML(true);
	$msg->SMTPAuth = true; // Autenticazione SMTP
	$msg->SMTPKeepAlive = "true";
	$msg->Host = $ServerSMTP;
	$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
	$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
	
	$msg->From = $mittente;
	$msg->FromName = $nomemittente;
	$msg->AddAddress($destinatario); 
//	$msg->AddCC("paolo.ditoma@libero.it");
	$msg->AddBCC("paolo.ditoma@libero.it");
	$msg->Subject = $oggetto; 
	$msg->Body = $corpo_messaggio;
	
	
	if(!$msg->Send()) {
	echo "Si è verificato un errore nell'invio della mail per la conferma da parte del tuo presidente:".$msg->ErrorInfo;
	} else {
	echo "Abbiamo inviato una mail al tuo presidente notificando la registrazione della squadra, all'interno della quale è necessario cliccare sul link per la conferma.";
	}
    
}

?>

<br /><br />
<form method="post" action="index.php">
<input type='submit' value='Torna alla Home Page'></form>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
