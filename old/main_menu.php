<ul>
	<li><a href="index.php" title="Home Page">Home Page</a>
		<ul>
			<li><a href="login.php" title="">Autenticati</a></li>
			<li><a href="chgpwd.php" title="">Cambia password</a></li>
			<li><a href="logout.php" title="">Esci</a></li>
		</ul>
	</li>
	<li><a >Preiscrizioni</a>
		<ul>
			<li><a href="spiega_preiscrizione.php" title="">Inserisci</a></li>
			<li><a href="riepilogo_preiscrizioni.php" title="">Elenco (Staff)</a></li>
		</ul>
	</li>
	<li><a >Iscrizioni</a>
		<ul>
			<li><a href="spiega_iscrizione_squadra.php" title="">Inserisci squadra</a></li>
			<li><a href="spiega_iscrizione_osservatore.php" title="">Inserisci osservatore con squadra</a></li>
			<li><a href="spiega_iscrizione_osservatore_senza_squadra.php" title="">Inserisci osservatore senza squadra</a></li>
			<li><a href="consulta_iscrizione_passwd_richiesta.php" title="">Consulta Iscrizione</a></li>
			<li><a href="tabellone_iscrizioni.php" title="">Tabellone Iscrizioni</a></li>
			<li><a href="inserisci_iscrizione_extra.php" title="">Inserisci extra (Back Office)</a></li>
			<li><a href="scelta_giudici.php" title="">Recluta Giudici (Back Office)</a></li>
			<li><a href="scelta_simulatori_truccatori.php" title="">Recluta Simulatori Truccatori (Back Office)</a></li>
			<li><a href="invio_singolo_mail_conferma.php" title="">Sollecito Conferma Mail Singolo (Back Office)</a></li>
			<li><a href="ricerca_iscrizioni.php" title="">Ricerca Iscrizioni (Back Office)</a></li>
		</ul>
	</li>

	<li><a >Meeting</a>
		<ul>
			<li><a href="programma_meeting.php" title="">Programma del Meeting</a></li>
			<li><a href="istruzioni_ingresso.php" title="">Istruzioni per l'ingresso</a></li>
			<li><a href="riepilogo_accoglienza.php" title="">Registrazioni in Piazza (Back Office)</a></li>
			<li><a href="registrazione_simtrugiustaospcoo.php" title="">Registrazioni al Desk (Back Office)</a></li>
			<li><a href="tabellone_registrazioni.php" title="">Tabellone Registrazioni</a></li>
            <li><a href="validazione_punteggi.php" title="">Validazione punteggi (Back Office)</a></li>
            <li><a href="controllo_punteggi.php" title="">Controllo punteggi (Back Office)</a></li>
			<li><a href="tabellone_aule_pieno.php" title="">Tabellone aule (Back Office)</a></li>
			<li><a href="quadro_punteggi_partecipanti.php" title="">Quadro Punteggi</a></li>
			<li><a href="classifica_finale.php" title="">Classifica Finale</a></li>
		</ul>
	</li>
	
	<li><a >Documenti</a>
        <ul>
            <li><a href="RegolamentoMeeting2016_Regionale.pdf" title="">Regolamento Meeting 2016 Regionale</a></li>
<!--            <li><a href="RegolamentoMeeting2015_Nazionale.pdf" title="">Regolamento Meeting 2015 Nazionale</a></li>     -->
        </ul>
	</li>
<!--
	<li><a >Service</a>
        <ul>
            <li><a href="riepilogo_materiale.php" title="">Materiale (Staff)</a></li>
            <li><a href="riepilogo_partner.php" title="">Partner (Staff)</a></li>
        </ul>
	</li>
-->
	<li><a >Contatti</a>
        <ul>
            <li><a href="contattaci.php" title="">Contattaci</a></li>
            <li><a href="come_arrivare.php" title="">Come arrivare</a></li>
        </ul>
	</li>
</ul>