<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice') && !($check['team'] == 'organigramma'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato all'organizzazione od al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Scelta Giudici</title>

	<!-- Contents xxxxx-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Parent -->
	<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/tableExport.js"></script>
	<script type="text/javascript" src="js/jquery.base64.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p018.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Scelta Giudici</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Scelta Giudici</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?
include("config.inc.php");
include ("apri_db.php");
?>

<font color=#2B3856 size='2' face='Tahoma'>

<form name="form1" enctype="multipart/form-data" method="post" action="salva_scelta_giudici.php">

<table id='stato_iscrizione' border='0' width=100%>
<thead>
<tr>
<th rowspan="2" align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>SEL</th>
<th colspan="3" align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='height:24px; '>DATI ANAGRAFICI</th>
<th colspan="8" align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='height:24px; '>GIUDICE</th>
</tr>
<tr>
<th align=center style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='height:24px; '>COMITATO</th>
<th align=center style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='height:24px; '>NOME</th>
<th align=center style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='height:24px; '>E-MAIL</th>
<th align=center style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='height:24px; '>Op. SALUTE</th>
<th align=center style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='height:24px; '>MONITORE PS</th>
<th align=center style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='height:24px; '>OSG</th>
<th align=center style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='height:24px; '>OPEM</th>
<th align=center style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='height:24px; '>Op. DRRCCA</th>
<th align=center style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='height:24px; '>DIU</th>
<th align=center style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='height:24px; '>Op. Attività per i Giovani</th>
<th align=center style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='height:24px; '>Op. SVILUPPO</th>
</tr>
</thead>

<?php
//Inizializzo i contatori
$tot_giudici_1 = 0;
$tot_giudici_3 = 0;
$tot_giudici_4 = 0;
$tot_giudici_5 = 0;
$tot_giudici_6 = 0;
$tot_giudici_7 = 0;
$tot_giudici_10 = 0;
$tot_giudici_12 = 0;

//Estraggo con mysql pivot
$query = "SELECT 	c.nome_comitato AS comitato,
					c.tipo_meeting AS meeting,
					i.id AS id,
					i.nome AS nome,
					i.cognome AS cognome,
					i.telefono AS telefono,
					i.mail AS mail,
					i.mail_confermata AS mail_confermata,
					i.giudice_1 AS giudice_1,
					i.giudice_2 AS giudice_2,
					i.giudice_3 AS giudice_3,
					i.giudice_4 AS giudice_4,
					i.giudice_5 AS giudice_5,
					i.giudice_6 AS giudice_6,
					i.giudice_7 AS giudice_7,
					i.giudice_10 AS giudice_10,
					i.giudice_12 AS giudice_12
					FROM iscrizioni AS i
					INNER JOIN comitati AS c
					ON i.id_comitato = c.id
					WHERE ruolo = 'osservatore'
					AND registrazione != 9
					AND (giudice_1 = 1 OR giudice_2 = 1 OR giudice_3 = 1 OR giudice_4 = 1 OR giudice_5 = 1 OR giudice_6 = 1 OR giudice_7 = 1 OR giudice_10 = 1 OR giudice_12 = 1)
					ORDER BY c.tipo_meeting DESC";
$result = mysql_query($query, $db);
$tot_estratti = mysql_num_rows($result);
while($row = mysql_fetch_array( $result )) 
{
	$re = "/(|\\s*comitato locale di\\s*|\\s*comitato provinciale di\\s*|\\s*comitato regionale\\s*|\\s*delegazione di\\s*|\\s*a valenza regionale\\s*)/i";  
	$subst = ""; 	 
	$comitato_trunc = preg_replace($re, $subst, $row[comitato]);
	?>
	<tr>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><input type="radio" name="id_scelto" value="<?= $row[id] ?>"/></td>
    <td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="width:80px; height:18px; overflow-y:auto; overflow-x:hidden;"><?= $comitato_trunc ?></td>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="width:120px; height:18px; overflow-y:hidden; overflow-x:hidden;"><?= $row[nome]." ".$row[cognome] ?></td>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style=" <?php if($row[mail_confermata] != 1) echo "color:red;"; ?> width:190px; height:18px; overflow-y:hidden; overflow-x:hidden;"><?= $row[mail] ?></td>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="width:40px; height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if($row[giudice_1] == 1 OR $row[giudice_2] == 1){ echo "si"; $tot_giudici_1++;} ?></td>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="width:40px; height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if($row[giudice_3] == 1){ echo "si"; $tot_giudici_3++;} ?></td>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="width:40px; height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if($row[giudice_4] == 1){ echo "si"; $tot_giudici_4++;} ?></td>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="width:40px; height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if($row[giudice_5] == 1){ echo "si"; $tot_giudici_5++;} ?></td>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="width:40px; height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if($row[giudice_6] == 1){ echo "si"; $tot_giudici_6++;} ?></td>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="width:40px; height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if($row[giudice_7] == 1){ echo "si"; $tot_giudici_7++;} ?></td>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="width:40px; height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if($row[giudice_10] == 1){ echo "si"; $tot_giudici_10++;} ?></td>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="width:40px; height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if($row[giudice_12] == 1){ echo "si"; $tot_giudici_12++;} ?></td>
	<?php
}
?>

<tr>
<td colspan="4" align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b>TOTALE (<?= $tot_estratti ?> giudici)</b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_giudici_1 ?></b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_giudici_3 ?></b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_giudici_4 ?></b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_giudici_5 ?></b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_giudici_6 ?></b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_giudici_7 ?></b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_giudici_10 ?></b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_giudici_12 ?></b></td>


<?php
mysql_close($db); 
?>
</table>

<br /><br />
<p align="left">
<input type="submit" value="Recluta" />
</form>
    	
<!-- <a href="#" onClick ="$('#stato_iscrizione').tableExport({type:'excel',escape:'false',fileName: 'Stato Iscrizione'});">Esporta in MS Excel</a> -->

<br /><br />


</div>
</div>
</div>


<!-- Page Content END -->

	</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
