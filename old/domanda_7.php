<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice') && !($check['team'] == 'capitani'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Domanda 7</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.php" title="Meeting 2016 - Homepage"><img src="logo_meeting.png" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		<p><strong>Domanda 7</strong></p>

        <div data-html="true">
		
		<?php
		$id_comitato = $_GET['id_comitato'];
		
		include ("config.inc.php");
		include ("apri_db.php");
		
		
		$query = "SELECT p.Q_SEL7 AS q_sel
						FROM preiscrizioni AS p
						WHERE id_comitato = $id_comitato";
		
		$result = mysql_query($query, $db);
		$row = mysql_fetch_array($result);
		
		?>

		<form name="form1" enctype="multipart/form-data" method="post" action="salva_questionario.php?domanda=7&id_comitato=<?= $id_comitato ?>">
		
		<fieldset data-role="controlgroup">
			<legend>I Comitati della Croce Rossa Italiana sono istituiti:</legend>
				<input type="radio" name="risposta" id="radio-choice-1" value="1" <?php if($row[q_sel] == 1) echo "checked= 'checked' "; ?>>
				<label for="radio-choice-1">Previa verifica della sussistenza dei requisiti concernenti il numero minimo dei soci, la presenza di adeguate risorse economiche, sufficienti a garantire lo svolgimento delle attività.</label><br /><br />
				<input type="radio" name="risposta" id="radio-choice-2" value="2" <?php if($row[q_sel] == 2) echo " checked= 'checked' "; ?>>
				<label for="radio-choice-2">Su richiesta di un gruppo di cittadini interessati a portare sul loro territorio le attività di Croce Rossa.</label><br /><br />
				<input type="radio" name="risposta" id="radio-choice-3" value="3" <?php if($row[q_sel] == 3) echo " checked= 'checked' "; ?>>
				<label for="radio-choice-3">Previa verifica di adeguate risorse economiche, sufficienti a garantire lo svolgimento delle attività.</label><br /><br />
				<input type="radio" name="risposta" id="radio-choice-4" value="4" <?php if($row[q_sel] == 4) echo " checked= 'checked' "; ?>>
				<label for="radio-choice-4">Su richiesta del sindaco del Comune che dispone già di una struttura che potrebbe ospitare il comitato.</label><br /><br />
				<input type="radio" name="risposta" id="radio-choice-5" value="5" <?php if($row[q_sel] == 5) echo " checked= 'checked' "; ?>>
				<label for="radio-choice-5">Non lo so.</label><br /><br />
		</fieldset>
        <br /> <br /> <br />
		
		<input type="submit" value="Conferma" />
		</form>
		
		<?php
		mysql_close($db); 
		?>
        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>