<?php
require('fpdf.php');
include ("config.inc.php");
define('EURO', chr(128));

class PDF extends FPDF
{
// Page header
function Header()
{
    // Logo
    $this->Image('./res/giovani_verticale.jpg',10,6,30);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(40,10,'Segreteria Meeting 2014',0,1,'C');

    // Secondo logo
    $this->Image('./res/logo_meeting.jpg',170,6,30);

    $this->Cell(80);
    $this->SetFont('Arial','',12);
	$this->Cell(40,10,'Ricevuta della partecipazione',0,0,'C');
	
    // Line break
    $this->Ln(30);
}

// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
	// $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	// Firma della ricevuta
	$this->Cell(330,0,'Croce Rossa Italiana',0,1,'C');
	// Line break
    //$this->Ln();
	$this->Cell(330,10,'Comitato Locale di Strambino',0,0,'C');
}
}



//Apro il DB
$db = mysql_connect($db_host, $db_user, $db_password);
if ($db == FALSE)
die ("Errore nella connessione. Verificare i parametri nel file config.inc.php");
mysql_select_db($db_name, $db)
or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");


//Faccio 


$totale =0;


// Instanciation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',12);



//Scrivo la formula di apertura
$pdf->Cell(0,8,'Con al presente si certifica che in data 19 ottobre 2014, presso Scuola Primaria “Generale C.A. Dalla Chiesa “- Via Madonna del Rosario, 25 – Strambino (TO), hanno partecipato al XV Meeting Provinciale - VIII Regionale dei Giovani della Croce Rossa Italiana i partecipanti di cui sotto.',0,1);

$pdf->SetFont('Times','',8);

$pdf->Cell(0,6,'Come membri della squadra iscritta (quota pro capite 10 euro):',0,1);
//estraggo i membri della squadra



//

$pdf->SetFont('Times','',8);
$pdf->Write(5,'Come ');
// Quindi inserisce un link blu sottolineato
$pdf->SetFont('','BI');
$pdf->Write(5,'osservatori ');
$pdf->SetFont('Times','',8);
$pdf->Write(5,'(quota pro capite 13'.EURO.'):');
$pdf->Ln(5);

//estraggo gli osservatori


$query = "SELECT * FROM osservatori WHERE ruolo = 'osservatore' AND comitato = 'cp.torino' ";
//echo "$query";
$result = mysql_query($query, $db);
while($row = mysql_fetch_array( $result )) 
{
$comitatotrunc = strtoupper(substr("$row[comitato]",3));
$totale = $totale + 13;

$pdf->Cell(50,4,''.$row[nome].' '.$row[cognome],0,1);

}

//

$pdf->Cell(0,8,'Come simulatori (quota pro capite 0€):',0,1);
//estraggo gli osservatori



//

$pdf->Cell(0,8,'Come giudici (quota pro capite 0€):',0,1);
//estraggo gli osservatori



//

$pdf->Cell(0,8,'Come staff (quota pro capite 0€):',0,1);
//estraggo gli osservatori



//

$pdf->Cell(0,8,'Il totale pagato è $totale',0,1);

?>