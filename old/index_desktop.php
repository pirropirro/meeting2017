<?php
$data_pubblicazione = strftime('201607140001');
$data_attuale = strftime('%Y%m%d%H%M', time());

if ($data_pubblicazione <= $data_attuale)
{
?>
	<!--IE 7 quirks mode please-->
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
	<head>
		<title>Meeting 2016 - Comitato Regionale del Piemonte - Homepage</title>
	
		<!-- Contents -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-Language" content="it" />
		<meta http-equiv="last-modified" content="07/01/2009 10.37.23" />
		<meta http-equiv="Content-Type-Script" content="text/javascript" />
		<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
		<meta name="keywords" content="" />
	
		<!-- Others -->
		<meta name="Author" content="Paolo di Toma" />
		<meta http-equiv="ImageToolbar" content="False" />
		<meta name="MSSmartTagsPreventParsing" content="True" />
		<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />
	
		<!-- Parent -->
		<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />
	
		<!-- Res -->
		<script type="text/javascript" src="res/x5engine.js"></script>
		<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
		<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
		<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
		<link rel="stylesheet" type="text/css" href="res/home.css" media="screen, print" />
		<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
		<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />
	
		<!-- Robots -->
		<meta http-equiv="Expires" content="0" />
		<meta name="Resource-Type" content="document" />
		<meta name="Distribution" content="global" />
		<meta name="Robots" content="index, follow" />
		<meta name="Revisit-After" content="21 days" />
		<meta name="Rating" content="general" />
	</head>
	<body>
	<div id="imSite">
	<div id="imHeader">
		
		<h1>Meeting 2016 - Comitato Regionale del Piemonte</h1>
	</div>
	<div class="imInvisible">
	<hr />
	<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
	<a name="imGoToMenu"></a>
	</div>
	<div id="imBody">
		<div id="imMenuMain">
	
	<!-- Menu Content START -->
	<p class="imInvisible">Menu principale:</p>
	<div id="imMnMn">
	
	<?php 
	include ("main_menu.php");
	?>
	
	</div>
	<!-- Menu Content END -->
	
		</div>
	<hr class="imInvisible" />
	<a name="imGoToCont"></a>
		<div id="imContent">
	
	<!-- Page Content START -->
	<div id="imToolTip"></div>
	<div id="imPage">
	
	<div id="imCel0_00">
	<div id="imCel0_00_Cont">
		<div id="imObj0_00">
	<p class="imAlign_left"><span class="ff2 fc0 fs10 ">
	<br /></span></p>
	<p class="imAlign_center"><span class="ff2 fc0 fs10 ">Benvenuto al MEETING 2016<br /><br />Siamo giunti alla X edizione Regionale. Abbiamo creato questo portale per gestire le iscrizioni, le registrazioni, le votazioni, la consultazione dei punteggi e quant'altro è funzionale al Meeting.<br /><br />Le preiscrizioni si sono chiuse lo scorso <b>31 agosto</b>.<br /><br />Le iscrizioni saranno disponibili dal <b>12 settembre</b> al <b>2 ottobre</b>.<br /><br /><br /><b>Guarda il video promo<b><br /><br /></p>
	
	<iframe width="560" height="315" src="https://www.youtube.com/embed/C5HIZguIIUY" frameborder="0" allowfullscreen></iframe>
	
	<br /><br />
	
	<p class="imAlign_center"><span class="ff2 fc2 fs10 "><b>Buon divertimento!<b></span></p>
	<br /><br />
	</li></ul>
	
	
		</div>
	</div>
	</div>
	
	</div>
	
	<!-- Page Content END -->
	
			</div>
		<div id="imFooter">
			<?php 
			include ("footer.php");
			?>
		</div>
	</div>
	</div>
	<div class="imInvisible">
	<hr />
	<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
	</div>
	
	<div id="imZIBackg" onClick="imZIHide()" onKeyPress="imZIHide()"></div>
	</body>
	</html>
<?php
}
else
{
	?>
	<!DOCTYPE html>
	<html>
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">

	<title>Meeting 2016</title>
	</head>

    <body>
    <img src="waiting_online.jpg" width="atuo" height="auto" alt="Stiamo lavorando per voi" align="center" />
    </body>
    </html>
<?php
}
?>