<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Contattaci</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Parent -->
	<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Meeting 2016 - Contattaci</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<h2>Contattaci</h2>
<p id="imPathTitle">Contatti</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
echo "<font color=#2B3856 size='3' face='Tahoma'>";

$nome			=$_REQUEST['nome'];
$mail			=$_REQUEST['mail'];
$oggetto		=$_REQUEST['oggetto'];
$richiesta		=$_REQUEST['richiesta'];

if (trim($nome) == "" ):
	echo "Non hai inserito il tuo nome";
	elseif (trim($mail) == "") :
		echo "Non hai inserito la tua mail";
		elseif (trim($oggetto) == "") :
			echo "Non hai selezionato l'oggetto";
			elseif (trim($richiesta) == "") :
				echo "Non hai inserito la tua richiesta";
					else:
						$nome = addslashes(stripslashes($nome)); 
						$nome = str_replace("<", "&lt;", $nome);
						$nome = str_replace(">", "&gt;", $nome);
						$mail = addslashes(stripslashes($mail)); 
						$mail = str_replace("<", "&lt;", $mail);
						$mail = str_replace(">", "&gt;", $mail);
						$richiesta = addslashes(stripslashes($richiesta)); 
						$richiesta = str_replace("<", "&lt;", $richiesta);
						$richiesta = str_replace(">", "&gt;", $richiesta);
	
						include('PHPMailer/class.phpmailer.php');
						//require_once('PHPMailer/class.smtp.php');  
	
						$mittente = "meeting.2016@piemonte.cri.it";
						$nomemittente = "$nome - Portale";
						$destinatario = "meeting.2016@piemonte.cri.it";
						$ServerSMTP = "mailbox.cri.it"; //server SMTP
						$oggetto = "[Contattaci] - $nome - $oggetto";
						$corpo_messaggio = "$richiesta\n\nRispondere a $nome all'indirizzo $mail\n\n\nInviata automaticamente dal portale";
	
						$msg = new PHPMailer;
						$msg->CharSet = "UTF-8";
						$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
						//$msg->IsHTML(true);
						$msg->SMTPAuth = true; // Autenticazione SMTP
						$msg->SMTPKeepAlive = "true";
						$msg->Host = $ServerSMTP;
						$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
						$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
	
						$msg->From = $mittente;
						$msg->FromName = $nomemittente;
						$msg->AddAddress($destinatario); 
	//					$msg->AddCC("paolo.ditoma@libero.it");
						$msg->AddBCC("paolo.ditoma@libero.it");
						$msg->Subject = $oggetto; 
						$msg->Body = $corpo_messaggio;
	
	
						if(!$msg->Send())
						{
							echo "Si è verificato un errore nell'invio della mail contatta l'amministratore:".$msg->ErrorInfo;
						}
						else
						{
							echo "Abbiamo inviato una mail con la tua richiesta al Back Office, che ti risponderà al più presto.<br><br>Grazie $nome per aver utilizzato il portale.";
						}
	
endif; // chiude la verifica della presenza dei dati
?>

<br /><br />
<form method="post" action="index.php">
<input type='submit' value='Torna alla Home Page'></form>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
