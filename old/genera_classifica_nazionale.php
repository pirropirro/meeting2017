<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Genera Classifica Nazionale</title>

	<!-- Contents xxxxx-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Parent -->
	<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p018.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<meta http-equiv="refresh" content="60" >
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Genera Classifica Nazionale</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Genera Classifica Nazionale</h2>
<p id="imPathTitle">Meeting</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?
include("config.inc.php");

echo "<font color=#2B3856 size='2' face='Tahoma'>";
echo "<table border='0' width=100%>";

include ("apri_db.php");
?>

<table id='stato_iscrizione' border='0' width=100%>
<thead>
<tr>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:150px; height:24px; '>POSIZIONE</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:700px; height:24px; '>SQUADRA</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:150px; height:24px; '>PUNTEGGIO</th>
</tr>
</thead>

<?php

//Comitati che hanno confermato
$query = "SELECT 	p.parziale_nazionale AS parziale,
								p.d1_c AS prova_india,
								p.d2_c AS prova_papa,
								p.id_comitato AS id_comitato,
								c.nome_comitato AS nome_comitato
								FROM preiscrizioni AS p
								INNER JOIN comitati AS c
								ON c.id = p.id_comitato
								WHERE c.tipo_meeting = 'N'
								AND p.iscrizione = '1'
								ORDER BY p.parziale_nazionale DESC";
$result = mysql_query($query, $db);
$i = 0;
while($row = mysql_fetch_array( $result )) 
{
	$i = $i + 1;
	$punteggio_finale = $row[parziale] + $row[prova_india] + $row[prova_papa];
	?>
    <tr>
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $i ?></td> 
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[nome_comitato] ?></td> 
	<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $punteggio_finale ?></td>
	</tr>
    <?php
	$query_classifica = "UPDATE preiscrizioni SET punteggio_finale = $punteggio_finale WHERE id_comitato = $row[id_comitato]";
	$result_classifica = mysql_query ($query_classifica, $db);
}
mysql_close($db); 
?>

</table>
<br /><br />

</div>
</div>
</div>


<!-- Page Content END -->

	</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
