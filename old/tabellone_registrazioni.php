<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato al Back Office.</b></font>";
	exit;	// Stop script execution
}

?>
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Tabellone Registrazioni</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/tableExport.js"></script>
	<script type="text/javascript" src="js/jquery.base64.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body style="background-color: white;">



<!-- Menu Content START -->

<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->
<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />



<?
include("config.inc.php");
include ("apri_db.php");
?>

<font color=#2B3856 size='2' face='Tahoma'>
<table id='stato_iscrizione' border='0' width=100%>
<thead>
<tr>
<th rowspan="2" align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:120px; height:24px; '>COMITATO</th>
<th colspan="3" align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:120px; height:24px; '>SQUADRA</th>
<th colspan="3" align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:120px; height:24px; '>OSSERVATORI</th>
<th colspan="3" align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:120; height:24px; '>SIMULATORI</th> 
<th colspan="3" align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:120px; height:24px; '>GIUDICI</th>
<th colspan="3" align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:120px; height:24px; '>OSPITI</th>
<th colspan="3" align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:120px; height:24px; '>STAFF</th>
<th colspan="3" align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:120px; height:24px; '>COORDINAMENTO</th>
<tr>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>T</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>P</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>A</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>T</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>P</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>A</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>T</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>P</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>A</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>T</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>P</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>A</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>T</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>P</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>A</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>T</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>P</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>A</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>T</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>P</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>A</th>

</tr>
</thead>

<?php
//Inizializzo i contatori
$tot_squadra = 0;
$tot_osservatori = 0;
$tot_simulatori = 0;
$tot_truccatori = 0;
$tot_giudici = 0;
$tot_ospiti = 0;
$tot_staff = 0;
$tot_coordinamento = 0;
$tot_iscritti = 0;
$tot_registrati = 0;
$tot_assenti = 0;
$tot_squadre_iscritte = 0;


//Estraggo con mysql pivot
$query = "SELECT c.nome_comitato AS comitato,
c.tipo_meeting AS meeting,
p.accoglienza AS accoglienza,
COUNT(
 CASE
  WHEN i.ruolo = 'capitano' OR i.ruolo = 'partecipante' OR i.ruolo = 'adulto' THEN 1
  ELSE NULL
  END
) AS squadra,
COUNT(
 CASE
  WHEN (i.ruolo = 'capitano' OR i.ruolo = 'partecipante' OR i.ruolo = 'adulto') AND (i.registrazione = 1 OR i.registrazione = 2) THEN 1
  ELSE NULL
  END
) AS squadra_registrati,
COUNT(
 CASE
  WHEN (i.ruolo = 'capitano' OR i.ruolo = 'partecipante' OR i.ruolo = 'adulto') AND i.registrazione = 9 THEN 1
  ELSE NULL
  END
) AS squadra_assenti,
COUNT(
 CASE
  WHEN i.ruolo = 'osservatore' THEN 1
  ELSE NULL
  END
) AS osservatori,
COUNT(
 CASE
  WHEN i.ruolo = 'osservatore'  AND (i.registrazione = 1 OR i.registrazione = 2) THEN 1
  ELSE NULL
  END
) AS osservatori_registrati,
COUNT(
 CASE
  WHEN i.ruolo = 'osservatore' AND i.registrazione = 9 THEN 1
  ELSE NULL
  END
) AS osservatori_assenti,
COUNT(
 CASE
  WHEN i.ruolo = 'simulatore' THEN 1
  ELSE NULL
  END
) AS simulatori,
COUNT(
 CASE
  WHEN i.ruolo = 'simulatore'  AND i.registrazione = 1 THEN 1
  ELSE NULL
  END
) AS simulatori_registrati,
COUNT(
 CASE
  WHEN i.ruolo = 'simulatore' AND i.registrazione = 9 THEN 1
  ELSE NULL
  END
) AS simulatori_assenti,
COUNT(
 CASE
  WHEN i.ruolo = 'truccatore' THEN 1
  ELSE NULL
  END
) AS truccatori,
COUNT(
 CASE
  WHEN i.ruolo = 'truccatore'  AND i.registrazione = 1 THEN 1
  ELSE NULL
  END
) AS truccatori_registrati,
COUNT(
 CASE
  WHEN i.ruolo = 'truccatore' AND i.registrazione = 9 THEN 1
  ELSE NULL
  END
) AS truccatori_assenti,
COUNT(
 CASE
  WHEN i.ruolo = 'giudice' THEN 1
  ELSE NULL
  END
) AS giudici,
COUNT(
 CASE
  WHEN i.ruolo = 'giudice'  AND i.registrazione = 1 THEN 1
  ELSE NULL
  END
) AS giudici_registrati,
COUNT(
 CASE
  WHEN i.ruolo = 'giudice' AND i.registrazione = 9 THEN 1
  ELSE NULL
  END
) AS giudici_assenti,
COUNT(
 CASE
  WHEN i.ruolo = 'ospite' THEN 1
  ELSE NULL
  END
) AS ospiti,
COUNT(
 CASE
  WHEN i.ruolo = 'ospite' AND i.registrazione = 1 THEN 1
  ELSE NULL
  END
) AS ospiti_registrati,
COUNT(
 CASE
  WHEN i.ruolo = 'ospite'  AND i.registrazione = 9 THEN 1
  ELSE NULL
  END
) AS ospiti_assenti,
COUNT(
 CASE
  WHEN i.ruolo = 'staff' THEN 1
  ELSE NULL
  END
) AS staff,
COUNT(
 CASE
  WHEN i.ruolo = 'staff'  AND i.registrazione = 1 THEN 1
  ELSE NULL
  END
) AS staff_registrati,
COUNT(
 CASE
  WHEN i.ruolo = 'staff' AND i.registrazione = 9 THEN 1
  ELSE NULL
  END
) AS staff_assenti,
COUNT(
 CASE
  WHEN i.ruolo = 'coordinamento' THEN 1
  ELSE NULL
  END
) AS coordinamento,
COUNT(
 CASE
  WHEN i.ruolo = 'coordinamento'  AND i.registrazione = 1 THEN 1
  ELSE NULL
  END
) AS coordinamento_registrati,
COUNT(
 CASE
  WHEN i.ruolo = 'coordinamento' AND i.registrazione = 9 THEN 1
  ELSE NULL
  END
) AS coordinamento_assenti
FROM iscrizioni AS i
INNER JOIN preiscrizioni AS p
ON i.id_comitato = p.id_comitato
INNER JOIN comitati AS c
ON i.id_comitato = c.id
GROUP BY i.id_comitato
ORDER BY FIND_IN_SET (p.accoglienza, '1,0,2'), c.nome_comitato";
$result = mysql_query($query, $db);
while($row = mysql_fetch_array( $result )) 
{
	$re = "/(\\s*comitato locale di\\s*|\\s*comitato provinciale di\\s*|\\s*comitato regionale\\s*|\\s*delegazione di\\s*|\\s*a valenza regionale\\s*|\\s*comitato\\s*)/i"; 
	$subst = ""; 	 
	$comitato_trunc = preg_replace($re, $subst, $row[comitato]);
	$tot_comitato_iscritti = $row[squadra] + $row[osservatori] + $row[simulatori] + $row[truccatori] + $row[giudici] + $row[ospiti] + $row[staff] + $row[coordinamento];
	$tot_comitato_registrati = $row[squadra_registrati] + $row[osservatori_registrati] + $row[simulatori_registrati] + $row[truccatori_registrati] + $row[giudici_registrati] + $row[ospiti_registrati] + $row[staff_registrati] + $row[coordinamento_registrati];
	$tot_comitato_assenti = $row[squadra_assenti] + $row[osservatori_assenti] + $row[simulatori_assenti] + $row[truccatori_assenti] + $row[giudici_assenti] + $row[ospiti_assenti] + $row[staff_assenti] + $row[coordinamento_assenti];
	
	?>
	<tr>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' <?php if(($tot_comitato_iscritti - $tot_comitato_registrati - $tot_comitato_assenti) == 0) {echo "color:#0000FF;";} elseif(($tot_comitato_iscritti - $tot_comitato_registrati - $tot_comitato_assenti) != 0 && ($row[accoglienza] == 1 OR $row[accoglienza] == 2)) {echo "color:red;";} ?> height:18px; overflow-y:hidden; overflow-x:hidden;'><?= $comitato_trunc ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[squadra] != 0){ echo $row[squadra]; $tot_squadra = $tot_squadra + $row[squadra]; $tot_iscritti = $tot_iscritti + $row[squadra]; $tot_squadre_iscritte++;} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[squadra_registrati] != 0){ echo $row[squadra_registrati]; $tot_registrati = $tot_registrati + $row[squadra_registrati];} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[squadra_assenti] != 0){ echo $row[squadra_assenti]; $tot_assenti = $tot_assenti + $row[squadra_assenti];} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[osservatori] != 0){ echo $row[osservatori]; $tot_osservatori = $tot_osservatori + $row[osservatori]; $tot_iscritti = $tot_iscritti + $row[osservatori];} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[osservatori_registrati] != 0){ echo $row[osservatori_registrati]; $tot_registrati = $tot_registrati + $row[osservatori_registrati];} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[osservatori_assenti] != 0){ echo $row[osservatori_assenti]; $tot_assenti = $tot_assenti + $row[osservatori_assenti];} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[simulatori] != 0){ echo $row[simulatori]; $tot_simulatori = $tot_simulatori + $row[simulatori]; $tot_iscritti = $tot_iscritti + $row[simulatori];} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[simulatori_registrati] != 0){ echo $row[simulatori_registrati]; $tot_registrati = $tot_registrati + $row[simulatori_registrati];} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[simulatori_assenti] != 0){ echo $row[simulatori_assenti]; $tot_assenti = $tot_assenti + $row[simulatori_assenti];} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[giudici] != 0){ echo $row[giudici]; $tot_giudici = $tot_giudici + $row[giudici]; $tot_iscritti = $tot_iscritti + $row[giudici];} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[giudici_registrati] != 0){ echo $row[giudici_registrati]; $tot_registrati = $tot_registrati + $row[giudici_registrati];} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[giudici_assenti] != 0){ echo $row[giudici_assenti]; $tot_assenti = $tot_assenti + $row[giudici_assenti];} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[ospiti] != 0){ echo $row[ospiti]; $tot_ospiti = $tot_ospiti + $row[ospiti]; $tot_iscritti = $tot_iscritti + $row[ospiti];} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[ospiti_registrati] != 0){ echo $row[ospiti_registrati]; $tot_registrati = $tot_registrati + $row[ospiti_registrati];} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[ospiti_assenti] != 0){ echo $row[ospiti_assenti]; $tot_assenti = $tot_assenti + $row[ospiti_assenti];} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[staff] != 0){ echo $row[staff]; $tot_staff = $tot_staff + $row[staff]; $tot_iscritti = $tot_iscritti + $row[staff];} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[staff_registrati] != 0){ echo $row[staff_registrati]; $tot_registrati = $tot_registrati + $row[staff_registrati];} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[staff_assenti] != 0){ echo $row[staff_assenti]; $tot_assenti = $tot_assenti + $row[staff_assenti];} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[coordinamento] != 0){ echo $row[coordinamento]; $tot_coordinamento = $tot_coordinamento + $row[coordinamento]; $tot_iscritti = $tot_iscritti + $row[coordinamento];} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[coordinamento_registrati] != 0){ echo $row[coordinamento_registrati]; $tot_registrati = $tot_registrati + $row[coordinamento_registrati];} ?></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><?php if($row[coordinamento_assenti] != 0){ echo $row[coordinamento_assenti]; $tot_assenti = $tot_assenti + $row[coordinamento_assenti];} ?></td>
	<?php
}
?>

<tr>
<td colspan='2' align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b>TOTALE (<?= $tot_squadre_iscritte ?> squadre)</b></td>
<td colspan='2' align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_iscritti." tutti" ?></b></td>
<td colspan='2' align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_registrati." registrati" ?></b></td>
<td colspan='2' align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_assenti." assenti" ?></b></td>
<td colspan='2' align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_squadra." par" ?></b></td>
<td colspan='2' align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_osservatori." oss" ?></b></td>
<td colspan='2' align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_simulatori." sim" ?></b></td>
<td colspan='2' align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_giudici." giu" ?></b></td>
<td colspan='2' align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_ospiti." osp" ?></b></td>
<td colspan='2' align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_staff." sta" ?></b></td>
<td colspan='2' align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_coordinamento." coo" ?></b></td>

<?php
$query_non_registrati = "SELECT id FROM iscrizioni WHERE registrazione = 0";
$result_non_registrati = mysql_query($query_non_registrati, $db);
$numero_volontari_non_registrati = mysql_num_rows($result_non_registrati);


mysql_close($db); 
?>
</table>


</div>
</div>

</body>
</html>