<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice') && !($check['team'] == 'organigramma') && !($check['team'] == 'nazionale'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato all'organizzazione od al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>

<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Riepilogo materiale</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Parent -->
	<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>

<script type="text/javascript" src="tabber.js"></script>
<link rel="stylesheet" href="example.css" TYPE="text/css" MEDIA="screen">
<link rel="stylesheet" href="example-print.css" TYPE="text/css" MEDIA="print">

<script type="text/javascript">

/* Optional: Temporarily hide the "tabber" class so it does not "flash"
   on the page as plain HTML. After tabber runs, the class is changed
   to "tabberlive" and it will appear. */

document.write('<style type="text/css">.tabber{display:none;}<\/style>');
</script>

<script type="text/javascript" src="scripts/wysiwyg.js"></script>
<script type="text/javascript" src="scripts/wysiwyg-settings.js"></script> 
<script type="text/javascript">
WYSIWYG.attach('textarea2', small); // full featured setup 
</script> 

<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Riepilogo materiale</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">
  
<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<h2>Inserisci materiale</h2>
<p id="imPathTitle">Service</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?
echo "<font color=#2B3856 size='2' face='Calibri'>";


$descrizione		=	$_REQUEST['descrizione'];
$costo_preventivo	=	$_REQUEST['costo_preventivo'];
$inizio_esigenza 	=	$_REQUEST['inizio_esigenza'];
$fine_esigenza 		=	$_REQUEST['fine_esigenza'];
$quantita			=	$_REQUEST['quantita'];



include("config.inc.php");

if ($descrizione != '')
{
					$db = mysql_connect($db_host, $db_user, $db_password);
  					if ($db == FALSE)
    				die ("Errore nella connessione. Verificare i parametri nel file config.inc.php");

  					mysql_select_db($db_name, $db)
    				or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");
					
					$query = "INSERT INTO shopping (descrizione, costo_preventivo, quantita, inizio_esigenza, fine_esigenza, richiedente) VALUES ('$descrizione', '$costo_preventivo', '$quantita', '$inizio_esigenza', '$fine_esigenza', '$check[uname]')";

					if (mysql_query($query, $db))
					{
						echo "Il materiale � stato correttamente inserito.<br><br>";
						
						
						include('PHPMailer/class.phpmailer.php');
						//require_once('PHPMailer/class.smtp.php');  
	
						$mittente = "cp.torino.giovani@piemonte.cri.it";
						$nomemittente = "Back Office Meeting 2015";
						$destinatario = "paolo.ditoma@libero.it";
						$ServerSMTP = "mailbox.cri.it"; //server SMTP
						$oggetto = "[Test] Nuova richiesta materiale";
						$corpo_messaggio = "Buongiorno Pilly,\n� stata inserita una nuova richiesta materiale da $check[uname].\nConsulta il portale cliccando sul seguente link \n\n   http://www.itempa.com/meeting2015/riepilogo_materiale.php\n\nQualora la richiesta non fosse di tua competenza, ti preghiamo di voler ignorare la presente mail.\n\nCordiali Saluti,\nBack Office";

						$msg = new PHPMailer;
						$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
						//$msg->IsHTML(true);
						$msg->SMTPAuth = true; // Autenticazione SMTP
						$msg->SMTPKeepAlive = "true";
						$msg->Host = $ServerSMTP;
						$msg->Username = "cp.torino.giovani@piemonte.cri.it"; // Nome utente SMTP autenticato
						$msg->Password = "Gioventu2014*3"; // Password account email con SMTP autenticato

						$msg->From = $mittente;
						$msg->FromName = $nomemittente;
						$msg->AddAddress($destinatario); 
						$msg->AddCC("backoffice.meeting@gmail.com");
//						$msg->AddBCC("backoffice.meeting@gmail.com");
						$msg->Subject = $oggetto; 
						$msg->Body = $corpo_messaggio;


						if(!$msg->Send()) {
						echo "Si � verificato un errore nell'invio della mail per la conferma da parte del tuo presidente:".$msg->ErrorInfo;
						} else {
						echo "Abbiamo inviato una mail al gruppo della logistica per opportuna conoscenza.";
						}
					}
  					else
    				{
						echo "Errore durante l'inserimento, rivolgersi all'amministratore.<br><br>";
					}
}
else
{
	echo "Non hai inserito la descrizione!<br><br>";
}

?>

<br /><br />

<form method="post" action="inserisci_materiale.php">
<input type="submit" value="Nuova richiesta" />
</form>


</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
