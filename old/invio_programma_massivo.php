<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato all'organizzazione od al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Invio massivo programma del Meeting</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Invio singolo notifica per conferma mail</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Invio programma massivo</h2>
<p id="imPathTitle">Meeting</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include ("config.inc.php");
include('PHPMailer/class.phpmailer.php');
echo "<font color=#2B3856 size='2' face='Tahoma'>";
$fp = fopen ("log_iscrizioni.txt",a);

$operatore=$check['uname'];


include ("apri_db.php");

//Cerco le info del volontario tramite id
$query_id = "SELECT c.nome_comitato AS nome_comitato,
					i.nome AS nome,
					i.cognome AS cognome,
					i.ruolo AS ruolo,
					i.id AS id,
					i.telefono AS telefono,
					i.mail AS mail,
					i.token AS token,
					i.mail_reminder AS mail_reminder
					FROM iscrizioni AS i
					INNER JOIN comitati AS c
					ON i.id_comitato = c.id
					WHERE i.registrazione != 9
					AND i.inviata_mail_programma = 0";
$result_id = mysql_query($query_id, $db);
while($row_id = mysql_fetch_array( $result_id )) 
{
	//Invio la mail di conferma all'iscritto
	$mittente = "meeting.2015@cri.it";
	$nomemittente = "Back Office Meeting 2015";
	$destinatario = "$row_id[mail]";
//	$destinatario = "paolo.ditoma@libero.it";
	$ServerSMTP = "mailbox.cri.it"; //server SMTP
	$oggetto = "Programma Meeting 2015";
	$corpo_messaggio = "Ciao $row_id[nome],\nabbiamo pubblicato il programma dettagliato del Meeting, puoi consultarlo sul portale dedicato al link seguente\n\n  http://www.itempa.com/meeting2015/programma_meeting.php\n\nTi raccomandiamo la massima puntualità. Per quanto riguarda la divisa, è sufficiente indossare la polo.\n\nPer qualsiasi dubbio, angoscia e/o perplessità, non esitare a contattarci.\n\n\nCordiali Saluti,\nBack Office\nMeeting 2015\n\n(inviata automaticamente dal Portale Web)";
	
	$msg = new PHPMailer;
	$msg->CharSet = "UTF-8";
	$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
	//$msg->IsHTML(true);
	$msg->SMTPAuth = true; // Autenticazione SMTP
	$msg->SMTPKeepAlive = "true";
	$msg->Host = $ServerSMTP;
	$msg->Username = "meeting.2015@cri.it"; // Nome utente SMTP autenticato
	$msg->Password = "Spaghett!44"; // Password account email con SMTP autenticato
	
	$msg->From = $mittente;
	$msg->FromName = $nomemittente;
	$msg->AddAddress($destinatario); 
//	$msg->AddBCC("paolo.ditoma@libero.it");
	$msg->Subject = $oggetto; 
	$msg->Body = $corpo_messaggio;
	
	
	if(!$msg->Send())
	{
		echo "Si è verificato un errore nell'invio del programma per l'iscritto $row_id[nome] $row_id[cognome] all'indirizzo $row_id[mail] :".$msg->ErrorInfo;
		echo "<br><br>";
		fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Invio Programma - Invio mail programma per l'iscritto $row_id[nome] $row_id[cognome] all'indrizzo $row_id[mail] con TOKEN $row_id[token] : ".$msg->ErrorInfo."\r\n");
	}
	else
	{
		echo "Abbiamo inviato la mail per il programma all'iscritto $row_id[nome] $row_id[cognome] all'indirizzo $row_id[mail] con TOKEN $row_id[token].<br><br>";
		fwrite($fp,date('d/m/Y H:i:s').' - '."Invio programma massivo - Invio programma all'iscritto $row_id[nome] $row_id[cognome] con TOKEN $row_id[token] correttamente effettuato all'indirizzo $row_id[mail]."."\r\n");

		//Faccio l'update del mail_reminder
		$mail_reminder_new = $row_id[mail_reminder] + 1;

		$query = "UPDATE iscrizioni SET mail_reminder = $mail_reminder_new, inviata_mail_programma = 1 WHERE id = $row_id[id]";
		$result_update=mysql_query($query, $db);
		
		//Se l'update è andata bene, scrivo nel log
		if ($result_update)
		{
			fwrite($fp,date('d/m/Y H:i:s').' - '."Invio programma massivo - Update del reminder a $mail_reminder_new per $row_id[nome] $row_id[cognome] correttamente inserito nel DB."."\r\n");
			echo "Update del reminder a $mail_reminder_new per $row_id[nome] $row_id[cognome] correttamente inserito nel DB.<br><br>";

		}
		else
		{
			echo "Update del reminder a $mail_reminder_new per $row_id[nome] $row_id[cognome] in errore:".$msg->ErrorInfo;
			fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Sollecito conferma mail - Update del reminder a $mail_reminder_new per $row_id[nome] $row_id[cognome]: ".$msg->ErrorInfo."\r\n");
		}
	}
}



mysql_close($db);
fclose($fp);

?>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
