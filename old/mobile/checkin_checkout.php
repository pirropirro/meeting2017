<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'tutor'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato ai Tutor od al Back Office.</b></font>";
	exit;	// Stop script execution
}

$ora_attuale = strftime('%H%M', time());
$tutor = $check['level'];
$nextAction = '';
$message = '';
$nextCommand = '';

if (isset($_POST['command1']))
{
	$azione = $_POST['command1'];
	$squadra_check = $_POST['squadra1'];
	$id = $_POST['id1'];
	$id_comitato = $_POST['id_comitato1'];
}
elseif (isset($_POST['command2']))
	{
		$azione = $_POST['command2'];
		$squadra_check = $_POST['squadra2'];
		$id = $_POST['id2'];		
		$id_comitato = $_POST['id_comitato2'];
	}	
	elseif (isset($_POST['command3']))
		{
			$azione = $_POST['command3'];
			$squadra_check = $_POST['squadra3'];
			$id = $_POST['id3'];
			$id_comitato = $_POST['id_comitato3'];
		}
		elseif (isset($_POST['command4']))
			{
				$azione = $_POST['command4'];
				$squadra_check = $_POST['squadra4'];
				$id = $_POST['id4'];
				$id_comitato = $_POST['id_comitato4'];
        	}
			elseif (isset($_POST['command5']))
				{
					$azione = $_POST['command5'];
					$squadra_check = $_POST['squadra5'];
					$id = $_POST['id5'];
					$id_comitato = $_POST['id_comitato5'];
				}
			
include("config.inc.php");
include ("apri_db.php");
		
if ($azione == "check-in"){
	// UPDATE TABLE SET ORA_CHECKIN WHERE ...
	$query = "UPDATE timesheet SET ora_checkin = '$ora_attuale' WHERE id = '$id'";
	mysql_query($query, $db);
	$message = "<b>Check-in</b> eseguito per <b>$squadra_check</b> alle ore <b>".substr($ora_attuale, 0, 2).":".substr($ora_attuale, 2, 2)."</b>";
} 
if ($azione == "check-out"){
	// UPDATE TABLE SET ORA_CHECKOUT WHERE ...
	$query = "UPDATE timesheet SET ora_checkout = '$ora_attuale', estrarre = 'finito' WHERE id = '$id'";
	mysql_query($query, $db);
//	$message = "Check-out eseguito per $squadra_check alle ore $ora_attuale";
	$query_id_prossimo = "UPDATE timesheet SET estrarre = 'prossimo' WHERE id IN (SELECT id from (SELECT id, MIN(inizio) FROM timesheet WHERE tutor = $tutor AND id_comitato = $id_comitato AND estrarre IS NULL) AS id)";
	mysql_query($query_id_prossimo, $db);
	$message = "<b>Check-out</b> eseguito per <b>$squadra_check</b> alle ore <b>".substr($ora_attuale, 0, 2).":".substr($ora_attuale, 2, 2)."</b>";
}

?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Check-In Check-Out</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="css/themes/default/custom.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.php" title="Meeting 2016 - Homepage"><img src="logo_meeting.png" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

        <div data-html="true">
			
			<?= $message ?>
            
			<form action="checkin_checkout.php" method="POST">
            	<?php
				$query_timesheet = "SELECT 	c.id AS id_comitato,	
								c.nome_comitato AS nome_comitato,
								c.tipo_meeting AS tipo_meeting,
								t.id AS id_timesheet,
								t.inizio AS ora_inizio,
								t.fine AS ora_fine,
								t.giudice AS giudice,
								t.tutor AS tutor,
								t.prova AS prova,
								t.aula AS aula,
								t.ora_checkin AS ora_checkin
								FROM timesheet AS t
								INNER JOIN comitati AS c ON t.id_comitato = c.id
								WHERE t.tutor = $tutor
								AND t.estrarre = 'prossimo'";
								
				$result_timesheet = mysql_query($query_timesheet, $db);
				$i = 1;
				while($row_timesheet = mysql_fetch_array( $result_timesheet ))
				{
					if ($row_timesheet['ora_checkin'] == '')
					{
						$nextCommand = 'check-in';
						$avviso_tempo = "entrare alle ".substr($row_timesheet[ora_inizio], 0, 2).":".substr($row_timesheet[ora_inizio], 2, 2)." in $row_timesheet[aula]";
						
					}
					else
					{
						$nextCommand = 'check-out';
						$avviso_tempo = "uscire entro le ".substr($row_timesheet[ora_fine], 0, 2).":".substr($row_timesheet[ora_fine], 2, 2)." da $row_timesheet[aula]";
					}	
					?>
                    <input id="squadra<?= $i ?>" type="hidden" name="squadra<?= $i ?>" value="<?= $row_timesheet['nome_comitato'] ?>">
                    <input id="id_comitato<?= $i ?>" type="hidden" name="id_comitato<?= $i ?>" value="<?= $row_timesheet['id_comitato'] ?>">
                    <input id="id<?= $i ?>" type="hidden" name="id<?= $i ?>" value="<?= $row_timesheet['id_timesheet'] ?>">
					<p><center><?= $row_timesheet[nome_comitato]." ".$avviso_tempo ?></center></p>
					<input class="ui-shadow ui-btn ui-corner-all ui-btn-inline ui-btn-b ui-mini <?= $nextCommand ?>-bg" type="submit" name="command<?= $i ?>" value="<?= $nextCommand ?>">
                    <br \>
                 	<?php
					$i++;
				}
				?>
			</form>

        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>