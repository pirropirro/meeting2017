<?php
require('fpdf.php');
include ("config.inc.php");

$data = strftime('%d/%m/%Y', time());
$time = strftime('%H:%M', time());

define('DATA', $data);
define('ORA', $time);

//Leggo il parametro passato in chiaro
$id_comitato=$_POST['id_comitato'];
$passwd = $_POST['passwd'];
//echo $id_comitato;
//echo $passwd;

include ("config.inc.php");
include ("apri_db.php");
        
//Comitato di cui si inserisci la passwd
$query = "SELECT nome_comitato, passwd FROM comitati WHERE id = '$id_comitato'";
$result = mysql_query($query, $db);
$row = mysql_fetch_array( $result );
        
        
if ($row[passwd] == $passwd  && !($id_comitato == ''))
{


	//Apro il DB
	$fp = fopen ("log_iscrizioni.txt",a);
	include ("apri_db.php");
	
	define('EURO', chr(128));
	
	define('NOME_FIRMA_REFERENTE', "Mattia");
	define('COGNOME_FIRMA_REFERENTE', "Di Febbo");
	define('NOME_FIRMA_BACKOFFICE', "Paolo");
	define('COGNOME_FIRMA_BACKOFFICE', "di Toma");
	
	$query = "SELECT		 	c.nome_comitato AS nome_comitato,
								p.nome_delegato AS nome_volontario,
								p.cognome_delegato AS cognome_volontario
								FROM comitati AS c
								INNER JOIN preiscrizioni AS p
								ON c.id = p.id_comitato
								WHERE c.id = '$id_comitato'";
	$result =  mysql_query($query, $db);
	$row = mysql_fetch_array($result);
	
	
	class PDF extends FPDF
	{
	// Page header
	function Header()
	{
		// Logo
		$this->Image('../res/ricevuta_upsx.png',10,6,30);
		// Arial bold 15
		$this->SetFont('Arial','B',15);
		// Move to the right
		$this->Cell(75);
		// Title
		$this->Cell(40,10,'Back Office Meeting 2015',0,1,'C');
	
		// Secondo logo
		$this->Image('../res/ricevuta_updx.png',170,7,30);
	
		$this->Cell(75);
		$this->SetFont('Arial','',12);
		$this->Cell(40,10,'Ricevuta Iscrizione',0,0,'C');
		
		// Line break
		$this->Ln(18);
	}
	
	// Page footer
	function Footer()
	{
		// Position at 1.5 cm from bottom
		$this->SetY(-22);
		// Arial italic 8
		$this->SetFont('Arial','',10);
		// Page number
		// $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
		// Firma della ricevuta
		$this->Cell(50,0,'Referente Nazionale Meeting',0,0,'C');
		$this->Cell(50);
		$this->Cell(130,0,'Back Office Meeting 2015',0,1,'C');
		// Line break
		$this->Ln(5);
		$this->SetFont('Arial','I',9);
		$this->Cell(50,0,'('.NOME_FIRMA_REFERENTE.' '.COGNOME_FIRMA_REFERENTE.')',0,0,'C');
		$this->Cell(50);
		$this->Cell(130,0,'('.NOME_FIRMA_BACKOFFICE.' '.COGNOME_FIRMA_BACKOFFICE.')',0,0,'C');
	
		// Position at 1.5 cm from bottom
		$this->SetY(-9);
		// Arial italic 8
		$this->SetFont('Arial','',7);
		// Page number
		$this->Cell(0,10,'Stampa effettuata il '.DATA.' alle ore '.ORA,0,0,'C');
	}
	}
	
	
	// STAMPA UNA SOLA COPIA
	
	//Fare estrazione del totale a pagare
	$totale = 0;
	
	// Instanciation of inherited class
	$pdf = new PDF();
	$pdf->AliasNbPages();
	$pdf->AddPage();
	
	$pdf->SetFont('helvetica','',10);
	$pdf->Ln(11);
	
	//Scrivo la formula di apertura
	$pdf->Multicell(0,5,'Con la presente si certifica che, come risulta dal portale dedicato www.itempa.com/meeting2015, per il '.$row[nome_comitato].' risultano essere stati iscritti dal volontario '.$row[nome_volontario].' '.$row[cognome_volontario].' al Meeting 2015 dei Giovani della Croce Rossa Italiana, in programma l\'11 ottobre 2015 presso l\' Istituto Sociale di Torino (TO), i partecipanti di cui sotto.');
	$pdf->Ln(6);
	
	
	//estraggo i membri della squadra
	$query = "SELECT * FROM iscrizioni WHERE (ruolo = 'capitano' OR ruolo = 'partecipante') AND id_comitato = '$id_comitato' ";
	//echo "$query";
	$result = mysql_query($query, $db);
	$occorrenze = mysql_num_rows($result);
	if ( $occorrenze != 0)
	{
		//linea della descrizione
		$pdf->SetFont('helvetica','',10);
		$pdf->Write(4,'Come ');
		$pdf->SetFont('','B');
		$pdf->Write(4,'membri della squadra ');
		$pdf->SetFont('helvetica','',10);
		$pdf->Write(4,'iscritta (quota pro capite 6 '.EURO.'):');
		$pdf->Ln(5);
		
		while($row = mysql_fetch_array( $result )) 
		{
	//	$comitatotrunc = strtoupper(substr("$row[comitato]",3));
		$totale = $totale + 6;
		$pdf->SetFont('helvetica','',9);
		$pdf->Cell(0,4,'          '.$row[nome].' '.$row[cognome],0,1,'L');
	
		}
		$pdf->Ln(5);
	}
	//
	
	
	//estraggo gli osservatori
	$query = "SELECT * FROM iscrizioni WHERE ruolo = 'osservatore' AND id_comitato = '$id_comitato'";
	//echo "$query";
	$result = mysql_query($query, $db);
	$occorrenze = mysql_num_rows($result);
	if ( $occorrenze != 0)
	{
		//linea della descrizione
		$pdf->SetFont('helvetica','',10);
		$pdf->Write(4,'Come ');
		$pdf->SetFont('','B');
		$pdf->Write(4,'osservatori ');
		$pdf->SetFont('helvetica','',10);
		$pdf->Write(4,'(quota pro capite 15 '.EURO.'):');
		$pdf->Ln(5);
		
		while($row = mysql_fetch_array( $result )) 
		{
	//	$comitatotrunc = strtoupper(substr("$row[comitato]",3));
		$totale = $totale + 15;
		$pdf->SetFont('helvetica','',9);
		$pdf->Cell(0,4,'          '.$row[nome].' '.$row[cognome],0,1,'L');
	
		}
		$pdf->Ln(5);
	}
	//
	
	//estraggo i simulatori
	$query = "SELECT * FROM iscrizioni WHERE ruolo = 'simulatore' AND id_comitato = '$id_comitato'";
	//echo "$query";
	$result = mysql_query($query, $db);
	$occorrenze = mysql_num_rows($result);
	if ( $occorrenze != 0)
	{
		//linea della descrizione
		$pdf->SetFont('helvetica','',10);
		$pdf->Write(4,'Come ');
		$pdf->SetFont('','B');
		$pdf->Write(4,'simulatori ');
		$pdf->SetFont('helvetica','',10);
		$pdf->Write(4,'(quota pro capite 0 '.EURO.'):');
		$pdf->Ln(5);
		
		while($row = mysql_fetch_array( $result )) 
		{
		$totale = $totale + 0;
		$pdf->SetFont('helvetica','',9);
		$pdf->Cell(0,4,'          '.$row[nome].' '.$row[cognome],0,1,'L');
	
		}
		$pdf->Ln(5);
	}
	//
	
	//estraggo i truccatori
	$query = "SELECT * FROM iscrizioni WHERE ruolo = 'truccatore' AND id_comitato = '$id_comitato'";
	//echo "$query";
	$result = mysql_query($query, $db);
	$occorrenze = mysql_num_rows($result);
	if ( $occorrenze != 0)
	{
		//linea della descrizione
		$pdf->SetFont('helvetica','',10);
		$pdf->Write(4,'Come ');
		$pdf->SetFont('','B');
		$pdf->Write(4,'truccatori ');
		$pdf->SetFont('helvetica','',10);
		$pdf->Write(4,'(quota pro capite 0 '.EURO.'):');
		$pdf->Ln(5);
		
		while($row = mysql_fetch_array( $result )) 
		{
		$totale = $totale + 0;
		$pdf->SetFont('helvetica','',9);
		$pdf->Cell(0,4,'          '.$row[nome].' '.$row[cognome],0,1,'L');
	
		}
		$pdf->Ln(5);
	}
	//
	
	//estraggo i giudici
	$query = "SELECT * FROM iscrizioni WHERE ruolo = 'giudice' AND id_comitato = '$id_comitato'";
	//echo "$query";
	$result = mysql_query($query, $db);
	$occorrenze = mysql_num_rows($result);
	if ( $occorrenze != 0)
	{
		//linea della descrizione
		$pdf->SetFont('helvetica','',10);
		$pdf->Write(4,'Come ');
		$pdf->SetFont('','B');
		$pdf->Write(4,'giudici ');
		$pdf->SetFont('helvetica','',10);
		$pdf->Write(4,'(quota pro capite 0 '.EURO.'):');
		$pdf->Ln(5);
		
		while($row = mysql_fetch_array( $result )) 
		{
		$totale = $totale + 0;
		$pdf->SetFont('helvetica','',9);
		$pdf->Cell(0,4,'          '.$row[nome].' '.$row[cognome],0,1,'L');
	
		}
		$pdf->Ln(5);
	}
	//
	
	//estraggo i membri dello staff
	$query = "SELECT * FROM iscrizioni WHERE ruolo = 'staff' AND id_comitato = '$id_comitato'";
	//echo "$query";
	$result = mysql_query($query, $db);
	$occorrenze = mysql_num_rows($result);
	if ( $occorrenze != 0)
	{
		//linea della descrizione
		$pdf->SetFont('helvetica','',10);
		$pdf->Write(4,'Come ');
		$pdf->SetFont('','B');
		$pdf->Write(4,'membri dello staff ');
		$pdf->SetFont('helvetica','',10);
		$pdf->Write(4,'(quota pro capite 0 '.EURO.'):');
		$pdf->Ln(5);
		
		while($row = mysql_fetch_array( $result )) 
		{
		$totale = $totale + 0;
		$pdf->SetFont('helvetica','',9);
		$pdf->Cell(0,4,'          '.$row[nome].' '.$row[cognome],0,1,'L');
	
		}
		$pdf->Ln(5);
	}
	//
	
	//estraggo gli ospiti
	$query = "SELECT * FROM iscrizioni WHERE ruolo = 'ospite' AND id_comitato = '$id_comitato'";
	//echo "$query";
	$result = mysql_query($query, $db);
	$occorrenze = mysql_num_rows($result);
	if ( $occorrenze != 0)
	{
		//linea della descrizione
		$pdf->SetFont('helvetica','',10);
		$pdf->Write(4,'Come ');
		$pdf->SetFont('','B');
		$pdf->Write(4,'ospiti ');
		$pdf->SetFont('helvetica','',10);
		$pdf->Write(4,'(quota pro capite 0 '.EURO.'):');
		$pdf->Ln(5);
		
		while($row = mysql_fetch_array( $result )) 
		{
		$totale = $totale + 0;
		$pdf->SetFont('helvetica','',9);
		$pdf->Cell(0,4,'          '.$row[nome].' '.$row[cognome],0,1,'L');
	
		}
		$pdf->Ln(5);
	}
	//
	
	//estraggo i membri del coordinamento
	$query = "SELECT * FROM iscrizioni WHERE ruolo = 'coordinamento' AND id_comitato = '$id_comitato'";
	//echo "$query";
	$result = mysql_query($query, $db);
	$occorrenze = mysql_num_rows($result);
	if ( $occorrenze != 0)
	{
		//linea della descrizione
		$pdf->SetFont('helvetica','',10);
		$pdf->Write(4,'Come ');
		$pdf->SetFont('','B');
		$pdf->Write(4,'membri del coordinamento ');
		$pdf->SetFont('helvetica','',10);
		$pdf->Write(4,'(quota pro capite 0'.EURO.'):');
		$pdf->Ln(5);
		
		while($row = mysql_fetch_array( $result )) 
		{
		$totale = $totale + 0;
		$pdf->SetFont('helvetica','',9);
		$pdf->Cell(0,4,'          '.$row[nome].' '.$row[cognome],0,1,'L');
	
		}
		$pdf->Ln(5);
	}
	//
	
	
	$pdf->Ln(5);
	$pdf->SetFont('helvetica','B',10);
	$pdf->Cell(0,5,'Totale da pagare per l\'iscrizione '.$totale." ".EURO,0,1);
	
	
	$pdf->Ln(8);
	$pdf->SetFont('helvetica','',10);
	$pdf->Write(5,'Da pagare tramite bonifico sul c/c intestato a ',0,0);
	$pdf->SetFont('helvetica','B',10);
	$pdf->Write(5,'CROCE ROSSA ITALIANA COMITATO PROVINCIALE DI TORINO',0,0);
	$pdf->SetFont('helvetica','',10);
	$pdf->Write(5,' con IBAN ',0,0);
	$pdf->SetFont('helvetica','B',10);
	$pdf->Write(5,'IT77X0760101000001017823038',0,0);
	$pdf->SetFont('helvetica','',10);
	$pdf->Write(5,' indicando come causale ',0,0);
	$pdf->SetFont('helvetica','B',10);
	$pdf->Write(5,'Iscrizione Meeting Griovani CRI 2015 - Codice '.$id_comitato.' (nome comitato)',0,0);
	$pdf->SetFont('helvetica','',10);
	$pdf->Write(5,' entro e non oltre il 7.cm.',0,1);
	
	
	$pdf->Output();

}
else
{
	echo "Password errata<br><br><br>";
}
mysql_close($db);
?>