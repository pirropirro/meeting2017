<?
	include_once ("auth.php");
	include_once ("authconfig.php");
	include_once ("check.php");
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2015 - Cambio Password</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2015 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2015 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2015</h1>


<?php
	// Get global variable values if there are any
	$USERNAME = $_COOKIE['USERNAME'];
	$PASSWORD = $_COOKIE['PASSWORD'];
	$oldpasswd = $_POST['oldpasswd'];
	$newpasswd = $_POST['newpasswd'];
	$confirmpasswd = $_POST['confirmpasswd'];

	$user = new auth();
	$connection = mysql_connect($dbhost, $dbusername, $dbpass);
	
	// REVISED CODE
	$SelectedDB = mysql_select_db($dbname);
	$userdata = mysql_query("SELECT * FROM authuser WHERE uname='$USERNAME' and passwd='$PASSWORD'");
	
	// Check if Old password is the correct
	if ($oldpasswd != $PASSWORD)
	{
		?>
   		<p><strong>Cambio password non eseguito</strong></p>

        <div data-html="true">

		<p>La vecchia password non è corretta.</p>
		<p>Clicca <a href="<? echo $changepassword; ?>">qui</a> per ricambiarla.</p>
		<?php
	}
		// Check if New password if blank
	elseif (trim($newpasswd) == "")
		{
			?>
   			<p><strong>Cambio password non eseguito</strong></p>

        	<div data-html="true">

			<p>La nuova password non può essere vuota.</p>
			<p>Clicca <a href="<? echo $changepassword; ?>">qui</a> per ricambiarla.</p>
			<?php
		}
			// Check if New password is confirmed
		elseif ($newpasswd != $confirmpasswd)
			{
				?>
   				<p><strong>Cambio password non eseguito</strong></p>

        		<div data-html="true">

				<p>La nuova password non è stata correttamente confermata.</p>
				<p>Clicca <a href="<? echo $changepassword; ?>">qui</a> per ricambiarla.</p>
				<?php
			}
			// If everything is ok, use auth class to modify the record
			else
			{
				$update = $user->modify_user($USERNAME, $newpasswd, $check["team"], $check["level"], $check["status"]);
				if ($update)
				{
					// Destroy Sessions
					setcookie ("USERNAME", "");
					setcookie ("PASSWORD", "");	
					?>
   					<p><strong>Cambio password eseguito</strong></p>

        			<div data-html="true">

					<p>Devi effettuare di nuovo l'autenticazione in modo che la tua sessione recepisca la nuova password.</p>
					<p>Clicca <a href="<? echo $login; ?>">qui</a> per autenticarti.</p>
					<?php
				}
			}
		
?>

        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>