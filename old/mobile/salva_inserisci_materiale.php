<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a giudice votante o coordinatore o tecnico
if (!($check['team'] == 'organigramma') && !($check['team'] == 'backoffice') && !($check['team'] == 'nazionale'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato all'organizzazione od al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2015 - Inserisci richiesta materiale</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2015 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2015 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2015</h1>

		<p><strong>Inserisci richiesta materiale</strong></p>

        <div data-html="true">

		<?
        $descrizione		=	$_REQUEST['descrizione'];
        $costo_preventivo	=	$_REQUEST['costo_preventivo'];
        $inizio_esigenza 	=	$_REQUEST['inizio_esigenza'];
        $fine_esigenza 		=	$_REQUEST['fine_esigenza'];
        $quantita			=	$_REQUEST['quantita'];
        
        
        
        include("config.inc.php");
        
        if ($descrizione != '')
        {
                            $db = mysql_connect($db_host, $db_user, $db_password);
                            if ($db == FALSE)
                            die ("Errore nella connessione. Verificare i parametri nel file config.inc.php");
        
                            mysql_select_db($db_name, $db)
                            or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");
                            
                            $query = "INSERT INTO shopping (descrizione, costo_preventivo, quantita, inizio_esigenza, fine_esigenza, richiedente) VALUES ('$descrizione', '$costo_preventivo', '$quantita', '$inizio_esigenza', '$fine_esigenza', '$check[uname]')";
        
                            if (mysql_query($query, $db))
                            {
                                echo "Il materiale è stato correttamente inserito.<br><br>";
                                
                                
                                include('../PHPMailer/class.phpmailer.php');
                                //require_once('PHPMailer/class.smtp.php');  
            
                                $mittente = "cp.torino.giovani@piemonte.cri.it";
                                $nomemittente = "Back Office Meeting 2015";
                                $destinatario = "paolo.ditoma@libero.it";
                                $ServerSMTP = "mailbox.cri.it"; //server SMTP
                                $oggetto = "[Test] Nuova richiesta materiale";
                                $corpo_messaggio = "Buongiorno Pilly,\nè stata inserita una nuova richiesta materiale da $check[uname].\nConsulta il portale cliccando sul seguente link \n\n   http://www.itempa.com/meeting2015/riepilogo_materiale.php\n\nQualora la richiesta non fosse di tua competenza, ti preghiamo di voler ignorare la presente mail.\n\nCordiali Saluti,\nBack Office";
        
                                $msg = new PHPMailer;
                                $msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
                                //$msg->IsHTML(true);
                                $msg->SMTPAuth = true; // Autenticazione SMTP
                                $msg->SMTPKeepAlive = "true";
                                $msg->Host = $ServerSMTP;
                                $msg->Username = "cp.torino.giovani@piemonte.cri.it"; // Nome utente SMTP autenticato
                                $msg->Password = "Gioventu2014*3"; // Password account email con SMTP autenticato
        
                                $msg->From = $mittente;
                                $msg->FromName = $nomemittente;
                                $msg->AddAddress($destinatario); 
                                $msg->AddCC("$check[mail]");
                                $msg->AddBCC("backoffice.meeting@gmail.com");
                                $msg->Subject = $oggetto; 
                                $msg->Body = $corpo_messaggio;
        
        
                                if(!$msg->Send()) {
                                echo "Si è verificato un errore nell'invio della mail per la conferma da parte del tuo presidente:".$msg->ErrorInfo;
                                } else {
                                echo "Abbiamo inviato una mail al gruppo della logistica per opportuna conoscenza.";
                                }
                            }
                            else
                            {
                                echo "Errore durante l'inserimento, rivolgersi all'amministratore.<br><br>";
                            }
        }
        else
        {
            echo "Non hai inserito la descrizione!<br><br>";
        }
        
        ?>
        
        <br /><br />
        
        <form method="post" action="inserisci_materiale.php">
        <input type="submit" value="Nuova richiesta" />
        </form>

        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>