<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a giudice votante o coordinatore o tecnico
if (!($check['team'] == 'giudice') && !($check['team'] == 'coordinatore') && !($check['team'] == 'tecnico'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per inserire il punteggio, è un compito riservato a giudici, coordinatori ed al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2014 - Salva Punteggio</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2014 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2014 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2014</h1>

		<p><strong>Salva Punteggio</strong></p>

        <div data-html="true">

			<?
            include("config.inc.php");
            
            $area_punteggio=$_POST['area_punteggio'];
            $desinenza_punteggio=$_POST['desinenza_punteggio'];
            
            //Se inserisce il Back Office cambio l'area punteggio in quella selezionata dal form
            if ($check['team'] == 'tecnico')
            {
                $area_punteggio=$_REQUEST['area_bo'];
            }
            
            $comitato=$_REQUEST['comitato'];
            $intero=$_REQUEST['intero'];
            $decimale=$_REQUEST['decimale'];
            
            $comitatotrunc = strtoupper(substr("$comitato",3));
            $gruppo = $check['team'];
            $punteggio = (float)"$intero.$decimale";
            //echo "Il punteggio che registro è $punteggio<br>";
            
            $campo_autorizzato ="s" . "$area_punteggio" . "_" . "$desinenza_punteggio";
            
            //Apro il DB
            $db = mysql_connect($db_host, $db_user, $db_password);
            if ($db == FALSE)
            die ("Errore nella connessione. Verificare i parametri nel file config.inc.php");
            mysql_select_db($db_name, $db)
            or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");
            
            //Faccio l'update senza alcun controllo (non c'è modo di avere campi non valorizzati
            $query = "UPDATE preiscrizioni SET $campo_autorizzato = '$punteggio' WHERE comitato = '$comitato'";
            if (mysql_query($query, $db))
            {
                //Espongo il messaggio a video
                if ($check['team'] == 'tecnico')
                {
                    echo "Hai inserito per la squada di $comitatotrunc il punteggio $punteggio come membro del Back Office. <br><br>";
                }
                else
                {
                    echo "Hai inserito per la squada di $comitatotrunc il punteggio $punteggio come $gruppo della prova area $area_punteggio. <br><br>";
                }
            }
            else
            {
                echo "Si è verificato un errore durante l'inserimento del punteggio, rivolgersi immediatamente al Back Office.<br><br>";
            }	
            
            mysql_close($db);
            
            ?>
            
            <br /><br />
            
            <form method="post" action="inserisci_punteggio.php">
            <input type="submit" value="Inserisci un altro punteggio" />
            </form>

        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
<li data-filtertext="meeting homepage" data-icon="home"><a href="index.html">Home</a></li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Autenticazione<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="accedi login autentica"><a href="login.php" data-ajax="false">Accedi</a></li>
			<li data-filtertext="password passwd"><a href="chgpwd.php" data-ajax="false">Cambia Password</a></li>
			<li data-filtertext="esci logout"><a href="logout.php" data-ajax="false">Esci</a></li>
		</ul>
	</div>
</li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Meeting<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="inserisci punteggio giudice coordinatore voto"><a href="inserisci_punteggio.php" data-ajax="false">Inserisci Punteggio</a></li>
			<li data-filtertext="quadro parziale punteggi punteggio risultati"><a href="quadro_punteggi_partecipanti.php" data-ajax="false">Quadro Punteggi</a></li>
			<li data-filtertext="classifica parziale punteggio temporaneo risultati"><a href="classifica_parziale.php" data-ajax="false">Classifica Parziale</a></li>
			<li data-filtertext="classifica finale punteggio temporaneo risultati"><a href="classifica_finale.php" data-ajax="false">Classifica Finale</a></li>
		</ul>
	</div>
</li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Feedback<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="feedback questionario valutazione"><a href="compila_questionario.php" data-ajax="false">Compila Questionario</a></li>
		</ul>
	</div>
</li>
		     </ul>
		</div><!-- /panel -->


	<div data-role="footer" data-position="fixed" data-tap-toggle="false" class="jqm-footer">
		<p>Vai alla <a href="../../index_desktop.html" data-ajax="false">versione desktop</a></p>
		<p>Scrivi a <a href="mailto:paolo.ditoma@libero.it?subject=Segnalazione per Meeting2014 da mobile" title="">paolo.ditoma@libero.it</a></p>
	</div><!-- /footer -->
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
<li data-filtertext="meeting homepage" data-icon="home"><a href="index.html">Home</a></li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Autenticazione<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="accedi login autentica"><a href="login.php" data-ajax="false">Accedi</a></li>
			<li data-filtertext="password passwd"><a href="chgpwd.php" data-ajax="false">Cambia Password</a></li>
			<li data-filtertext="esci logout"><a href="logout.php" data-ajax="false">Esci</a></li>
		</ul>
	</div>
</li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Meeting<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="inserisci punteggio giudice coordinatore voto"><a href="inserisci_punteggio.php" data-ajax="false">Inserisci Punteggio</a></li>
			<li data-filtertext="quadro parziale punteggi punteggio risultati"><a href="quadro_punteggi_partecipanti.php" data-ajax="false">Quadro Punteggi</a></li>
			<li data-filtertext="classifica parziale punteggio temporaneo risultati"><a href="classifica_parziale.php" data-ajax="false">Classifica Parziale</a></li>
			<li data-filtertext="classifica finale punteggio temporaneo risultati"><a href="classifica_finale.php" data-ajax="false">Classifica Finale</a></li>
		</ul>
	</div>
</li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Feedback<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="feedback questionario valutazione"><a href="compila_questionario.php" data-ajax="false">Compila Questionario</a></li>
		</ul>
	</div>
</li>



			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>