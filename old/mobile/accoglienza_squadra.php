<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice') && !($check['team'] == 'accoglienza'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Accoglienza Squadra</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.php" title="Meeting 2016 - Homepage"><img src="logo_meeting.png" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		<p><strong>Accoglienza Squadra</strong></p>

        <div data-html="true">

            <form name="form1" enctype="multipart/form-data" method="post" action="salva_accoglienza_squadra.php">

		        <fieldset data-role="controlgroup">
        		<legend>Squadre attese</legend>
					<?php
                    include ("config.inc.php");
                    include ("apri_db.php");
                    
                    //Comitato di cui si inserisci la passwd
                    $query = "SELECT 	c.nome_comitato AS nome_comitato,
										c.id AS id_comitato
										FROM iscrizioni AS i
										INNER JOIN comitati AS c
										ON c.id = i.id_comitato
										INNER JOIN preiscrizioni AS p
										ON p.id_comitato = i.id_comitato
										WHERE p.accoglienza = '0'
										AND p.iscrizione = '1' OR p.iscrizione = '3'
										GROUP BY i.id_comitato
										ORDER BY c.nome_comitato";
                    $result = mysql_query($query, $db);
                    while($row = mysql_fetch_array( $result ))
                    {
                        ?>
                        <input name="squadra" id="id_comitato<?= $row[id_comitato] ?>" value="<?= $row[id_comitato] ?>" type="radio">
                        <label for="id_comitato<?= $row[id_comitato] ?>"><?= $row[nome_comitato] ?></label>
						<?php
                    }
                    mysql_close($db); 
                    ?>
				</fieldset>
                <br />
                <input type="submit" value="Inserisci" />

            </form>

        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>