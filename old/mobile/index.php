<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><img src="logo_meeting.png" alt="Portale Meeting 2016 - Mobile"></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		Benvenuto al MEETING 2016<br /><br />Siamo giunti alla X edizione Regionale. Abbiamo creato questo portale per gestire le iscrizioni, le registrazioni, le votazioni, la consultazione dei punteggi e quant'altro è funzionale al Meeting.<br /><br />Le preiscrizioni si sono chiuse lo scorso <b>31 agosto</b>.<br /><br />Le iscrizioni saranno disponibili dal <b>12 settembre</b> al <b>2 ottobre</b>.<br /><br />
		<b>Guarda il video promo<b><br /><br />
        
        <iframe src="https://www.youtube.com/embed/C5HIZguIIUY" frameborder="0" allowfullscreen></iframe>

		<p>Buon divertimento!</p>

        <div class="ui-grid-a ui-responsive">
        	<div class="ui-block-a">
        		<div class="jqm-block-content">
        			<h3>Link rapidi</h3>
<!--

        			<p><a href="spiega_preiscrizione.php" data-ajax="false">Inserisci Preiscrizione</a></p>
                    <p><a href="spiega_iscrizione_squadra.php" data-ajax="false">Iscrizione Squadra</a></p>
                    <p><a href="spiega_iscrizione_osservatore.php" data-ajax="false">Iscrizione Osservatore</a></p>
-->
                    <p><a href="spiega_iscrizione_squadra.php" data-ajax="false">Iscrizione Squadra</a></p>
                    <p><a href="spiega_iscrizione_osservatore.php" data-ajax="false">Iscrizione Osservatore</a></p>
        			<p><a href="RegolamentoMeeting2016_Regionale.pdf" data-ajax="false">Regolamento Regionale</a></p>
         			<p><a href="contattaci.php" data-ajax="false">Contattaci</a></p>
        		</div>
        	</div>
        </div>

	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		    </ul>
        </div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>