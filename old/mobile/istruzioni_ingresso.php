<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Programma del Meeting</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
	<script type="text/javascript">
		$(document).on("change", "#checkbox-1", function () {
			if ($(this).prop("checked")) {
				$("#submit").button("enable");
			} else {
				$("#submit").button("disable");
			}
		});
    </script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2016 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		<p><strong>Istruzioni per l'ingresso</strong></p>

        <div data-html="true">

            <br /><br />

            <b>Domenica 23 ottobre</b><br /><br />
            Prima delle 7.00 la struttura è chiusa. Fino alle 7.30 sarà permesso l'ingresso solo allo staff!<br />
			All'ingresso troverai due volontari con gilet ad alta visibilità ai quali dovrai comunicare il tuo arrivo (ogni squadra ed i relativi osservatori si muoveranno in gruppo), gli stessi ti indirizzeranno opportunamente.
			<br /><br />
            
			Se sei un <b>Giudice</b> o un <b>Simulatore</b><br />
			Puoi entrare dalle 07.45 alle 08.30 e devi recarti al <b>Desk</b> per la registrazione, fatta questa verrai indirizzato in Piazza.
			<br /><br />
			
 			Se sei un <b>Partecipante</b> o un <b>Osservatore</b><br />
			Puoi entrare in gruppo con tutti quelli del tuo comitato dalle 08.00 alle 08.30 e devi recarti direttamente in <b>Piazza Incontro</b> per visitare gli stand, sarai raggiunto dal personale della Control Room per la registrazione.
			<br /><br />

            <b>N.B. </b>Per garantire a tutti un rapido accesso evitando antipatiche attese, ti preghiamo di attenerti a queste indicazioni. Eventuali assenze o difformità dai dati delle iscrizioni saranno gestite dal personale della Control Room in fase di registrazione.
			<br /><br />
			
            <br /><br />
            <br /><br />
            
        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>