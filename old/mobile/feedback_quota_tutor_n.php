<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2015 - Feedback Tutor</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.php" title="Meeting 2015 - Homepage"><img src="logo_meeting.png" alt="Portale Meeting 2015 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2015</h1>
		
		<p><strong>Tutor</strong></p>

		<?
        //recupero i dati nella barra dell'indirizzo
        $ruolo=$_GET['ruolo'];
        $id=$_GET['id'];
        //echo  "Ruolo                    (tutti):                     $ruolo <br>";
        //echo  "ID          (tutti):           $id <br>";
        
		$valutazione_accoglienza_venerdi = $_POST['valutazione_accoglienza_venerdi'];
		$valutazione_speed_meeting = $_POST['valutazione_speed_meeting'];
		$valutazione_pernottamento = $_POST['valutazione_pernottamento'];
		$valutazione_pasti = $_POST['valutazione_pasti'];
		$valutazione_logistica = $_POST['valutazione_logistica'];
		$valutazione_tour = $_POST['valutazione_tour'];
		
		$valutazione_gradimento_prova1 = $_POST['valutazione_gradimento_prova1'];
		//echo "Valutazione gradimento prova area 1: $valutazione_gradimento_prova1 <br>";
		$valutazione_formativa_prova1 = $_POST['valutazione_formativa_prova1'];
		//echo "Valutazione validità formativa prova area 1: $valutazione_formativa_prova1 <br>";
		$valutazione_difficolta_prova1 = $_POST['valutazione_difficolta_prova1'];
		//echo "Valutazione difficoltà prova area 1: $valutazione_difficolta_prova1 <br>";
		$valutazione_lavoro_gruppo_prova1 = $_POST['valutazione_lavoro_gruppo_prova1'];
		//echo "Valutazione lavoro di gruppo prova area 1: $valutazione_lavoro_gruppo_prova1 <br>";
		$valutazione_performance_giudice_prova1 = $_POST['valutazione_performance_giudice_prova1'];
		//echo "Valutazione performance giudici prova area 1: $valutazione_performance_giudice_prova1 <br>";
		
		$valutazione_gradimento_prova2 = $_POST['valutazione_gradimento_prova2'];
		//echo "Valutazione gradimento prova area 2: $valutazione_gradimento_prova2 <br>";
		$valutazione_formativa_prova2 = $_POST['valutazione_formativa_prova2'];
		//echo "Valutazione validità formativa prova area 2: $valutazione_formativa_prova2 <br>";
		$valutazione_difficolta_prova2 = $_POST['valutazione_difficolta_prova2'];
		//echo "Valutazione difficoltà prova area 2: $valutazione_difficolta_prova2 <br>";
		$valutazione_lavoro_gruppo_prova2 = $_POST['valutazione_lavoro_gruppo_prova2'];
		//echo "Valutazione lavoro di gruppo prova area 2: $valutazione_lavoro_gruppo_prova2 <br>";
		$valutazione_performance_giudice_prova2 = $_POST['valutazione_performance_giudice_prova2'];
		//echo "Valutazione performance giudici prova area 2: $valutazione_performance_giudice_prova2 <br>";
		$valutazione_interpretazione_simulatore_prova2 = $_POST['valutazione_interpretazione_simulatore_prova2'];
		//echo "Valutazione interpretazione simulatori prova area 2: $valutazione_interpretazione_simulatore_prova2 <br>";
		$valutazione_utilita_simulatore_prova2 = $_POST['valutazione_utilita_simulatore_prova2'];
		//echo "Valutazione utilità simulatori prova area 2: $valutazione_utilita_simulatore_prova2 <br>";
		
		$valutazione_gradimento_prova3 = $_POST['valutazione_gradimento_prova3'];
		//echo "Valutazione gradimento prova area 3: $valutazione_gradimento_prova3 <br>";
		$valutazione_formativa_prova3 = $_POST['valutazione_formativa_prova3'];
		//echo "Valutazione validità formativa prova area 3: $valutazione_formativa_prova3 <br>";
		$valutazione_difficolta_prova3 = $_POST['valutazione_difficolta_prova3'];
		//echo "Valutazione difficoltà prova area 3: $valutazione_difficolta_prova3 <br>";
		$valutazione_lavoro_gruppo_prova3 = $_POST['valutazione_lavoro_gruppo_prova3'];
		//echo "Valutazione lavoro di gruppo prova area 3: $valutazione_lavoro_gruppo_prova3 <br>";
		$valutazione_performance_giudice_prova3 = $_POST['valutazione_performance_giudice_prova3'];
		//echo "Valutazione performance giudici prova area 3: $valutazione_performance_giudice_prova3 <br>";
		$valutazione_interpretazione_simulatore_prova3 = $_POST['valutazione_interpretazione_simulatore_prova3'];
		//echo "Valutazione interpretazione simulatori prova area 2: $valutazione_interpretazione_simulatore_prova2 <br>";
		$valutazione_utilita_simulatore_prova3 = $_POST['valutazione_utilita_simulatore_prova3'];
		//echo "Valutazione utilità simulatori prova area 2: $valutazione_utilita_simulatore_prova2 <br>";
		
		$valutazione_gradimento_prova4 = $_POST['valutazione_gradimento_prova4'];
		//echo "Valutazione gradimento prova area 4: $valutazione_gradimento_prova4 <br>";
		$valutazione_formativa_prova4 = $_POST['valutazione_formativa_prova4'];
		//echo "Valutazione validità formativa prova area 4: $valutazione_formativa_prova4 <br>";
		$valutazione_difficolta_prova4 = $_POST['valutazione_difficolta_prova4'];
		//echo "Valutazione difficoltà prova area 4: $valutazione_difficolta_prova4 <br>";
		$valutazione_lavoro_gruppo_prova4 = $_POST['valutazione_lavoro_gruppo_prova4'];
		//echo "Valutazione lavoro di gruppo prova area 4: $valutazione_lavoro_gruppo_prova4 <br>";
		$valutazione_performance_giudice_prova4 = $_POST['valutazione_performance_giudice_prova4'];
		//echo "Valutazione performance giudici prova area 4: $valutazione_performance_giudice_prova4 <br>";
        
		$valutazione_gradimento_prova5 = $_POST['valutazione_gradimento_prova5'];
		//echo "Valutazione gradimento prova area 5: $valutazione_gradimento_prova5 <br>";
		$valutazione_formativa_prova5 = $_POST['valutazione_formativa_prova5'];
		//echo "Valutazione validità formativa prova area 5: $valutazione_formativa_prova5 <br>";
		$valutazione_difficolta_prova5 = $_POST['valutazione_difficolta_prova5'];
		//echo "Valutazione difficoltà prova area 5: $valutazione_difficolta_prova5 <br>";
		$valutazione_lavoro_gruppo_prova5 = $_POST['valutazione_lavoro_gruppo_prova5'];
		//echo "Valutazione lavoro di gruppo prova area 5: $valutazione_lavoro_gruppo_prova5 <br>";
		$valutazione_performance_giudice_prova5 = $_POST['valutazione_performance_giudice_prova5'];
		//echo "Valutazione performance giudici prova area 5: $valutazione_performance_giudice_prova5 <br>";
		$valutazione_interpretazione_simulatore_prova5 = $_POST['valutazione_interpretazione_simulatore_prova5'];
		//echo "Valutazione interpretazione simulatori prova area 4: $valutazione_interpretazione_simulatore_prova4 <br>";
		$valutazione_utilita_simulatore_prova5 = $_POST['valutazione_utilita_simulatore_prova5'];
		//echo "Valutazione utilità simulatori prova area 4: $valutazione_utilita_simulatore_prova4 <br>";
		
		$valutazione_gradimento_prova6 = $_POST['valutazione_gradimento_prova6'];
		//echo "Valutazione gradimento prova area 6: $valutazione_gradimento_prova6 <br>";
		$valutazione_formativa_prova6 = $_POST['valutazione_formativa_prova6'];
		//echo "Valutazione validità formativa prova area 6: $valutazione_formativa_prova6 <br>";
		$valutazione_difficolta_prova6 = $_POST['valutazione_difficolta_prova6'];
		//echo "Valutazione difficoltà prova area 6: $valutazione_difficolta_prova6 <br>";
		$valutazione_lavoro_gruppo_prova6 = $_POST['valutazione_lavoro_gruppo_prova6'];
		//echo "Valutazione lavoro di gruppo prova area 6: $valutazione_lavoro_gruppo_prova6 <br>";
		$valutazione_performance_giudice_prova6 = $_POST['valutazione_performance_giudice_prova6'];
		//echo "Valutazione performance giudici prova area 6: $valutazione_performance_giudice_prova6 <br>";
		
        //recupero i dati di input del form precedente
		$valutazione_gradimento_prova7 = $_REQUEST['valutazione_gradimento_prova7'];
		//echo "Valutazione gradimento prova area 7: $valutazione_gradimento_prova7 <br>";
		$valutazione_formativa_prova7 = $_REQUEST['valutazione_formativa_prova7'];
		//echo "Valutazione validità formativa prova area 7: $valutazione_formativa_prova7 <br>";
		$valutazione_difficolta_prova7 = $_REQUEST['valutazione_difficolta_prova7'];
		//echo "Valutazione difficoltà prova area 7: $valutazione_difficolta_prova7 <br>";
		$valutazione_lavoro_gruppo_prova7 = $_REQUEST['valutazione_lavoro_gruppo_prova7'];
		//echo "Valutazione lavoro di gruppo prova area 7: $valutazione_lavoro_gruppo_prova7 <br>";
		$valutazione_performance_giudice_prova7 = $_REQUEST['valutazione_performance_giudice_prova7'];
		//echo "Valutazione performance giudici prova area 7: $valutazione_performance_giudice_prova7 <br>";

		//Annullo il form dei simulatori, giudici e staff
		$valutazione_performance_giudice_prova = '';
		//echo "Valutazione performance dei giudici		 					(simulatori):                  					$valutazione_performance_giudice_prova <br>";
		$prova_assegnata = '';
		//echo "Prova assegnata 												(giudici/simulatori): 							$prova_assegnata <br>";
		$valutazione_prova_assegnata = '';
		//echo "Valutazione organizzazione prova assegnata 					(giudici/simulatori): 							$valutazione_prova_assegnata <br>";
		$valutazione_inserimento_voti_portale = '';
		//echo "Valutazione inserimento voti portale 							(giudici): 										$valutazione_inserimento_voti_portale <br>";
		$valutazione_interpretazione_simulatore_prova = '';
		//echo "Valutazione interpretazione dei simulatori 					(giudici): 										$valutazione_interpretazione_simulatore_prova <br>";
		$valutazione_utilita_simulatore_prova = '';
		//echo "Valutazione utilità dei simulatori 							(giudici): 										$valutazione_utilita_simulatore_prova <br>";
		$valutazione_coordinamento_backoffice = '';
		//echo "Valutazione coordinamento backoffice                 			(giudici/staff):								$valutazione_coordinamento_backoffice <br>";
		$valutazione_preparazione = '';
		//echo "Valutazione fase preparatoria al meeting 						(staff):                     					$valutazione_preparazione <br>";
		$valutazione_soddisfazione_incarico = '';
		//echo "Valutazione soddisfazione incarico 							(staff):                     					$valutazione_soddisfazione_incarico <br>";
		$valutazione_mezzi_informazioni = '';
		//echo "Valutazione mezzi ed informazioni 							(staff):                     					$valutazione_mezzi_informazioni <br>";
		
		$valutazione_gradimento_prova8=$_REQUEST['valutazione_gradimento_prova8'];
		$valutazione_formativa_prova8=$_REQUEST['valutazione_formativa_prova8'];
		$valutazione_difficolta_prova8=$_REQUEST['valutazione_difficolta_prova8'];
		$valutazione_lavoro_gruppo_prova8=$_REQUEST['valutazione_lavoro_gruppo_prova8'];
		$valutazione_performance_giudice_prova8=$_REQUEST['valutazione_performance_giudice_prova8'];
		
        ?>


		<form name="feedback_quota_tutor_n" enctype="multipart/form-data" method="post" action="feedback_generale_ospitante_n.php?ruolo=<?php echo $ruolo ?>&id=<?php echo $id ?>">

		<label for="slider-fill">Valuta la competenza dei Tutor</label>
		<input type="range" name="valutazione_tutor_competenza" id="valutazione_tutor_competenza" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<label for="slider-fill">Valuta la disponibilità dei Tutor</label>
		<input type="range" name="valutazione_tutor_disponibilita" id="valutazione_tutor_disponibilita" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<br /><br />
		
		<input type="hidden" name="valutazione_accoglienza_venerdi" value="<?php echo $valutazione_accoglienza_venerdi; ?>" />
		<input type="hidden" name="valutazione_speed_meeting" value="<?php echo $valutazione_speed_meeting; ?>" />
		<input type="hidden" name="valutazione_pernottamento" value="<?php echo $valutazione_pernottamento; ?>" />
		<input type="hidden" name="valutazione_pasti" value="<?php echo $valutazione_pasti; ?>" />
		<input type="hidden" name="valutazione_logistica" value="<?php echo $valutazione_logistica; ?>" />
		<input type="hidden" name="valutazione_tour" value="<?php echo $valutazione_tour; ?>" />	
		
        <input type="hidden" name="valutazione_gradimento_prova1" value="<?php echo $valutazione_gradimento_prova1; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova1" value="<?php echo $valutazione_difficolta_prova1; ?>" />
        <input type="hidden" name="valutazione_formativa_prova1" value="<?php echo $valutazione_formativa_prova1; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova1" value="<?php echo $valutazione_lavoro_gruppo_prova1; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova1" value="<?php echo $valutazione_performance_giudice_prova1; ?>" />

        <input type="hidden" name="valutazione_gradimento_prova2" value="<?php echo $valutazione_gradimento_prova2; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova2" value="<?php echo $valutazione_difficolta_prova2; ?>" />
        <input type="hidden" name="valutazione_formativa_prova2" value="<?php echo $valutazione_formativa_prova2; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova2" value="<?php echo $valutazione_lavoro_gruppo_prova2; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova2" value="<?php echo $valutazione_performance_giudice_prova2; ?>" />
        <input type="hidden" name="valutazione_interpretazione_simulatore_prova2" value="<?php echo $valutazione_interpretazione_simulatore_prova2; ?>" />
        <input type="hidden" name="valutazione_utilita_simulatore_prova2" value="<?php echo $valutazione_utilita_simulatore_prova2; ?>" />
		
        <input type="hidden" name="valutazione_gradimento_prova3" value="<?php echo $valutazione_gradimento_prova3; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova3" value="<?php echo $valutazione_difficolta_prova3; ?>" />
        <input type="hidden" name="valutazione_formativa_prova3" value="<?php echo $valutazione_formativa_prova3; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova3" value="<?php echo $valutazione_lavoro_gruppo_prova3; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova3" value="<?php echo $valutazione_performance_giudice_prova3; ?>" />
		<input type="hidden" name="valutazione_interpretazione_simulatore_prova3" value="<?php echo $valutazione_interpretazione_simulatore_prova3; ?>" />
        <input type="hidden" name="valutazione_utilita_simulatore_prova3" value="<?php echo $valutazione_utilita_simulatore_prova3; ?>" />
		
        <input type="hidden" name="valutazione_gradimento_prova4" value="<?php echo $valutazione_gradimento_prova4; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova4" value="<?php echo $valutazione_difficolta_prova4; ?>" />
        <input type="hidden" name="valutazione_formativa_prova4" value="<?php echo $valutazione_formativa_prova4; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova4" value="<?php echo $valutazione_lavoro_gruppo_prova4; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova4" value="<?php echo $valutazione_performance_giudice_prova4; ?>" />

        <input type="hidden" name="valutazione_gradimento_prova5" value="<?php echo $valutazione_gradimento_prova5; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova5" value="<?php echo $valutazione_difficolta_prova5; ?>" />
        <input type="hidden" name="valutazione_formativa_prova5" value="<?php echo $valutazione_formativa_prova5; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova5" value="<?php echo $valutazione_lavoro_gruppo_prova5; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova5" value="<?php echo $valutazione_performance_giudice_prova5; ?>" />
		<input type="hidden" name="valutazione_interpretazione_simulatore_prova5" value="<?php echo $valutazione_interpretazione_simulatore_prova5; ?>" />
        <input type="hidden" name="valutazione_utilita_simulatore_prova5" value="<?php echo $valutazione_utilita_simulatore_prova5; ?>" />
		
        <input type="hidden" name="valutazione_gradimento_prova6" value="<?php echo $valutazione_gradimento_prova6; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova6" value="<?php echo $valutazione_difficolta_prova6; ?>" />
        <input type="hidden" name="valutazione_formativa_prova6" value="<?php echo $valutazione_formativa_prova6; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova6" value="<?php echo $valutazione_lavoro_gruppo_prova6; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova6" value="<?php echo $valutazione_performance_giudice_prova6; ?>" />
		
		<input type="hidden" name="valutazione_gradimento_prova7" value="<?php echo $valutazione_gradimento_prova7; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova7" value="<?php echo $valutazione_difficolta_prova7; ?>" />
        <input type="hidden" name="valutazione_formativa_prova7" value="<?php echo $valutazione_formativa_prova7; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova7" value="<?php echo $valutazione_lavoro_gruppo_prova7; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova7" value="<?php echo $valutazione_performance_giudice_prova7; ?>" />
		
		
        <input type="hidden" name="valutazione_performance_giudice_prova" value="<?php echo $valutazione_performance_giudice_prova; ?>" />
        
        <input type="hidden" name="prova_assegnata" value="<?php echo $prova_assegnata; ?>" />
        <input type="hidden" name="valutazione_prova_assegnata" value="<?php echo $valutazione_prova_assegnata; ?>" />
        
        <input type="hidden" name="valutazione_inserimento_voti_portale" value="<?php echo $valutazione_inserimento_voti_portale; ?>" />
        <input type="hidden" name="valutazione_interpretazione_simulatore_prova" value="<?php echo $valutazione_interpretazione_simulatore_prova; ?>" />
        <input type="hidden" name="valutazione_utilita_simulatore_prova" value="<?php echo $valutazione_utilita_simulatore_prova; ?>" />
        
        <input type="hidden" name="valutazione_coordinamento_backoffice" value="<?php echo $valutazione_coordinamento_backoffice; ?>" />
        
        <input type="hidden" name="valutazione_preparazione" value="<?php echo $valutazione_preparazione; ?>" />
        <input type="hidden" name="valutazione_soddisfazione_incarico" value="<?php echo $valutazione_soddisfazione_incarico; ?>" />
        <input type="hidden" name="valutazione_mezzi_informazioni" value="<?php echo $valutazione_mezzi_informazioni; ?>" />
		
		<input type="submit" value="Avanti" />
		</form>


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>