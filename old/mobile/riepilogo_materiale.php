<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice') && !($check['team'] == 'organigramma') && !($check['team'] == 'nazionale'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato all'organizzazione od al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2015 - Elenco Materiale</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2015 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2015 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2015</h1>
		

		<?php
		include ("config.inc.php");
		echo "<font color=#2B3856 size='2'>";

		$db = mysql_connect($db_host, $db_user, $db_password);
		if ($db == FALSE)
		die ("Errore nella connessione. Verificare i parametri nel file config.inc.php");
		mysql_select_db($db_name, $db)
		or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");
		?>

		<p><strong>Elenco materiale</strong></p>
		
   		<table data-role="table" id="table-column-toggle" data-mode="columntoggle" class="ui-responsive table-stroke">
     <thead>
       <tr>
         <th>Richiedente</th>
         <th data-priority="1">Data richiesta</th>
         <th data-priority="7">Quantità</th>
         <th data-priority="8">Descrizione</th>
         <th data-priority="3">Inizio esigenza</th>
         <th data-priority="4">Fine esigenza</th>
         <th data-priority="5">Owner</th>
         <th data-priority="6">Data chiusura</th>
     </thead>
     <tbody>        
            
			<?php
            $query = "SELECT *, DATE_FORMAT(data_richiesta, '%d-%m-%Y'), DATE_FORMAT(inizio_esigenza, '%d-%m-%Y %m:%s') as inizio_esigenza FROM shopping ORDER BY data_richiesta";
            $result = mysql_query($query, $db);
            while($row = mysql_fetch_array( $result )) 
            {
				?>
				<tr>
                <th><?php echo "$row[richiedente]"?></th>
                <td><?php echo "$row[data_richiesta]"?></td>
                <td><?php echo "$row[quantita]"?></td>
                <td><a href=modifica_materiale.php?id=<?php echo $row[id]?>><?php echo "$row[descrizione]"?></a></td>
                <td><?php echo "$row[inizio_esigenza]"?></td>
                <td><?php echo "$row[fine_esigenza]"?></td>
                <td><?php echo "$row[owner]"?></td>
                <td><?php echo "$row[data_chiusura]"?></td>
                </tr>
                <?php
            }
            mysql_close($db);
            ?>
    </tbody>
   </table>   

	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>