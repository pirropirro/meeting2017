<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2015 - Feedback Generale Ospitante</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.php" title="Meeting 2015 - Homepage"><img src="logo_meeting.png" alt="Portale Meeting 2015 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2015</h1>
		
		<p><strong>Comitato Ospitante</strong></p>

		<?
        //recupero i dati nella barra dell'indirizzo
        $ruolo=$_GET['ruolo'];
        $id=$_GET['id'];
        //echo "Ruolo                    (tutti):                     $ruolo <br>";
        //echo "ID          (tutti):           $id <br>";
        
        //recupero i dati passati in hidden dal form precedente
        $valutazione_quota_partecipante = $_POST['valutazione_quota_partecipante'];
        //echo "Valutazione quota partecipante: $valutazione_quota_partecipante <br>";
        $valutazione_quota_osservatore = $_POST['valutazione_quota_osservatore'];
        //echo "Valutazione quota osservatore: $valutazione_quota_osservatore <br>";
        $valutazione_tutor_competenza = $_POST['valutazione_tutor_competenza'];
        //echo "Valutazione tutor competenza: $valutazione_tutor_competenza <br>";
        $valutazione_tutor_disponibilita = $_POST['valutazione_tutor_disponibilita'];
        //echo "Valutazione tutor disponibilità: $valutazione_tutor_disponibilita <br>";
		
		$valutazione_gradimento_prova1 = $_POST['valutazione_gradimento_prova1'];
		//echo "Valutazione gradimento prova area 1: $valutazione_gradimento_prova1 <br>";
		$valutazione_formativa_prova1 = $_POST['valutazione_formativa_prova1'];
		//echo "Valutazione validità formativa prova area 1: $valutazione_formativa_prova1 <br>";
		$valutazione_difficolta_prova1 = $_POST['valutazione_difficolta_prova1'];
		//echo "Valutazione difficoltà prova area 1: $valutazione_difficolta_prova1 <br>";
		$valutazione_lavoro_gruppo_prova1 = $_POST['valutazione_lavoro_gruppo_prova1'];
		//echo "Valutazione lavoro di gruppo prova area 1: $valutazione_lavoro_gruppo_prova1 <br>";
		$valutazione_performance_giudice_prova1 = $_POST['valutazione_performance_giudice_prova1'];
		//echo "Valutazione performance giudici prova area 1: $valutazione_performance_giudice_prova1 <br>";
		
		$valutazione_gradimento_prova2 = $_POST['valutazione_gradimento_prova2'];
		//echo "Valutazione gradimento prova area 2: $valutazione_gradimento_prova2 <br>";
		$valutazione_formativa_prova2 = $_POST['valutazione_formativa_prova2'];
		//echo "Valutazione validità formativa prova area 2: $valutazione_formativa_prova2 <br>";
		$valutazione_difficolta_prova2 = $_POST['valutazione_difficolta_prova2'];
		//echo "Valutazione difficoltà prova area 2: $valutazione_difficolta_prova2 <br>";
		$valutazione_lavoro_gruppo_prova2 = $_POST['valutazione_lavoro_gruppo_prova2'];
		//echo "Valutazione lavoro di gruppo prova area 2: $valutazione_lavoro_gruppo_prova2 <br>";
		$valutazione_performance_giudice_prova2 = $_POST['valutazione_performance_giudice_prova2'];
		//echo "Valutazione performance giudici prova area 2: $valutazione_performance_giudice_prova2 <br>";
		
		$valutazione_gradimento_prova3 = $_POST['valutazione_gradimento_prova3'];
		//echo "Valutazione gradimento prova area 3: $valutazione_gradimento_prova3 <br>";
		$valutazione_formativa_prova3 = $_POST['valutazione_formativa_prova3'];
		//echo "Valutazione validità formativa prova area 3: $valutazione_formativa_prova3 <br>";
		$valutazione_difficolta_prova3 = $_POST['valutazione_difficolta_prova3'];
		//echo "Valutazione difficoltà prova area 3: $valutazione_difficolta_prova3 <br>";
		$valutazione_lavoro_gruppo_prova3 = $_POST['valutazione_lavoro_gruppo_prova3'];
		//echo "Valutazione lavoro di gruppo prova area 3: $valutazione_lavoro_gruppo_prova3 <br>";
		$valutazione_performance_giudice_prova3 = $_POST['valutazione_performance_giudice_prova3'];
		//echo "Valutazione performance giudici prova area 3: $valutazione_performance_giudice_prova3 <br>";
		
		$valutazione_gradimento_prova4 = $_POST['valutazione_gradimento_prova4'];
		//echo "Valutazione gradimento prova area 4: $valutazione_gradimento_prova4 <br>";
		$valutazione_formativa_prova4 = $_POST['valutazione_formativa_prova4'];
		//echo "Valutazione validità formativa prova area 4: $valutazione_formativa_prova4 <br>";
		$valutazione_difficolta_prova4 = $_POST['valutazione_difficolta_prova4'];
		//echo "Valutazione difficoltà prova area 4: $valutazione_difficolta_prova4 <br>";
		$valutazione_lavoro_gruppo_prova4 = $_POST['valutazione_lavoro_gruppo_prova4'];
		//echo "Valutazione lavoro di gruppo prova area 4: $valutazione_lavoro_gruppo_prova4 <br>";
		$valutazione_performance_giudice_prova4 = $_POST['valutazione_performance_giudice_prova4'];
		//echo "Valutazione performance giudici prova area 4: $valutazione_performance_giudice_prova4 <br>";
		$valutazione_interpretazione_simulatore_prova4 = $_POST['valutazione_interpretazione_simulatore_prova4'];
		//echo "Valutazione interpretazione simulatori prova area 4: $valutazione_interpretazione_simulatore_prova4 <br>";
		$valutazione_utilita_simulatore_prova4 = $_POST['valutazione_utilita_simulatore_prova4'];
		//echo "Valutazione utilità simulatori prova area 4: $valutazione_utilita_simulatore_prova4 <br>";
		
		$valutazione_gradimento_prova5 = $_POST['valutazione_gradimento_prova5'];
		//echo "Valutazione gradimento prova area 5: $valutazione_gradimento_prova5 <br>";
		$valutazione_formativa_prova5 = $_POST['valutazione_formativa_prova5'];
		//echo "Valutazione validità formativa prova area 5: $valutazione_formativa_prova5 <br>";
		$valutazione_difficolta_prova5 = $_POST['valutazione_difficolta_prova5'];
		//echo "Valutazione difficoltà prova area 5: $valutazione_difficolta_prova5 <br>";
		$valutazione_lavoro_gruppo_prova5 = $_POST['valutazione_lavoro_gruppo_prova5'];
		//echo "Valutazione lavoro di gruppo prova area 5: $valutazione_lavoro_gruppo_prova5 <br>";
		$valutazione_performance_giudice_prova5 = $_POST['valutazione_performance_giudice_prova5'];
		//echo "Valutazione performance giudici prova area 5: $valutazione_performance_giudice_prova5 <br>";
		
        $valutazione_gradimento_prova6 = $_POST['valutazione_gradimento_prova6'];
		//echo "Valutazione gradimento prova area 5: $valutazione_gradimento_prova5 <br>";
		$valutazione_formativa_prova6 = $_POST['valutazione_formativa_prova6'];
		//echo "Valutazione validità formativa prova area 5: $valutazione_formativa_prova5 <br>";
		$valutazione_difficolta_prova6 = $_POST['valutazione_difficolta_prova6'];
		//echo "Valutazione difficoltà prova area 5: $valutazione_difficolta_prova5 <br>";
		$valutazione_lavoro_gruppo_prova6 = $_POST['valutazione_lavoro_gruppo_prova6'];
		//echo "Valutazione lavoro di gruppo prova area 5: $valutazione_lavoro_gruppo_prova5 <br>";
		$valutazione_performance_giudice_prova6 = $_POST['valutazione_performance_giudice_prova6'];
		//echo "Valutazione performance giudici prova area 5: $valutazione_performance_giudice_prova5 <br>";
		
		
		//recupero i dati di input del form precedente feedback_prova_area6.php
		$valutazione_gradimento_prova7=$_REQUEST['valutazione_gradimento_prova7'];
		$valutazione_formativa_prova7=$_REQUEST['valutazione_formativa_prova7'];
		$valutazione_difficolta_prova7=$_REQUEST['valutazione_difficolta_prova7'];
		$valutazione_lavoro_gruppo_prova7=$_REQUEST['valutazione_lavoro_gruppo_prova7'];
		$valutazione_performance_giudice_prova7=$_REQUEST['valutazione_performance_giudice_prova7'];
		
		//recupero i dati di input del form precedente feedback_simulatori.php
		$valutazione_gruppo_lavoro=$_REQUEST['valutazione_gruppo_lavoro'];
		$valutazione_informazioni_prova=$_REQUEST['valutazione_informazioni_prova'];
		$valutazione_abbandono_simulatori=$_REQUEST['valutazione_abbandono_simulatori'];
		$valutazione_prova_assegnata=$_REQUEST['valutazione_prova_assegnata'];
		$valutazione_performance_giudice_prova=$_REQUEST['valutazione_performance_giudice_prova'];
		
		//recupero i dati di input del form precedente feedback_giudici.php
		$valutazione_inserimento_voti_portale=$_REQUEST['valutazione_inserimento_voti_portale'];
		$valutazione_coordinamento_backoffice=$_REQUEST['valutazione_coordinamento_backoffice'];
		$prova_assegnata=$_REQUEST['prova_assegnata'];
		$valutazione_prova_assegnata=$_REQUEST['valutazione_prova_assegnata'];
		$valutazione_interpretazione_simulatore_prova=$_REQUEST['valutazione_interpretazione_simulatore_prova'];
		$valutazione_utilita_simulatore_prova=$_REQUEST['valutazione_utilita_simulatore_prova'];
		
		//recupero i dati di input del form precedente feedback_staff.php
		$valutazione_preparazione=$_REQUEST['valutazione_preparazione'];
		$valutazione_soddisfazione_incarico=$_REQUEST['valutazione_soddisfazione_incarico'];
		$valutazione_mezzi_informazioni=$_REQUEST['valutazione_mezzi_informazioni'];
		$valutazione_coordinamento_backoffice=$_REQUEST['valutazione_coordinamento_backoffice'];

        ?>


		<form name="feedback_generale_ospitante" enctype="multipart/form-data" method="post" action="feedback_generale_organizzazione.php?ruolo=<?php echo $ruolo ?>&id=<?php echo $id ?>">

		<label for="slider-fill">Valuta l'adeguatezza della struttura ospitante</label>
		<input type="range" name="valutazione_adeguatezza_struttura" id="valutazione_adeguatezza_struttura" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<label for="slider-fill">Valuta la raggiungibilità della struttura ospitante</label>
		<input type="range" name="valutazione_raggiugngibilita_struttura" id="valutazione_raggiugngibilita_struttura" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<label for="slider-fill">Valuta l'efficienza della mensa</label>
		<input type="range" name="valutazione_efficienza_mensa" id="valutazione_efficienza_mensa" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<label for="slider-fill">Valuta la qualità del cibo</label>
		<input type="range" name="valutazione_qualita_cibo" id="valutazione_qualita_cibo" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
		
		<label for="slider-fill">Valuta la Piazza Italia</label>
		<input type="range" name="valutazione_piazza" id="valutazione_piazza" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<br /><br />

        <input type="hidden" name="valutazione_quota_partecipante" value="<?php echo $valutazione_quota_partecipante; ?>" />
        <input type="hidden" name="valutazione_quota_osservatore" value="<?php echo $valutazione_quota_osservatore; ?>" />
        <input type="hidden" name="valutazione_tutor_competenza" value="<?php echo $valutazione_tutor_competenza; ?>" />
        <input type="hidden" name="valutazione_tutor_disponibilita" value="<?php echo $valutazione_tutor_disponibilita; ?>" />
		
        <input type="hidden" name="valutazione_gradimento_prova1" value="<?php echo $valutazione_gradimento_prova1; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova1" value="<?php echo $valutazione_difficolta_prova1; ?>" />
        <input type="hidden" name="valutazione_formativa_prova1" value="<?php echo $valutazione_formativa_prova1; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova1" value="<?php echo $valutazione_lavoro_gruppo_prova1; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova1" value="<?php echo $valutazione_performance_giudice_prova1; ?>" />

        <input type="hidden" name="valutazione_gradimento_prova2" value="<?php echo $valutazione_gradimento_prova2; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova2" value="<?php echo $valutazione_difficolta_prova2; ?>" />
        <input type="hidden" name="valutazione_formativa_prova2" value="<?php echo $valutazione_formativa_prova2; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova2" value="<?php echo $valutazione_lavoro_gruppo_prova2; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova2" value="<?php echo $valutazione_performance_giudice_prova2; ?>" />

        <input type="hidden" name="valutazione_gradimento_prova3" value="<?php echo $valutazione_gradimento_prova3; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova3" value="<?php echo $valutazione_difficolta_prova3; ?>" />
        <input type="hidden" name="valutazione_formativa_prova3" value="<?php echo $valutazione_formativa_prova3; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova3" value="<?php echo $valutazione_lavoro_gruppo_prova3; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova3" value="<?php echo $valutazione_performance_giudice_prova3; ?>" />
		
        <input type="hidden" name="valutazione_gradimento_prova4" value="<?php echo $valutazione_gradimento_prova4; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova4" value="<?php echo $valutazione_difficolta_prova4; ?>" />
        <input type="hidden" name="valutazione_formativa_prova4" value="<?php echo $valutazione_formativa_prova4; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova4" value="<?php echo $valutazione_lavoro_gruppo_prova4; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova4" value="<?php echo $valutazione_performance_giudice_prova4; ?>" />
        <input type="hidden" name="valutazione_interpretazione_simulatore_prova4" value="<?php echo $valutazione_interpretazione_simulatore_prova4; ?>" />
        <input type="hidden" name="valutazione_utilita_simulatore_prova4" value="<?php echo $valutazione_utilita_simulatore_prova4; ?>" />
		
        <input type="hidden" name="valutazione_gradimento_prova5" value="<?php echo $valutazione_gradimento_prova5; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova5" value="<?php echo $valutazione_difficolta_prova5; ?>" />
        <input type="hidden" name="valutazione_formativa_prova5" value="<?php echo $valutazione_formativa_prova5; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova5" value="<?php echo $valutazione_lavoro_gruppo_prova5; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova5" value="<?php echo $valutazione_performance_giudice_prova5; ?>" />
		
        <input type="hidden" name="valutazione_gradimento_prova6" value="<?php echo $valutazione_gradimento_prova6; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova6" value="<?php echo $valutazione_difficolta_prova6; ?>" />
        <input type="hidden" name="valutazione_formativa_prova6" value="<?php echo $valutazione_formativa_prova6; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova6" value="<?php echo $valutazione_lavoro_gruppo_prova6; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova6" value="<?php echo $valutazione_performance_giudice_prova6; ?>" />
		
        <input type="hidden" name="valutazione_gradimento_prova7" value="<?php echo $valutazione_gradimento_prova7; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova7" value="<?php echo $valutazione_difficolta_prova7; ?>" />
        <input type="hidden" name="valutazione_formativa_prova7" value="<?php echo $valutazione_formativa_prova7; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova7" value="<?php echo $valutazione_lavoro_gruppo_prova7; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova7" value="<?php echo $valutazione_performance_giudice_prova7; ?>" />
		
        
        <input type="hidden" name="valutazione_performance_giudice_prova" value="<?php echo $valutazione_performance_giudice_prova; ?>" />
        
        <input type="hidden" name="valutazione_gruppo_lavoro" value="<?php echo $valutazione_gruppo_lavoro; ?>" />
		<input type="hidden" name="valutazione_informazioni_prova" value="<?php echo $valutazione_informazioni_prova; ?>" />
		<input type="hidden" name="valutazione_abbandono_simulatori" value="<?php echo $valutazione_abbandono_simulatori; ?>" />
		<input type="hidden" name="prova_assegnata" value="<?php echo $prova_assegnata; ?>" />
        <input type="hidden" name="valutazione_prova_assegnata" value="<?php echo $valutazione_prova_assegnata; ?>" />
        
        <input type="hidden" name="valutazione_inserimento_voti_portale" value="<?php echo $valutazione_inserimento_voti_portale; ?>" />
        <input type="hidden" name="valutazione_interpretazione_simulatore_prova" value="<?php echo $valutazione_interpretazione_simulatore_prova; ?>" />
        <input type="hidden" name="valutazione_utilita_simulatore_prova" value="<?php echo $valutazione_utilita_simulatore_prova; ?>" />
        
        <input type="hidden" name="valutazione_coordinamento_backoffice" value="<?php echo $valutazione_coordinamento_backoffice; ?>" />
        <input type="hidden" name="valutazione_preparazione" value="<?php echo $valutazione_preparazione; ?>" />
        <input type="hidden" name="valutazione_soddisfazione_incarico" value="<?php echo $valutazione_soddisfazione_incarico; ?>" />
        <input type="hidden" name="valutazione_mezzi_informazioni" value="<?php echo $valutazione_mezzi_informazioni; ?>" />
		
		<input type="submit" value="Avanti" />
		</form>


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>