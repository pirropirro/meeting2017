<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Contattaci</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
	<script type="text/javascript">
		$(document).on("change", "#checkbox-1", function () {
			if ($(this).prop("checked")) {
				$("#submit").button("enable");
			} else {
				$("#submit").button("disable");
			}
		});
    </script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.php" title="Meeting 2016 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		<p><strong>Contattaci</strong></p>

        <div data-html="true">

			<form method="post" action="salva_contattaci.php">



			<b>Indicaci i tuoi dati</b> &nbsp&nbsp
			<br /><br />
			Nome&nbsp&nbsp
			<textarea style="overflow:hidden" cols="30" rows="1" name="nome"></textarea><br />
			<br />

			E-mail&nbsp&nbsp
			<textarea style="overflow:hidden" cols="30" rows="1" name="mail"></textarea><br />
			<br />

			<br />
			Oggetto&nbsp&nbsp
			 
			<select name="oggetto" >
			   <option value=''></option>
			   <option value='Preiscrizioni'>Preiscrizioni</option>
			   <option value='Iscrizioni'>Iscrizioni</option>
			   <option value='Orari'>Orari</option>
			   <option value='Costi'>Costi</option>
			   <option value='Location'>Location</option>
			   <option value='Vitto'>Vitto</option>
			   <option value='Altro...'>Altro...</option>
			</select>


			<br /><br /><br />


			Richiesta<br />
			<textarea style="overflow:hidden" cols="40" rows="4" name="richiesta"></textarea><br />
			<br />

			<input type="submit" name="submit" value="Invia" />
			<br />
			<br />
			</form>

        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>