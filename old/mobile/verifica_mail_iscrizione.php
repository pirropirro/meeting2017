<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Verifica Mail Iscrizione</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
	<script type="text/javascript">
		$(document).on("change", "#checkbox-1", function () {
			if ($(this).prop("checked")) {
				$("#submit").button("enable");
			} else {
				$("#submit").button("disable");
			}
		});
    </script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2016 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		<p><strong>Verifica Mail Iscrizione</strong></p>

        <div data-html="true">

			<?php
            include ("config.inc.php");
            $fp = fopen ("log_iscrizioni.txt",a);
            
            //Leggo i dati in input
            $token=$_GET['token'];
            
            
            include ("apri_db.php");
            //Aggiorno la tabella preiscrzioni settando ad 1 il valore di 'iscrizione'
            $query = "UPDATE iscrizioni SET mail_confermata = '1' WHERE token = '$token'";
            if (mysql_query($query, $db))
            {
                //Scrivo l'avvenuta conferma a video e nel file di log
                $query_estrazione = "SELECT * FROM iscrizioni WHERE token = '$token'";
                $result = mysql_query($query_estrazione, $db);
                $row = mysql_fetch_array($result);
                                
                include('../PHPMailer/class.phpmailer.php');
                //require_once('PHPMailer/class.smtp.php');  
                
                $mittente = "meeting.2016@piemonte.cri.it";
                $nomemittente = "Back Office Meeting 2016";
                $destinatario = "$row[mail]";
                $ServerSMTP = "mailbox.cri.it"; //server SMTP
                $oggetto = "Iscrizione Meeting 2016 - Indirizzo mail confermato";
                $corpo_messaggio = "Ciao $row[nome],\nla presente per notificare l'avvenuta conferma del tuo indirizzo mail $row[mail]\n\nTi ricordiamo che ogni altra eventuale comunicazione riguardante il Meeting verrà recapitata a questo indirizzo.\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
                
                $msg = new PHPMailer;
                $msg->CharSet = "UTF-8";
                $msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
                //$msg->IsHTML(true);
                $msg->SMTPAuth = true; // Autenticazione SMTP
                $msg->SMTPKeepAlive = "true";
                $msg->Host = $ServerSMTP;
                $msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
                $msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
                
                $msg->From = $mittente;
                $msg->FromName = $nomemittente;
                $msg->AddAddress($destinatario); 
            //	$msg->AddCC("$row[mail_comitato]");
            //	$msg->AddBCC("paolo.ditoma@libero.it");
                $msg->Subject = $oggetto; 
                $msg->Body = $corpo_messaggio;
                
                if(!$msg->Send())
                {
                    fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE: Invio mail per mail confermata a $row[nome] $row[cognome] all'indirizzo $row[mail] con Token $token".$msg->ErrorInfo."\r\n");
                }
                else
                {
                    echo "Il tuo indirizzo mail è stato confermato.";
                    fwrite($fp,date('d/m/Y H:i:s').' - '."Mail confermata per $row[nome] $row[cognome] con indirizzo $row[mail] con Token $token"."\r\n");
                }
                            
            }
            else
            {
                echo "Questo token non esiste, ricontrolla la mail ricevuta.";
                fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE - Update su Token inesistente: $token"."\r\n");
            }
            
            mysql_close($db);
            fclose($fp);
            
            ?>

        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>