<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Conferma Preiscrizione</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.php" title="Meeting 2016 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		<p><strong>Conferma Preiscrizione</strong></p>

        <div data-html="true">

			<?php
            include("config.inc.php");
            
            echo "<font color=#2B3856 size='3' face='Calibri'>";
            
            $passwd="";
            $id_preiscrizione=$_GET['id'];
            //echo $id;
            
            $db = mysql_connect($db_host, $db_user, $db_password);
            if ($db == FALSE)
            die ("Errore nella connessione. Verificare i parametri nel file config.inc.php");
            mysql_select_db($db_name, $db)
            or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");
            
            //Recupero l'id_comitato dalla tabella preiscrizioni
            $query = "SELECT id_comitato FROM preiscrizioni WHERE id = $id_preiscrizione";
            $result = mysql_query($query, $db);
            $row = mysql_fetch_array( $result );
            $id_comitato = $row[id_comitato];
            
            //Recupero il nome comitato dalla tabella comitati
            $query = "SELECT nome_comitato FROM comitati WHERE id = $id_comitato";
            $result = mysql_query($query, $db);
            $row = mysql_fetch_array( $result );
            $nome_comitato = $row[nome_comitato];
            mysql_close($db); 
            ?>
            
            <form method="post" action="conferma_preiscrizione_passwd_verificata.php?id=<?php echo $id_preiscrizione; ?>">
            <br /><br />
            
            <b>Inserisci la password indicata nella mail di conferma preiscrizione per il <?php echo $nome_comitato; ?></b> &nbsp&nbsp
            &nbsp&nbsp
            <textarea style="overflow:hidden" cols="10" rows="1" name="passwd"></textarea><br />
            <br /><br />
            
            <input type="submit" value="Verifica" />
            <br />
            <br />
            </form>

        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>