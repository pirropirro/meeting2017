<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Inserisci Preiscrizione</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
	<script type="text/javascript">
		$(document).on("change", "#checkbox-1", function () {
			if ($(this).prop("checked")) {
				$("#submit").button("enable");
			} else {
				$("#submit").button("disable");
			}
		});
    </script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.php" title="Meeting 2016 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		<p><strong>Inserisci Preiscrizione</strong></p>

        <div data-html="true">

            <form method="post" action="salva_inserisci_preiscrizione.php">
            
            <br />
            <label>Squadra del<br>
             
            <select name="id_comitato" size="8" >
               <optgroup label="Provincia di Torino">
               <option value='1'>Comitato Locale di Torino</option>
               <option value='2'>Comitato Locale di Agliè</option>
               <option value='3'>Comitato Locale di Airasca</option>
               <option value='4'>Comitato Locale di Bardonecchia</option>
               <option value='5'>Comitato Locale di Beinasco</option>
               <option value='6'>Comitato Locale di Carignano</option>
               <option value='7'>Comitato Locale di Carmagnola</option>
               <option value='8'>Comitato Locale di Mappano</option>
               <option value='9'>Comitato Locale di Castellamonte</option>
               <option value='10'>Comitato Locale di Chieri</option>
               <option value='11'>Comitato Locale di Chivasso</option>
               <option value='12'>Comitato Locale di Valli di Lanzo</option>
               <option value='13'>Comitato Locale di Cuorgné</option>
               <option value='14'>Comitato Locale di Druento</option>
               <option value='15'>Comitato Locale di Fiano</option>
               <option value='16'>Comitato Locale di Gassino Torinese</option>
               <option value='17'>Comitato Locale di Giaveno</option>
               <option value='124'>Delegazione di Grugliasco</option>
               <option value='18'>Comitato Locale di Ivrea</option>
               <option value='19'>Comitato Locale di Lauriano</option>
               <option value='20'>Comitato Locale di Leinì</option>
               <option value='123'>Delegazione di Mathi</option>
               <option value='21'>Comitato Locale di Moncalieri</option>
               <option value='22'>Comitato Locale di Montanaro</option>
               <option value='23'>Comitato Locale di Nichelino</option>
               <option value='120'>Delegazione di Pianezza</option>
               <option value='121'>Delegazione di Pinerolo</option>
               <option value='26'>Comitato Locale di Piossasco</option>
               <option value='27'>Comitato Locale di Poirino</option>
               <option value='28'>Comitato Locale di Pont Canavese</option>
               <option value='29'>Comitato Locale di Rivarolo Canavese</option>
               <option value='30'>Comitato Locale di Rivoli</option>
               <option value='31'>Comitato Locale di San Francesco al Campo</option>
               <option value='32'>Comitato Locale di San Giorgio Canavese</option>
               <option value='33'>Comitato Locale di Santena</option>
               <option value='34'>Comitato Locale di Settimo Torinese</option>
               <option value='35'>Comitato Locale di Settimo Vittone</option>
               <option value='36'>Comitato Locale di Strambino</option>
               <option value='37'>Comitato Locale di Susa</option>
               <option value='38'>Comitato Locale di Torre Pellice</option>
               <option value='39'>Comitato Locale di Trofarello</option>
               <option value='122'>Delegazione di Val Della Torre</option>   
               <option value='40'>Comitato Locale di Vigone</option>
               <option value='41'>Comitato Locale di Villar Dora</option>
               </optgroup>
               <optgroup label="Provincia di Alessandria">
               <option value='42'>Comitato Locale di Alessandria</option>
               <option value='43'>Comitato Locale di Acqui Terme</option>
               <option value='44'>Comitato Locale di Monferrato</option>
               <option value='45'>Comitato Locale di Cassine</option> 
               <option value='46'>Comitato Locale di Gavi Ligure</option> 
               <option value='47'>Comitato Locale di Novi Ligure</option> 
               <option value='48'>Comitato Locale di Serravalle Scrivia</option> 
               <option value='49'>Comitato Locale di Tortona</option> 
               <option value='50'>Comitato Locale di Vignole Borbera</option>
               </optgroup> 
               <optgroup label="Provincia di Asti">
               <option value='51'>Comitato Locale di Asti</option> 
               <option value='52'>Comitato Locale di Canelli</option>
               <option value='53'>Comitato Locale di Castelnuovo Don Bosco</option>   
               <option value='54'>Comitato Locale di Cocconato</option> 
               <option value='55'>Comitato Locale di Isola D'Asti </option> 
               <option value='56'>Comitato Locale di Monale</option> 
               <option value='57'>Comitato Locale di Montegrosso D'Asti</option> 
               <option value='58'>Comitato Locale di San Damiano D'Asti</option> 
               <option value='59'>Comitato Locale di Villanova D'Asti</option>
               </optgroup> 
               <optgroup label="Provincia di Cuneo"> 
               <option value='63'>Comitato Locale Provincia Granda</option> 
               <option value='64'>Comitato Locale di Alba</option> 
               <option value='65'>Comitato Locale di Borgo San Dalmazzo</option> 
               <option value='66'>Comitato Locale di Bra</option> 
               <option value='67'>Comitato Locale di Busca</option> 
               <option value='68'>Comitato Locale di Caraglio</option> 
               <option value='69'>Comitato Locale di Centallo</option> 
               <option value='70'>Comitato Locale di Cuneo</option>
               <option value='71'>Comitato Locale di Dronero</option> 
               <option value='72'>Comitato Locale di Limone Piemonte</option> 
               <option value='73'>Comitato Locale di Melle</option> 
               <option value='74'>Comitato Locale di Mondovì</option> 
               <option value='75'>Comitato Locale di Monesiglio</option> 
               <option value='76'>Comitato Locale di Moretta</option> 
               <option value='77'>Comitato Locale di Peveragno</option> 
               <option value='78'>Comitato Locale di Racconigi</option>
               <option value='79'>Comitato Locale di Sanpeyre</option>   
               <option value='80'>Comitato Locale di Savigliano</option>
               <option value='81'>Comitato Locale di Sommariva Bosco</option>
               <option value='82'>Comitato Locale di Valle Stura</option>
               </optgroup> 
               <optgroup label="Provincia di Verbania">  
               <option value='90'>Comitato Locale di Verbania</option> 
               <option value='91'>Comitato Locale di Baveno</option>
               <option value='92'>Comitato Locale di Cannobio</option> 
               <option value='93'>Comitato Locale di Domodossola</option> 
               <option value='94'>Comitato Locale di Stresa</option>
               </optgroup>
            
               <optgroup label="Provincia di VerNoBi">  
               <option value='95'>Comitato Locale di Vercelli</option>  
               <option value='96'>Comitato Locale di Borgosesia</option> 
               <option value='97'>Comitato Locale di Crescentino</option> 
               <option value='98'>Comitato Locale di Gattinara</option> 
               <option value='84'>Comitato Locale di Arona</option> 
               <option value='85'>Comitato Locale di Borgomanero</option>
               <option value='86'>Comitato Locale di Galliate</option> 
               <option value='87'>Comitato Locale di Novara</option> 
               <option value='88'>Comitato Locale di Oleggio</option> 
               <option value='89'>Comitato Locale di Trecate</option>
               <option value='60'>Comitato Locale di Biella</option> 
               <option value='61'>Comitato Locale di Cavaglià</option> 
               <option value='62'>Comitato Locale di Cossato</option>
               </optgroup> 
            
            <!--
               <optgroup label="Comitati Regionali"> 
               <option value='99'>Comitato Regionale dell'Abruzzo</option> 
               <option value='100'>Comitato Regionale della Basilicata</option> 
               <option value='101'>Comitato Regionale della Calabria</option>
               <option value='102'>Comitato Regionale della Campania</option>
               <option value='103'>Comitato Regionale dell'Emilia Romagna</option>
               <option value='104'>Comitato Regionale del Friuli Venezia Giulia</option>
               <option value='105'>Comitato Regionale del Lazio</option>
               <option value='106'>Comitato Regionale della Liguria</option>
               <option value='107'>Comitato Regionale della Lombardia</option>
               <option value='108'>Comitato Regionale delle Marche</option>
               <option value='109'>Comitato Regionale del Molise</option>
               <option value='110'>Comitato Regionale del Piemonte</option>
               <option value='111'>Comitato Regionale della Puglia</option>
               <option value='112'>Comitato Regionale della Sardegna</option>
               <option value='113'>Comitato Regionale della Sicilia</option>
               <option value='114'>Comitato Regionale della Toscana</option>
               <option value='115'>Comitato Provinciale di Bolzano a valenza Regionale</option>
               <option value='116'>Comitato Provinciale di Trento a valenza Regionale</option>
               <option value='117'>Comitato Regionale dell'Umbria</option>
               <option value='118'>Comitato Regionale della Valle D'Aosta</option>
               <option value='119'>Comitato Regionale del Veneto</option>
               </optgroup>
            -->
              </select>
            </label>
            
            <br /><br /><br />
            
            <b>Dati del Delegato Area V </b> &nbsp&nbsp
            <br /><br />
            Nome&nbsp&nbsp
            <textarea style="overflow:hidden" cols="30" rows="1" name="nome_delegato"></textarea><br />
            <br />
            
            Cognome&nbsp&nbsp
            <textarea style="overflow:hidden" cols="30" rows="1" name="cognome_delegato"></textarea><br />
            <br />
            
            Recapito telefonico&nbsp&nbsp
            <textarea style="overflow:hidden" cols="11" rows="1" name="telefono_delegato"></textarea><br />
            <br />
            
            E-mail&nbsp&nbsp
            <textarea style="overflow:hidden" cols="25" rows="1" name="mail_delegato"></textarea><br />
            <br />
            <br />
            
            <input type="checkbox" name="checkbox-1" id="checkbox-1" class="chk" />
            <label for="checkbox-1">Confermo di aver preso visione del Regolamento per i Comitati Locali al Meeting Regionale (disponibile al <a class="ImLink" href="http://www.itempa.com/meeting2016/mobile/RegolamentoMeeting2016_Regionale.pdf" title="Regolamento Meeting 2016 Regionale">link</a>)</label>

            <input type="submit" id="submit" data-inline="true" value="Registra Preiscrizione" disabled/>


            <br />
            <br />
            </form>

        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>