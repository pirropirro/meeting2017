<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Conferma Preiscrizione</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.php" title="Meeting 2016 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		<p><strong>Conferma Preiscrizione</strong></p>

        <div data-html="true">

			<?
            include("config.inc.php");
            echo "<font color=#2B3856 size='3' face='Tahoma'>";
            
            $id_preiscrizione=$_GET['id'];
            $passwd=$_REQUEST['passwd'];
            $passwd=trim($passwd);
            
            $db = mysql_connect($db_host, $db_user, $db_password);
            if ($db == FALSE)
            die ("Errore nella connessione. Verificare i parametri nel file config.inc.php");
            mysql_select_db($db_name, $db)
            or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");
            
            //Recupero i dati della preiscrizione
            $query = "SELECT * FROM preiscrizioni WHERE id = '$id_preiscrizione'";
            $result = mysql_query($query, $db);
            $row = mysql_fetch_array( $result );
            $nome_comitato = $row[comitato];
            $id_comitato = $row[id_comitato];
            $nome_delegato = $row[nome_delegato];
            $cognome_delegato = $row[cognome_delegato];
            $mail_delegato = $row[mail_delegato];
            
            //recupero i dati del comitato
            $query = "SELECT * FROM comitati WHERE id = '$id_comitato'";
            $result = mysql_query($query, $db);
            $row = mysql_fetch_array( $result );
            $nome_comitato = $row[nome_comitato];
            $mail_comitato = $row[mail_comitato];
            $nome_presidente = $row[nome_presidente];
            $cognome_presidente = $row[cognome_presidente];
            $passwd_comitato = $row[passwd];
            
            if ($passwd_comitato == $passwd)
            {
                //Faccio l'update sulla tabella preiscrizioni
                mysql_select_db($db_name, $db)
                or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");
                $query = "UPDATE preiscrizioni SET conferma_presidente = '1' WHERE id=$id_preiscrizione";
                if (mysql_query($query, $db))
                {
                    echo "Hai confermato la partecipazione della squadra del $nome_comitato.<br>";
            
                    //Invio la mail di conferma
                    include('../PHPMailer/class.phpmailer.php');
                    //require_once('PHPMailer/class.smtp.php');  
            
                    $mittente = "meeting.2016@piemonte.cri.it";
                    $nomemittente = "Back Office Meeting 2016";
                    $destinatario = "$mail_comitato";
                    $ServerSMTP = "mailbox.cri.it"; //server SMTP
                    $oggetto = "Preiscrizione confermata per il Meeting 2016";
                    $corpo_messaggio = "Buongiorno Presidente $nome_presidente $cognome_presidente,\n\nla presente per notificare che la preiscrizione della squadra del $nome_comitato è stata confermata.\nIl volontario $nome_delegato $cognome_delegato che ha effettuato la preiscrizione è inserito in copia alla mail.\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016";
            
            
                    $msg = new PHPMailer;
					$msg->CharSet = "UTF-8";
					$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
                    //$msg->IsHTML(true);
                    $msg->SMTPAuth = true; // Autenticazione SMTP
                    $msg->SMTPKeepAlive = "true";
                    $msg->Host = $ServerSMTP;
                    $msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
                    $msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
            
                    $msg->From = $mittente;
                    $msg->FromName = $nomemittente;
                    $msg->AddAddress($destinatario); 
                    $msg->AddCC("$mail_delegato");
                    $msg->AddBCC("paolo.ditoma@libero.it");
                    $msg->Subject = $oggetto; 
                    $msg->Body = $corpo_messaggio;
            
                            
                    if(!$msg->Send())
                        {
                        echo "Si è verificato un errore nell'invio della mail per la conferma da parte del tuo presidente:".$msg->ErrorInfo;
                        }
                    else
                        {
                        echo "Abbiamo inviato una mail di notifica al presidente ed in copia al consigliere giovane che ha effettuato la preiscrizione.";
                        }
                    }
            else
                    {
                    echo "&nbsp&nbsp&nbspSi è verificato un errore durante la conferma, contatta l'amministratore";
                    }
            //endif;  
            mysql_close($db);
            
            
            }
            else
            {
            echo "Password errata";
            }
            
            
            
            
            
            $db = mysql_connect($db_host, $db_user, $db_password);
              if ($db == FALSE)
                die ("Errore nella connessione. Verificare i parametri nel file config.inc.php");
            
            //ricavo l'id
            mysql_select_db($db_name, $db)
            or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");
            $query = "SELECT * FROM preiscrizioni WHERE id='$id_preiscrizione'";
            $result = mysql_query($query, $db);
            $row = mysql_fetch_array($result);
            $nome_comitato= $row[nome_comitato];
            //echo "$comitato";
            
            
            
            ?>

        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>