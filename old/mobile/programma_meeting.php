<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Programma del Meeting</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
	<script type="text/javascript">
		$(document).on("change", "#checkbox-1", function () {
			if ($(this).prop("checked")) {
				$("#submit").button("enable");
			} else {
				$("#submit").button("disable");
			}
		});
    </script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2016 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		<p><strong>Programma</strong></p>

        <div data-html="true">

            <br /><br />

            <b>Domenica 23 ottobre</b><br /><br />
            Prima delle 7.00 la struttura è chiusa.<br />
            07.00 - 07.30  	Ingresso in struttura dello staff e degli espositori<br />
            07.45 - 08.30 	Ingresso in struttura dei giudici e simulatori (la registrazione avviene al Desk)<br />
            08.00 - 08.30 	Ingresso in struttura delle squadre ed osservatori (la registrazione avviene in Piazza)<br />
            08.00 - 09.00 	Visite agli stand in <i>Piazza Incontro</i><br />
            09.10 - 09.50 	Cerimonia di apertura del X Meeting Regionale<br />
            10.00 - 12.30	Prove del mattino*<br />
            12.30 - 14.30  	Turni per pausa pranzo*<br />
            14.45 - 16.20	Prove del pomeriggio*<br />
            16.20 - 16.35	Pausa<br />
            16.35 - 18.10	Prove del pomeriggio*<br />
            18.10 - 19.30	Cerimonia di chiusura Meeting Regionale<br />
            20.00 - 22.00	Smontaggio staff <br /><br /><br />

			
            <b>N.B. </b>Consigliamo caldamente l'arrivo al mattino entro le ore 08.00  e non prima delle 7.30! <a href="come_arrivare.php" title="">Indicazioni per la struttura</a>
			<br /><br />
			
			<b>* </b><i>Gli orari riportati sono puramente indicativi, fanno fede quelli ricevuti personalmente via mail e quelli comunicati dai Tutor</i>
 
            <br /><br />
            <br /><br />
            
        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>