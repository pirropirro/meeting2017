<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Iscrizione personale extra</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
	<script type="text/javascript">
		$(document).on("change", "#checkbox-1", function () {
			if ($(this).prop("checked")) {
				$("#submit").button("enable");
			} else {
				$("#submit").button("disable");
			}
		});
    </script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2016 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		<p><strong>Iscrizione personale extra</strong></p>

        <div data-html="true">

			<?php
            include ("config.inc.php");
            include ("apri_db.php");
            ?>
			
            <form name="form1" enctype="multipart/form-data" method="post" action="salva_inserisci_iscrizione_extra.php">
            
			<br />
            <label>Afferente al<br>
             
            <select name="id_comitato" size="8" >
            
				<?php
				$query = "SELECT 	c.nome_comitato AS nome_comitato,
									c.id AS id_comitato
									FROM comitati AS c
									ORDER BY tipo_meeting DESC";
			
				$result = mysql_query($query, $db);
				while($row = mysql_fetch_array( $result )) 
				{
					?>
					<option value='<?php echo $row[id_comitato]; ?>'><?php echo $row[nome_comitato]; ?></option>
					<?php
				}
				mysql_close($db);
				?>
            </select>
            </label>
						
			<fieldset data-role="controlgroup" data-type="horizontal"  data-mini="true">
				<legend>Ruolo</legend>
				<input name="ruolo" id="simulatore" value="simulatore" type="radio">
				<label for="simulatore">Simulatore</label>
				<input name="ruolo" id="truccatore" value="truccatore" type="radio">
				<label for="truccatore">Truccatore</label>
				<input name="ruolo" id="giudice" value="giudice" type="radio">
				<label for="giudice">Giudice</label>
				<input name="ruolo" id="ospite" value="ospite" type="radio">
				<label for="ospite">Ospite</label>
				<input name="ruolo" id="staff" value="staff" checked="checked" type="radio">
				<label for="staff">Staff</label>
				<input name="ruolo" id="coordinamento" value="coordinamento" type="radio">
				<label for="coordinamento">Coordinamento</label>
			</fieldset>
			<br />


			<label for="nome">Nome</label>
			<input type="text" name="nome" size="20" maxlength="40" placeholder="Campo obbligatorio" required>

			<label for="cognome">Cognome</label>
			<input type="text" name="cognome" size="20" maxlength="40" placeholder="Campo obbligatorio" required>
			
			<label for="telefono">Recapito telefonico</label>						
			<input type="tel" name="telefono" size="8" required title="prefisso e numero senza spazi o altri caratteri (max 10 cifre)" placeholder="prefisso e numero senza spazi o altri caratteri (max 10 cifre)"pattern="[0-9]{10}">
			
			<label for="mail">Indrizzo e-mail</label>
			<input type="email" name="mail" size="28" placeholder="Campo obbligatorio" required>

			<label for="esigenze">Esigenze alimentari</label>
			<input type="text" name="esigenze" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario">

			<label for="disabilita">Eventuali disabilità</label>
			<input type="text" name="disabilita" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario">
					
           
			<br />
			<p align="left">
			<input type="submit" value="Inserisci" />
			</form>
            
        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>