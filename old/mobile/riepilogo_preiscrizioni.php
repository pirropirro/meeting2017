<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice') && !($check['team'] == 'organigramma') && !($check['team'] == 'nazionale'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato all'organizzazione od al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2015 - Riepilogo Preiscrizioni</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2015 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2015 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2015</h1>
		

		<?php
		include ("config.inc.php");
		echo "<font color=#2B3856 size='2'>";

		include ("apri_db.php");
		?>

		<p><strong>Elenco preiscrizioni</strong></p>
		
   		<table data-role="table" id="table-column-toggle" data-mode="columntoggle" class="ui-responsive table-stroke">
     <thead>
       <tr>
         <th>Comitato</th>
         <th data-priority="1">Nome Cognome</th>
         <th data-priority="7">Data richiesta</th>
         <th data-priority="8">Data conferma</th>
         <th data-priority="3">Mail</th>
         <th data-priority="3">Telefono</th>
     </thead>
     <tbody>        
            
			<?php
            $query = "SELECT 	c.nome_comitato AS nome_comitato,
								p.nome_delegato AS nome_delegato,
								p.cognome_delegato AS cognome_delegato,
								p.mail_delegato AS mail_delegato,
								p.telefono_delegato AS telefono_delegato,
								DATE_FORMAT(p.data_inserimento, '%d-%m') AS data_inserimento,
								DATE_FORMAT(p.data_conferma, '%d-%m') AS data_conferma 
								FROM preiscrizioni AS p
								INNER JOIN comitati AS c
								ON p.id_comitato = c.id
								ORDER BY p.data_conferma DESC";
            $result = mysql_query($query, $db);
            while($row = mysql_fetch_array( $result )) 
            {
				?>
				<tr>
                <th><?php echo "$row[nome_comitato]"?></th>
                <td><?php echo "$row[nome_delegato] $row[cognome_delegato]"?></td>
                <td><?php echo "$row[data_inserimento]"?></td>
                <td><?php echo "$row[data_conferma]"?></td>
                <td><?php echo "$row[mail_delegato]"?></td>
                <td><?php echo "$row[telefono_delegato]"?></td>
                </tr>
                <?php
            }
            mysql_close($db);
            ?>
    </tbody>
   </table>   

	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>