<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Contattaci</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
	<script type="text/javascript">
		$(document).on("change", "#checkbox-1", function () {
			if ($(this).prop("checked")) {
				$("#submit").button("enable");
			} else {
				$("#submit").button("disable");
			}
		});
    </script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.php" title="Meeting 2016 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		<p><strong>Contattaci</strong></p>

        <div data-html="true">

			<?php
			$nome			=$_REQUEST['nome'];
			$mail			=$_REQUEST['mail'];
			$oggetto		=$_REQUEST['oggetto'];
			$richiesta		=$_REQUEST['richiesta'];

			if (trim($nome) == "" ):
				echo "Non hai inserito il tuo nome";
				elseif (trim($mail) == "") :
					echo "Non hai inserito la tua mail";
					elseif (trim($oggetto) == "") :
						echo "Non hai selezionato l'oggetto";
						elseif (trim($richiesta) == "") :
							echo "Non hai inserito la tua richiesta";
								else:
									$nome = addslashes(stripslashes($nome)); 
									$nome = str_replace("<", "&lt;", $nome);
									$nome = str_replace(">", "&gt;", $nome);
									$mail = addslashes(stripslashes($mail)); 
									$mail = str_replace("<", "&lt;", $mail);
									$mail = str_replace(">", "&gt;", $mail);
									$richiesta = addslashes(stripslashes($richiesta)); 
									$richiesta = str_replace("<", "&lt;", $richiesta);
									$richiesta = str_replace(">", "&gt;", $richiesta);
				
									include('PHPMailer/class.phpmailer.php');
									//require_once('PHPMailer/class.smtp.php');  
				
									$mittente = "meeting.2016@piemonte.cri.it";
									$nomemittente = "$nome - Portale";
									$destinatario = "meeting.2016@piemonte.cri.it";
									$ServerSMTP = "mailbox.cri.it"; //server SMTP
									$oggetto = "[Contattaci] - $nome - $oggetto";
									$corpo_messaggio = "$richiesta\n\nRispondere a $nome all'indirizzo $mail\n\n\nInviata automaticamente dal portale";
				
									$msg = new PHPMailer;
									$msg->CharSet = "UTF-8";
									$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
									//$msg->IsHTML(true);
									$msg->SMTPAuth = true; // Autenticazione SMTP
									$msg->SMTPKeepAlive = "true";
									$msg->Host = $ServerSMTP;
									$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
									$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
				
									$msg->From = $mittente;
									$msg->FromName = $nomemittente;
									$msg->AddAddress($destinatario); 
				//					$msg->AddCC("paolo.ditoma@libero.it");
									$msg->AddBCC("paolo.ditoma@libero.it");
									$msg->Subject = $oggetto; 
									$msg->Body = $corpo_messaggio;
				
				
									if(!$msg->Send())
									{
										echo "Si è verificato un errore nell'invio della mail contatta l'amministratore:".$msg->ErrorInfo;
									}
									else
									{
										echo "Abbiamo inviato una mail con la tua richiesta al Back Office, che ti risponderà al più presto.<br><br>Grazie $nome per aver utilizzato il portale.";
									}
				
			endif; // chiude la verifica della presenza dei dati
			?>

			<br /><br />
			<form method="post" action="index.php">
			<input type='submit' value='Torna alla Home Page'></form>
        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>