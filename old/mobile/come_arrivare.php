<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Come arrivare</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
	<script type="text/javascript">
		$(document).on("change", "#checkbox-1", function () {
			if ($(this).prop("checked")) {
				$("#submit").button("enable");
			} else {
				$("#submit").button("disable");
			}
		});
    </script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2016 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		<p><strong>Come arrivare</strong></p>

        <div data-html="true">

		
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2821.42680820065!2d7.663205815772361!3d44.995953972585255!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478812656f84d02d%3A0x891c2109f0125b3b!2sVia+Bertero%2C+2%2C+10024+Moncalieri+TO!5e0!3m2!1sit!2sit!4v1476694395827" frameborder="0" style="border:0" allowfullscreen></iframe><br /><br />

			<b>Con mezzo proprio:</b> raggiungere la destinazione <b>Scuola Principessa Maria Clotilde (Istituto comprensivo Santa Maria), Via Bertero, 2, Moncalieri (TO)</b><br /><br />

			<b>In treno:</b> raggiungere la stazione FS di <b>Porta Nuova</b> o <b>Porta Susa</b><br /><br />
				
			<b>In aereo:</b> raggiungere l'<b>Aeroporto internazionale di Torino-Caselle "Sandro Pertini"</b><br /><br /><br />
			
			Lo staff del Meeting si occuperà di garantire il servizio navetta da e verso le stazioni e l'aeroporto per le squadre provenienti dalle altre Regioni.<br />
			<b>N.B.</b> Si raccomanda di comunicare gli orari di partenza/arrivo all'indirizzo <a href="mailto:meeting.2016@piemonte.cri.it?subject=Indicazioni arrivo/partenza da Portale" title="">meeting.2016@piemonte.cri.it</a>
        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>