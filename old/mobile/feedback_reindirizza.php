<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2014 - Reindirizza feedback</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
</head>
<body>

<?php
$id=$_GET['id'];
$ruolo=$_REQUEST['ruolo'];

switch ($ruolo) {
    case 'partecipante':
		?>
		<meta http-equiv="refresh" content="0;URL='feedback_prova_scritta.php?ruolo=<?php echo $ruolo ?>&id=<?php echo $id ?>'">
		<?php	
        break;
    case 'osservatore':
		?>
		<meta http-equiv="refresh" content="0;URL='feedback_quota_tutor.php?ruolo=<?php echo $ruolo ?>&id=<?php echo $id ?>'">
		<?php	
        break;
    case 'simulatore':
		?>
		<meta http-equiv="refresh" content="0;URL='feedback_simulatori.php?ruolo=<?php echo $ruolo ?>&id=<?php echo $id ?>'">
		<?php	
        break;
    case 'giudice':
		?>
		<meta http-equiv="refresh" content="0;URL='feedback_giudici.php?ruolo=<?php echo $ruolo ?>&id=<?php echo $id ?>'">
		<?php	
        break;
    case 'staff':
		?>
		<meta http-equiv="refresh" content="0;URL='feedback_staff.php?ruolo=<?php echo $ruolo ?>&id=<?php echo $id ?>'">
		<?php	
        break;
    case 'ospite':
		?>
		<meta http-equiv="refresh" content="0;URL='feedback_generale_ospitante.php?ruolo=<?php echo $ruolo ?>&id=<?php echo $id ?>'">
		<?php	
        break;
    case 'nonindicato':
		?>
		<meta http-equiv="refresh" content="0;URL='feedback_generale_ospitante.php?ruolo=<?php echo $ruolo ?>&id=<?php echo $id ?>'">
		<?php	
        break;
    default:
		?>
		<meta http-equiv="refresh" content="0;URL='feedback_generale_ospitante.php?ruolo=<?php echo $ruolo ?>&id=<?php echo $id ?>'">
		<?php	
}
?>
</body>
</html>