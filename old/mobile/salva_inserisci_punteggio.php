<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a giudice votante o coordinatore o tecnico
if (!($check['team'] == 'giudice') && !($check['team'] == 'coordinatore'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per inserire il punteggio, è un compito riservato a giudici, coordinatori ed al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Inserisci punteggio</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
	<script type="text/javascript">
		$(document).on("change", "#checkbox-1", function () {
			if ($(this).prop("checked")) {
				$("#submit").button("enable");
			} else {
				$("#submit").button("disable");
			}
		});
    </script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2016 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		<p><strong>Inserisci punteggio</strong></p>

        <div data-html="true">

			<?
			include ("config.inc.php");
			include ("apri_db.php");
            $fp = fopen ("log_iscrizioni.txt",a);
            
            $campo_autorizzato=$_POST['campo_autorizzato'];
            $id_comitato=$_POST['id_comitato'];
            //recupero il nome del comitato
			$query_comitato = "SELECT nome_comitato FROM comitati WHERE id = $id_comitato";
			$result_comitato = mysql_query($query_comitato, $db);
			$row_comitato = mysql_fetch_array($result_comitato);
			$nome_comitato = $row_comitato[nome_comitato];
			$area_punteggio = substr($campo_autorizzato,0,1);
			$intero=$_REQUEST['intero'];
            $decimale=$_REQUEST['decimale'];
            $punteggio = (float)"$intero.$decimale";
            //echo "Il punteggio che registro è $punteggio<br>";
            
            
            //Faccio l'update senza alcun controllo (non c'è modo di avere campi non valorizzati
            $query = "UPDATE preiscrizioni SET $campo_autorizzato = '$punteggio' WHERE id_comitato = '$id_comitato'";
            if (mysql_query($query, $db))
            {
                //Sostituire TEST con $nome_comitato
                echo "Hai inserito per la squadra di $nome_comitato il punteggio $punteggio come $check[team] della prova area $area_punteggio. <br><br>";
                fwrite($fp,date('d/m/Y H:i:s').' - '."Inserimento PUNTEGGIO per $nome_comitato di valore $punteggio effettuata da $check[uname]."."\r\n");
            }
            else
            {
                echo "Si è verificato un errore durante l'inserimento del punteggio, rivolgersi immediatamente alla Control Room.<br><br>";
                fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Inserimento PUNTEGGIO per $nome_comitato di valore $punteggio effettuata da $check[uname]: ".$msg->ErrorInfo."\r\n");
            }	
            
            fclose($fp);
            mysql_close($db);
			
			//RICORDATI DI CAMBIARE LA DESTINAZIONE DEL FORM A INSERISCI_PUNTEGGIO.PHP ENTRO SABATO SERA
			
			?>
            
            <br /><br />
            
            <form method="post" action="inserisci_punteggio.php">
            <input type="submit" value="Inserisci un altro punteggio" />
            </form>

        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>