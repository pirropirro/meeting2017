<li data-filtertext="meeting homepage" data-icon="home"><a href="index.php">Home</a></li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Autenticazione<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="accedi login autentica"><a href="login.php" data-ajax="false">Accedi</a></li>
			<li data-filtertext="password passwd"><a href="chgpwd.php" data-ajax="false">Cambia Password</a></li>
			<li data-filtertext="esci logout"><a href="logout.php" data-ajax="false">Esci</a></li>
		</ul>
	</div>
</li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Preiscrizioni<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="inserisci iscrizione preiscrizione preiscrizioni preiscrizioni iscrizioni registra registrazione"><a href="spiega_preiscrizione.php" data-ajax="false">Inserisci Preiscrizione</a></li>
			<li data-filtertext="elenco riepilogo consulta preiscrizione preiscrizioni preiscrizioni iscrizioni registra registrazione"><a href="riepilogo_preiscrizioni.php" data-ajax="false">Elenco Preiscrizioni (BO)</a></li>
		</ul>
	</div>
</li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Iscrizioni<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="inserisci iscrizione preiscrizione preiscrizioni iscrizioni registra registrazione squadra"><a href="spiega_iscrizione_squadra.php" data-ajax="false">Inserisci Squadra</a></li>
			<li data-filtertext="inserisci iscrizione preiscrizione preiscrizioni iscrizioni registra registrazione osservatore giudice giudici simulatore simulatori truccatore truccatori"><a href="spiega_iscrizione_osservatore.php" data-ajax="false">Inserisci Osservatore con squadra</a></li>
			<li data-filtertext="inserisci iscrizione preiscrizione preiscrizioni iscrizioni registra registrazione osservatore giudice giudici simulatore simulatori truccatore truccatori"><a href="spiega_iscrizione_osservatore_senza_squadra.php" data-ajax="false">Inserisci Osservatore senza squadra</a></li>
			<li data-filtertext="inserisci iscrizione preiscrizione preiscrizioni iscrizioni registra registrazione staff ospiti ospite"><a href="inserisci_iscrizione_extra.php" data-ajax="false">Inserisci Extra (Back Office)</a></li>
			<li data-filtertext="elenco riepilogo consulta preiscrizione preiscrizioni iscrizioni iscrizione registra registrazione"><a href="consulta_iscrizione_passwd_richiesta.php" data-ajax="false">Consulta Iscrizione</a></li>
		</ul>
	</div>
</li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Meeting<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="programma orari meeting registrazioni domenica partenza arrivo squadra squadre"><a href="programma_meeting.php" data-ajax="false">Programma del Meeting</a></li>
			<li data-filtertext="programma orari ingresso istruzioni meeting registrazioni domenica partenza arrivo squadra squadre partecipanti osservatori simulatori giudici partecipante osservatore simulatore giudice"><a href="istruzioni_ingresso.php" data-ajax="false">Istruzioni per l'ingresso</a></li>
			<li data-filtertext="accoglienza registrazioni domenica arrivo squadra squadre"><a href="accoglienza_squadra.php" data-ajax="false">Accoglienza Squadra (Back Office)</a></li>
			<li data-filtertext="check-in check-out tutor"><a href="checkin_checkout.php" data-ajax="false">Check-in Check-out (Tutor)</a></li>
			<li data-filtertext="giudice giudice voto prova"><a href="inserisci_punteggio.php" data-ajax="false">Inserisci punteggio (Giudici e Coordinatori)</a></li>						<li data-filtertext="questionario"><a href="questionario.php" data-ajax="false">Questionario</a></li>
			<li data-filtertext="classifica parziale punteggio vince risultati prova"><a href="classifica_parziale.php" data-ajax="false">Classifica Parziale</a></li>
			<li data-filtertext="classifica finale punteggio vincitori vincitrice vince risultati prova prove provinciale regionale"><a href="classifica_finale.php" data-ajax="false">Classifica Finale</a></li>
			</ul>
	</div>
</li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Documenti<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="regolamento consulta regionale provinciale orari giudici regole"><a href="RegolamentoMeeting2016_Regionale.pdf" data-ajax="false">Regolamento 2016 Regionale</a></li>
		</ul>
	</div>
</li>
<!--
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Service<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="inserisci materiale richiesta organizzazione elenco esigenze"><a href="inserisci_materiale.php" data-ajax="false">Inserisci richiesta materiale</a></li>
			<li data-filtertext="consulta materiale richiesta organizzazione elenco esigenze"><a href="riepilogo_materiale.php" data-ajax="false">Consulta richieste materiali</a></li>
		</ul>
	</div>
</li>
-->
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Contatti<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="inserisci materiale richiesta organizzazione elenco esigenze"><a href="contattaci.php" data-ajax="false">Contattaci</a></li>
			<li data-filtertext="consulta materiale richiesta organizzazione elenco esigenze"><a href="come_arrivare.php" data-ajax="false">Come arrivare</a></li>
		</ul>
	</div>
</l