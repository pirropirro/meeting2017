<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a giudice votante o coordinatore o tecnico
if (!($check['team'] == 'giudice') && !($check['team'] == 'coordinatore'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per inserire il punteggio, è un compito riservato a giudici, coordinatori ed al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2015 - Inserisci punteggio giudice</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
	<script type="text/javascript">
		$(document).on("change", "#checkbox-1", function () {
			if ($(this).prop("checked")) {
				$("#submit").button("enable");
			} else {
				$("#submit").button("disable");
			}
		});
    </script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2015 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2015 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2015</h1>

		<p><strong>Inserisci punteggio giudice</strong></p>

        <div data-html="true">

			<?php
			$giudice = $check['level'];
			$gruppo = $check['team'];
			$stringa_livello = (string) $check['level'];
			$area_punteggio = substr($stringa_livello,0,1);
			//echo"$area_punteggio";
			$desinenza_punteggio = substr($check['team'],0,1);
			$campo_autorizzato ="d" . "$area_punteggio" . "_" . "$desinenza_punteggio";
			//echo "Campo autorizzato: $campo_autorizzato<br>";

			?>
			<form id="inserisci_punteggio" method='post' enctype="multipart/form-data" action="salva_inserisci_punteggio.php">
			<?php

			echo "Stai inserendo il punteggio per la prova area $area_punteggio come $gruppo<br><br>";
			include ("config.inc.php");
			include ("apri_db.php");

			?>

			<p>Squadra di</p>
			<select name="id_comitato">

			<?php
			if ($gruppo == 'coordinatore')
			{
				//seleziono tutte le squadre nazionali
				$query = "SELECT 	c.id AS id_comitato,
									c.nome_comitato AS nome_comitato
									FROM comitati AS c
									INNER JOIN preiscrizioni AS p
									ON p.id_comitato = c.id
									WHERE p.$campo_autorizzato IS NULL
									AND p.iscrizione = '1'";
			}
			elseif ($gruppo == 'giudice')
			{
				//Seleziono i comitati che hanno il giudiceX uguale al livello dell'utente ed il relativo campo autorizzato vuoto
				$query = "SELECT 	c.id AS id_comitato,
									c.nome_comitato AS nome_comitato
									FROM timesheet AS t
									INNER JOIN comitati AS c
									ON c.id = t.id_comitato
									INNER JOIN preiscrizioni AS p
									ON p.id_comitato = t.id_comitato
									WHERE p.$campo_autorizzato IS NULL
									AND t.giudice = $giudice";
			}
			else
			{
				echo "Non so chi sei! Rivolgiti immediatamente al Back Office... ma di corsa!";
			}
			$result = mysql_query($query, $db);
			while($row = mysql_fetch_array( $result )) 
			{
				?>
                <option value="<?= $row[id_comitato] ?>"><?= $row[nome_comitato] ?></option>
                <?php
			}
			mysql_close($db);	
			?>
			</select>
			<br /><br />

            <fieldset data-role="controlgroup" data-type="horizontal">
                <legend>Punteggio</legend>
                <label for="intero">Intero</label>
                <select name="intero" id="intero">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
                <div style="float:left; margin-left:5px; margin-right:5px; font-size:25px;">
				,
				</div>
				<label for="decimale">Decimale</label>
                <select name="decimale" id="decimale">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
            </fieldset>
            <br /><br />
            
			<input type="submit" value="Inserisci" />
			<input type="hidden" name="campo_autorizzato" value="<?= $campo_autorizzato ?>" />
			<input type="hidden" name="nome_comitato" value="<?= $nome_comitato ?>" />
			</form>

        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>