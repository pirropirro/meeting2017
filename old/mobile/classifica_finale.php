<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Inserisci punteggio</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
	<script type="text/javascript">
		$(document).on("change", "#checkbox-1", function () {
			if ($(this).prop("checked")) {
				$("#submit").button("enable");
			} else {
				$("#submit").button("disable");
			}
		});
    </script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2016 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>
		
		<p><strong>Classifica finale</strong></p>

        <div data-html="true">
		
		
		<?php
		include ("config.inc.php");
		echo "<font color=#2B3856 size='2'>";

		$data_pubblicazione = strftime('201610231930');

		$data_attuale = strftime('%Y%m%d%H%M', time());

		if ($data_pubblicazione <= $data_attuale)
		{
			$db = mysql_connect($db_host, $db_user, $db_password);
			if ($db == FALSE)
			die ("Errore nella connessione. Verificare i parametri nel file config.inc.php");
			mysql_select_db($db_name, $db)
			or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");
			
			?>

		
   		<table data-role="table" id="table-column-toggle" data-mode="columntoggle" class="ui-responsive table-stroke">
     <thead>
       <tr>
         <th data-priority="1">Posizione</th>
         <th>Squadra</th>
         <th data-priority="3">Punteggio</th>
	  </tr>
     </thead>
     <tbody>        
            
	
			<?php
			$posizione = 1;
			
			$query = "SELECT 	ROUND(((p.punteggio_finale + p.Q_punteggio - p.penalita_assenza) * p.penalita_over), 2) AS punteggio_finale,
													c.nome_comitato AS nome_comitato
													FROM preiscrizioni AS p
													INNER JOIN comitati AS c
													ON c.id = p.id_comitato
													WHERE p.iscrizione = '1'
													ORDER BY punteggio_finale DESC";
			$result = mysql_query($query, $db);
			while($row = mysql_fetch_array( $result )) 
			{
				$re = "/(\\s*comitato locale di\\s*|\\s*comitato provinciale di\\s*|\\s*comitato regionale\\s*|\\s*delegazione di\\s*|\\s*a valenza regionale\\s*|\\s*comitato\\s*)/i"; 
				$subst = ""; 	 
				$comitato_trunc = preg_replace($re, $subst, $row[nome_comitato]);
				?>
				<tr>
				<th><?php echo $posizione ?></th>
				<td><?php echo $comitato_trunc ?></td>
				<td><?php echo $row[punteggio_finale] ?></td>
				</tr>
				<?php
				$posizione ++;
			}
			mysql_close($db);
			?>
    </tbody>
   </table>   
			<?php
		}
		else
		{
			echo "La classifica finale sarà consultabile dalle ore 19.30 del 23 ottobre 2016. Saranno inseriti anche i rispettivi punteggi.";
		}

?>



        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>