<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2015 - Feedback Salvataggio Dati</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.php" title="Meeting 2015 - Homepage"><img src="logo_meeting.png" alt="Portale Meeting 2015 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2015</h1>
		
		<p><strong>Salva i dati inseriti</strong></p>

		<?
		include("config.inc.php");

        //recupero i dati nella barra dell'indirizzo
        $ruolo=$_GET['ruolo'];
        $id=$_GET['id'];
        //echo "Ruolo                    (tutti):                     $ruolo <br>";
        //echo "ID          (tutti):           $id <br>";
        
        //recupero i dati passati in hidden dal form precedente
		$valutazione_accoglienza_venerdi = $_REQUEST['valutazione_accoglienza_venerdi'];
		$valutazione_speed_meeting = $_REQUEST['valutazione_speed_meeting'];
		
        $valutazione_quota_partecipante = $_POST['valutazione_quota_partecipante'];
        $valutazione_quota_osservatore = $_POST['valutazione_quota_osservatore'];
        $valutazione_tutor_competenza = $_POST['valutazione_tutor_competenza'];
        $valutazione_tutor_disponibilita = $_POST['valutazione_tutor_disponibilita'];
       
		$valutazione_gradimento_prova1=$_POST['valutazione_gradimento_prova1'];
		$valutazione_formativa_prova1=$_POST['valutazione_formativa_prova1'];
		$valutazione_difficolta_prova1=$_POST['valutazione_difficolta_prova1'];
		$valutazione_lavoro_gruppo_prova1=$_POST['valutazione_lavoro_gruppo_prova1'];
		$valutazione_performance_giudice_prova1=$_POST['valutazione_performance_giudice_prova1'];
		
		$valutazione_gradimento_prova2=$_POST['valutazione_gradimento_prova2'];
		$valutazione_formativa_prova2=$_POST['valutazione_formativa_prova2'];
		$valutazione_difficolta_prova2=$_POST['valutazione_difficolta_prova2'];
		$valutazione_lavoro_gruppo_prova2=$_POST['valutazione_lavoro_gruppo_prova2'];
		$valutazione_performance_giudice_prova2=$_POST['valutazione_performance_giudice_prova2'];
		$valutazione_interpretazione_simulatore_prova2 = $_POST['valutazione_interpretazione_simulatore_prova2'];
		$valutazione_utilita_simulatore_prova2 = $_POST['valutazione_utilita_simulatore_prova2'];	
		
		$valutazione_gradimento_prova3=$_POST['valutazione_gradimento_prova3'];
		$valutazione_formativa_prova3=$_POST['valutazione_formativa_prova3'];
		$valutazione_difficolta_prova3=$_POST['valutazione_difficolta_prova3'];
		$valutazione_lavoro_gruppo_prova3=$_POST['valutazione_lavoro_gruppo_prova3'];
		$valutazione_performance_giudice_prova3=$_POST['valutazione_performance_giudice_prova3'];
		$valutazione_interpretazione_simulatore_prova3 = $_POST['valutazione_interpretazione_simulatore_prova3'];
		$valutazione_utilita_simulatore_prova3 = $_POST['valutazione_utilita_simulatore_prova3'];
		
		$valutazione_gradimento_prova4=$_POST['valutazione_gradimento_prova4'];
		$valutazione_formativa_prova4=$_POST['valutazione_formativa_prova4'];
		$valutazione_difficolta_prova4=$_POST['valutazione_difficolta_prova4'];
		$valutazione_lavoro_gruppo_prova4=$_POST['valutazione_lavoro_gruppo_prova4'];
		$valutazione_performance_giudice_prova4=$_POST['valutazione_performance_giudice_prova4'];
		$valutazione_interpretazione_simulatore_prova4 = $_POST['valutazione_interpretazione_simulatore_prova4'];
		$valutazione_utilita_simulatore_prova4 = $_POST['valutazione_utilita_simulatore_prova4'];	
		
		$valutazione_gradimento_prova5=$_POST['valutazione_gradimento_prova5'];
		$valutazione_formativa_prova5=$_POST['valutazione_formativa_prova5'];
		$valutazione_difficolta_prova5=$_POST['valutazione_difficolta_prova5'];
		$valutazione_lavoro_gruppo_prova5=$_POST['valutazione_lavoro_gruppo_prova5'];
		$valutazione_performance_giudice_prova5=$_POST['valutazione_performance_giudice_prova5'];
		$valutazione_interpretazione_simulatore_prova5 = $_POST['valutazione_interpretazione_simulatore_prova5'];
		$valutazione_utilita_simulatore_prova5 = $_POST['valutazione_utilita_simulatore_prova5'];
		
		$valutazione_gradimento_prova6=$_POST['valutazione_gradimento_prova6'];
		$valutazione_formativa_prova6=$_POST['valutazione_formativa_prova6'];
		$valutazione_difficolta_prova6=$_POST['valutazione_difficolta_prova6'];
		$valutazione_lavoro_gruppo_prova6=$_POST['valutazione_lavoro_gruppo_prova6'];
		$valutazione_performance_giudice_prova6=$_POST['valutazione_performance_giudice_prova6'];
		$valutazione_interpretazione_simulatore_prova6 = $_POST['valutazione_interpretazione_simulatore_prova6'];
		$valutazione_utilita_simulatore_prova6 = $_POST['valutazione_utilita_simulatore_prova6'];
		
		$valutazione_gradimento_prova7=$_POST['valutazione_gradimento_prova7'];
		$valutazione_formativa_prova7=$_POST['valutazione_formativa_prova7'];
		$valutazione_difficolta_prova7=$_POST['valutazione_difficolta_prova7'];
		$valutazione_lavoro_gruppo_prova7=$_POST['valutazione_lavoro_gruppo_prova7'];
		$valutazione_performance_giudice_prova7=$_POST['valutazione_performance_giudice_prova7'];
		
		$valutazione_gradimento_prova8=$_POST['valutazione_gradimento_prova8'];
		$valutazione_formativa_prova8=$_POST['valutazione_formativa_prova8'];
		$valutazione_difficolta_prova8=$_POST['valutazione_difficolta_prova8'];
		$valutazione_lavoro_gruppo_prova8=$_POST['valutazione_lavoro_gruppo_prova8'];
		$valutazione_performance_giudice_prova8=$_POST['valutazione_performance_giudice_prova8'];		
		
		$prova_assegnata=$_POST['prova_assegnata'];
		$valutazione_prova_assegnata=$_POST['valutazione_prova_assegnata'];
		$valutazione_performance_giudice_prova=$_POST['valutazione_performance_giudice_prova'];
		
		$valutazione_inserimento_voti_portale=$_POST['valutazione_inserimento_voti_portale'];
		$valutazione_coordinamento_backoffice=$_POST['valutazione_coordinamento_backoffice'];
		$prova_assegnata=$_POST['prova_assegnata'];
		$valutazione_prova_assegnata=$_POST['valutazione_prova_assegnata'];
		$valutazione_interpretazione_simulatore_prova=$_POST['valutazione_interpretazione_simulatore_prova'];
		$valutazione_utilita_simulatore_prova=$_POST['valutazione_utilita_simulatore_prova'];
		
		$valutazione_preparazione=$_POST['valutazione_preparazione'];
		$valutazione_soddisfazione_incarico=$_POST['valutazione_soddisfazione_incarico'];
		$valutazione_mezzi_informazioni=$_POST['valutazione_mezzi_informazioni'];
		$valutazione_coordinamento_backoffice=$_POST['valutazione_coordinamento_backoffice'];
		
		$valutazione_gruppo_lavoro 			 =$_POST['valutazione_gruppo_lavoro'];
		$valutazione_informazioni_prova 	 =$_POST['valutazione_informazioni_prova'];
		$valutazione_abbandono_simulatori	 =$_POST['valutazione_abbandono_simulatori'];

		$valutazione_adeguatezza_struttura=$_POST['valutazione_adeguatezza_struttura'];
		$valutazione_raggiugngibilita_struttura=$_POST['valutazione_raggiugngibilita_struttura'];
		$valutazione_efficienza_mensa=$_POST['valutazione_efficienza_mensa'];
		$valutazione_qualita_cibo=$_POST['valutazione_qualita_cibo'];
		
		$valutazione_accoglienza=$_POST['valutazione_accoglienza'];
		$valutazione_tempistiche=$_POST['valutazione_tempistiche'];
		$valutazione_distribuzione_pause=$_POST['valutazione_distribuzione_pause'];
		$valutazione_disponibilita_staff=$_POST['valutazione_disponibilita_staff'];
		$valutazione_portaleweb=$_POST['valutazione_portaleweb'];	

		$valutazione_accoglienza_venerdi=$_POST['valutazione_accoglienza_venerdi'];	
		$valutazione_speed_meeting=$_POST['valutazione_speed_meeting'];	
		$valutazione_pernottamento=$_POST['valutazione_pernottamento'];	
		$valutazione_pasti=$_POST['valutazione_pasti'];	
		$valutazione_logistica=$_POST['valutazione_logistica'];	
		$valutazione_tour=$_POST['valutazione_tour'];	
		$valutazione_piazza=$_POST['valutazione_piazza'];	

		//recupero i dati di input del form precedente feedback_generale_complessivo.php
		$valutazione_festa_finale=$_REQUEST['valutazione_festa_finale'];
		$valutazione_meeting=$_REQUEST['valutazione_meeting'];
		$partecipazione_prossimo_meeting=$_REQUEST['partecipazione_prossimo_meeting'];
		$ruolo_prossimo_meeting=$_REQUEST['ruolo_prossimo_meeting'];
	
		
		$commenti=$_REQUEST['commenti'];
		$commenti = addslashes(stripslashes($commenti)); 
		$commenti = str_replace("<", "&lt;", $commenti);
		$commenti = str_replace(">", "&gt;", $commenti);

/* 		echo "Valutazione accoglienza Venerdì: $valutazione_accoglienza_venerdi <br>";
		echo "Valutazione Speed Meeting: $valutazione_speed_meeting <br>";
		
		echo "Valutazione quota partecipante: $valutazione_quota_partecipante <br>";
		echo "Valutazione quota osservatore: $valutazione_quota_osservatore <br>";
        echo "Valutazione tutor competenza: $valutazione_tutor_competenza <br>";
		echo "Valutazione tutor disponibilità: $valutazione_tutor_disponibilita <br>";
		
		echo "Valutazione gradimento prova 1: $valutazione_gradimento_prova1 <br>";
		echo "Valutazione formativa prova 1: $valutazione_formativa_prova1 <br>";
		echo "Valutazione difficoltà prova 1: $valutazione_difficolta_prova1 <br>";
		echo "Valutazione lavoro gruppo prova 1: $valutazione_lavoro_gruppo_prova1 <br>";
		echo "Valutazione performance giudice prova 1: $valutazione_performance_giudice_prova1 <br>";
		
		echo "Valutazione gradimento prova 2: $valutazione_gradimento_prova2 <br>";
		echo "Valutazione formativa prova 2: $valutazione_formativa_prova2 <br>";
		echo "Valutazione difficoltà prova 2: $valutazione_difficolta_prova2 <br>";
		echo "Valutazione lavoro gruppo prova 2: $valutazione_lavoro_gruppo_prova2 <br>";
		echo "Valutazione performance giudice prova 2: $valutazione_performance_giudice_prova2 <br>";
		echo "Valutazione interpretazione simulatore prova 2: $valutazione_interpretazione_simulatore_prova2 <br>";
		echo "Valutazione utilità simulatore prova 2: $valutazione_utilita_simulatore_prova2 <br>";
		
		echo "Valutazione gradimento prova 3: $valutazione_gradimento_prova3 <br>";
		echo "Valutazione formativa prova 3: $valutazione_formativa_prova3 <br>";
		echo "Valutazione difficoltà prova 3: $valutazione_difficolta_prova3 <br>";
		echo "Valutazione lavoro gruppo prova 3: $valutazione_lavoro_gruppo_prova3 <br>";
		echo "Valutazione performance giudice prova 3: $valutazione_performance_giudice_prova3 <br>";
		echo "Valutazione interpretazione simulatore prova 3: $valutazione_interpretazione_simulatore_prova3 <br>";
		echo "Valutazione utilità simulatore prova 3: $valutazione_utilita_simulatore_prova3 <br>";
		
		echo "Valutazione gradimento prova 4: $valutazione_gradimento_prova4 <br>";
		echo "Valutazione formativa prova 4: $valutazione_formativa_prova4 <br>";
		echo "Valutazione difficoltà prova 4: $valutazione_difficolta_prova4 <br>";
		echo "Valutazione lavoro gruppo prova 4: $valutazione_lavoro_gruppo_prova4 <br>";
		echo "Valutazione performance giudice prova 4: $valutazione_performance_giudice_prova4 <br>";
		echo "Valutazione interpretazione simulatore prova 4: $valutazione_interpretazione_simulatore_prova4 <br>";
		echo "Valutazione utilità simulatore prova 4: $valutazione_utilita_simulatore_prova4 <br>";
		
		echo "Valutazione gradimento prova 5: $valutazione_gradimento_prova5 <br>";
		echo "Valutazione formativa prova 5: $valutazione_formativa_prova5 <br>";
		echo "Valutazione difficoltà prova 5: $valutazione_difficolta_prova5 <br>";
		echo "Valutazione lavoro gruppo prova 5: $valutazione_lavoro_gruppo_prova5 <br>";
		echo "Valutazione performance giudice prova 5: $valutazione_performance_giudice_prova5 <br>";
		echo "Valutazione interpretazione simulatore prova 5: $valutazione_interpretazione_simulatore_prova5 <br>";
		echo "Valutazione utilità simulatore prova 5: $valutazione_utilita_simulatore_prova5 <br>";
		
		echo "Valutazione gradimento prova 6: $valutazione_gradimento_prova6 <br>";
		echo "Valutazione formativa prova 6: $valutazione_formativa_prova6 <br>";
		echo "Valutazione difficoltà prova 6: $valutazione_difficolta_prova6 <br>";
		echo "Valutazione lavoro gruppo prova 6: $valutazione_lavoro_gruppo_prova6 <br>";
		echo "Valutazione performance giudice prova 6: $valutazione_performance_giudice_prova6 <br>";
		echo "Valutazione interpretazione simulatore prova 6: $valutazione_interpretazione_simulatore_prova6 <br>";
		echo "Valutazione utilità simulatore prova 6: $valutazione_utilita_simulatore_prova6 <br>";
		
		echo "Valutazione gradimento prova 7: $valutazione_gradimento_prova7 <br>";
		echo "Valutazione formativa prova 7: $valutazione_formativa_prova7 <br>";
		echo "Valutazione difficoltà prova 7: $valutazione_difficolta_prova7 <br>";
		echo "Valutazione lavoro gruppo prova 7: $valutazione_lavoro_gruppo_prova7 <br>";
		echo "Valutazione performance giudice prova 7: $valutazione_performance_giudice_prova7 <br>";
		echo "Valutazione interpretazione simulatore prova 7: $valutazione_interpretazione_simulatore_prova7 <br>";
		echo "Valutazione utilità simulatore prova 7: $valutazione_utilita_simulatore_prova7 <br>";
		
		echo "Valutazione gradimento prova 8: $valutazione_gradimento_prova8 <br>";
		echo "Valutazione formativa prova 8: $valutazione_formativa_prova8 <br>";
		echo "Valutazione difficoltà prova 8: $valutazione_difficolta_prova8 <br>";
		echo "Valutazione lavoro gruppo prova 8: $valutazione_lavoro_gruppo_prova8 <br>";
		echo "Valutazione performance giudice prova 8: $valutazione_performance_giudice_prova8 <br>";
		echo "Valutazione interpretazione simulatore prova 8: $valutazione_interpretazione_simulatore_prova8 <br>";
		echo "Valutazione utilità simulatore prova 8: $valutazione_utilita_simulatore_prova8 <br>";
		
		echo "Prova Assegnata: $prova_assegnata <br>";
		echo "Valutazione prova assegnata: $valutazione_prova_assegnata <br>";
		echo "Valutazione performance giudice prova: $valutazione_performance_giudice_prova <br>";
		
		echo "Valutazione inserimento voti portale: $valutazione_inserimento_voti_portale <br>";
		echo "Valutazione coordinamento backoffice: $valutazione_coordinamento_backoffice <br>";
		echo "Prova assegnata: $prova_assegnata <br>";
		echo "Valutazione prova assegnata: $valutazione_prova_assegnata <br>";
		echo "Valutazione interpretazione simulatore prova: $valutazione_interpretazione_simulatore_prova <br>";
		echo "Valutazione utilità simulatore prova: $valutazione_utilita_simulatore_prova <br>";
		
		echo "Valutazione preparazione: $valutazione_preparazione <br>";
		echo "Valutazione soddisfazione incarico: $valutazione_soddisfazione_incarico <br>";
		echo "Valutazione mezzi informazioni: $valutazione_mezzi_informazioni <br>";
		echo "Valutazione coordinamento backoffice: $valutazione_coordinamento_backoffice <br>";
		
		echo "Valutazione gruppo lavoro: $valutazione_gruppo_lavoro <br>";
		echo "Valutazione informazioni prova: $valutazione_informazioni_prova <br>";
		echo "Valutazione abbandono simulatori: $valutazione_abbandono_simulatori <br>";

		echo "Valutazione adeguatezza struttura:                     $valutazione_adeguatezza_struttura <br>";
		echo "Valutazione raggiungibilità struttura:                     $valutazione_raggiugngibilita_struttura <br>";
		echo "Valutazione efficienza della mensa:                     $valutazione_efficienza_mensa <br>";
		echo "Valutazione qualità del cibo:                     $valutazione_qualita_cibo <br>";
		
		echo "Valutazione accoglienza: $valutazione_accoglienza <br>";
		echo "Valutazione tempistiche: $valutazione_tempistiche <br>";
		echo "Valutazione distribuzione pause: $valutazione_distribuzione_pause <br>";
		echo "Valutazione disponibilità staff: $valutazione_disponibilita_staff <br>";
		echo "Valutazione portale web: $valutazione_portaleweb <br>";
		
		echo "Valutazione premiazione e festa finale: $valutazione_festa_finale <br>";
		echo "Valutazione meeting nel suo complesso: $valutazione_meeting <br>";
		echo "Parteciperai al prossimo meeting: $partecipazione_prossimo_meeting <br>";
		echo "Che ruolo vorresti ricoprire al prossimo meeting: $ruolo_prossimo_meeting <br>";
		echo "Commenti/suggerimenti/critiche: <br> $commenti <br>"; */

		
		//Apro il DB
		$db = mysql_connect($db_host, $db_user, $db_password);
		if ($db == FALSE)
		die ("Errore nella connessione. Verificare i parametri nel file config.inc.php");
		mysql_select_db($db_name, $db)
		or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");
		
		if ( $id != '0' )
		{
			//Cerco l'id nella tabella osservatori
			$query_feedback = "SELECT 	c.tipo_meeting AS tipo_meeting,
										i.nome AS nome,
										i.cognome AS cognome,
										i.ruolo AS ruolo,
										i.token AS token,
										i.feedback_rilasciato AS feedback_rilasciato
										FROM iscrizioni AS i
										INNER JOIN comitati AS c
										ON i.id_comitato = c.id
										WHERE i.token = '$id'";
			$result_feedback = mysql_query($query_feedback, $db);
			$esiste = mysql_num_rows($result_feedback);
			if ( $esiste != 1)
			{
				//Ha cambiato l'id e non è stato trovato
				echo "La tua valutazione non può essere registrata. Forse hai modificato i dati del link nella barra degli indirizzi, vota utilizzando il link ricevuto via mail. Per qualsiasi altro problema scrivi a <b>meeting.2015@cri.it</b><br>";	
			}	
			else
			{
				//estraggo le informazioni del votante
				$row_feedback = mysql_fetch_array( $result_feedback );
				//controllo che non abbia già votato
				
				//echo "Feedback_rilasciato=".$row_feedback['feedback_rilasciato']."<br>";
				
				if ( $row_feedback['feedback_rilasciato'] == NULL )
				{
					if ($row_feedback['tipo_meeting'] == 'N')
					{
						$query = "INSERT INTO feedback (token,
													ruolo,
													tipo_meeting,
													valutazione_tutor_competenza,
													valutazione_tutor_disponibilita,
													valutazione_gradimento_prova1,
													valutazione_formativa_prova1,
													valutazione_difficolta_prova1,
													valutazione_lavoro_gruppo_prova1,
													valutazione_performance_giudice_prova1,
													valutazione_gradimento_prova2,
													valutazione_formativa_prova2,
													valutazione_difficolta_prova2,
													valutazione_lavoro_gruppo_prova2,
													valutazione_performance_giudice_prova2,
													valutazione_interpretazione_simulatore_prova2,
													valutazione_utilita_simulatore_prova2,
													valutazione_gradimento_prova3,
													valutazione_formativa_prova3,
													valutazione_difficolta_prova3,
													valutazione_lavoro_gruppo_prova3,
													valutazione_performance_giudice_prova3,
													valutazione_interpretazione_simulatore_prova3,
													valutazione_utilita_simulatore_prova3,
													valutazione_gradimento_prova4,
													valutazione_formativa_prova4,
													valutazione_difficolta_prova4,
													valutazione_lavoro_gruppo_prova4,
													valutazione_performance_giudice_prova4,
													valutazione_gradimento_prova5,
													valutazione_formativa_prova5,
													valutazione_difficolta_prova5,
													valutazione_lavoro_gruppo_prova5,
													valutazione_performance_giudice_prova5,
													valutazione_interpretazione_simulatore_prova5,
													valutazione_utilita_simulatore_prova5,
													valutazione_gradimento_prova6,
													valutazione_formativa_prova6,
													valutazione_difficolta_prova6,
													valutazione_lavoro_gruppo_prova6,
													valutazione_performance_giudice_prova6,
													valutazione_gradimento_india,
													valutazione_formativa_india,
													valutazione_difficolta_india,
													valutazione_lavoro_gruppo_india,
													valutazione_performance_giudice_india,
													valutazione_gradimento_papa,
													valutazione_formativa_papa,
													valutazione_difficolta_papa,
													valutazione_lavoro_gruppo_papa,
													valutazione_performance_giudice_papa,
													valutazione_gruppo_lavoro,
													valutazione_informazioni_prova,
													valutazione_abbandono_simulatori,
													prova_assegnata,
													valutazione_prova_assegnata,
													valutazione_performance_giudice_prova,
													valutazione_inserimento_voti_portale,
													valutazione_coordinamento_backoffice,
													valutazione_interpretazione_simulatore_prova,
													valutazione_utilita_simulatore_prova,
													valutazione_adeguatezza_struttura,
													valutazione_raggiugngibilita_struttura,
													valutazione_efficienza_mensa,
													valutazione_qualita_cibo,
													valutazione_accoglienza,
													valutazione_tempistiche,
													valutazione_distribuzione_pause,
													valutazione_disponibilita_staff,
													valutazione_portaleweb,
													valutazione_festa_finale,
													valutazione_meeting,
													partecipazione_prossimo_meeting,
													ruolo_prossimo_meeting,
													commenti,
													valutazione_accoglienza_venerdi,
													valutazione_speed_meeting,
													valutazione_pernottamento,
													valutazione_pasti,
													valutazione_logistica,
													valutazione_tour,
													valutazione_piazza) 
											VALUES ('$id',
													'$ruolo',
													'$row_feedback[tipo_meeting]',
													'$valutazione_tutor_competenza',
													'$valutazione_tutor_disponibilita',
													'$valutazione_gradimento_prova1',
													'$valutazione_formativa_prova1',
													'$valutazione_difficolta_prova1',
													'$valutazione_lavoro_gruppo_prova1',
													'$valutazione_performance_giudice_prova1',
													'$valutazione_gradimento_prova2',
													'$valutazione_formativa_prova2',
													'$valutazione_difficolta_prova2',
													'$valutazione_lavoro_gruppo_prova2',
													'$valutazione_performance_giudice_prova2',
													'$valutazione_interpretazione_simulatore_prova2',
													'$valutazione_utilita_simulatore_prova2',
													'$valutazione_gradimento_prova3',
													'$valutazione_formativa_prova3',
													'$valutazione_difficolta_prova3',
													'$valutazione_lavoro_gruppo_prova3',
													'$valutazione_performance_giudice_prova3',
													'$valutazione_interpretazione_simulatore_prova3',
													'$valutazione_utilita_simulatore_prova3',
													'$valutazione_gradimento_prova4',
													'$valutazione_formativa_prova4',
													'$valutazione_difficolta_prova4',
													'$valutazione_lavoro_gruppo_prova4',
													'$valutazione_performance_giudice_prova4',
													'$valutazione_gradimento_prova5',
													'$valutazione_formativa_prova5',
													'$valutazione_difficolta_prova5',
													'$valutazione_lavoro_gruppo_prova5',
													'$valutazione_performance_giudice_prova5',
													'$valutazione_interpretazione_simulatore_prova5',
													'$valutazione_utilita_simulatore_prova5',
													'$valutazione_gradimento_prova6',
													'$valutazione_formativa_prova6',
													'$valutazione_difficolta_prova6',
													'$valutazione_lavoro_gruppo_prova6',
													'$valutazione_performance_giudice_prova6',
													'$valutazione_gradimento_prova7',
													'$valutazione_formativa_prova7',
													'$valutazione_difficolta_prova7',
													'$valutazione_lavoro_gruppo_prova7',
													'$valutazione_performance_giudice_prova7',
													'$valutazione_gradimento_prova8',
													'$valutazione_formativa_prova8',
													'$valutazione_difficolta_prova8',
													'$valutazione_lavoro_gruppo_prova8',
													'$valutazione_performance_giudice_prova8',
													'$valutazione_gruppo_lavoro',
													'$valutazione_informazioni_prova',
													'$valutazione_abbandono_simulatori',
													'$prova_assegnata',
													'$valutazione_prova_assegnata',
													'$valutazione_performance_giudice_prova',
													'$valutazione_inserimento_voti_portale',
													'$valutazione_coordinamento_backoffice',
													'$valutazione_interpretazione_simulatore_prova',
													'$valutazione_utilita_simulatore_prova',
													'$valutazione_adeguatezza_struttura',
													'$valutazione_raggiugngibilita_struttura',
													'$valutazione_efficienza_mensa',
													'$valutazione_qualita_cibo',
													'$valutazione_accoglienza',
													'$valutazione_tempistiche',
													'$valutazione_distribuzione_pause',
													'$valutazione_disponibilita_staff',
													'$valutazione_portaleweb',
													'$valutazione_festa_finale',
													'$valutazione_meeting',
													'$partecipazione_prossimo_meeting',
													'$ruolo_prossimo_meeting',
													'$commenti',
													'$valutazione_accoglienza_venerdi',
													'$valutazione_speed_meeting',
													'$valutazione_pernottamento',
													'$valutazione_pasti',
													'$valutazione_logistica',
													'$valutazione_tour',
													'$valutazione_piazza')"; 
					}
					else
					{
						$query = "INSERT INTO feedback (token,
														ruolo,
														tipo_meeting,
														valutazione_quota_partecipante,
														valutazione_quota_osservatore,
														valutazione_tutor_competenza,
														valutazione_tutor_disponibilita,
														valutazione_gradimento_india,
														valutazione_formativa_india,
														valutazione_difficolta_india,
														valutazione_lavoro_gruppo_india,
														valutazione_performance_giudice_india,
														valutazione_gradimento_papa,
														valutazione_formativa_papa,
														valutazione_difficolta_papa,
														valutazione_lavoro_gruppo_papa,
														valutazione_performance_giudice_papa,
														valutazione_gradimento_tango,
														valutazione_formativa_tango,
														valutazione_difficolta_tango,
														valutazione_lavoro_gruppo_tango,
														valutazione_performance_giudice_tango,
														valutazione_gradimento_oscar,
														valutazione_formativa_oscar,
														valutazione_difficolta_oscar,
														valutazione_lavoro_gruppo_oscar,
														valutazione_performance_giudice_oscar,
														valutazione_interpretazione_simulatore_oscar,
														valutazione_utilita_simulatore_oscar,
														valutazione_gradimento_hotel,
														valutazione_formativa_hotel,
														valutazione_difficolta_hotel,
														valutazione_lavoro_gruppo_hotel,
														valutazione_performance_giudice_hotel,
														valutazione_gradimento_victor,
														valutazione_formativa_victor,
														valutazione_difficolta_victor,
														valutazione_lavoro_gruppo_victor,
														valutazione_performance_giudice_victor,
														valutazione_gradimento_quebec,
														valutazione_formativa_quebec,
														valutazione_difficolta_quebec,
														valutazione_lavoro_gruppo_quebec,
														valutazione_gruppo_lavoro,
														valutazione_informazioni_prova,
														valutazione_abbandono_simulatori,
														prova_assegnata,
														valutazione_prova_assegnata,
														valutazione_performance_giudice_prova,
														valutazione_inserimento_voti_portale,
														valutazione_coordinamento_backoffice,
														valutazione_interpretazione_simulatore_prova,
														valutazione_utilita_simulatore_prova,
														valutazione_preparazione,
														valutazione_soddisfazione_incarico,
														valutazione_mezzi_informazioni,
														valutazione_adeguatezza_struttura,
														valutazione_raggiugngibilita_struttura,
														valutazione_efficienza_mensa,
														valutazione_qualita_cibo,
														valutazione_accoglienza,
														valutazione_tempistiche,
														valutazione_distribuzione_pause,
														valutazione_disponibilita_staff,
														valutazione_portaleweb,
														valutazione_festa_finale,
														valutazione_meeting,
														partecipazione_prossimo_meeting,
														ruolo_prossimo_meeting,
														commenti,
														valutazione_piazza)
											VALUES ('$id',
													'$ruolo',
													'$row_feedback[tipo_meeting]',
													'$valutazione_quota_partecipante',
													'$valutazione_quota_osservatore',
													'$valutazione_tutor_competenza',
													'$valutazione_tutor_disponibilita',
													'$valutazione_gradimento_prova1',
													'$valutazione_formativa_prova1',
													'$valutazione_difficolta_prova1',
													'$valutazione_lavoro_gruppo_prova1',
													'$valutazione_performance_giudice_prova1',
													'$valutazione_gradimento_prova2',
													'$valutazione_formativa_prova2',
													'$valutazione_difficolta_prova2',
													'$valutazione_lavoro_gruppo_prova2',
													'$valutazione_performance_giudice_prova2',
													'$valutazione_gradimento_prova3',
													'$valutazione_formativa_prova3',
													'$valutazione_difficolta_prova3',
													'$valutazione_lavoro_gruppo_prova3',
													'$valutazione_performance_giudice_prova3',
													'$valutazione_gradimento_prova4',
													'$valutazione_formativa_prova4',
													'$valutazione_difficolta_prova4',
													'$valutazione_lavoro_gruppo_prova4',
													'$valutazione_performance_giudice_prova4',
													'$valutazione_interpretazione_simulatore_prova4',
													'$valutazione_utilita_simulatore_prova4',
													'$valutazione_gradimento_prova5',
													'$valutazione_formativa_prova5',
													'$valutazione_difficolta_prova5',
													'$valutazione_lavoro_gruppo_prova5',
													'$valutazione_performance_giudice_prova5',
													'$valutazione_gradimento_prova6',
													'$valutazione_formativa_prova6',
													'$valutazione_difficolta_prova6',
													'$valutazione_lavoro_gruppo_prova6',
													'$valutazione_performance_giudice_prova6',
													'$valutazione_gradimento_prova7',
													'$valutazione_formativa_prova7',
													'$valutazione_difficolta_prova7',
													'$valutazione_lavoro_gruppo_prova7',
													'$valutazione_gruppo_lavoro',
													'$valutazione_informazioni_prova',
													'$valutazione_abbandono_simulatori',
													'$prova_assegnata',
													'$valutazione_prova_assegnata',
													'$valutazione_performance_giudice_prova',
													'$valutazione_inserimento_voti_portale',
													'$valutazione_coordinamento_backoffice',
													'$valutazione_interpretazione_simulatore_prova',
													'$valutazione_utilita_simulatore_prova',
													'$valutazione_preparazione',
													'$valutazione_soddisfazione_incarico',
													'$valutazione_mezzi_informazioni',
													'$valutazione_adeguatezza_struttura',
													'$valutazione_raggiugngibilita_struttura',
													'$valutazione_efficienza_mensa',
													'$valutazione_qualita_cibo',
													'$valutazione_accoglienza',
													'$valutazione_tempistiche',
													'$valutazione_distribuzione_pause',
													'$valutazione_disponibilita_staff',
													'$valutazione_portaleweb',
													'$valutazione_festa_finale',
													'$valutazione_meeting',
													'$partecipazione_prossimo_meeting',
													'$ruolo_prossimo_meeting',
													'$commenti',
													'$valutazione_piazza')"; 
					}
					//echo "$query <br>";
					if (mysql_query($query, $db))
					{
						echo "I dati da te inseriti sono stati correttamente salvati.<br><br>";
						$query = "UPDATE iscrizioni SET feedback_rilasciato = '1' WHERE token = '$id'";
						mysql_query($query, $db);
						echo "Grazie $row_feedback[nome], per la tua collaborazione.<br><br>";
					}
					else
					{
						echo "Errore durante l'inserimento, rivolgersi al Back Office scrivendo a <b>meeting.2015@cri.it</b><br><br>";
					}
				}
				else
				{
					//Espongo il messaggio a video
					echo "La valutazione non è stata inserita, è possibile inviare una sola valutazione per ogni volontario.<br><br>$row_feedback[nome], a noi risulta tu abbia già inserito la tua valutazione.<br>Per qualsiasi altro problema scrivi a <b>meeting.2015@cri.it</b><br>";	
				}
			
			}
		} 
		else
		{
			//Ha cambiato l'id e non è stato trovato
			echo "La tua valutazione non può essere registrata. Forse hai modificato i dati del link nella barra degli indirizzi, vota utilizzando il link ricevuto via mail. Per qualsiasi altro problema scrivi a <b>meeting.2015@cri.it</b><br>";		}
		
		mysql_close($db); 
		?>




	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>