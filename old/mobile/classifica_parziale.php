<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Inserisci punteggio</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
	<script type="text/javascript">
		$(document).on("change", "#checkbox-1", function () {
			if ($(this).prop("checked")) {
				$("#submit").button("enable");
			} else {
				$("#submit").button("disable");
			}
		});
    </script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2016 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		<p><strong>Inserisci punteggio</strong></p>

        <div data-html="true">

		<p><strong>Classifica Parziale dopo 4 prove (giro di boa)</strong></p>

		
   		<table data-role="table" id="table-column-toggle" data-mode="columntoggle" class="ui-responsive table-stroke">
     <thead>
       <tr>
         <th data-priority="1">Posizione</th>
         <th>Squadra</th>
         </tr>
     </thead>
     <tbody>        
            
      
        <tr>		<th>1</th>        <td> - </td>		</tr>
        <tr>		<th>2</th>        <td> - </td>		</tr>
		<tr>		<th>3</th>        <td> - </td>		</tr>

		<tr>		<th>4</th>        <td> Nichelino </td>		</tr>
		<tr>		<th>5</th>        <td> Cossato</td>						</tr>
		<th>6</th>
        <td> Giaveno </td>
		</tr>
		<th>7</th>
        <td> Susa </td>
		</tr>
		<th>8</th>
        <td> Torino </td>
		</tr>
		<th>9</th>
        <td> Casale Monferrato </td>
		</tr>
		<th>10</th>
        <td> Vado Ligure Quiliano </td>
		</tr>
		<th>11</th>
        <td> Lombardia </td>
		</tr>
		<th>12</th>
        <td> Villanova D'Asti </td>
		</tr>
		<th>13</th>
        <td> Galliate </td>
		</tr>
		<th>14</th>
        <td> Chivasso </td>
		</tr>
		<th>15</th>
        <td> Alessandria </td>
		</tr>
		<th>16</th>
        <td> Domodossola </td>
		</tr>
		<th>17</th>
        <td> Lauriano </td>
		</tr>
		<th>18</th>
        <td> Carmagnola </td>
		</tr>
		<th>19</th>
        <td> Rivoli </td>
		</tr>

		
		<th>20</th>
        <td> - </td>
		</tr>
		<th>21</th>
        <td> - </td>
		</tr>
		<th>22</th>
        <td> - </td>
		</tr>
		<th>23</th>
        <td> - </td>
		</tr>
		<th>24</th>
        <td> - </td>
		</tr>
		<th>25</th>
        <td> - </td>
		</tr>
		<th>26</th>
        <td> - </td>
		</tr>

		<th>27</th>
        <td> Canelli </td>
		</tr>
		<th>28</th>
        <td> Santena </td>
		</tr>
		<th>29</th>
        <td> Leinì </td>
		</tr>
		<th>30</th>
        <td> Borgosesia </td>
		</tr>
		<th>31</th>
        <td> Pianezza </td>
		</tr>
		<th>32</th>
        <td> Mondovì </td>
		</tr>
		<th>33</th>
        <td> Novi Ligure </td>
		</tr>
		<th>34</th>
        <td> Beinasco </td>
		</tr>
		<th>35</th>
        <td> Chieri </td>
		</tr>
		<th>36</th>
        <td> Baveno </td>
		</tr>
		<th>37</th>
        <td> Lazio </td>
		</tr>
		<th>38</th>
        <td> Novara </td>
		</tr>
		<th>39</th>
        <td> Torre Pellice </td>
		</tr>
		<th>40</th>
        <td> - </td>
		</tr>
		<th>41</th>
        <td> - </td>
		</tr>
		<th>42</th>
        <td> - </td>
		</tr>
		<th>43</th>
        <td> - </td>
		</tr>
		<th>44</th>
        <td> - </td>
		</tr>
		<th>45</th>
        <td> - </td>
		</tr>
		<th>46</th>
        <td> Liguria </td>
		</tr>
		
		
     </tbody>
   </table>            



        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>