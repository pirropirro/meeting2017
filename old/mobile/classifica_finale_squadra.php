<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2015 - Classifica Finale Squadra</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.php" title="Meeting 2015 - Homepage"><img src="logo_meeting.png" alt="Portale Meeting 2015 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2015</h1>
		

		<?php
		include ("config.inc.php");
		include ("apri_db.php");
		?>

		<p><strong>Classifica Finale Squadra</strong></p>
		
		<?php

		$token = $_GET['token'];
		$tipo_meeting = $_GET['type'];

		if ($tipo_meeting == 'N')
		{
			//Squadra Meeting Nazionale
			$query = "SELECT 		c.nome_comitato AS nome_comitato,
									i.feedback_rilasciato AS feedback_rilasciato,
									p.id_comitato AS id_comitato,
									p.s1_c AS prova_1,
									p.s2_c AS prova_2,
									p.s3_c AS prova_3,
									p.s4_c AS prova_4,
									p.s5_c AS prova_5,
									p.s6_c AS prova_6,
									p.d1_c AS prova_india,
									p.d2_c AS prova_papa
									FROM preiscrizioni AS p
									INNER JOIN comitati AS c
									ON p.id_comitato = c.id
									INNER JOIN iscrizioni AS i
									ON p.id_comitato = i.id_comitato
									WHERE i.token = '$token'";
			$result = mysql_query($query, $db);
			$row = mysql_fetch_array($result);
			
			if ($row[feedback_rilasciato] == 1)
			{
				$re = "/(\\s*comitato lacale di\\s*|\\s*delegazione di\\s*|\\s*comitato provinciale di\\s*)/i"; 
				$subst = ""; 	 
				$comitato_trunc = preg_replace($re, $subst, $row[nome_comitato]);			
					
				?>
				<table data-role="table" id="table-column-toggle" data-mode="columntoggle" class="ui-responsive table-stroke">
					<thead>
						<tr>
							<td colspan="2" align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[nome_comitato] ?></td> 
						</tr>
							<th>Prova</th>
							<th data-priority="1">Punteggio</th>
					</thead>
					<tbody>        
	
					<tr>
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">UNO</td> 
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_1],1) ?></td>
					</tr>
					<tr>
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">DUE</td> 
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_2],1) ?></td>
					</tr>
					<tr>
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">TRE</td> 
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_3],1) ?></td>
					</tr>
					<tr>
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">QUATTRO</td> 
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_4],1) ?></td>
					</tr>
					<tr>
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">CINQUE</td> 
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_5],1) ?></td>
					</tr>
					<tr>
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">SEI</td> 
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_6],1) ?></td>
					</tr>
					<tr>
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">INDIA</td> 
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_india],1) ?></td>
					</tr>
					<tr>
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">PAPA</td> 
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_papa],1) ?></td>
					</tr>
					<tr>
					<?php
				}
				else
				{
					echo "Per poter visualizzare i punteggi parziali, ti chiediamo di rilasciare il feedback tramite il link che hai ricevuto via mail";
				}
		}
		else
		{
			//Squadra Meeting Regionale-Provinciale
			$query = "SELECT 		c.nome_comitato AS nome_comitato,
									i.feedback_rilasciato AS feedback_rilasciato,
									p.id_comitato AS id_comitato,
									p.d1_c AS prova_india,
									p.d2_c AS prova_papa,
									p.d5_g AS prova_oscar,
									p.d6_c AS prova_tango,
									p.d7_g AS prova_hotel,
									p.d8_g AS prova_victor,
									p.Q_P1 AS q1,
									p.Q_P2 AS q2,
									p.Q_P3 AS q3,
									p.Q_P4 AS q4,
									p.Q_P5 AS q5,
									p.Q_P6 AS q6,
									p.Q_P7 AS q7,
									p.Q_P8 AS q8,
									p.Q_P9 AS q9,
									p.Q_P10 AS q10
									FROM preiscrizioni AS p
									INNER JOIN comitati AS c
									ON p.id_comitato = c.id
									INNER JOIN iscrizioni AS i
									ON p.id_comitato = i.id_comitato
									WHERE i.token = '$token'";
			$result = mysql_query($query, $db);
			$row = mysql_fetch_array($result);
			
			if ($row[feedback_rilasciato] == 1)
			{
				$prova_quebec = $row[q1] + $row[q2] + $row[q3] + $row[q4] + $row[q5] + $row[q6] + $row[q7] + $row[q8] + $row[q9] + $row[q10];
				
				$re = "/(\\s*comitato lacale di\\s*|\\s*delegazione di\\s*|\\s*comitato provinciale di\\s*)/i"; 
				$subst = ""; 	 
				$comitato_trunc = preg_replace($re, $subst, $row[nome_comitato]);			
				?>
				<table data-role="table" id="table-column-toggle" data-mode="columntoggle" class="ui-responsive table-stroke">
					<thead>
						<tr>
							<td colspan="2" align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= $row[nome_comitato] ?></td> 
						</tr>
							<th>Prova</th>
							<th data-priority="1">Punteggio</th>
					</thead>
					<tbody>        
	
					<tr>
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">INDIA</td> 
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_india],1) ?></td>
					</tr>
					<tr>
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">PAPA</td> 
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_papa],1) ?></td>
					</tr>
					<tr>
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">QUEBEC</td> 
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($prova_quebec,1) ?></td>
					</tr>
					<tr>
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">OSCAR</td> 
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_oscar],1) ?></td>
					</tr>
					<tr>
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">TANGO</td> 
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_tango],1) ?></td>
					</tr>
					<tr>
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">HOTEL</td> 
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_hotel],1) ?></td>
					</tr>
					<tr>
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">VICTOR</td> 
					<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_victor],1) ?></td>
					</tr>
					<?php
				}
				else
				{
					echo "Per poter visualizzare i punteggi parziali, ti chiediamo di rilasciare il feedback tramite il link che hai ricevuto via mail";
				}
			}
			mysql_close($db); 
			?>
			</tbody>
		</table>   

	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>