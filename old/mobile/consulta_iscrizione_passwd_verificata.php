<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Consulta iscrizioni</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2016 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>
		

		<?php
        include ("config.inc.php");
        
        $id_comitato=$_REQUEST['id_comitato'];
        $passwd=$_REQUEST['passwd'];
        //echo $id_comitato;
        //echo $passwd;
        
        include ("apri_db.php");
        
        //Comitato di cui si inserisci la passwd
        $query = "SELECT nome_comitato, passwd FROM comitati WHERE id = '$id_comitato'";
        $result = mysql_query($query, $db);
        $row = mysql_fetch_array( $result );
        
        
        if ($row[passwd] == $passwd  && !($id_comitato == ''))
        {
            ?>

            <p><strong>Consulta iscrizioni</strong></p>
            
            <table data-role="table" id="table-column-toggle" data-mode="columntoggle" class="ui-responsive table-stroke">
				<thead>
					<tr>
					<th>Ruolo</th>
                    <th>Nome Cognome</th>
                    <th data-priority="1">Telefono</th>
                    <th data-priority="7">Mail</th>
                    <th data-priority="8">Verifica mail</th>
                    <th data-priority="3">Esigenze alimentari</th>
                    <th data-priority="3">Eventuali disabilità</th>
				</thead>
             	<tbody>        
            
					<?php
                    //Faccio la query sulla tabella iscrizioni per estrarre i dati da visualizzare
                    $query_iscrizioni = "SELECT	*
                                                FROM iscrizioni
                                                WHERE id_comitato = '$id_comitato'
                                                ORDER BY FIND_IN_SET (ruolo, 'capitano,partecipante,osservatore,giudice,simulatore,truccatore,organo,staff,ospite')";
                    $result_iscrizioni = mysql_query($query_iscrizioni, $db);
                    while($row_iscrizioni = mysql_fetch_array( $result_iscrizioni )) 
                    {
                        ?>
                        <tr>
                        <th><?= $row_iscrizioni[ruolo] ?></th>
                        <td><?= $row_iscrizioni[nome]." ".$row_iscrizioni[cognome] ?></td>
                        <td><?= $row_iscrizioni[telefono] ?></td>
                        <td><?= $row_iscrizioni[mail] ?></td>
                        <td><?php if($row_iscrizioni[mail_confermata] == 1) echo "si";  ?></td>
                        <th><?= $row_iscrizioni[esigenze] ?></th>
                        <th><?= $row_iscrizioni[disabilita] ?></th>
                        </tr>
                        <?php
                    }
					?>
                </tbody>
            </table>   
            <br /><br />

			<a class="ImLink" href="http://www.itempa.com/meeting2016/mobile/ricevuta_comitato_<?= $id_comitato ?>.pdf" title="Ricevuta">Stampa la ricevuta</a>

<!--   Ricevuta nascosta per indisponibilità - da rifare
			<form method="post" action="stampa_ricevuta_comitato.php">
			<input type="hidden" name="id_comitato" value="<?php echo $id_comitato; ?>" />
			<input type="hidden" name="passwd" value="<?php echo $passwd; ?>" />
			<input type="submit" value="Stampa la ricevuta" />
			</form>
-->			
			
			
			<?php
        }
        else
        {
            echo "Password errata<br><br><br>";
        }
        
        mysql_close($db); 
        ?>

	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>