<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Iscrizione Osservatore</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
	<script type="text/javascript">
		$(document).on("change", "#checkbox-1", function () {
			if ($(this).prop("checked")) {
				$("#submit").button("enable");
			} else {
				$("#submit").button("disable");
			}
		});
    </script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2016 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		<p><strong>Iscrizione Osservatore</strong></p>

        <div data-html="true">

            <br /><br />
            
            L'iscrizione può essere effettuata dal Consigliere Giovane o da un volontario che ne fa le veci, in possesso della password notificata via mail all'indirizzo inserito in fase di preiscrizione.
            <br /><br />
            
            Prima di procedere con l'iscrizione dell'osservatore assicurati di avere con te, i seguenti dati:<br />
             - nome;<br />
             - cognome;<br />
             - telefono;<br />
             - e-mail (valido e personale);<br />
             - esigenze alimentari;<br />
             - eventuali disabilità;<br />
             - disponibilità come simulatore;<br />
             - disponibilità come truccatore;<br />
             - disponibilità come giudice.
            <br /><br />
            
            Tutti i dati vanno inseriti in un solo form, non è possibile fare inserimenti parziali.
            <br /><br />
            <br /><br />
            
            <form method="post" action="inserisci_iscrizione_osservatore_passwd_richiesta.php">
            <input type="submit" value="Prosegui" />
            </form>

        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>