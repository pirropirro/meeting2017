<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2015 - Feedback Simulatori</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.php" title="Meeting 2015 - Homepage"><img src="logo_meeting.png" alt="Portale Meeting 2015 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2015</h1>
		
		<p><strong>Riservato ai Simulatori</strong></p>

		<?
        //recupero i dati nella barra dell'indirizzo
        $ruolo=$_GET['ruolo'];
        $id=$_GET['id'];
        //echo "Ruolo                    (tutti):                     $ruolo <br>";
        //echo "ID          (tutti):           $id <br>";
        
        //Annullo le valutazioni non di competenze
        $valutazione_quota_partecipante = '';
        //echo "Valutazione quota partecipante: $valutazione_quota_partecipante <br>";
        $valutazione_quota_osservatore = '';
        //echo "Valutazione quota osservatore: $valutazione_quota_osservatore <br>";
        $valutazione_tutor_competenza = '';
        //echo "Valutazione competenza tutor: $valutazione_tutor_competenza <br>";
        $valutazione_tutor_disponibilita='';
        //echo "Valutazione disponibilità tutor: $valutazione_tutor_disponibilita <br>";
		
        $valutazione_gradimento_prova1 = '';
        //echo "Valutazione gradimento prova area 1: $valutazione_gradimento_prova1 <br>";
        $valutazione_formativa_prova1 = '';
        //echo "Valutazione validità formativa prova area 1: $valutazione_formativa_prova1 <br>";
        $valutazione_difficolta_prova1 = '';
        //echo "Valutazione difficoltà prova area 1: $valutazione_difficolta_prova1 <br>";
        $valutazione_lavoro_gruppo_prova1 = '';
        //echo "Valutazione lavoro di gruppo prova area 1: $valutazione_lavoro_gruppo_prova1 <br>";
        $valutazione_performance_giudice_prova1 = '';
        //echo "Valutazione performance giudici prova area 1: $valutazione_performance_giudice_prova1 <br>";
        $valutazione_interpretazione_simulatore_prova1 = '';
        //echo "Valutazione interpretazione simulatori prova area 1: $valutazione_interpretazione_simulatore_prova1 <br>";
        $valutazione_utilita_simulatore_prova1 = '';
        //echo "Valutazione utilità simulatori prova area 1: $valutazione_utilita_simulatore_prova1 <br>";
		
        $valutazione_gradimento_prova2 = '';
        //echo "Valutazione gradimento prova area 2: $valutazione_gradimento_prova2 <br>";
        $valutazione_formativa_prova2 = '';
        //echo "Valutazione validità formativa prova area 2: $valutazione_formativa_prova2 <br>";
        $valutazione_difficolta_prova2 = '';
        //echo "Valutazione difficoltà prova area 2: $valutazione_difficolta_prova2 <br>";
        $valutazione_lavoro_gruppo_prova2 = '';
        //echo "Valutazione lavoro di gruppo prova area 2: $valutazione_lavoro_gruppo_prova2 <br>";
        $valutazione_performance_giudice_prova2 = '';
        //echo "Valutazione performance giudici prova area 2: $valutazione_performance_giudice_prova2 <br>";
        $valutazione_interpretazione_simulatore_prova2 = '';
        //echo "Valutazione interpretazione simulatori prova area 2: $valutazione_interpretazione_simulatore_prova2 <br>";
        $valutazione_utilita_simulatore_prova2 = '';
        //echo "Valutazione utilità simulatori prova area 2: $valutazione_utilita_simulatore_prova2 <br>";
		
        $valutazione_gradimento_prova3 = '';
        //echo "Valutazione gradimento prova area 3: $valutazione_gradimento_prova3 <br>";
        $valutazione_formativa_prova3 = '';
        //echo "Valutazione validità formativa prova area 3: $valutazione_formativa_prova3 <br>";
        $valutazione_difficolta_prova3 = '';
        //echo "Valutazione difficoltà prova area 3: $valutazione_difficolta_prova3 <br>";
        $valutazione_lavoro_gruppo_prova3 = '';
        //echo "Valutazione lavoro di gruppo prova area 3: $valutazione_lavoro_gruppo_prova3 <br>";
        $valutazione_performance_giudice_prova3 = '';
        //echo "Valutazione performance giudici prova area 3: $valutazione_performance_giudice_prova3 <br>";
		$valutazione_interpretazione_simulatore_prova3 = '';
        //echo "Valutazione interpretazione simulatori prova area 3: $valutazione_interpretazione_simulatore_prova3 <br>";
        $valutazione_utilita_simulatore_prova3 = '';
        //echo "Valutazione utilità simulatori prova area 3: $valutazione_utilita_simulatore_prova3 <br>";
		
        $valutazione_gradimento_prova4 = '';
        //echo "Valutazione gradimento prova area 4: $valutazione_gradimento_prova4 <br>";
        $valutazione_formativa_prova4 = '';
        //echo "Valutazione validità formativa prova area 4: $valutazione_formativa_prova4 <br>";
        $valutazione_difficolta_prova4 = '';
        //echo "Valutazione difficoltà prova area 4: $valutazione_difficolta_prova4 <br>";
        $valutazione_lavoro_gruppo_prova4 = '';
        //echo "Valutazione lavoro di gruppo prova area 4: $valutazione_lavoro_gruppo_prova4 <br>";
        $valutazione_performance_giudice_prova4 = '';
        //echo "Valutazione performance giudici prova area 4: $valutazione_performance_giudice_prova4 <br>";
        $valutazione_interpretazione_simulatore_prova4 = '';
        //echo "Valutazione interpretazione simulatori prova area 4: $valutazione_interpretazione_simulatore_prova4 <br>";
        $valutazione_utilita_simulatore_prova4 = '';
        //echo "Valutazione utilità simulatori prova area 4: $valutazione_utilita_simulatore_prova4 <br>";
		
        $valutazione_gradimento_prova5 = '';
        //echo "Valutazione gradimento prova area 5: $valutazione_gradimento_prova5 <br>";
        $valutazione_formativa_prova5 = '';
        //echo "Valutazione validità formativa prova area 5: $valutazione_formativa_prova5 <br>";
        $valutazione_difficolta_prova5 = '';
        //echo "Valutazione difficoltà prova area 5: $valutazione_difficolta_prova5 <br>";
        $valutazione_lavoro_gruppo_prova5 = '';
        //echo "Valutazione lavoro di gruppo prova area 5: $valutazione_lavoro_gruppo_prova5 <br>";
        $valutazione_performance_giudice_prova5 = '';
        //echo "Valutazione performance giudici prova area 5: $valutazione_performance_giudice_prova5 <br>";
		$valutazione_interpretazione_simulatore_prova5 = '';
        //echo "Valutazione interpretazione simulatori prova area 2: $valutazione_interpretazione_simulatore_prova2 <br>";
        $valutazione_utilita_simulatore_prova5 = '';
        //echo "Valutazione utilità simulatori prova area 2: $valutazione_utilita_simulatore_prova2 <br>";
		
        //Annullo il form prova area 6
        $valutazione_gradimento_prova6 = '';
        //echo "Valutazione gradimento prova area 6: $valutazione_gradimento_prova6 <br>";
        $valutazione_formativa_prova6 = '';
        //echo "Valutazione validità formativa prova area 6: $valutazione_formativa_prova6 <br>";
        $valutazione_difficolta_prova6 = '';
        //echo "Valutazione difficoltà prova area 6: $valutazione_difficolta_prova6 <br>";
        $valutazione_lavoro_gruppo_prova6 = '';
        //echo "Valutazione lavoro di gruppo prova area 6: $valutazione_lavoro_gruppo_prova6 <br>";
        $valutazione_performance_giudice_prova6 = '';
        //echo "Valutazione performance giudici prova area 6: $valutazione_performance_giudice_prova6 <br>";
		$valutazione_interpretazione_simulatore_prova6 = '';
        //echo "Valutazione interpretazione simulatori prova area 2: $valutazione_interpretazione_simulatore_prova2 <br>";
        $valutazione_utilita_simulatore_prova6 = '';
        //echo "Valutazione utilità simulatori prova area 2: $valutazione_utilita_simulatore_prova2 <br>";
		
		//Annullo il form prova area 7
        $valutazione_gradimento_prova7 = '';
        //echo "Valutazione gradimento prova area 6: $valutazione_gradimento_prova6 <br>";
        $valutazione_formativa_prova7 = '';
        //echo "Valutazione validità formativa prova area 6: $valutazione_formativa_prova6 <br>";
        $valutazione_difficolta_prova7 = '';
        //echo "Valutazione difficoltà prova area 6: $valutazione_difficolta_prova6 <br>";
        $valutazione_lavoro_gruppo_prova7 = '';
        //echo "Valutazione lavoro di gruppo prova area 6: $valutazione_lavoro_gruppo_prova6 <br>";
        $valutazione_performance_giudice_prova7 = '';
        //echo "Valutazione performance giudici prova area 6: $valutazione_performance_giudice_prova6 <br>";
		
		//Annullo il form prova area 8
        $valutazione_gradimento_prova8 = '';
        //echo "Valutazione gradimento prova area 6: $valutazione_gradimento_prova6 <br>";
        $valutazione_formativa_prova8 = '';
        //echo "Valutazione validità formativa prova area 6: $valutazione_formativa_prova6 <br>";
        $valutazione_difficolta_prova8 = '';
        //echo "Valutazione difficoltà prova area 6: $valutazione_difficolta_prova6 <br>";
        $valutazione_lavoro_gruppo_prova8 = '';
        //echo "Valutazione lavoro di gruppo prova area 6: $valutazione_lavoro_gruppo_prova6 <br>";
        $valutazione_performance_giudice_prova8 = '';
        //echo "Valutazione performance giudici prova area 6: $valutazione_performance_giudice_prova6 <br>";
        
        //Annullo il form dei giudici e staff
        $valutazione_inserimento_voti_portale = '';
        //echo "Valutazione inserimento voti portale                 			(giudici):                     					$valutazione_inserimento_voti_portale <br>";
        $valutazione_interpretazione_simulatore_prova = '';
        //echo "Valutazione interpretazione dei simulatori 					(giudici):                     					$valutazione_interpretazione_simulatore_prova <br>";
        $valutazione_utilita_simulatore_prova = '';
        //echo "Valutazione utilità dei simulatori 							(giudici):                     					$valutazione_utilita_simulatore_prova <br>";
        $valutazione_coordinamento_backoffice = '';
        //echo "Valutazione coordinamento backoffice                 			(giudici/staff):								$valutazione_coordinamento_backoffice <br>";
        $valutazione_preparazione = '';
        //echo "Valutazione fase preparatoria al meeting 						(staff):                     					$valutazione_preparazione <br>";
        $valutazione_soddisfazione_incarico = '';
        //echo "Valutazione soddisfazione incarico 							(staff):                     					$valutazione_soddisfazione_incarico <br>";
        $valutazione_mezzi_informazioni = '';
        //echo "Valutazione mezzi ed informazioni 							(staff):                     					$valutazione_mezzi_informazioni <br>";
        
        //Annullo le variabili per il form feedback_generale_ospitante.php in modo che escluda le pagine di provenienza:
        //feedback_prova_area6.php
        $gradimento_prova6_intero = '';
        
        //feedback_staff.php
        $valutazione_preparazione_intero = '';
        
        //feedback_giudici.php
        $inserimento_voti_portale_intero = '';
        
        ?>


		<form name="feedback_simulatori" enctype="multipart/form-data" method="post" action="feedback_generale_ospitante.php?ruolo=<?php echo $ruolo ?>&id=<?php echo $id ?>">

		<?php
		//<label for="prova_assegnata" class="select">Seleziona la prova a cui sei stato assegnato</label>
        // <select name="prova_assegnata" id="prova_assegnata">
        //    <option value="1">Area 1</option>
        //    <option value="2">Area 2</option>
        //    <option value="4">Area 4</option>
        //</select>	
		//<br />
		?>
		
		<label for="slider-fill">Valuta la tua esperienza con il gruppo di lavoro (coordinatori Prove e Giudici) durante la preparazione delle stesse</label>
		<input type="range" name="valutazione_gruppo_lavoro" id="valutazione_gruppo_lavoro" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
		
		<label for="slider-fill">Valuta quanto le informazioni ricevute nell'incontro avuto con Cristina Marzano, prima della suddivisione per le prove, siano state utili</label>
		<input type="range" name="valutazione_informazioni_prova" id="valutazione_informazioni_prova" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
		
		<label for="slider-fill">Valuta se in qualche momento ti sei sentito "abbandonato a te stesso" </label>
		<input type="range" name="valutazione_abbandono_simulatori" id="valutazione_abbandono_simulatori" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
		
		<label for="slider-fill">Valuta l'organizzazione della prova a cui sei stato assegnato</label>
		<input type="range" name="valutazione_prova_assegnata" id="valutazione_prova_assegnata" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<label for="slider-fill">Valuta la performance dei giudici</label>
		<input type="range" name="valutazione_performance_giudice_prova" id="valutazione_performance_giudice_prova" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<br /><br />

        <input type="hidden" name="valutazione_quota_partecipante" value="<?php echo $valutazione_quota_partecipante; ?>" />
        <input type="hidden" name="valutazione_quota_osservatore" value="<?php echo $valutazione_quota_osservatore; ?>" />
        <input type="hidden" name="valutazione_tutor_competenza" value="<?php echo $valutazione_tutor_competenza; ?>" />
        <input type="hidden" name="valutazione_tutor_disponibilita" value="<?php echo $valutazione_tutor_disponibilita; ?>" />
		
        <input type="hidden" name="valutazione_gradimento_prova1" value="<?php echo $valutazione_gradimento_prova1; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova1" value="<?php echo $valutazione_difficolta_prova1; ?>" />
        <input type="hidden" name="valutazione_formativa_prova1" value="<?php echo $valutazione_formativa_prova1; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova1" value="<?php echo $valutazione_lavoro_gruppo_prova1; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova1" value="<?php echo $valutazione_performance_giudice_prova1; ?>" />
		<input type="hidden" name="valutazione_interpretazione_simulatore_prova1" value="<?php echo $valutazione_interpretazione_simulatore_prova1; ?>" />
        <input type="hidden" name="valutazione_utilita_simulatore_prova1" value="<?php echo $valutazione_utilita_simulatore_prova1; ?>" />

        <input type="hidden" name="valutazione_gradimento_prova2" value="<?php echo $valutazione_gradimento_prova2; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova2" value="<?php echo $valutazione_difficolta_prova2; ?>" />
        <input type="hidden" name="valutazione_formativa_prova2" value="<?php echo $valutazione_formativa_prova2; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova2" value="<?php echo $valutazione_lavoro_gruppo_prova2; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova2" value="<?php echo $valutazione_performance_giudice_prova2; ?>" />
        <input type="hidden" name="valutazione_interpretazione_simulatore_prova2" value="<?php echo $valutazione_interpretazione_simulatore_prova2; ?>" />
        <input type="hidden" name="valutazione_utilita_simulatore_prova2" value="<?php echo $valutazione_utilita_simulatore_prova2; ?>" />
		
        <input type="hidden" name="valutazione_gradimento_prova3" value="<?php echo $valutazione_gradimento_prova3; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova3" value="<?php echo $valutazione_difficolta_prova3; ?>" />
        <input type="hidden" name="valutazione_formativa_prova3" value="<?php echo $valutazione_formativa_prova3; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova3" value="<?php echo $valutazione_lavoro_gruppo_prova3; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova3" value="<?php echo $valutazione_performance_giudice_prova3; ?>" />
		<input type="hidden" name="valutazione_interpretazione_simulatore_prova3" value="<?php echo $valutazione_interpretazione_simulatore_prova3; ?>" />
        <input type="hidden" name="valutazione_utilita_simulatore_prova3" value="<?php echo $valutazione_utilita_simulatore_prova3; ?>" />
		
        <input type="hidden" name="valutazione_gradimento_prova4" value="<?php echo $valutazione_gradimento_prova4; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova4" value="<?php echo $valutazione_difficolta_prova4; ?>" />
        <input type="hidden" name="valutazione_formativa_prova4" value="<?php echo $valutazione_formativa_prova4; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova4" value="<?php echo $valutazione_lavoro_gruppo_prova4; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova4" value="<?php echo $valutazione_performance_giudice_prova4; ?>" />
		<input type="hidden" name="valutazione_interpretazione_simulatore_prova4" value="<?php echo $valutazione_interpretazione_simulatore_prova4; ?>" />
        <input type="hidden" name="valutazione_utilita_simulatore_prova4" value="<?php echo $valutazione_utilita_simulatore_prova4; ?>" />

        <input type="hidden" name="valutazione_gradimento_prova5" value="<?php echo $valutazione_gradimento_prova5; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova5" value="<?php echo $valutazione_difficolta_prova5; ?>" />
        <input type="hidden" name="valutazione_formativa_prova5" value="<?php echo $valutazione_formativa_prova5; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova5" value="<?php echo $valutazione_lavoro_gruppo_prova5; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova5" value="<?php echo $valutazione_performance_giudice_prova5; ?>" />
		<input type="hidden" name="valutazione_interpretazione_simulatore_prova5" value="<?php echo $valutazione_interpretazione_simulatore_prova5; ?>" />
        <input type="hidden" name="valutazione_utilita_simulatore_prova5" value="<?php echo $valutazione_utilita_simulatore_prova5; ?>" />
		
        <input type="hidden" name="valutazione_gradimento_prova6" value="<?php echo $valutazione_gradimento_prova6; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova6" value="<?php echo $valutazione_difficolta_prova6; ?>" />
        <input type="hidden" name="valutazione_formativa_prova6" value="<?php echo $valutazione_formativa_prova6; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova6" value="<?php echo $valutazione_lavoro_gruppo_prova6; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova6" value="<?php echo $valutazione_performance_giudice_prova6; ?>" />
		<input type="hidden" name="valutazione_interpretazione_simulatore_prova6" value="<?php echo $valutazione_interpretazione_simulatore_prova6; ?>" />
        <input type="hidden" name="valutazione_utilita_simulatore_prova6" value="<?php echo $valutazione_utilita_simulatore_prova6; ?>" />
		
		<input type="hidden" name="valutazione_gradimento_prova7" value="<?php echo $valutazione_gradimento_prova7; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova7" value="<?php echo $valutazione_difficolta_prova7; ?>" />
        <input type="hidden" name="valutazione_formativa_prova7" value="<?php echo $valutazione_formativa_prova7; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova7" value="<?php echo $valutazione_lavoro_gruppo_prova7; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova7" value="<?php echo $valutazione_performance_giudice_prova7; ?>" />
		
        <input type="hidden" name="valutazione_gradimento_prova8" value="<?php echo $valutazione_gradimento_prova8; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova8" value="<?php echo $valutazione_difficolta_prova8; ?>" />
        <input type="hidden" name="valutazione_formativa_prova8" value="<?php echo $valutazione_formativa_prova8; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova8" value="<?php echo $valutazione_lavoro_gruppo_prova8; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova8" value="<?php echo $valutazione_performance_giudice_prova8; ?>" />
		
        <input type="hidden" name="valutazione_inserimento_voti_portale" value="<?php echo $valutazione_inserimento_voti_portale; ?>" />
        <input type="hidden" name="valutazione_interpretazione_simulatore_prova" value="<?php echo $valutazione_interpretazione_simulatore_prova; ?>" />
        <input type="hidden" name="valutazione_utilita_simulatore_prova" value="<?php echo $valutazione_utilita_simulatore_prova; ?>" />
        
        <input type="hidden" name="valutazione_coordinamento_backoffice" value="<?php echo $valutazione_coordinamento_backoffice; ?>" />
        
        <input type="hidden" name="valutazione_preparazione" value="<?php echo $valutazione_preparazione; ?>" />
        <input type="hidden" name="valutazione_soddisfazione_incarico" value="<?php echo $valutazione_soddisfazione_incarico; ?>" />
        <input type="hidden" name="valutazione_mezzi_informazioni" value="<?php echo $valutazione_mezzi_informazioni; ?>" />
		<input type="submit" value="Avanti" />
		</form>


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>