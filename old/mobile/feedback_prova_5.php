<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2015 - Feedback Prova Cinque</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.php" title="Meeting 2015 - Homepage"><img src="logo_meeting.png" alt="Portale Meeting 2015 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2015</h1>
		
		<p><strong>Feedback Prova Cinque</strong></p>

		<?
        //recupero i dati nella barra dell'indirizzo
        $ruolo=$_GET['ruolo'];
        $id=$_GET['id'];
        //echo "Ruolo                    (tutti):                     $ruolo <br>";
        //echo "ID          (tutti):           $id <br>";
		
		$valutazione_accoglienza_venerdi = $_POST['valutazione_accoglienza_venerdi'];
		$valutazione_speed_meeting = $_POST['valutazione_speed_meeting'];
		$valutazione_pernottamento = $_POST['valutazione_pernottamento'];
		$valutazione_pasti = $_POST['valutazione_pasti'];
		$valutazione_logistica = $_POST['valutazione_logistica'];
		$valutazione_tour = $_POST['valutazione_tour'];
        
		$valutazione_gradimento_prova1 = $_POST['valutazione_gradimento_prova1'];
		//echo "Valutazione gradimento prova area 1: $valutazione_gradimento_prova1 <br>";
		$valutazione_formativa_prova1 = $_POST['valutazione_formativa_prova1'];
		//echo "Valutazione validità formativa prova area 1: $valutazione_formativa_prova1 <br>";
		$valutazione_difficolta_prova1 = $_POST['valutazione_difficolta_prova1'];
		//echo "Valutazione difficoltà prova area 1: $valutazione_difficolta_prova1 <br>";
		$valutazione_lavoro_gruppo_prova1 = $_POST['valutazione_lavoro_gruppo_prova1'];
		//echo "Valutazione lavoro di gruppo prova area 1: $valutazione_lavoro_gruppo_prova1 <br>";
		$valutazione_performance_giudice_prova1 = $_POST['valutazione_performance_giudice_prova1'];
		//echo "Valutazione performance giudici prova area 1: $valutazione_performance_giudice_prova1 <br>";

		$valutazione_gradimento_prova2 = $_POST['valutazione_gradimento_prova2'];
		//echo "Valutazione gradimento prova area 2: $valutazione_gradimento_prova2 <br>";
		$valutazione_formativa_prova2 = $_POST['valutazione_formativa_prova2'];
		//echo "Valutazione validità formativa prova area 2: $valutazione_formativa_prova2 <br>";
		$valutazione_difficolta_prova2 = $_POST['valutazione_difficolta_prova2'];
		//echo "Valutazione difficoltà prova area 2: $valutazione_difficolta_prova2 <br>";
		$valutazione_lavoro_gruppo_prova2 = $_POST['valutazione_lavoro_gruppo_prova2'];
		//echo "Valutazione lavoro di gruppo prova area 2: $valutazione_lavoro_gruppo_prova2 <br>";
		$valutazione_performance_giudice_prova2 = $_POST['valutazione_performance_giudice_prova2'];
		//echo "Valutazione performance giudici prova area 2: $valutazione_performance_giudice_prova2 <br>";
		$valutazione_interpretazione_simulatore_prova2 = $_POST['valutazione_interpretazione_simulatore_prova2'];
		//echo "Valutazione interpretazione simulatori prova area 2: $valutazione_interpretazione_simulatore_prova2 <br>";
		$valutazione_utilita_simulatore_prova2 = $_POST['valutazione_utilita_simulatore_prova2'];
		//echo "Valutazione utilità simulatori prova area 2: $valutazione_utilita_simulatore_prova2 <br>";
		
		$valutazione_gradimento_prova3 = $_POST['valutazione_gradimento_prova3'];
		//echo "Valutazione gradimento prova area 3: $valutazione_gradimento_prova3 <br>";
		$valutazione_formativa_prova3 = $_POST['valutazione_formativa_prova3'];
		//echo "Valutazione validità formativa prova area 3: $valutazione_formativa_prova3 <br>";
		$valutazione_difficolta_prova3 = $_POST['valutazione_difficolta_prova3'];
		//echo "Valutazione difficoltà prova area 3: $valutazione_difficolta_prova3 <br>";
		$valutazione_lavoro_gruppo_prova3 = $_POST['valutazione_lavoro_gruppo_prova3'];
		//echo "Valutazione lavoro di gruppo prova area 3: $valutazione_lavoro_gruppo_prova3 <br>";
		$valutazione_performance_giudice_prova3 = $_POST['valutazione_performance_giudice_prova3'];
		//echo "Valutazione performance giudici prova area 3: $valutazione_performance_giudice_prova3 <br>";
		$valutazione_interpretazione_simulatore_prova3 = $_POST['valutazione_interpretazione_simulatore_prova3'];
		//echo "Valutazione interpretazione simulatori prova area 2: $valutazione_interpretazione_simulatore_prova2 <br>";
		$valutazione_utilita_simulatore_prova3 = $_POST['valutazione_utilita_simulatore_prova3'];
		//echo "Valutazione utilità simulatori prova area 2: $valutazione_utilita_simulatore_prova2 <br>";
        
        //recupero i dati di input del form precedente
		$valutazione_gradimento_prova4 = $_REQUEST['valutazione_gradimento_prova4'];
		//echo "Valutazione gradimento prova area 4: $valutazione_gradimento_prova4 <br>";
		$valutazione_formativa_prova4 = $_REQUEST['valutazione_formativa_prova4'];
		//echo "Valutazione validità formativa prova area 4: $valutazione_formativa_prova4 <br>";
		$valutazione_difficolta_prova4 = $_REQUEST['valutazione_difficolta_prova4'];
		//echo "Valutazione difficoltà prova area 4: $valutazione_difficolta_prova4 <br>";
		$valutazione_lavoro_gruppo_prova4 = $_REQUEST['valutazione_lavoro_gruppo_prova4'];
		//echo "Valutazione lavoro di gruppo prova area 4: $valutazione_lavoro_gruppo_prova4 <br>";
		$valutazione_performance_giudice_prova4 = $_REQUEST['valutazione_performance_giudice_prova4'];
		//echo "Valutazione performance giudici prova area 4: $valutazione_performance_giudice_prova4 <br>";
        ?>


		<form name="feedback_prova_5" enctype="multipart/form-data" method="post" action="feedback_prova_6.php?ruolo=<?php echo $ruolo ?>&id=<?php echo $id ?>">

		<a href="#popupCloseRight" data-rel="popup" class="ui-btn ui-corner-all ui-shadow ui-btn-inline">Descrizione della Prova</a>
		
		<div data-role="popup" id="popupCloseRight" class="ui-content" style="max-width:280px">
		<a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
		<p>Questa prova prevede di valutare il comportamento dei partecipanti in una simulazione di una scena di quotidianità di fronte all’impiego di alcolici e droghe come spunto per la riflessione sul tema delle dipendenze. 
		<br />
		In una seconda parte della prova viene richiesto alla squadra di 3 partecipanti di simulare un’attività di sensibilizzazione in piazza sul tema delle dipendenze. 
		<br />
		Durante tutto lo svolgimento della prova 2 membri della squadra svolgono una valutazione esterna del comportamento dei compagni nella prima simulazione e compilano un questionario per conoscere quale sia il loro livello di conoscenza su questa tematica.</p>
		</div>
		<br />
		<br />
		
		
		<label for="slider-fill">Valuta quanto ti è piaciuta</label>
		<input type="range" name="valutazione_gradimento_prova5" id="valutazione_gradimento_prova5" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<label for="slider-fill">Valuta quanto è stata formativa</label>
		<input type="range" name="valutazione_formativa_prova5" id="valutazione_formativa_prova5" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<label for="slider-fill">Valuta la difficoltà</label>
		<input type="range" name="valutazione_difficolta_prova5" id="valutazione_difficolta_prova5" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<label for="slider-fill">Valuta quanto ha facilitato il lavoro in gruppo</label>
		<input type="range" name="valutazione_lavoro_gruppo_prova5" id="valutazione_lavoro_gruppo_prova5" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<label for="slider-fill">Valuta la performance dei giudici</label>
		<input type="range" name="valutazione_performance_giudice_prova5" id="valutazione_performance_giudice_prova5" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
				
		<label for="slider-fill">Valuta l'interpretazione dei simulatori</label>
		<input type="range" name="valutazione_interpretazione_simulatore_prova5" id="valutazione_interpretazione_simulatore_prova5" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<label for="slider-fill">Valuta l'utilità dei simulatori</label>
		<input type="range" name="valutazione_utilita_simulatore_prova5" id="valutazione_utilita_simulatore_prova5" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<br /><br />
		
		<input type="hidden" name="valutazione_accoglienza_venerdi" value="<?php echo $valutazione_accoglienza_venerdi; ?>" />
		<input type="hidden" name="valutazione_speed_meeting" value="<?php echo $valutazione_speed_meeting; ?>" />
		<input type="hidden" name="valutazione_pernottamento" value="<?php echo $valutazione_pernottamento; ?>" />
		<input type="hidden" name="valutazione_pasti" value="<?php echo $valutazione_pasti; ?>" />
		<input type="hidden" name="valutazione_logistica" value="<?php echo $valutazione_logistica; ?>" />
		<input type="hidden" name="valutazione_tour" value="<?php echo $valutazione_tour; ?>" />	
		
        <input type="hidden" name="valutazione_gradimento_prova1" value="<?php echo $valutazione_gradimento_prova1; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova1" value="<?php echo $valutazione_difficolta_prova1; ?>" />
        <input type="hidden" name="valutazione_formativa_prova1" value="<?php echo $valutazione_formativa_prova1; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova1" value="<?php echo $valutazione_lavoro_gruppo_prova1; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova1" value="<?php echo $valutazione_performance_giudice_prova1; ?>" />
		
        <input type="hidden" name="valutazione_gradimento_prova2" value="<?php echo $valutazione_gradimento_prova2; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova2" value="<?php echo $valutazione_difficolta_prova2; ?>" />
        <input type="hidden" name="valutazione_formativa_prova2" value="<?php echo $valutazione_formativa_prova2; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova2" value="<?php echo $valutazione_lavoro_gruppo_prova2; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova2" value="<?php echo $valutazione_performance_giudice_prova2; ?>" />
        <input type="hidden" name="valutazione_interpretazione_simulatore_prova2" value="<?php echo $valutazione_interpretazione_simulatore_prova2; ?>" />
        <input type="hidden" name="valutazione_utilita_simulatore_prova2" value="<?php echo $valutazione_utilita_simulatore_prova2; ?>" />
		
        <input type="hidden" name="valutazione_gradimento_prova3" value="<?php echo $valutazione_gradimento_prova3; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova3" value="<?php echo $valutazione_difficolta_prova3; ?>" />
        <input type="hidden" name="valutazione_formativa_prova3" value="<?php echo $valutazione_formativa_prova3; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova3" value="<?php echo $valutazione_lavoro_gruppo_prova3; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova3" value="<?php echo $valutazione_performance_giudice_prova3; ?>" />
		<input type="hidden" name="valutazione_interpretazione_simulatore_prova3" value="<?php echo $valutazione_interpretazione_simulatore_prova3; ?>" />
        <input type="hidden" name="valutazione_utilita_simulatore_prova3" value="<?php echo $valutazione_utilita_simulatore_prova3; ?>" />
		
        <input type="hidden" name="valutazione_gradimento_prova4" value="<?php echo $valutazione_gradimento_prova4; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova4" value="<?php echo $valutazione_difficolta_prova4; ?>" />
        <input type="hidden" name="valutazione_formativa_prova4" value="<?php echo $valutazione_formativa_prova4; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova4" value="<?php echo $valutazione_lavoro_gruppo_prova4; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova4" value="<?php echo $valutazione_performance_giudice_prova4; ?>" />
		<input type="submit" value="Avanti" />
		</form>


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>