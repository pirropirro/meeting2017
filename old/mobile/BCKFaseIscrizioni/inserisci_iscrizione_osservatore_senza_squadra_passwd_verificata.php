<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Iscrizione Osservatore senza squadra</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
	<script type="text/javascript">
		$(document).on("change", "#checkbox-1", function () {
			if ($(this).prop("checked")) {
				$("#submit").button("enable");
			} else {
				$("#submit").button("disable");
			}
		});
    </script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2016 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		<p><strong>Iscrizione Osservatore senza squadra</strong></p>

        <div data-html="true">

			<?php
            include ("config.inc.php");
            
            $id_comitato=$_REQUEST['id_comitato'];
            $passwd=$_REQUEST['passwd'];
            //echo $id_comitato;
            //echo $passwd;
            
            include ("apri_db.php");
            
            //Comitato di cui si inserisci la passwd
            $query = "SELECT passwd, tipo_meeting FROM comitati WHERE id = '$id_comitato'";
            $result = mysql_query($query, $db);
            $row = mysql_fetch_array( $result );
            mysql_close($db); 
            
            if ($row[passwd] == $passwd  && !($id_comitato == ''))
            {
                ?>
                <form name="form1" enctype="multipart/form-data" method="post" action="salva_inserisci_iscrizione_osservatore_senza_squadra.php">
            
						<label for="nome">Nome</label>
						<input type="text" name="nome" size="20" maxlength="40" placeholder="Campo obbligatorio" required>

						<label for="cognome">Cognome</label>
						<input type="text" name="cognome" size="20" maxlength="40" placeholder="Campo obbligatorio" required>
						
						<label for="telefono">Recapito telefonico</label>						
						<input type="tel" name="telefono" size="8" required title="prefisso e numero senza spazi o altri caratteri (max 10 cifre)" placeholder="prefisso e numero senza spazi o altri caratteri (max 10 cifre)"pattern="[0-9]{10}">
						
						<label for="mail">Indrizzo e-mail</label>
						<input type="email" name="mail" size="28" placeholder="Campo obbligatorio" required>

						<label for="esigenze">Esigenze alimentari</label>
						<input type="text" name="esigenze" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario">

						<label for="disabilita">Eventuali disabilità</label>
						<input type="text" name="disabilita" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario">
                
                <?php
                if ($row[tipo_meeting] == 'PR'  || $row[tipo_meeting] == 'R')
                {
                    ?>

					<fieldset data-role="controlgroup" data-type="horizontal"  data-mini="true">
						<legend>Disponibilità come simulatore</legend>
						<input name="simulatore" id="simulatore_2" value="2" type="radio">
						<label for="simulatore_2">Qualificato</label>
						<input name="simulatore" id="simulatore_1" value="1" type="radio">
						<label for="simulatore_1">Aspirante simulatore</label>
						<input name="simulatore" id="simulatore_0" value="0" checked="checked" type="radio">
						<label for="simulatore_0">Non disponibile</label>
					</fieldset>
                    <br />
            
					<fieldset data-role="controlgroup" data-type="horizontal"  data-mini="true">
						<legend>Disponibilità come truccatore</legend>
						<input name="truccatore" id="truccatore_2" value="2" type="radio">
						<label for="truccatore_2">Qualificato</label>
						<input name="truccatore" id="truccatore_0" value="0" checked="checked" type="radio">
						<label for="truccatore_0">Non disponibile</label>
					</fieldset>
                    <br />
                <?php
                }
                ?>

                <fieldset data-role="controlgroup">
                    <legend>Disponibilità come giudice</legend>
                    <input name="giudice_1" id="giudice_1_MST" value="1" type="checkbox">
                    <label for="giudice_1_MST">Istruttore di Educazione alla Sessualità e Prevenzione dalle MST</label>
                    <input name="giudice_2" id="giudice_2_EA" value="1" type="checkbox">
                    <label for="giudice_2_EA">Istruttore di Educazione Alimentare</label>
                    <input name="giudice_3" id="giudice_3_MON" value="1" type="checkbox">
                    <label for="giudice_3_MON">Monitore PS</label>
                    <input name="giudice_4" id="giudice_4_OSG" value="1" type="checkbox">
                    <label for="giudice_4_OSG">Operatore Sociale Generico</label>
                    <input name="giudice_5" id="giudice_5_OPEM" value="1" type="checkbox">
                    <label for="giudice_5_OPEM">OPEM</label>
                    <input name="giudice_6" id="giudice_6_CIA" value="1" type="checkbox">
                    <label for="giudice_6_CIA">Operatore DRRCCA</label>
                    <input name="giudice_7" id="giudice_7_DIU" value="1" type="checkbox">
                    <label for="giudice_7_DIU">Istruttore DIU</label>
                    <input name="giudice_8" id="giudice_8_ACI" value="1" type="checkbox">
                    <label for="giudice_8_ACI">Operatore di Cooperazione Internazionale</label>
                    <input name="giudice_9" id="giudice_9_GIA" value="1" type="checkbox">
                    <label for="giudice_9_GIA">Operatore Giovani in Azione</label>
                    <input name="giudice_10" id="giudice_10_APG" value="1" type="checkbox">
                    <label for="giudice_10_APG">Operatore Attività per i Giovani</label>
                    <input name="giudice_11" id="giudice_11_EAP" value="1" type="checkbox">
                    <label for="giudice_11_EAP">Istruttore di EducAzione alla Pace</label>
                    <input name="giudice_12" id="giudice_12_SVIL" value="1" type="checkbox">
                    <label for="giudice_12_SVIL">Operatore Sviluppo</label>
                </fieldset>
                <br />
            
                <br />
                <p align="left">
                <input type="hidden" name="id_comitato" value="<?php echo $id_comitato; ?>" />
                <input type="hidden" name="passwd" value="<?php echo $passwd; ?>" />
                <input type="submit" value="Inserisci" />
                </form>
            
                <?php
            }
            else
            {
                echo "Password errata";
                ?>
                <br /><br />
                <form method="post" action="index.php">
                <input type='submit' value='Torna alla Home'></form>
                <?php
            }
            ?>

        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>