<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Iscrizione Osservatore senza squadra</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
	<script type="text/javascript">
		$(document).on("change", "#checkbox-1", function () {
			if ($(this).prop("checked")) {
				$("#submit").button("enable");
			} else {
				$("#submit").button("disable");
			}
		});
    </script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2016 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		<p><strong>Iscrizione Osservatore senza squadra</strong></p>

        <div data-html="true">

			<?php
            include ("config.inc.php");
            $fp = fopen ("log_iscrizioni.txt",a);
            
            //Leggo i dati in input
            $id_comitato=$_REQUEST['id_comitato'];
            $passwd=$_REQUEST['passwd'];
            
            $nome=$_REQUEST['nome'];
            $nome_ins = addslashes(stripslashes($nome)); 
            $nome_ins = str_replace("<", "&lt;", $nome_ins);
            $nome_ins = str_replace(">", "&gt;", $nome_ins);
            $cognome=$_REQUEST['cognome'];
            $cognome_ins = addslashes(stripslashes($cognome)); 
            $cognome_ins = str_replace("<", "&lt;", $cognome_ins);
            $cognome_ins = str_replace(">", "&gt;", $cognome_ins);
            $telefono=$_REQUEST['telefono'];
            $mail=strtolower($_REQUEST['mail']);
            $esigenze=$_REQUEST['esigenze'];
            $esigenze_ins = addslashes(stripslashes($esigenze)); 
            $esigenze_ins = str_replace("<", "&lt;", $esigenze_ins);
            $esigenze_ins = str_replace(">", "&gt;", $esigenze_ins);
            $disabilita=$_REQUEST['disabilita'];
            $disabilita_ins = addslashes(stripslashes($disabilita)); 
            $disabilita_ins = str_replace("<", "&lt;", $disabilita_ins);
            $disabilita_ins = str_replace(">", "&gt;", $disabilita_ins);
            $simulatore=$_REQUEST['simulatore'];
            $truccatore=$_REQUEST['truccatore'];
            $giudice_1=$_REQUEST['giudice_1'];
            $giudice_2=$_REQUEST['giudice_2'];
            $giudice_3=$_REQUEST['giudice_3'];
            $giudice_4=$_REQUEST['giudice_4'];
            $giudice_5=$_REQUEST['giudice_5'];
            $giudice_6=$_REQUEST['giudice_6'];
            $giudice_7=$_REQUEST['giudice_7'];
            $giudice_8=$_REQUEST['giudice_8'];
            $giudice_9=$_REQUEST['giudice_9'];
            $giudice_10=$_REQUEST['giudice_10'];
            $giudice_11=$_REQUEST['giudice_11'];
            $giudice_12=$_REQUEST['giudice_12'];
            
            include ("apri_db.php");
            
            //Estraggo i dati del comitato
			$query_comitato = "SELECT		c.nome_comitato AS nome_comitato,
															c.tipo_meeting AS tipo_meeting,
															c.nome_presidente AS nome_presidente,
															c.cognome_presidente AS cognome_presidente,
															c.mail_comitato AS mail_comitato,
															c.nome_consigliere AS nome_consigliere,
															c.cognome_consigliere AS cognome_consigliere,
															c.mail_consigliere AS mail_volontario
										FROM comitati AS c
										WHERE c.id = $id_comitato";
			$result_comitato = mysql_query($query_comitato, $db);
			$row_comitato = mysql_fetch_array($result_comitato);

			//Controllo che non ci sia già una mail registrata uguale
            $query_estrazione = "SELECT * FROM iscrizioni WHERE mail = '$mail'";
            $result = mysql_query($query_estrazione, $db);
            
            if (mysql_num_rows($result) == '1')
            {
                echo "Hai inserito un indirizzo mail già presente nelle iscrizioni. Ogni volontario iscritto al Meeting deve avere un indirizzo mail valido e distinto!<br><br>";
            }
            else
            {
                //Faccio la insert con i dati dell'osservatore facendo differenza se è PR,R o N
                if ($row_comitato[tipo_meeting] == 'PR' || $row_comitato[tipo_meeting] == 'R')
                {
                    $query_inserimento = "INSERT INTO iscrizioni (id_comitato, ruolo, nome, cognome, telefono, mail, esigenze, disabilita, simulatore, truccatore, giudice_1, giudice_2, giudice_3, giudice_4, giudice_5, giudice_6, giudice_7, giudice_8, giudice_9, giudice_10, giudice_11, giudice_12) VALUES ('$id_comitato', 'osservatore', '$nome_ins', '$cognome_ins', '$telefono', '$mail', '$esigenze_ins', '$disabilita_ins', '$simulatore', '$truccatore', '$giudice_1', '$giudice_2', '$giudice_3', '$giudice_4', '$giudice_5', '$giudice_6', '$giudice_7', '$giudice_8', '$giudice_9', '$giudice_10', '$giudice_11', '$giudice_12')";
                }
                else
                {
                    $query_inserimento = "INSERT INTO iscrizioni (id_comitato, ruolo, nome, cognome, telefono, mail, esigenze, disabilita, giudice_1, giudice_2, giudice_3, giudice_4, giudice_5, giudice_6, giudice_7, giudice_8, giudice_9, giudice_10, giudice_11, giudice_12) VALUES ('$id_comitato', 'osservatore', '$nome_ins', '$cognome_ins', '$telefono', '$mail', '$esigenze_ins', '$disabilita_ins', '$giudice_1', '$giudice_2', '$giudice_3', '$giudice_4', '$giudice_5', '$giudice_6', '$giudice_7', '$giudice_8', '$giudice_9', '$giudice_10', '$giudice_11', '$giudice_12')";
                }
                
                $result_inserimento =  mysql_query($query_inserimento, $db);
                //Controllo che sia andata a buon fine la insert
                
                if ($result_inserimento)
                {
                    fwrite($fp,date('d/m/Y H:i:s').' - '."Iscrizione osservatore $nome $cognome per il $row_comitato[nome_comitato] effettuata da $row_comitato[nome_consigliere] $row_comitato[cognome_consigliere] correttamente inserita nel DB."."\r\n");
                    echo "L'inserimento dell'osservatore $nome $cognome afferente al $row_comitato[nome_comitato] è avvenuto correttamente.<br><br>";
                    //Invio la mail di conferma al delagato che ha inserito l'osservatore
                    include('../PHPMailer/class.phpmailer.php');
                    //require_once('PHPMailer/class.smtp.php');  
                
                    $mittente = "meeting.2016@piemonte.cri.it";
                    $nomemittente = "Back Office Meeting 2016";
                    $destinatario = "$row_comitato[mail_volontario]";
                    $ServerSMTP = "mailbox.cri.it"; //server SMTP
                    $oggetto = "Iscrizione Osservatore Meeting 2016";
                    $corpo_messaggio = "Ciao $row_comitato[nome_consigliere],\nla presente per notificare l'avvenuta iscrizione dell'osservatore $nome $cognome afferente al $row_comitato[nome_comitato].\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
                
                    $msg = new PHPMailer;
                    $msg->CharSet = "UTF-8";
                    $msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
                    //$msg->IsHTML(true);
                    $msg->SMTPAuth = true; // Autenticazione SMTP
                    $msg->SMTPKeepAlive = "true";
                    $msg->Host = $ServerSMTP;
                    $msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
                    $msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
                
                    $msg->From = $mittente;
                    $msg->FromName = $nomemittente;
                    $msg->AddAddress($destinatario); 
                    $msg->AddCC("$row_comitato[mail_comitato]");
                    $msg->AddBCC("paolo.ditoma@libero.it");
					$msg->AddBCC("backoffice.meeting@gmail.com");
                    $msg->Subject = $oggetto; 
                    $msg->Body = $corpo_messaggio;
                
                
                    if(!$msg->Send())
                    {
                        echo "Si è verificato un errore nell'invio della mail di notifica iscrizione osservatore:".$msg->ErrorInfo;
                        fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Invio mail notifica iscrizione osservatore $nome $cognome effettuata da $row_comitato[nome_consigliere] $row_comitato[cognome_consigliere] con indirizzo $row_comitato[mail_volontario]: ".$msg->ErrorInfo."\r\n");
                    }
                    else
                    {
                        echo "Ti abbiamo inviato una mail di notifica.";
                        fwrite($fp,date('d/m/Y H:i:s').' - '."Invio mail notifica avvenuta iscrizione osservatore $nome $cognome effettuata da $row_comitato[nome_consigliere] $row_comitato[cognome_consigliere] correttamente effettuato all'indirizzo $row_comitato[mail_volontario]."."\r\n");
                    }
                    
                    //Calcolo il Token, lo immetto nel DB ed invio la mail per la conferma
                    $code = array();
                    $query_id = "SELECT * FROM iscrizioni WHERE id_comitato = '$id_comitato' AND nome = '$nome_ins' AND cognome = '$cognome_ins' AND telefono = '$telefono' AND mail = '$mail'";
                    $result_id = mysql_query($query_id, $db);
                    $row_id = mysql_fetch_array($result_id);
                    $md5 = strtoupper(md5($row_id[id] . $cognome . $telefono . $mail));
                    $code[] = substr ($md5, 0, 5);
                    $code[] = substr ($md5, 5, 5);
                    $code[] = substr ($md5, 10, 5);
                    $code[] = substr ($md5, 15, 5);
                    $token = implode ("-", $code); 
                    $query = "UPDATE iscrizioni SET token = '$token' WHERE id = '$row_id[id]'";
                    mysql_query($query, $db);
            
                    $mittente = "meeting.2016@piemonte.cri.it";
                    $nomemittente = "Back Office Meeting 2016";
                    $destinatario = "$mail";
                    $ServerSMTP = "mailbox.cri.it"; //server SMTP
                    $oggetto = "Iscrizione Osservatore Meeting 2016 - Conferma indirizzo mail";
                    $corpo_messaggio = "Ciao $nome,\nsei appena stato iscritto come osservatore della squadra del $row_comitato[nome_comitato] per la partecipazione al Meeting 2016 dei Giovani della Croce Rossa Italiana. Al fine di verificare la veridicità delle informazioni inserite ti chiediamo di confermare il tuo indirizzo mail cliccando sul link seguente\n\n  http://www.itempa.com/meeting2016/verifica_mail_iscrizione.php?token=$token\n\nOgni altra notifica riguardante il Meeting ti sarà recapitata all'indirizzo mail confermato.\n\nN.B.Qualora tu intenda utilizzare un altro indirizzo, ti preghiamo di indicarcelo rispondendo a questa mail senza procedere quindi alla conferma.\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
                
                    $msg = new PHPMailer;
                    $msg->CharSet = "UTF-8";
                    $msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
                    //$msg->IsHTML(true);
                    $msg->SMTPAuth = true; // Autenticazione SMTP
                    $msg->SMTPKeepAlive = "true";
                    $msg->Host = $ServerSMTP;
                    $msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
                    $msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
            
                    $msg->From = $mittente;
                    $msg->FromName = $nomemittente;
                    $msg->AddAddress($destinatario); 
            //		$msg->AddCC("$row[mail_comitato]");
            		$msg->AddBCC("paolo.ditoma@libero.it");
                    $msg->Subject = $oggetto; 
                    $msg->Body = $corpo_messaggio;
                    
                    if(!$msg->Send())
                    {
                        fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE invio della mail di verifica indirizzo mail a $nome $cognome all'indirizzo $mail con Token $token".$msg->ErrorInfo."\r\n");
                    }
                    else
                    {
                        $query = "UPDATE iscrizioni SET mail_reminder ='1' WHERE token = '$token'";
                        mysql_query($query, $db);
                        fwrite($fp,date('d/m/Y H:i:s').' - '."Inviata mail di verifica indirizzo mail a $nome $cognome per conferma mail $mail con Token $token"."\r\n");
                    }
                }
                else
                {
                    echo "Si è verificato un errore nell'inserimento dell'iscrizione, contattare l'amministratore del sistema.<br><br>";
                    fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE query insert ( $query_inserimento ) dell'osservatore $nome $cognome per il $row_comitato[nome_comitato] : ".$result_inserimento."\r\n");
                }
            }
            
            mysql_close($db);
            fclose($fp);
            
            ?>

        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>