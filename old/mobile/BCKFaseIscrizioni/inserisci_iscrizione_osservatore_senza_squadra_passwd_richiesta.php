<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Iscrizione Osservatore senza squadra</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2016 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		<p><strong>Iscrizione Osservatore senza squadra</strong></p>

        <div data-html="true">

			<?php
            include ("config.inc.php");
            include ("apri_db.php");
            ?>
            
            <form method="post" action="inserisci_iscrizione_osservatore_senza_squadra_passwd_verificata.php">
            
            <br />
            <label>Squadra del<br>
             
            <select name="id_comitato" size="8" >
            
            <?php
            $query = "SELECT 	c.nome_comitato AS nome_comitato,
											c.id AS id,
											c.nome_presidente AS nome_presidente,
											c.cognome_presidente AS cognome_presidente,
											c.mail_comitato AS mail_comitato,
											c.nome_consigliere AS nome_delegato,
											c.cognome_consigliere AS cognome_delegato,
											c.mail_consigliere AS mail_delegato
											FROM comitati AS c
											WHERE c.id NOT IN (
																SELECT p.id_comitato
																FROM preiscrizioni AS p
																WHERE p.id_comitato = c.id
																AND (p.iscrizione = '1' OR p.iscrizione IS NULL)
															)
											AND c.tipo_meeting!='N'
											AND c.mail_consigliere!=''";
            
            $result = mysql_query($query, $db);
            while($row = mysql_fetch_array( $result )) 
            {
                ?>
                <option value='<?php echo $row[id]; ?>'><?php echo $row[nome_comitato]; ?></option>
                <?php
            }
            mysql_close($db);
            ?>
            </select>
            </label>
            
            <br /><br /><br />
            
            <b>Password</b>
            <input name="passwd" id="passwd" value="" autocomplete="off" type="password" maxlength="15" title="Inserisci la password ricevuta via mail" required><br />
            <br /><br />
            
            <input type="submit" value="Verifica" />
            <br />
            <br />
            </form>

        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>