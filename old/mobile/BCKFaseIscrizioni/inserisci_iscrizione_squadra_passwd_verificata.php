<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2016 - Iscrizione Squadra</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
	<script type="text/javascript">
		$(document).on("change", "#checkbox-1", function () {
			if ($(this).prop("checked")) {
				$("#submit").button("enable");
			} else {
				$("#submit").button("disable");
			}
		});
    </script>
    
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2016 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2016 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2016</h1>

		<p><strong>Iscrizione Squadra</strong></p>

        <div data-html="true">

			<?php
			//Apro il DB e recupero nome comitato e passwd
			include ("config.inc.php");

			$id_comitato=$_REQUEST['id_comitato'];
			//echo $id_comitato;
			$passwd=$_REQUEST['passwd'];
			//echo $passwd;
			include ("apri_db.php");

			//Comitato di cui si inserisci la passwd
			$query = "SELECT nome_comitato, passwd FROM comitati WHERE id = '$id_comitato'";
			$result = mysql_query($query, $db);
			$row = mysql_fetch_array( $result );
			mysql_close($db); 

			if ($row[passwd] == $passwd  && !($id_comitato == ''))
			{
				//Se la passwd è verificata	apro il form di inserimento
				?>
				<form name="form_iscrizione_squadra" enctype="multipart/form-data" method="post" action="salva_inserisci_iscrizione_squadra.php">

					<div class="ui-corner-all custom-corners">
					  <div class="ui-bar ui-bar-a">
						<h3>Capitano</h3>
					  </div>
					  <div class="ui-body ui-body-a">
						<label for="nome_1">Nome</label>
						<input type="text" name="nome_1" size="20" maxlength="40" placeholder="Campo obbligatorio" required>

						<label for="cognome_1">Cognome</label>
						<input type="text" name="cognome_1" size="20" maxlength="40" placeholder="Campo obbligatorio" required>
						
						<label for="telefono_1">Recapito telefonico</label>						
						<input type="tel" name="telefono_1" size="8" required title="prefisso e numero senza spazi o altri caratteri (max 10 cifre)" placeholder="prefisso e numero senza spazi o altri caratteri (max 10 cifre)"pattern="[0-9]{10}">
						
						<label for="mail_1">Indrizzo e-mail</label>
						<input type="email" name="mail_1" size="28" placeholder="Campo obbligatorio" required>

						<label for="esigenze_1">Esigenze alimentari</label>
						<input type="text" name="esigenze_1" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario">

						<label for="disabilita_1">Eventuali disabilità</label>
						<input type="text" name="disabilita_1" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario">
					
					  </div>
					</div>
					
					<br />
				
					<div class="ui-corner-all custom-corners">
					  <div class="ui-bar ui-bar-a">
						<h3>Partecipante</h3>
					  </div>
					  <div class="ui-body ui-body-a">
						<label for="nome_2">Nome</label>
						<input type="text" name="nome_2" size="20" maxlength="40" placeholder="Campo obbligatorio" required>

						<label for="cognome_2">Cognome</label>
						<input type="text" name="cognome_2" size="20" maxlength="40" placeholder="Campo obbligatorio" required>
						
						<label for="telefono_2">Recapito telefonico</label>						
						<input type="tel" name="telefono_2" size="8" required title="prefisso e numero senza spazi o altri caratteri (max 10 cifre)" placeholder="prefisso e numero senza spazi o altri caratteri (max 10 cifre)"pattern="[0-9]{10}">
						
						<label for="mail_2">Indrizzo e-mail</label>
						<input type="email" name="mail_2" size="28" placeholder="Campo obbligatorio" required>
					
						<label for="esigenze_2">Esigenze alimentari</label>
						<input type="text" name="esigenze_2" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario">
					
						<label for="disabilita_2">Eventuali disabilità</label>
						<input type="text" name="disabilita_2" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario">
					
					  </div>
					</div>

					<br />
					
					<div class="ui-corner-all custom-corners">
					  <div class="ui-bar ui-bar-a">
						<h3>Partecipante</h3>
					  </div>
					  <div class="ui-body ui-body-a">
						<label for="nome_3">Nome</label>
						<input type="text" name="nome_3" size="20" maxlength="40" placeholder="Campo obbligatorio" required>

						<label for="cognome_3">Cognome</label>
						<input type="text" name="cognome_3" size="20" maxlength="40" placeholder="Campo obbligatorio" required>
						
						<label for="telefono_3">Recapito telefonico</label>						
						<input type="tel" name="telefono_3" size="8" required title="prefisso e numero senza spazi o altri caratteri (max 10 cifre)" placeholder="prefisso e numero senza spazi o altri caratteri (max 10 cifre)"pattern="[0-9]{10}">
						
						<label for="mail_3">Indrizzo e-mail</label>
						<input type="email" name="mail_3" size="28" placeholder="Campo obbligatorio" required>
					
						<label for="esigenze_3">Esigenze alimentari</label>
						<input type="text" name="esigenze_3" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario">
					
						<label for="disabilita_3">Eventuali disabilità</label>
						<input type="text" name="disabilita_3" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario">
					
					  </div>
					</div>					

					<br />
					
					<div class="ui-corner-all custom-corners">
					  <div class="ui-bar ui-bar-a">
						<h3>Partecipante</h3>
					  </div>
					  <div class="ui-body ui-body-a">
						<label for="nome_4">Nome</label>
						<input type="text" name="nome_4" size="20" maxlength="40" placeholder="Campo obbligatorio" required>

						<label for="cognome_4">Cognome</label>
						<input type="text" name="cognome_4" size="20" maxlength="40" placeholder="Campo obbligatorio" required>
						
						<label for="telefono_4">Recapito telefonico</label>						
						<input type="tel" name="telefono_4" size="8" required title="prefisso e numero senza spazi o altri caratteri (max 10 cifre)" placeholder="prefisso e numero senza spazi o altri caratteri (max 10 cifre)"pattern="[0-9]{10}">
						
						<label for="mail_4">Indrizzo e-mail</label>
						<input type="email" name="mail_4" size="28" placeholder="Campo obbligatorio" required>
					
						<label for="esigenze_4">Esigenze alimentari</label>
						<input type="text" name="esigenze_4" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario">
					
						<label for="disabilita_4">Eventuali disabilità</label>
						<input type="text" name="disabilita_4" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario">
					
					  </div>
					</div>
					
					<br />
					
					<div class="ui-corner-all custom-corners">
					  <div class="ui-bar ui-bar-a">
						<h3>Partecipante</h3>
					  </div>
					  <div class="ui-body ui-body-a">
						<label for="nome_5">Nome</label>
						<input type="text" name="nome_5" size="20" maxlength="40" placeholder="Campo obbligatorio" required>

						<label for="cognome_5">Cognome</label>
						<input type="text" name="cognome_5" size="20" maxlength="40" placeholder="Campo obbligatorio" required>
						
						<label for="telefono_5">Recapito telefonico</label>						
						<input type="tel" name="telefono_5" size="8" required title="prefisso e numero senza spazi o altri caratteri (max 10 cifre)" placeholder="prefisso e numero senza spazi o altri caratteri (max 10 cifre)"pattern="[0-9]{10}">
						
						<label for="mail_5">Indrizzo e-mail</label>
						<input type="email" name="mail_5" size="28" placeholder="Campo obbligatorio" required>
					
						<label for="esigenze_5">Esigenze alimentari</label>
						<input type="text" name="esigenze_5" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario">
					
						<label for="disabilita_5">Eventuali disabilità</label>
						<input type="text" name="disabilita_5" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario">
					
					  </div>
					</div>

					<br />
					
					<div class="ui-corner-all custom-corners">
					  <div class="ui-bar ui-bar-a">
						<h3>Adulto</h3>
					  </div>
					  <div class="ui-body ui-body-a">
						<label for="nome_6">Nome</label>
						<input type="text" name="nome_6" size="20" maxlength="40" placeholder="Campo obbligatorio" required>

						<label for="cognome_6">Cognome</label>
						<input type="text" name="cognome_6" size="20" maxlength="40" placeholder="Campo obbligatorio" required>
						
						<label for="telefono_6">Recapito telefonico</label>						
						<input type="tel" name="telefono_6" size="8" required title="prefisso e numero senza spazi o altri caratteri (max 10 cifre)" placeholder="prefisso e numero senza spazi o altri caratteri (max 10 cifre)"pattern="[0-9]{10}">
						
						<label for="mail_6">Indrizzo e-mail</label>
						<input type="email" name="mail_6" size="28" placeholder="Campo obbligatorio" required>
					
						<label for="esigenze_6">Esigenze alimentari</label>
						<input type="text" name="esigenze_6" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario">
					
						<label for="disabilita_6">Eventuali disabilità</label>
						<input type="text" name="disabilita_6" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario">
					
					  </div>
					</div>


                <input type="hidden" name="id_comitato" value="<?php echo $id_comitato; ?>" />
                <input type="hidden" name="passwd" value="<?php echo $passwd; ?>" />
                                            
                <input type="submit" value="Registra" />
                <br />
                <br />

				</form>

				<?php

			}
			else
			{
				echo "Password errata";
			}
			?>				

        </div><!-- /demo-html -->


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
			<?php include("menu.php") ?>
		     </ul>
		</div><!-- /panel -->


	<?php include("footer.php") ?>
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
			<?php include("menu.php") ?>
			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>