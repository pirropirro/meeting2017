<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato al Back Office.</b></font>";
	exit;	// Stop script execution
}

$stanze_tenso = array ('tensostruttura');


?>
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Tabellone Aule</title>
</head>
<body style="background-color: white;">



<!-- Page Content START -->
<div id="imPageSub">


<?php
include ("config.inc.php");
echo "<font color=#2B3856 size='2' face='Tahoma'>";
include ("apri_db.php");
$ora_attuale = strftime('%H:%M', time());
?>
<table class="bordata" id="tabellone_aule">

<td align="center" style="order-style:outset" style="font size:100%"><div style="background-color:#587d3a; width:287px; height:21px;"><b>Tensostruttura</b></td>

<tr>


<?php
//Faccio un ciclo da 1
foreach ($stanze_tenso as $stanza)
{
	//Faccio la query sulla tabella iscrizioni per estrarre i dati da visualizzare
	$query = "SELECT		c.nome_comitato AS nome_comitato,
							c.id AS id_comitato,
							t.ora_checkin AS ora_checkin
                            FROM timesheet AS t
                            INNER JOIN comitati AS c
                            ON t.id_comitato = c.id
                            WHERE t.estrarre = 'prossimo'
							AND t.aula = '$stanza'
							AND t.ora_checkin IS NOT NULL
							AND t.ora_checkout IS NULL";
	$result = mysql_query($query, $db);
	$squadre_presenti = mysql_num_rows($result);
	$spazi_vuoti = 10 - $squadre_presenti;
	?>
	<td align=center style="font size:90%"><div style="background-color:<?php if($squadre_presenti != 0) {echo "#F08080";} else{echo "#ffffff";} ?>; width:250px; height:21; overflow-y:hidden; overflow-x:hidden;">
	<?php
	while ($row = mysql_fetch_array($result)) 
	{
		$query_persone = "SELECT id FROM iscrizioni WHERE id_comitato = $row[id_comitato] AND (ruolo = 'capitano' OR ruolo = 'partecipante' OR ruolo = 'adulto' OR ruolo = 'osservatore')";
		$result_persone = mysql_query($query_persone, $db);
		$num_persone = mysql_num_rows($result_persone);
		
		$re = "/(|\\s*comitato\\s*|\\s*provinciale\\s*|\\s*regionale\\s*|\\s*locale\\s*|\\s*di \\s*|\\s*delegazione\\s*|\\sa\\s|\\s*valenza\\s*)/i"; 
		$subst = ""; 	 
		$comitato_trunc = preg_replace($re, $subst, $row[nome_comitato]);

		?>
		<font color="#2B3856"><?= $comitato_trunc." (".$num_persone.") - ".$row[ora_checkin] ?></font>
		<br />
		<?php
    }
    for ($i = 1; $i <= $spazi_vuoti; $i++) echo "<br>";
	?>
    
	</td>
	<?php
}
?>
</table>


</div>

</body>
</html>