<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice') && !($check['team'] == 'organigramma'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato all'organizzazione od al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Scelta Simulatori e Truccatori</title>

	<!-- Contents xxxxx-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Parent -->
	<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/tableExport.js"></script>
	<script type="text/javascript" src="js/jquery.base64.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p018.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Scelta Simulatori e Truccatori</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Scelta Simulatori e Truccatori</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?
include("config.inc.php");
include ("apri_db.php");
?>

<font color=#2B3856 size='2' face='Tahoma'>

<form name="form1" enctype="multipart/form-data" method="post" action="salva_scelta_simulatori_truccatori.php">

<table id='stato_iscrizione' border='0' width=100%>
<thead>
<tr>
<th rowspan="2" align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:30px; height:24px; '>SEL</th>
<th colspan="6" align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:500px; height:24px; '>DATI ANAGRAFICI</th>
<th colspan="2" align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:160px; height:24px; '>SIMULATORE</th>
<th rowspan="2" align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:80px; height:24px; '>TRUCCATORE</th>
</tr>
<tr>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:200px; height:24px; '>COMITATO</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:60px; height:24px; '>MEETING</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:70px; height:24px; '>NOME</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:90px; height:24px; '>COGNOME</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:70px; height:24px; '>TELEFONO</th> 
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:90px; height:24px; '>E-MAIL</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:90px; height:24px; '>QUALIFICATO</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:90px; height:24px; '>ASPIRANTE</th>
</tr>
</thead>

<?php
//Inizializzo i contatori
$tot_simulatori_qualificati = 0;
$tot_simulatori_aspiranti = 0;
$tot_truccatori = 0;

//Estraggo con mysql pivot
$query = "SELECT 	c.nome_comitato AS comitato,
					c.tipo_meeting AS meeting,
					i.id AS id,
					i.nome AS nome,
					i.cognome AS cognome,
					i.telefono AS telefono,
					i.mail AS mail,
					i.mail_confermata AS mail_confermata,
					i.simulatore AS simulatore,
					i.truccatore AS truccatore
					FROM iscrizioni AS i
					INNER JOIN comitati AS c
					ON i.id_comitato = c.id
					WHERE ruolo = 'osservatore'
					AND ((simulatore = 1 OR simulatore = 2) OR (truccatore = 1))
					AND i.registrazione != 9
					ORDER BY c.tipo_meeting DESC";
$result = mysql_query($query, $db);
$tot_estratti = mysql_num_rows($result);
while($row = mysql_fetch_array( $result )) 
{
	?>
	<tr>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><input type="radio" name="id_scelto" value="<?= $row[id] ?>"/></td>
    <td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><?= $row[comitato] ?></td>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><?= $row[meeting] ?></td>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><?= $row[nome] ?></td>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><?= $row[cognome] ?></td>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><?= $row[telefono] ?></td>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style=" <?php if($row[mail_confermata] != 1) echo "color:red;"; ?> height:18px; overflow-y:hidden; overflow-x:hidden;"><?= $row[mail] ?></td>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if($row[simulatore] == 2){ echo "si"; $tot_simulatori_qualificati++;} ?></td>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if($row[simulatore] == 1){ echo "si"; $tot_simulatori_aspiranti++;} ?></td>
	<td align=center style="font size:90%" bgcolor="#FFFFFF"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if($row[truccatore] == 1){ echo "si"; $tot_truccatori++;} ?></td>
	<?php
}
?>

<tr>
<td colspan="7" align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b>TOTALE (<?= $tot_estratti ?> osservatori)</b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_simulatori_qualificati." qua" ?></b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_simulatori_aspiranti." asp" ?></b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_truccatori." tru" ?></b></td>

<?php
mysql_close($db); 
?>
</table>

<br /><br />
<p align="left">
<input type="submit" value="Recluta" />
</form>
    	
<!-- <a href="#" onClick ="$('#stato_iscrizione').tableExport({type:'excel',escape:'false',fileName: 'Stato Iscrizione'});">Esporta in MS Excel</a> -->

<br /><br />


</div>
</div>
</div>


<!-- Page Content END -->

	</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
