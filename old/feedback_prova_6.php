<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2015 - Feedback Prova Sei</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2015 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2015 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2015</h1>
		
		<p><strong>Feedback Prova Sei</strong></p>

		<?
        //recupero i dati nella barra dell'indirizzo
        $ruolo=$_GET['ruolo'];
        $id=$_GET['id'];
        //echo "Ruolo                    (tutti):                     $ruolo <br>";
        //echo "ID          (tutti):           $id <br>";
        
		
		$valutazione_accoglienza_venerdi = $_POST['valutazione_accoglienza_venerdi'];
		$valutazione_speed_meeting = $_POST['valutazione_speed_meeting'];
		$valutazione_pernottamento = $_POST['valutazione_pernottamento'];
		$valutazione_pasti = $_POST['valutazione_pasti'];
		$valutazione_logistica = $_POST['valutazione_logistica'];
		$valutazione_tour = $_POST['valutazione_tour'];
		
        //recupero i dati passati in hidden dal form precedente
		$valutazione_gradimento_prova1 = $_POST['valutazione_gradimento_prova1'];
		//echo "Valutazione gradimento prova area 1: $valutazione_gradimento_prova1 <br>";
		$valutazione_formativa_prova1 = $_POST['valutazione_formativa_prova1'];
		//echo "Valutazione validità formativa prova area 1: $valutazione_formativa_prova1 <br>";
		$valutazione_difficolta_prova1 = $_POST['valutazione_difficolta_prova1'];
		//echo "Valutazione difficoltà prova area 1: $valutazione_difficolta_prova1 <br>";
		$valutazione_lavoro_gruppo_prova1 = $_POST['valutazione_lavoro_gruppo_prova1'];
		//echo "Valutazione lavoro di gruppo prova area 1: $valutazione_lavoro_gruppo_prova1 <br>";
		$valutazione_performance_giudice_prova1 = $_POST['valutazione_performance_giudice_prova1'];
		//echo "Valutazione performance giudici prova area 1: $valutazione_performance_giudice_prova1 <br>";
		
		$valutazione_gradimento_prova2 = $_POST['valutazione_gradimento_prova2'];
		//echo "Valutazione gradimento prova area 2: $valutazione_gradimento_prova2 <br>";
		$valutazione_formativa_prova2 = $_POST['valutazione_formativa_prova2'];
		//echo "Valutazione validità formativa prova area 2: $valutazione_formativa_prova2 <br>";
		$valutazione_difficolta_prova2 = $_POST['valutazione_difficolta_prova2'];
		//echo "Valutazione difficoltà prova area 2: $valutazione_difficolta_prova2 <br>";
		$valutazione_lavoro_gruppo_prova2 = $_POST['valutazione_lavoro_gruppo_prova2'];
		//echo "Valutazione lavoro di gruppo prova area 2: $valutazione_lavoro_gruppo_prova2 <br>";
		$valutazione_performance_giudice_prova2 = $_POST['valutazione_performance_giudice_prova2'];
		//echo "Valutazione performance giudici prova area 2: $valutazione_performance_giudice_prova2 <br>";
		$valutazione_interpretazione_simulatore_prova2 = $_POST['valutazione_interpretazione_simulatore_prova2'];
		//echo "Valutazione interpretazione simulatori prova area 2: $valutazione_interpretazione_simulatore_prova2 <br>";
		$valutazione_utilita_simulatore_prova2 = $_POST['valutazione_utilita_simulatore_prova2'];
		//echo "Valutazione utilità simulatori prova area 2: $valutazione_utilita_simulatore_prova2 <br>";
		
		$valutazione_gradimento_prova3 = $_POST['valutazione_gradimento_prova3'];
		//echo "Valutazione gradimento prova area 3: $valutazione_gradimento_prova3 <br>";
		$valutazione_formativa_prova3 = $_POST['valutazione_formativa_prova3'];
		//echo "Valutazione validità formativa prova area 3: $valutazione_formativa_prova3 <br>";
		$valutazione_difficolta_prova3 = $_POST['valutazione_difficolta_prova3'];
		//echo "Valutazione difficoltà prova area 3: $valutazione_difficolta_prova3 <br>";
		$valutazione_lavoro_gruppo_prova3 = $_POST['valutazione_lavoro_gruppo_prova3'];
		//echo "Valutazione lavoro di gruppo prova area 3: $valutazione_lavoro_gruppo_prova3 <br>";
		$valutazione_performance_giudice_prova3 = $_POST['valutazione_performance_giudice_prova3'];
		//echo "Valutazione performance giudici prova area 3: $valutazione_performance_giudice_prova3 <br>";
		$valutazione_interpretazione_simulatore_prova3 = $_POST['valutazione_interpretazione_simulatore_prova3'];
		//echo "Valutazione interpretazione simulatori prova area 2: $valutazione_interpretazione_simulatore_prova2 <br>";
		$valutazione_utilita_simulatore_prova3 = $_POST['valutazione_utilita_simulatore_prova3'];
		//echo "Valutazione utilità simulatori prova area 2: $valutazione_utilita_simulatore_prova2 <br>";
		
		$valutazione_gradimento_prova4 = $_POST['valutazione_gradimento_prova4'];
		//echo "Valutazione gradimento prova area 4: $valutazione_gradimento_prova4 <br>";
		$valutazione_formativa_prova4 = $_POST['valutazione_formativa_prova4'];
		//echo "Valutazione validità formativa prova area 4: $valutazione_formativa_prova4 <br>";
		$valutazione_difficolta_prova4 = $_POST['valutazione_difficolta_prova4'];
		//echo "Valutazione difficoltà prova area 4: $valutazione_difficolta_prova4 <br>";
		$valutazione_lavoro_gruppo_prova4 = $_POST['valutazione_lavoro_gruppo_prova4'];
		//echo "Valutazione lavoro di gruppo prova area 4: $valutazione_lavoro_gruppo_prova4 <br>";
		$valutazione_performance_giudice_prova4 = $_POST['valutazione_performance_giudice_prova4'];
		//echo "Valutazione performance giudici prova area 4: $valutazione_performance_giudice_prova4 <br>";

        
        //recupero i dati di input del form precedente
		$valutazione_gradimento_prova5 = $_REQUEST['valutazione_gradimento_prova5'];
		//echo "Valutazione gradimento prova area 5: $valutazione_gradimento_prova5 <br>";
		$valutazione_formativa_prova5 = $_REQUEST['valutazione_formativa_prova5'];
		//echo "Valutazione validità formativa prova area 5: $valutazione_formativa_prova5 <br>";
		$valutazione_difficolta_prova5 = $_REQUEST['valutazione_difficolta_prova5'];
		//echo "Valutazione difficoltà prova area 5: $valutazione_difficolta_prova5 <br>";
		$valutazione_lavoro_gruppo_prova5 = $_REQUEST['valutazione_lavoro_gruppo_prova5'];
		//echo "Valutazione lavoro di gruppo prova area 5: $valutazione_lavoro_gruppo_prova5 <br>";
		$valutazione_performance_giudice_prova5 = $_REQUEST['valutazione_performance_giudice_prova5'];
		//echo "Valutazione performance giudici prova area 5: $valutazione_performance_giudice_prova5 <br>";
		$valutazione_interpretazione_simulatore_prova5 = $_REQUEST['valutazione_interpretazione_simulatore_prova5'];
		//echo "Valutazione interpretazione simulatori prova area 4: $valutazione_interpretazione_simulatore_prova4 <br>";
		$valutazione_utilita_simulatore_prova5 = $_REQUEST['valutazione_utilita_simulatore_prova5'];
		//echo "Valutazione utilità simulatori prova area 4: $valutazione_utilita_simulatore_prova4 <br>";

		//Annullo il form dei simulatori, giudici e staff
		$valutazione_performance_giudice_prova = '';
		//echo "Valutazione performance dei giudici		 					(simulatori):                  					$valutazione_performance_giudice_prova <br>";
		$prova_assegnata = '';
		//echo "Prova assegnata 												(giudici/simulatori): 							$prova_assegnata <br>";
		$valutazione_prova_assegnata = '';
		//echo "Valutazione organizzazione prova assegnata 					(giudici/simulatori): 							$valutazione_prova_assegnata <br>";
		$valutazione_inserimento_voti_portale = '';
		//echo "Valutazione inserimento voti portale 							(giudici): 										$valutazione_inserimento_voti_portale <br>";
		$valutazione_interpretazione_simulatore_prova = '';
		//echo "Valutazione interpretazione dei simulatori 					(giudici): 										$valutazione_interpretazione_simulatore_prova <br>";
		$valutazione_utilita_simulatore_prova = '';
		//echo "Valutazione utilità dei simulatori 							(giudici): 										$valutazione_utilita_simulatore_prova <br>";
		$valutazione_coordinamento_backoffice = '';
		//echo "Valutazione coordinamento backoffice                 			(giudici/staff):								$valutazione_coordinamento_backoffice <br>";
		$valutazione_preparazione = '';
		//echo "Valutazione fase preparatoria al meeting 						(staff):                     					$valutazione_preparazione <br>";
		$valutazione_soddisfazione_incarico = '';
		//echo "Valutazione soddisfazione incarico 							(staff):                     					$valutazione_soddisfazione_incarico <br>";
		$valutazione_mezzi_informazioni = '';
		//echo "Valutazione mezzi ed informazioni 							(staff):                     					$valutazione_mezzi_informazioni <br>";
        ?>


		<form name="feedback_prova_6" enctype="multipart/form-data" method="post" action="feedback_prova_indian.php?ruolo=<?php echo $ruolo ?>&id=<?php echo $id ?>">

		<a href="#popupCloseRight" data-rel="popup" class="ui-btn ui-corner-all ui-shadow ui-btn-inline">Descrizione della Prova</a>
		
		<div data-role="popup" id="popupCloseRight" class="ui-content" style="max-width:280px">
		<a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
		<p>Si tratta di una prova in cui le squadre si sono messe alla prova circa l'utilizzo dei termini e le parole inerenti al tema dell'immigrazione. La prova ha voluto far riflettere i Giovani sull'importanza di alcune parole specifiche quali “immigrato”, “clandestino”, “rifugiato politico”, “richiedente asilo”, “migrante”, e altre in quanto parole usate e talvolta abusate dai media e dalla popolazione. 
		<br />
		I partecipanti hanno appreso che termini spesso vi e' confusione e poca conoscenza del significato dei termini che sentiamo e utilizziamo nella quotidianità, ciò comporta un utilizzo improprio e talvolta ingiusto di queste parole, rischiando di utilizzare questi termini in modo offensivo.</p>
		</div>
		<br />
		<br />		
		
		<label for="slider-fill">Valuta quanto ti è piaciuta</label>
		<input type="range" name="valutazione_gradimento_prova6" id="valutazione_gradimento_prova6" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<label for="slider-fill">Valuta quanto è stata formativa</label>
		<input type="range" name="valutazione_formativa_prova6" id="valutazione_formativa_prova6" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<label for="slider-fill">Valuta la difficoltà</label>
		<input type="range" name="valutazione_difficolta_prova6" id="valutazione_difficolta_prova6" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<label for="slider-fill">Valuta quanto ha facilitato il lavoro in gruppo</label>
		<input type="range" name="valutazione_lavoro_gruppo_prova6" id="valutazione_lavoro_gruppo_prova6" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<label for="slider-fill">Valuta la performance dei giudici</label>
		<input type="range" name="valutazione_performance_giudice_prova6" id="valutazione_performance_giudice_prova6" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
		
		<br /><br />

		<input type="hidden" name="valutazione_accoglienza_venerdi" value="<?php echo $valutazione_accoglienza_venerdi; ?>" />
		<input type="hidden" name="valutazione_speed_meeting" value="<?php echo $valutazione_speed_meeting; ?>" />
		<input type="hidden" name="valutazione_pernottamento" value="<?php echo $valutazione_pernottamento; ?>" />
		<input type="hidden" name="valutazione_pasti" value="<?php echo $valutazione_pasti; ?>" />
		<input type="hidden" name="valutazione_logistica" value="<?php echo $valutazione_logistica; ?>" />
		<input type="hidden" name="valutazione_tour" value="<?php echo $valutazione_tour; ?>" />	
		
        <input type="hidden" name="valutazione_gradimento_prova1" value="<?php echo $valutazione_gradimento_prova1; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova1" value="<?php echo $valutazione_difficolta_prova1; ?>" />
        <input type="hidden" name="valutazione_formativa_prova1" value="<?php echo $valutazione_formativa_prova1; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova1" value="<?php echo $valutazione_lavoro_gruppo_prova1; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova1" value="<?php echo $valutazione_performance_giudice_prova1; ?>" />

        <input type="hidden" name="valutazione_gradimento_prova2" value="<?php echo $valutazione_gradimento_prova2; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova2" value="<?php echo $valutazione_difficolta_prova2; ?>" />
        <input type="hidden" name="valutazione_formativa_prova2" value="<?php echo $valutazione_formativa_prova2; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova2" value="<?php echo $valutazione_lavoro_gruppo_prova2; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova2" value="<?php echo $valutazione_performance_giudice_prova2; ?>" />
        <input type="hidden" name="valutazione_interpretazione_simulatore_prova2" value="<?php echo $valutazione_interpretazione_simulatore_prova2; ?>" />
        <input type="hidden" name="valutazione_utilita_simulatore_prova2" value="<?php echo $valutazione_utilita_simulatore_prova2; ?>" />
		
        <input type="hidden" name="valutazione_gradimento_prova3" value="<?php echo $valutazione_gradimento_prova3; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova3" value="<?php echo $valutazione_difficolta_prova3; ?>" />
        <input type="hidden" name="valutazione_formativa_prova3" value="<?php echo $valutazione_formativa_prova3; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova3" value="<?php echo $valutazione_lavoro_gruppo_prova3; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova3" value="<?php echo $valutazione_performance_giudice_prova3; ?>" />
		<input type="hidden" name="valutazione_interpretazione_simulatore_prova3" value="<?php echo $valutazione_interpretazione_simulatore_prova3; ?>" />
        <input type="hidden" name="valutazione_utilita_simulatore_prova3" value="<?php echo $valutazione_utilita_simulatore_prova3; ?>" />
		
        <input type="hidden" name="valutazione_gradimento_prova4" value="<?php echo $valutazione_gradimento_prova4; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova4" value="<?php echo $valutazione_difficolta_prova4; ?>" />
        <input type="hidden" name="valutazione_formativa_prova4" value="<?php echo $valutazione_formativa_prova4; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova4" value="<?php echo $valutazione_lavoro_gruppo_prova4; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova4" value="<?php echo $valutazione_performance_giudice_prova4; ?>" />

        <input type="hidden" name="valutazione_gradimento_prova5" value="<?php echo $valutazione_gradimento_prova5; ?>" />
        <input type="hidden" name="valutazione_difficolta_prova5" value="<?php echo $valutazione_difficolta_prova5; ?>" />
        <input type="hidden" name="valutazione_formativa_prova5" value="<?php echo $valutazione_formativa_prova5; ?>" />
        <input type="hidden" name="valutazione_lavoro_gruppo_prova5" value="<?php echo $valutazione_lavoro_gruppo_prova5; ?>" />
        <input type="hidden" name="valutazione_performance_giudice_prova5" value="<?php echo $valutazione_performance_giudice_prova5; ?>" />
		<input type="hidden" name="valutazione_interpretazione_simulatore_prova5" value="<?php echo $valutazione_interpretazione_simulatore_prova5; ?>" />
        <input type="hidden" name="valutazione_utilita_simulatore_prova5" value="<?php echo $valutazione_utilita_simulatore_prova5; ?>" />
       
		<input type="submit" value="Avanti" />
		</form>


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
<li data-filtertext="meeting homepage" data-icon="home"><a href="index.html">Home</a></li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Autenticazione<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="accedi login autentica"><a href="login.php" data-ajax="false">Accedi</a></li>
			<li data-filtertext="password passwd"><a href="chgpwd.php" data-ajax="false">Cambia Password</a></li>
			<li data-filtertext="esci logout"><a href="logout.php" data-ajax="false">Esci</a></li>
		</ul>
	</div>
</li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Meeting<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="inserisci punteggio giudice coordinatore voto"><a href="inserisci_punteggio.php" data-ajax="false">Inserisci Punteggio</a></li>
			<li data-filtertext="quadro parziale punteggi punteggio risultati"><a href="quadro_punteggi_partecipanti.php" data-ajax="false">Quadro Punteggi</a></li>
			<li data-filtertext="classifica parziale punteggio temporaneo risultati"><a href="classifica_parziale.php" data-ajax="false">Classifica Parziale</a></li>
			<li data-filtertext="classifica finale punteggio temporaneo risultati"><a href="classifica_finale.php" data-ajax="false">Classifica Finale</a></li>
		</ul>
	</div>
</li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Feedback<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="feedback questionario valutazione"><a href="compila_questionario.php" data-ajax="false">Compila Questionario</a></li>
		</ul>
	</div>
</li>
		     </ul>
		</div><!-- /panel -->


	<div data-role="footer" data-position="fixed" data-tap-toggle="false" class="jqm-footer">
		<p>Vai alla <a href="../../index_desktop.html" data-ajax="false">versione desktop</a></p>
		<p>Scrivi a <a href="mailto:paolo.ditoma@libero.it?subject=Segnalazione per Meeting2015 da mobile" title="">paolo.ditoma@libero.it</a></p>
	</div><!-- /footer -->
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
<li data-filtertext="meeting homepage" data-icon="home"><a href="index.html">Home</a></li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Autenticazione<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="accedi login autentica"><a href="login.php" data-ajax="false">Accedi</a></li>
			<li data-filtertext="password passwd"><a href="chgpwd.php" data-ajax="false">Cambia Password</a></li>
			<li data-filtertext="esci logout"><a href="logout.php" data-ajax="false">Esci</a></li>
		</ul>
	</div>
</li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Meeting<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="inserisci punteggio giudice coordinatore voto"><a href="inserisci_punteggio.php" data-ajax="false">Inserisci Punteggio</a></li>
			<li data-filtertext="quadro parziale punteggi punteggio risultati"><a href="quadro_punteggi_partecipanti.php" data-ajax="false">Quadro Punteggi</a></li>
			<li data-filtertext="classifica parziale punteggio temporaneo risultati"><a href="classifica_parziale.php" data-ajax="false">Classifica Parziale</a></li>
			<li data-filtertext="classifica finale punteggio temporaneo risultati"><a href="classifica_finale.php" data-ajax="false">Classifica Finale</a></li>
		</ul>
	</div>
</li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Feedback<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="feedback questionario valutazione"><a href="compila_questionario.php" data-ajax="false">Compila Questionario</a></li>
		</ul>
	</div>
</li>



			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>