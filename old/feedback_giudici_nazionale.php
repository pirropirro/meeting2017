<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting 2015 - Feedback Giornata di Venerdì</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="css/themes/default/jquery.mobile-1.4.4.min.css">
	<link rel="stylesheet" href="_assets/css/jqm-demos.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="js/jquery.js"></script>
	<script src="_assets/js/index.js"></script>
	<script src="js/jquery.mobile-1.4.4.min.js"></script>
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><a href="index.html" title="Meeting 2015 - Homepage"><img src="giovanicri.jpg" alt="Portale Meeting 2015 - Mobile"></a></h2>
		<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
		<a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	</div><!-- /header -->

	<div role="main" class="ui-content jqm-content">

		<h1>Meeting 2015</h1>
		
		<p><strong>Giornata di Venerdì</strong></p>

		<?
        //recupero i dati nella barra dell'indirizzo
        $ruolo=$_GET['ruolo'];
        $id=$_GET['id'];
        //echo "Ruolo                    (tutti):                     $ruolo <br>";
        //echo "ID          (tutti):           $id <br>";
        

        ?>


		<form name="feedback_giudici_nazionale" enctype="multipart/form-data" method="post" action="feedback_giudici_n.php?ruolo=<?php echo $ruolo ?>&id=<?php echo $id ?>">

		<label for="slider-fill">Valuta l'accoglienza di Venerdì</label>
		<input type="range" name="valutazione_accoglienza_venerdi" id="valutazione_accoglienza_venerdi" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<label for="slider-fill">Valuta lo Speed-Meeting</label>
		<input type="range" name="valutazione_speed_meeting" id="valutazione_speed_meeting" value="5" min="0" max="10" step="0.1" data-highlight="true">
		<br />
	
		<br /><br />

		<input type="submit" value="Avanti" />
		</form>


	</div><!-- /content -->
	    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
<li data-filtertext="meeting homepage" data-icon="home"><a href="index.html">Home</a></li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Autenticazione<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="accedi login autentica"><a href="login.php" data-ajax="false">Accedi</a></li>
			<li data-filtertext="password passwd"><a href="chgpwd.php" data-ajax="false">Cambia Password</a></li>
			<li data-filtertext="esci logout"><a href="logout.php" data-ajax="false">Esci</a></li>
		</ul>
	</div>
</li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Meeting<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="inserisci punteggio giudice coordinatore voto"><a href="inserisci_punteggio.php" data-ajax="false">Inserisci Punteggio</a></li>
			<li data-filtertext="quadro parziale punteggi punteggio risultati"><a href="quadro_punteggi_partecipanti.php" data-ajax="false">Quadro Punteggi</a></li>
			<li data-filtertext="classifica parziale punteggio temporaneo risultati"><a href="classifica_parziale.php" data-ajax="false">Classifica Parziale</a></li>
			<li data-filtertext="classifica finale punteggio temporaneo risultati"><a href="classifica_finale.php" data-ajax="false">Classifica Finale</a></li>
		</ul>
	</div>
</li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Feedback<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="feedback questionario valutazione"><a href="compila_questionario.php" data-ajax="false">Compila Questionario</a></li>
		</ul>
	</div>
</li>
		     </ul>
		</div><!-- /panel -->


	<div data-role="footer" data-position="fixed" data-tap-toggle="false" class="jqm-footer">
		<p>Vai alla <a href="../../index_desktop.html" data-ajax="false">versione desktop</a></p>
		<p>Scrivi a <a href="mailto:paolo.ditoma@libero.it?subject=Segnalazione per Meeting2015 da mobile" title="">paolo.ditoma@libero.it</a></p>
	</div><!-- /footer -->
	<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->
    <div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a">
		<div class="jqm-search">
			<ul class="jqm-list" data-filter-placeholder="Cerca nel portale..." data-filter-reveal="true">
<li data-filtertext="meeting homepage" data-icon="home"><a href="index.html">Home</a></li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Autenticazione<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="accedi login autentica"><a href="login.php" data-ajax="false">Accedi</a></li>
			<li data-filtertext="password passwd"><a href="chgpwd.php" data-ajax="false">Cambia Password</a></li>
			<li data-filtertext="esci logout"><a href="logout.php" data-ajax="false">Esci</a></li>
		</ul>
	</div>
</li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Meeting<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="inserisci punteggio giudice coordinatore voto"><a href="inserisci_punteggio.php" data-ajax="false">Inserisci Punteggio</a></li>
			<li data-filtertext="quadro parziale punteggi punteggio risultati"><a href="quadro_punteggi_partecipanti.php" data-ajax="false">Quadro Punteggi</a></li>
			<li data-filtertext="classifica parziale punteggio temporaneo risultati"><a href="classifica_parziale.php" data-ajax="false">Classifica Parziale</a></li>
			<li data-filtertext="classifica finale punteggio temporaneo risultati"><a href="classifica_finale.php" data-ajax="false">Classifica Finale</a></li>
		</ul>
	</div>
</li>
<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
	<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
		<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">
		    Feedback<span class="ui-collapsible-heading-status"> click to expand contents</span>
		</a>
	</h3>
	<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
		<ul>
			<li data-filtertext="feedback questionario valutazione"><a href="compila_questionario.php" data-ajax="false">Compila Questionario</a></li>
		</ul>
	</div>
</li>



			</ul>
		</div>
	</div><!-- /panel -->


</div><!-- /page -->

</body>
</html>