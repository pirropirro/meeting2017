<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice') && !($check['team'] == 'organigramma') && !($check['team'] == 'nazionale'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato all'organizzazione od al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>

<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Ricerca iscrizioni</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/tableExport.js"></script>
	<script type="text/javascript" src="js/jquery.base64.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Ricerca iscrizioni</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Ricerca iscrizioni</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include ("config.inc.php");
echo "<font color=#2B3856 size='2' face='Tahoma'>";
$fp = fopen ("log_iscrizioni.txt",a);

//Leggo i dati in input
$id_comitato=$_REQUEST['id_comitato'];
$ruolo=$_REQUEST['ruolo'];
$nome=$_REQUEST['nome'];
$nome_ins = addslashes(stripslashes($nome)); 
$nome_ins = str_replace("<", "&lt;", $nome_ins);
$nome_ins = str_replace(">", "&gt;", $nome_ins);
$cognome=$_REQUEST['cognome'];
$cognome_ins = addslashes(stripslashes($cognome)); 
$cognome_ins = str_replace("<", "&lt;", $cognome_ins);
$cognome_ins = str_replace(">", "&gt;", $cognome_ins);
$telefono=$_REQUEST['telefono'];
$mail=strtolower($_REQUEST['mail']);
$esigenze=$_REQUEST['esigenze'];
$disabilita=$_REQUEST['disabilita'];
$simulatore=$_REQUEST['simulatore'];
$truccatore=$_REQUEST['truccatore'];
$giudice_1=$_REQUEST['giudice_1'];
$giudice_2=$_REQUEST['giudice_2'];
$giudice_3=$_REQUEST['giudice_3'];
$giudice_4=$_REQUEST['giudice_4'];
$giudice_5=$_REQUEST['giudice_5'];
$giudice_6=$_REQUEST['giudice_6'];
$giudice_7=$_REQUEST['giudice_7'];
$giudice_8=$_REQUEST['giudice_8'];
$giudice_9=$_REQUEST['giudice_9'];
$giudice_10=$_REQUEST['giudice_10'];
$giudice_11=$_REQUEST['giudice_11'];
$giudice_12=$_REQUEST['giudice_12'];
$mail_confermata=$_REQUEST['mail_confermata'];

include ("apri_db.php");

?>

	<table id='stato_iscrizione' border='0' width=100%>

	<thead>
	<tr>

	<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Comitato</th>
	<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Meeting</th> 
	<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Ruolo</th>
	<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Nome</th> 
	<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Cognome</th>
	<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Telefono</th> 
	<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>E-mail</th>
	<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Esigenze alimentari</th>
    <th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Eventuali disabilità</th> </tr>
	</thead>	

<?php

	$otale_estratti = 0;

	//Estraggo i dati del comitato
	$query_iscrizioni = "SELECT		c.nome_comitato AS nome_comitato,
								c.tipo_meeting AS tipo_meeting,
								i.ruolo AS ruolo,
								i.nome AS nome,
								i.cognome AS cognome,
								i.telefono AS telefono,
								i.mail AS mail,
								i.mail_confermata AS mail_confermata,
								i.esigenze AS esigenze,
								i.disabilita AS disabilita
                    			FROM iscrizioni AS i
                    			INNER JOIN comitati AS c
                    			ON i.id_comitato = c.id
                    			WHERE i.id_comitato LIKE '$id_comitato'
								AND $ruolo
								AND nome LIKE '%$nome%'
								AND cognome LIKE '%$cognome_ins%'
								AND telefono LIKE '%$telefono%'
								AND mail LIKE '%$mail%'
								AND $esigenze
								AND $disabilita
								AND $simulatore
								AND $truccatore
								AND (1=1 $giudice_1 $giudice_2 $giudice_3 $giudice_4 $giudice_5 $giudice_6 $giudice_7 $giudice_8 $giudice_9 $giudice_10 $giudice_11 $giudice_12)
								AND $mail_confermata
								AND i.registrazione != 9
								ORDER BY c.tipo_meeting, c.nome_comitato, ruolo";
								
	$result_iscrizioni = mysql_query($query_iscrizioni, $db);
	while($row_iscrizioni = mysql_fetch_array( $result_iscrizioni )) 
	{
		$re = "/(|\\s*comitato\\s*|\\s*provinciale\\s*|\\s*regionale\\s*|\\s*locale\\s*|\\s*di\\s*|\\s*delegazione\\s*|\\sa\\s|\\s*valenza\\s*)/i"; 
		$subst = ""; 	 
		$comitato_trunc = preg_replace($re, $subst, $row_iscrizioni[nome_comitato]);
		?>
		
        <td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:110px; height:18px; overflow-y:auto; overflow-x:hidden;'><?= $comitato_trunc ?></td>
        <td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:20px; height:18px; overflow-y:hidden; overflow-x:hidden;'><?= $row_iscrizioni[tipo_meeting] ?></td>
		<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:110px; height:18px; overflow-y:hidden; overflow-x:hidden;'><b><font color="#2B3856"><?= $row_iscrizioni[ruolo] ?></b></font></td>
		<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:100px; height:18px; overflow-y:hidden; overflow-x:hidden;'><?= $row_iscrizioni[nome] ?></td>
		<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:100px; height:18px; overflow-y:hidden; overflow-x:hidden;'><?= $row_iscrizioni[cognome] ?></td>
		<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:80px; height:18px; overflow-y:hidden; overflow-x:hidden;'><?= $row_iscrizioni[telefono] ?></td>
		<td align=center style='font size:90%' bgcolor=#ffffff><div style=' <?php if($row_iscrizioni[mail_confermata] != 1) echo "color:red;"; ?>width:190px; height:18px; overflow-y:hidden; overflow-x:hidden;'><?= $row_iscrizioni[mail] ?></td>
		<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:160px; height:18px; overflow-y:auto; overflow-x:hidden;'><?= $row_iscrizioni[esigenze] ?></td>
		<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:160px; height:18px; overflow-y:hidden; overflow-x:hidden;'><?= $row_iscrizioni[disabilita] ?></td>
		<tr>
    	<?php
		$totale_estratti++;
	}
	?>
	<td colspan="3" align=center style='font size:90%' bgcolor=#ffffff><div style=' width:160px; height:18px; overflow-y:hidden; overflow-x:hidden;'>Totale estratti <?= $totale_estratti ?></td>
    </table>

	<br /><br />
	<p align="left">
	
	<a href="#" onClick ="$('#stato_iscrizione').tableExport({type:'excel',escape:'false',fileName: 'Stato Iscrizione'});">Esporta in MS Excel</a>

	<br /><br />

<?php
mysql_close($db);
fclose($fp);
?>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
