<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice') && !($check['team'] == 'organigramma') && !($check['team'] == 'nazionale'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato all'organizzazione od al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>

<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Service - Inserisci materiale</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 10.37.23" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Parent -->
	<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/home.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Service - Inserisci materiale</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Inserisci materiale</h2>
<p id="imPathTitle">Service</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?
echo "<font color=#2B3856 size='3' face='Calibri'>";
//echo "$check[uname]";
//echo "<br />";
?>

<?php
include ("config.inc.php");
echo "<font color=#2B3856 size='3' face='Calibri'>";

$id=$_GET['id'];

$db = mysql_connect($db_host, $db_user, $db_password);
if ($db == FALSE)
	die ("Errore nella connessione. Verificare i parametri nel file config.inc.php");
mysql_select_db($db_name, $db)
	or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");

$query = "SELECT * FROM shopping WHERE id = '$id'";
$result = mysql_query($query, $db);
$row = mysql_fetch_array( $result );

?>
<form name="form1" enctype="multipart/form-data" method="post" action="salva_chiudi_materiale.php?id=<?php echo $id?>">
Quantità&nbsp&nbsp&nbsp&nbsp&nbsp		<?php echo"$row[quantita]" ?>
<br />
<br />

Descrizione&nbsp&nbsp&nbsp&nbsp&nbsp	<?php echo"$row[descrizione]" ?>
<br />
<br />

Costo complessivo stimato in &euro; &nbsp&nbsp&nbsp&nbsp&nbsp <?php echo"$row[costo_preventivo]" ?>
<br />
<br />

Si richiede sia disponibile dalle:&nbsp&nbsp	<?php echo"$row[inizio_esigenza]" ?>
<br />
<br />

Si prevede il suo utilizzo fino alle:&nbsp&nbsp	<?php echo"$row[fine_esigenza]" ?>
<br />
<br />



<b>Inserisci di seguito i dettagli della tua chiusura</b>&nbsp&nbsp
<br />
<br />

Tipo di chiusura:&nbsp&nbsp
<select name="tipo_chiusura">
<option value="acquisto">acquisto</option>
<option value="reperito">reperito</option>
<option value="partner">partner</option>
</select>
<br />
<br />

Dove è stato reperito:&nbsp&nbsp
<textarea style="overflow:hidden" cols="25" rows="2" name="reperimento"></textarea>
<br />
<br />

Costo complessivo in €&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
<input type="number" name="costo_consuntivo" min="0" max="9999">
<br />
<br />


<p align="left">
<input type="submit" value="Chiudi" />
</form>

</div>
</div>
</div>

</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>