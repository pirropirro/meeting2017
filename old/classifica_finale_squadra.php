<?php
	$token = $_GET['token'];
	
	// settiamo a zero la variabile mobile_browser 
	
	$mobile_browser = '0';
	
	//  controlliamo se l'user agent corrisponde a un dispositivo mobile
	 
	if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
		$mobile_browser++;
	}
	
	// controlliamo HTTP_ACCEPT corrisponde alla tecnologia WAP per telefonini
	 
	if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0)
	or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
		$mobile_browser++;
	}    
	
	// estraiamo i primi 4 caratteri da USER_AGENT e creiamo un array con tutti i header mobile
	 
	$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
	$mobile_agents = array(
		'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
		'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
		'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
		'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
		'newt','noki','oper','palm','pana','pant','phil','play','port','prox',
		'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
		'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
		'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
		'wapr','webc','winw','winw','xda ','xda-');
	
	// se corrisponde a mobile aumentiamo la nostra variabile di 1 
	 
	if (in_array($mobile_ua,$mobile_agents)) {
		$mobile_browser++;
	}
	 
	if (strpos(strtolower($_SERVER['ALL_HTTP']),'OperaMini') > 0) {
		$mobile_browser++;
	}
	 
	if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'windows') > 0) {
		$mobile_browser = 0;
	}
	
	// eccezione per windows phone
	if (preg_match('/(windows phone|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
		$mobile_browser++;
	}
	
	
	// quindi se mobile_browser � inferiore di 0 vuol dire che un dispositivo mobile
	 
	if ($mobile_browser > 0) {
	
	// reindirizzamento per mobile
	
	header('Location: http://www.itempa.com/meeting2015/mobile/classifica_finale_squadra.php?token=$token'); 
	
	}
	else 
	{
		// altrimenti restiamo dove siamo
		?>

		<!--IE 7 quirks mode please-->
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
		<head>
			<title>Classifica Finale Squadra</title>

			<!-- Contents xxxxx-->
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
			<meta http-equiv="Content-Type-Script" content="text/javascript" />
			<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
			<meta name="keywords" content="" />

			<!-- Others -->
			<meta name="Author" content="Paolo di Toma" />
			<meta http-equiv="ImageToolbar" content="False" />
			<meta name="MSSmartTagsPreventParsing" content="True" />
			<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

			<!-- Parent -->
			<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

			<!-- Res -->
			<script type="text/javascript" src="res/x5engine.js"></script>
			<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
			<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
			<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
			<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
			<link rel="stylesheet" type="text/css" href="res/p018.css" media="screen, print" />
			<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
			<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

			<!-- Robots -->
			<meta http-equiv="Expires" content="0" />
			<meta name="Resource-Type" content="document" />
			<meta name="Distribution" content="global" />
			<meta name="Robots" content="index, follow" />
			<meta name="Revisit-After" content="21 days" />
			<meta name="Rating" content="general" />
		</head>
		<body>
		<div id="imSite">
		<div id="imHeader">
			
			<h1>Classifica Finale Squadra</h1>
		</div>
		<div class="imInvisible">
		<hr />
		<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
		<a name="imGoToMenu"></a>
		</div>
		<div id="imBody">
			<div id="imMenuMain">

		<!-- Menu Content START -->
		<p class="imInvisible">Menu principale:</p>
		<div id="imMnMn">

		<?php 
		include ("main_menu.php");
		?>

		</div>
		<!-- Menu Content END -->

			</div>
		<hr class="imInvisible" />
		<a name="imGoToCont"></a>
			<div id="imContent">

		<!-- Page Content START -->
		<div id="imPageSub">
		<br />
		<h2>Classifica Finale Squadra</h2>
		<p id="imPathTitle">Meeting</p>
		<div id="imToolTip"></div>
		<div id="imBody">
		<div id="imContent">

		<?
		include("config.inc.php");

		echo "<font color=#2B3856 size='2' face='Tahoma'>";
		echo "<table border='0' width=100%>";

		include ("apri_db.php");
		
		$token = $_GET['token'];
		$tipo_meeting = $_GET['type'];
		//echo "Token: ".$token."<br>";

		if ($tipo_meeting == 'N')
		{
			//Squadra Meeting Nazionale
			$query = "SELECT 		c.nome_comitato AS nome_comitato,
									i.feedback_rilasciato AS feedback_rilasciato,
									p.id_comitato AS id_comitato,
									p.s1_c AS prova_1,
									p.s2_c AS prova_2,
									p.s3_c AS prova_3,
									p.s4_c AS prova_4,
									p.s5_c AS prova_5,
									p.s6_c AS prova_6,
									p.d1_c AS prova_india,
									p.d2_c AS prova_papa
									FROM preiscrizioni AS p
									INNER JOIN comitati AS c
									ON p.id_comitato = c.id
									INNER JOIN iscrizioni AS i
									ON p.id_comitato = i.id_comitato
									WHERE i.token = '$token'";
			$result = mysql_query($query, $db);
			$row = mysql_fetch_array($result);
			
			if ($row[feedback_rilasciato] == 1)
			{
				$re = "/(\\s*comitato lacale di\\s*|\\s*delegazione di\\s*|\\s*comitato provinciale di\\s*)/i"; 
				$subst = ""; 	 
				$comitato_trunc = preg_replace($re, $subst, $row[nome_comitato]);			
					
				?>
				<table id='stato_iscrizione' border='0' width=100%>
				<thead>
				<tr>
				<th colspan="2" align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:640px; height:24px; '><?= $row[nome_comitato] ?></th>
				</tr>
				<tr>
				<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:300px; height:24px; '>PROVA</th>
				<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:300px; height:24px; '>PUNTEGGIO</th>
				</tr>
				</thead>
				<tr>
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">UNO</td> 
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_1],1) ?></td>
				</tr>
				<tr>
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">DUE</td> 
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_2],1) ?></td>
				</tr>
				<tr>
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">TRE</td> 
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_3],1) ?></td>
				</tr>
				<tr>
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">QUATTRO</td> 
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_4],1) ?></td>
				</tr>
				<tr>
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">CINQUE</td> 
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_5],1) ?></td>
				</tr>
				<tr>
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">SEI</td> 
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_6],1) ?></td>
				</tr>
				<tr>
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">INDIA</td> 
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_india],1) ?></td>
				</tr>
				<tr>
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">PAPA</td> 
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_papa],1) ?></td>
				</tr>
				<?php
			}
			else
			{
				echo "Per poter visualizzare i punteggi parziali, ti chiediamo di rilasciare il feedback tramite il link che hai ricevuto via mail";
			}
		}
		else
		{
			$query = "SELECT 		c.nome_comitato AS nome_comitato,
									i.feedback_rilasciato AS feedback_rilasciato,
									p.id_comitato AS id_comitato,
									p.d1_c AS prova_india,
									p.d2_c AS prova_papa,
									p.d5_g AS prova_oscar,
									p.d6_c AS prova_tango,
									p.d7_g AS prova_hotel,
									p.d8_g AS prova_victor,
									p.Q_P1 AS q1,
									p.Q_P2 AS q2,
									p.Q_P3 AS q3,
									p.Q_P4 AS q4,
									p.Q_P5 AS q5,
									p.Q_P6 AS q6,
									p.Q_P7 AS q7,
									p.Q_P8 AS q8,
									p.Q_P9 AS q9,
									p.Q_P10 AS q10
									FROM preiscrizioni AS p
									INNER JOIN comitati AS c
									ON p.id_comitato = c.id
									INNER JOIN iscrizioni AS i
									ON p.id_comitato = i.id_comitato
									WHERE i.token = '$token'";
			$result = mysql_query($query, $db);
			$row = mysql_fetch_array($result);
			
			if ($row[feedback_rilasciato] == 1)
			{
				$prova_quebec = $row[q1] + $row[q2] + $row[q3] + $row[q4] + $row[q5] + $row[q6] + $row[q7] + $row[q8] + $row[q9] + $row[q10];
				
				$re = "/(\\s*comitato lacale di\\s*|\\s*delegazione di\\s*|\\s*comitato provinciale di\\s*)/i"; 
				$subst = ""; 	 
				$comitato_trunc = preg_replace($re, $subst, $row[nome_comitato]);			
					
				?>
				<table id='stato_iscrizione' border='0' width=100%>
				<thead>
				<tr>
				<th colspan="2" align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:640px; height:24px; '><?= $row[nome_comitato] ?></th>
				</tr>
				<tr>
				<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:300px; height:24px; '>PROVA</th>
				<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:300px; height:24px; '>PUNTEGGIO</th>
				</tr>
				</thead>
				<tr>
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">INDIA</td> 
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_india],1) ?></td>
				</tr>
				<tr>
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">PAPA</td> 
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_papa],1) ?></td>
				</tr>
				<tr>
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">QUEBEC</td> 
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($prova_quebec,1) ?></td>
				</tr>
				<tr>
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">OSCAR</td> 
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_oscar],1) ?></td>
				</tr>
				<tr>
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">TANGO</td> 
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_tango],1) ?></td>
				</tr>
				<tr>
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">HOTEL</td> 
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_hotel],1) ?></td>
				</tr>
				<tr>
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;">VICTOR</td> 
				<td align=center style='font size:100%' style=color:#FFFF99><div style="height:24px;"><?= number_format($row[prova_victor],1) ?></td>
				</tr>
				<?php
			}
			else
			{
				echo "Per poter visualizzare i punteggi parziali, ti chiediamo di rilasciare il feedback tramite il link che hai ricevuto via mail";
			}
		}
		mysql_close($db); 
		?>

		</table>
		<br /><br />

		</div>
		</div>
		</div>


		<!-- Page Content END -->

			</div>
			<div id="imFooter">
				<?php 
				include ("footer.php");
				?>
			</div>
		</div>
		</div>
		<div class="imInvisible">
		<hr />
		<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
		</div>

		<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
		</body>
		</html>
        
		<?php
		echo "";
	}   
?>