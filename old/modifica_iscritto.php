<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice') && !($check['team'] == 'tutor'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
 
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Modifica Iscritto</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Modifica Iscritto</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Modifica Iscritto</h2>
<p id="imPathTitle">Registrazioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include ("config.inc.php");
echo "<font color=#2B3856 size='3' face='Tahoma'>";
$fp = fopen ("log_iscrizioni.txt",a);

$id=$_REQUEST['id'];
$id_comitato =$_REQUEST['id_comitato'];
$operatore = $check['uname'];
//echo "$id<br>";

include ("apri_db.php");

$query = "SELECT * FROM iscrizioni WHERE id = $id";
$result = mysql_query($query, $db);
$row = mysql_fetch_array( $result );

if (($_POST['nome'] != '') && ($_POST['cognome'] != '') && ($_POST['telefono'] != '') && ($_POST['mail'] != '') && ($_SERVER['REQUEST_METHOD'] == 'POST')){
	//Proseguo con l'update
	$nome = $_POST['nome'];
	$nome_ins = addslashes(stripslashes($nome)); 
	$nome_ins = str_replace("<", "&lt;", $nome_ins);
	$nome_ins = str_replace(">", "&gt;", $nome_ins);
	$cognome = $_POST['cognome'];
	$cognome_ins = addslashes(stripslashes($cognome)); 
	$cognome_ins = str_replace("<", "&lt;", $cognome_ins);
	$cognome_ins = str_replace(">", "&gt;", $cognome_ins);
	$telefono = $_POST['telefono'];
	$mail = $_POST['mail'];
	
	//Controllo che non ci sia già una mail registrata uguale
	$query_estrazione = "SELECT id FROM iscrizioni WHERE mail = '$mail' AND id != '$id'";
	$result = mysql_query($query_estrazione, $db);
	
	if (mysql_num_rows($result) == '1')
	{
		echo "Hai inserito un indirizzo mail già presente nelle iscrizioni. Ogni volontario iscritto al Meeting deve avere un indirizzo mail valido e distinto!<br><br>";
	}
	else
	{
		//Faccio la insert con i dati dell'osservatore facendo differenza se è PR,R o N
		$query_update = "UPDATE iscrizioni SET nome = '$nome_ins', cognome = '$cognome_ins', telefono = '$telefono', mail = '$mail', mail_confermata = NULL WHERE id = '$id'";
		$result_update =  mysql_query($query_update, $db);
		//Controllo che sia andata a buon fine la insert
		
		if ($result_update)
		{
			$query_ricerca = "SELECT 	c.nome_comitato AS comitato,
										i.id AS id,
										i.ruolo AS ruolo,
										i.nome AS nome,
										i.cognome AS cognome,
										i.telefono AS telefono,
										i.mail AS mail,
										i.token AS token,
										p.nome_delegato AS nome_volontario,
										p.cognome_delegato AS cognome_volontario,
										p.mail_delegato AS mail_volontario
										FROM iscrizioni AS i
										INNER JOIN comitati AS c
										ON i.id_comitato = c.id
										INNER JOIN preiscrizioni AS p
										ON c.id = p.id_comitato
										WHERE i.id = '$id'";
			$result_ricerca =  mysql_query($query_ricerca, $db);
			$row_ricerca = mysql_fetch_array($result_ricerca);
			
			fwrite($fp,date('d/m/Y H:i:s').' - '."Modifica nuovo iscritto $nome $cognome correttamente inserita nel DB."."\r\n");
			echo "La modifica del nuovo iscritto $nome $cognome è avvenuta corretamente.<br><br>";
			//Invio la mail di conferma al delagato che ha inserito l'osservatore
			include('PHPMailer/class.phpmailer.php');
			//require_once('PHPMailer/class.smtp.php');  
		
			$mittente = "meeting.2016@piemonte.cri.it";
			$nomemittente = "Back Office Meeting 2016";
// Da rimettere il giorno delle iscrizioni
//			$destinatario = "$row_ricerca[mail_volontario]";
			$ServerSMTP = "mailbox.cri.it"; //server SMTP
			$oggetto = "Sostituzione $row_ricerca[ruolo] Meeting 2016";
			$corpo_messaggio = "Ciao $row_ricerca[nome_volontario],\nla presente per notificare l'avvenuta iscrizione come sostituto nel ruolo di $row_ricerca[ruolo] di $row_ricerca[nome] $row_ricerca[cognome] afferente al $row_ricerca[comitato].\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
		
			$msg = new PHPMailer;
			$msg->CharSet = "UTF-8";
			$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
			//$msg->IsHTML(true);
			$msg->SMTPAuth = true; // Autenticazione SMTP
			$msg->SMTPKeepAlive = "true";
			$msg->Host = $ServerSMTP;
			$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
			$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
		
			$msg->From = $mittente;
			$msg->FromName = $nomemittente;
			$msg->AddAddress($destinatario); 
//			$msg->AddCC("$row_ricerca[mail_comitato]");
			$msg->AddBCC("paolo.ditoma@libero.it");
//			$msg->AddBCC("backoffice.meeting@gmail.com");
			$msg->Subject = $oggetto; 
			$msg->Body = $corpo_messaggio;
		
		
			if(!$msg->Send())
			{
				echo "Si è verificato un errore nell'invio della mail di notifica sostituzione $row_ricerca[ruolo]:".$msg->ErrorInfo;
				fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Invio mail notifica sostituzione $row_ricerca[ruolo] $row_ricerca[nome] $row_ricerca[cognome] effettuata da $operatore al volontario $row_ricerca[nome_volontario] $row_ricerca[cognome_volontario] con indirizzo $row_ricerca[mail_volontario]: ".$msg->ErrorInfo."\r\n");
			}
			else
			{
				echo "Abbiamo inviato una mail di notifica a $row_ricerca[nome_volontario] $row_ricerca[cognome_volontario] con indirizzo $row_ricerca[mail_volontario]";
				fwrite($fp,date('d/m/Y H:i:s').' - '."Invio mail notifica avvenuta sostituzione $row_ricerca[ruolo] $row_ricerca[nome] $row_ricerca[cognome] effettuata da $operatore al volontario $row_ricerca[nome_volontario] $row_ricerca[cognome_volontario] con indirizzo $row_ricerca[mail_volontario] correttamente effettuato."."\r\n");
			}
			
			//Invio la mail per la conferma
			$mittente = "meeting.2016@piemonte.cri.it";
			$nomemittente = "Back Office Meeting 2016";
			$destinatario = "$row_ricerca[mail]";
			$ServerSMTP = "mailbox.cri.it"; //server SMTP
			$oggetto = "Sostituzione $row_ricerca[ruolo] Meeting 2016 - Conferma indirizzo mail";
			$corpo_messaggio = "Ciao $row_ricerca[nome],\nsei appena stato iscritto come $row_ricerca[ruolo] della squadra del $row_ricerca[comitato] per la partecipazione al Meeting 2016 dei Giovani della Croce Rossa Italiana. Al fine di verificare la veridicità delle informazioni inserite ti chiediamo di confermare il tuo indirizzo mail cliccando sul link seguente\n\n  http://www.itempa.com/meeting2016/verifica_mail_iscrizione.php?token=$row_ricerca[token]\n\nOgni altra notifica riguardante il Meeting ti sarà recapitata all'indirizzo mail confermato.\n\nN.B.Qualora tu intenda utilizzare un altro indirizzo, ti preghiamo di indicarcelo rispondendo a questa mail senza procedere quindi alla conferma.\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
		
			$msg = new PHPMailer;
			$msg->CharSet = "UTF-8";
			$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
			//$msg->IsHTML(true);
			$msg->SMTPAuth = true; // Autenticazione SMTP
			$msg->SMTPKeepAlive = "true";
			$msg->Host = $ServerSMTP;
			$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
			$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
	
			$msg->From = $mittente;
			$msg->FromName = $nomemittente;
			$msg->AddAddress($destinatario); 
	//		$msg->AddCC("$row[mail_comitato]");
	//		$msg->AddBCC("paolo.ditoma@libero.it");
			$msg->Subject = $oggetto; 
			$msg->Body = $corpo_messaggio;
			
			if(!$msg->Send())
			{
				fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE invio della mail di verifica indirizzo mail a $nome $cognome all'indirizzo $mail con Token $row_riceca[token]".$msg->ErrorInfo."\r\n");
			}
			else
			{
				$query = "UPDATE iscrizioni SET mail_reminder ='1' WHERE token = '$token'";
				mysql_query($query, $db);
				fwrite($fp,date('d/m/Y H:i:s').' - '."Inviata mail di verifica indirizzo mail a $nome $cognome per conferma mail $mail con Token $row_ricerca[token]"."\r\n");
			}
		}
		else
		{
			echo "Si è verificato un errore nell'inserimento dell'iscrizione, contattare l'amministratore del sistema.<br><br>";
			fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE query update ( $query_update ) del nuovo  $nome $cognome : ".$result_update."\r\n");
		}
	}
	?>
   	<form name="form1" enctype="multipart/form-data" method="POST" action="registrazione_squadra_osservatori.php?id_comitato=<?= $id_comitato ?>">
	<input type="submit" value="Torna alla registrazione" />
	</form>
	<?php
	mysql_close($db);
	fclose($fp);
} else {
	$errore ="errore";
	?>
	<form name="form1" enctype="multipart/form-data" method="POST" action="modifica_iscritto.php">

	<fieldset>
		<legend><b>Dati del volontario</b></legend>
        <br />
        Nome&nbsp&nbsp
        <input type="text" name="nome" size="20" maxlength="40" required >
        
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        
        Cognome&nbsp&nbsp
        <input type="text" name="cognome" size="20" maxlength="40" required><br />
        <br />
        
        Recapito telefonico&nbsp&nbsp
        <input type="tel" name="telefono" size="8" required title="prefisso e numero senza spazi o altri caratteri (max 10 cifre)" pattern="[0-9]{10}">
        
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        
        E-mail&nbsp&nbsp
        <input type="email" name="mail" size="28" required><br />
        <br />
        
	</fieldset><br />
	<br />
	<br />
	<p align="left">
	<input type="hidden" name="id" value="<?= $id ?>" />
	<input type="submit" value="Inserisci" />
	</form>
	<?php
}
?>


</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
