<?php
$token = $_GET['token'];

	// settiamo a zero la variabile mobile_browser 
	
	$mobile_browser = '0';
	
	//  controlliamo se l'user agent corrisponde a un dispositivo mobile
	 
	if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
		$mobile_browser++;
	}
	
	// controlliamo HTTP_ACCEPT corrisponde alla tecnologia WAP per telefonini
	 
	if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0)
	or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
		$mobile_browser++;
	}    
	
	// estraiamo i primi 4 caratteri da USER_AGENT e creiamo un array con tutti i header mobile
	 
	$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
	$mobile_agents = array(
		'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
		'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
		'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
		'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
		'newt','noki','oper','palm','pana','pant','phil','play','port','prox',
		'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
		'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
		'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
		'wapr','webc','winw','winw','xda ','xda-');
	
	// se corrisponde a mobile aumentiamo la nostra variabile di 1 
	 
	if (in_array($mobile_ua,$mobile_agents)) {
		$mobile_browser++;
	}
	 
	if (strpos(strtolower($_SERVER['ALL_HTTP']),'OperaMini') > 0) {
		$mobile_browser++;
	}
	 
	if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'windows') > 0) {
		$mobile_browser = 0;
	}
	
	// eccezione per windows phone
	if (preg_match('/(windows phone|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
		$mobile_browser++;
	}
	
	
	// quindi se mobile_browser � inferiore di 0 vuol dire che un dispositivo mobile
	 
	if ($mobile_browser > 0) {
	
	// reindirizzamento per mobile
	
	header('Location: http://www.itempa.com/meeting2016/mobile/verifica_mail_iscrizione.php?token='.$token); 
	
	}
	else 
	{
	// altrimenti restiamo dove siamo
	?>
    
        <!--IE 7 quirks mode please-->
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
        <head>
            <title>Verifica mail iscrizione</title>
        
            <!-- Contents -->
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta http-equiv="Content-Language" content="it" />
            <meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
            <meta http-equiv="Content-Type-Script" content="text/javascript" />
            <meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
            <meta name="keywords" content="" />
        
            <!-- Others -->
            <meta name="Author" content="Paolo di Toma" />
            <meta http-equiv="ImageToolbar" content="False" />
            <meta name="MSSmartTagsPreventParsing" content="True" />
            <link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />
        
            <!-- Parent -->
            <link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />
        
            <!-- Res -->
            <script type="text/javascript" src="res/x5engine.js"></script>
            <link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
            <link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
            <link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
            <!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
            <link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
            <link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
            <link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />
        
            <!-- Robots -->
            <meta http-equiv="Expires" content="0" />
            <meta name="Resource-Type" content="document" />
            <meta name="Distribution" content="global" />
            <meta name="Robots" content="index, follow" />
            <meta name="Revisit-After" content="21 days" />
            <meta name="Rating" content="general" />
        </head>
        <body>
        <div id="imSite">
        <div id="imHeader">
            
            <h1>Verifica mail iscrizione</h1>
        </div>
        <div class="imInvisible">
        <hr />
        <a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
        <a name="imGoToMenu"></a>
        </div>
        <div id="imBody">
            <div id="imMenuMain">
        
        <!-- Menu Content START -->
        <p class="imInvisible">Menu principale:</p>
        <div id="imMnMn">
        
        <?php 
        include ("main_menu.php");
        ?>
        
        </div>
        <!-- Menu Content END -->
        
            </div>
        <hr class="imInvisible" />
        <a name="imGoToCont"></a>
            <div id="imContent">
        
        <!-- Page Content START -->
        <div id="imPageSub">
        <br />
        <h2>Verifica mail iscrizione</h2>
        <p id="imPathTitle">Iscrizioni</p>
        <div id="imToolTip"></div>
        <div id="imBody">
        <div id="imContent">
        
        <?php
        include ("config.inc.php");
        echo "<font color=#2B3856 size='3' face='Tahoma'>";
        $fp = fopen ("log_iscrizioni.txt",a);
        
        //Leggo i dati in input
        $token=$_GET['token'];
        
        
        include ("apri_db.php");
        //Aggiorno la tabella preiscrzioni settando ad 1 il valore di 'iscrizione'
        $query = "UPDATE iscrizioni SET mail_confermata = '1' WHERE token = '$token'";
        if (mysql_query($query, $db))
        {
            //Scrivo l'avvenuta conferma a video e nel file di log
            $query_estrazione = "SELECT * FROM iscrizioni WHERE token = '$token'";
            $result = mysql_query($query_estrazione, $db);
            $row = mysql_fetch_array($result);
                            
            include('PHPMailer/class.phpmailer.php');
            //require_once('PHPMailer/class.smtp.php');  
            
            $mittente = "meeting.2016@piemonte.cri.it";
            $nomemittente = "Back Office Meeting 2016";
            $destinatario = "$row[mail]";
            $ServerSMTP = "mailbox.cri.it"; //server SMTP
            $oggetto = "Iscrizione Meeting 2016 - Indirizzo mail confermato";
            $corpo_messaggio = "Ciao $row[nome],\nla presente per notificare l'avvenuta conferma del tuo indirizzo mail $row[mail]\n\nTi ricordiamo che ogni altra eventuale comunicazione riguardante il Meeting verrà recapitata a questo indirizzo.\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
            
            $msg = new PHPMailer;
            $msg->CharSet = "UTF-8";
            $msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
            //$msg->IsHTML(true);
            $msg->SMTPAuth = true; // Autenticazione SMTP
            $msg->SMTPKeepAlive = "true";
            $msg->Host = $ServerSMTP;
            $msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
            $msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
            
            $msg->From = $mittente;
            $msg->FromName = $nomemittente;
            $msg->AddAddress($destinatario); 
        //	$msg->AddCC("$row[mail_comitato]");
        //	$msg->AddBCC("paolo.ditoma@libero.it");
            $msg->Subject = $oggetto; 
            $msg->Body = $corpo_messaggio;
            
            if(!$msg->Send())
            {
                fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE: Invio mail per mail confermata a $row[nome] $row[cognome] all'indirizzo $row[mail] con Token $token".$msg->ErrorInfo."\r\n");
            }
            else
            {
                echo "Il tuo indirizzo mail è stato confermato.";
                fwrite($fp,date('d/m/Y H:i:s').' - '."Mail confermata per $row[nome] $row[cognome] con indirizzo $row[mail] con Token $token"."\r\n");
            }
                        
        }
        else
        {
            echo "Questo token non esiste, ricontrolla la mail ricevuta.";
            fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE - Update su Token inesistente: $token"."\r\n");
        }
        
        mysql_close($db);
        fclose($fp);
        
        ?>
        
        </div>
        </div>
        </div>
        
        <!-- Page Content END -->
        
                </div>
            <div id="imFooter">
                <?php 
                include ("footer.php");
                ?>
            </div>
        </div>
        </div>
        <div class="imInvisible">
        <hr />
        <a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
        </div>
        
        <div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
        </body>
        </html>
    
    
    
	<?php
	
	echo "";
	
	}   
?>

