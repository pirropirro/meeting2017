<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Tabellone iscrizioni</title>

	<!-- Contents xxxxx-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Parent -->
	<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/tableExport.js"></script>
	<script type="text/javascript" src="js/jquery.base64.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p018.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Tabellone iscrizioni</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Tabellone iscrizioni</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?
include("config.inc.php");
include ("apri_db.php");
?>

<font color=#2B3856 size='2' face='Tahoma'>
<table id='stato_iscrizione' border='0' width=100%>
<thead>
<tr>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:170px; height:24px; '>COMITATO</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:60px; height:24px; '>MEETING</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:70px; height:24px; '>BONIFICO</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:70px; height:24px; '>SQUADRA</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:90px; height:24px; '>OSSERVATORI</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:90px; height:24px; '>SIMULATORI</th> 
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:90px; height:24px; '>TRUCCATORI</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:70px; height:24px; '>GIUDICI</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:70px; height:24px; '>OSPITI</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:70px; height:24px; '>STAFF</th>
<th align=center style=border-style:outset style='font size:100%' bgcolor="#81BEF7" style=color:#FFFF99><div style='width:110px; height:24px; '>COORDINAMENTO</th>
</tr>
</thead>

<?php
//Inizializzo i contatori
$tot_squadra = 0;
$tot_osservatori = 0;
$tot_simulatori = 0;
$tot_truccatori = 0;
$tot_giudici = 0;
$tot_ospiti = 0;
$tot_staff = 0;
$tot_coordinamento = 0;
$tot_iscritti = 0;
$tot_squadre_iscritte = 0;

//Estraggo con mysql pivot
$query = "SELECT c.nome_comitato AS comitato,
p.pagamento AS pagamento,
c.tipo_meeting AS meeting,
COUNT(
 CASE
  WHEN i.ruolo = 'capitano' OR i.ruolo = 'partecipante' OR i.ruolo = 'adulto' THEN 1
  ELSE NULL
  END
) AS squadra,
COUNT(
 CASE
  WHEN i.ruolo = 'osservatore' THEN 1
  ELSE NULL
  END
) AS osservatori,
COUNT(
 CASE
  WHEN i.ruolo = 'simulatore' THEN 1
  ELSE NULL
  END
) AS simulatori,
COUNT(
 CASE
  WHEN i.ruolo = 'truccatore' THEN 1
  ELSE NULL
  END
) AS truccatori,
COUNT(
 CASE
  WHEN i.ruolo = 'giudice' THEN 1
  ELSE NULL
  END
) AS giudici,
COUNT(
 CASE
  WHEN i.ruolo = 'ospite' THEN 1
  ELSE NULL
  END
) AS ospiti,
COUNT(
 CASE
  WHEN i.ruolo = 'staff' THEN 1
  ELSE NULL
  END
) AS staff,
COUNT(
 CASE
  WHEN i.ruolo = 'coordinamento' THEN 1
  ELSE NULL
  END
) AS coordinamento
FROM iscrizioni AS i
INNER JOIN comitati AS c
ON i.id_comitato = c.id
INNER JOIN preiscrizioni AS p
ON i.id_comitato = p.id_comitato
WHERE i.registrazione != 9
GROUP BY i.id_comitato
ORDER BY c.tipo_meeting DESC, c.nome_comitato";
$result = mysql_query($query, $db);
while($row = mysql_fetch_array( $result )) 
{
	$re = "/(|\\s*comitato locale di\\s*|\\s*comitato provinciale di\\s*|\\s*comitato regionale\\s*|\\s*delegazione di\\s*|\\s*a valenza regionale\\s*)/i"; 
	$subst = ""; 	 
	$comitato_trunc = preg_replace($re, $subst, $row[comitato]);
	?>
	<tr>
	<td align=center style="font size:90%" bgcolor=#ffffff><div style=" <?php if($row[pagamento] == 2) {echo "color:#0000FF;";} elseif($row[pagamento] == NULL) {echo "color:red;";} ?> height:18px; overflow-y:hidden; overflow-x:hidden;"><?= $comitato_trunc ?></td>
	<td align=center style="font size:90%" bgcolor=#ffffff><div style=" height:18px; overflow-y:hidden; overflow-x:hidden;"><?= $row[meeting] ?></td>
   	<td align=center style="font size:90%" bgcolor=#ffffff><div style=" height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if ($row[pagamento] == 1 || $row[pagamento] == 2) echo "<img src=images/spunta.jpg width='20' height='20' align='middle' border='' title='Già registrato'>"; ?></td>
	<td align=center style="font size:90%' bgcolor=#ffffff><div style=" height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if($row[squadra] != 0){ echo $row[squadra]; $tot_squadra = $tot_squadra + $row[squadra]; $tot_iscritti = $tot_iscritti + $row[squadra]; $tot_squadre_iscritte++;} ?></td>
	<td align=center style="font size:90%" bgcolor=#ffffff><div style=" height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if($row[osservatori] != 0){ echo $row[osservatori]; $tot_osservatori = $tot_osservatori + $row[osservatori]; $tot_iscritti = $tot_iscritti + $row[osservatori];} ?></td>
	<td align=center style="font size:90%" bgcolor=#ffffff><div style=" height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if($row[simulatori] != 0){ echo $row[simulatori]; $tot_simulatori = $tot_simulatori + $row[simulatori]; $tot_iscritti = $tot_iscritti + $row[simulatori];} ?></td>
	<td align=center style="font size:90%" bgcolor=#ffffff><div style=" height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if($row[truccatori] != 0){ echo $row[truccatori]; $tot_truccatori = $tot_truccatori + $row[truccatori]; $tot_iscritti = $tot_iscritti + $row[truccatori];} ?></td>
	<td align=center style="font size:90%" bgcolor=#ffffff><div style=" height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if($row[giudici] != 0){ echo $row[giudici]; $tot_giudici = $tot_giudici + $row[giudici]; $tot_iscritti = $tot_iscritti + $row[giudici];} ?></td>
	<td align=center style="font size:90%" bgcolor=#ffffff><div style=" height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if($row[ospiti] != 0){ echo $row[ospiti]; $tot_ospiti = $tot_ospiti + $row[ospiti]; $tot_iscritti = $tot_iscritti + $row[ospiti];} ?></td>
	<td align=center style="font size:90%" bgcolor=#ffffff><div style=" height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if($row[staff] != 0){ echo $row[staff]; $tot_staff = $tot_staff + $row[staff]; $tot_iscritti = $tot_iscritti + $row[staff];} ?></td>
	<td align=center style="font size:90%" bgcolor=#ffffff><div style=" height:18px; overflow-y:hidden; overflow-x:hidden;"><?php if($row[coordinamento] != 0){ echo $row[coordinamento]; $tot_coordinamento = $tot_coordinamento + $row[coordinamento]; $tot_iscritti = $tot_iscritti + $row[coordinamento];} ?></td>
	<?php
}

$query_assenti = "SELECT id FROM iscrizioni WHERE registrazione = 9";
$result_assenti = mysql_query($query_assenti, $db);
$tot_assenti = mysql_num_rows($result_assenti);

?>

<tr>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b>TOTALE (<?= $tot_squadre_iscritte ?> squadre)</b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_iscritti." tutti" ?></b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_assenti." assenti" ?></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_squadra." par" ?></b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_osservatori." oss" ?></b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_simulatori." sim" ?></b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_truccatori." tru" ?></b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_giudici." giu" ?></b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_ospiti." osp" ?></b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_staff." sta" ?></b></td>
<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><b><?= $tot_coordinamento." coo" ?></b></td>

<?php
$query = "SELECT		p.id
                    	FROM preiscrizioni AS p
                    	INNER JOIN comitati AS c
                    	ON p.id_comitato = c.id
                    	WHERE p.conferma_presidente = '1'
						AND p.iscrizione IS NULL";
$result = mysql_query($query, $db);
$numero_squadre_non_iscritte = mysql_num_rows($result);
?>

<tr>
<td colspan="11" align="center" style="font size:90%" bgcolor="#ffffff"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"> </td>
<tr>
<td colspan="11" align="center" style="font size:90%" bgcolor="#81BEF7"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;">NON ANCORA ISCRITTE <b><?= $numero_squadre_non_iscritte ?></b> SQUADRE</td>
<tr>
<td colspan="5" align="center" style="font size:90%" bgcolor="#81BEF7"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><b>Comitato</b></td>
<td colspan="4" align="center" style="font size:90%" bgcolor="#81BEF7"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><b>Volontario che ha preiscritto</b></td>
<td colspan="2" align="center" style="font size:90%" bgcolor="#81BEF7"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><b>Conferma preiscrizione</b></td>

<?php
$query = "SELECT		c.nome_comitato AS nome_comitato,
						c.tipo_meeting AS tipo_meeting,
                    	p.nome_delegato AS nome_volontario,
                    	p.cognome_delegato AS cognome_volontario,
						DATE_FORMAT(p.data_conferma, '%d-%m-%Y') AS data_conferma
                    	FROM preiscrizioni AS p
                    	INNER JOIN comitati AS c
                    	ON p.id_comitato = c.id
                    	WHERE p.conferma_presidente = '1'
						AND p.iscrizione IS NULL
						ORDER BY c.tipo_meeting DESC, p.data_conferma";
$result = mysql_query($query, $db);
while($row = mysql_fetch_array( $result )) 
{
	?>
	<tr>
	<td colspan="5" align="center" style="font size:90%" bgcolor="#ffffff"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><?= $row[nome_comitato] ?></td>
	<td colspan="4" align="center" style="font size:90%" bgcolor="#ffffff"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><?= $row[nome_volontario]." ".$row[cognome_volontario] ?></td>
	<td colspan="2" align="center" style="font size:90%" bgcolor="#ffffff"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><?= $row[data_conferma] ?></td>
	<?php
}

$query = "SELECT		p.id
                    	FROM preiscrizioni AS p
                    	INNER JOIN comitati AS c
                    	ON p.id_comitato = c.id
                    	WHERE p.conferma_presidente = '1'
						AND p.iscrizione = '2'";
$result = mysql_query($query, $db);
$numero_squadre_ritirate = mysql_num_rows($result);
?>

<tr>
<td colspan="11" align="center" style="font size:90%" bgcolor="#ffffff"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"> </td>
<tr>
<td colspan="11" align="center" style="font size:90%" bgcolor="#F08080"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><b><?= $numero_squadre_ritirate ?></b> SQUADRE RITIRATE</td>
<tr>
<td colspan="5" align="center" style="font size:90%" bgcolor="#F08080"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><b>Comitato</b></td>
<td colspan="4" align="center" style="font size:90%" bgcolor="#F08080"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><b>Volontario che ha preiscritto</b></td>
<td colspan="2" align="center" style="font size:90%" bgcolor="#F08080"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><b>Comunicazione ritiro</b></td>

<?php
$query = "SELECT		c.nome_comitato AS nome_comitato,
						c.tipo_meeting AS tipo_meeting,
                    	p.nome_delegato AS nome_volontario,
                    	p.cognome_delegato AS cognome_volontario,
						DATE_FORMAT(p.data_conferma, '%d-%m-%Y') AS data_conferma
                    	FROM preiscrizioni AS p
                    	INNER JOIN comitati AS c
                    	ON p.id_comitato = c.id
                    	WHERE p.conferma_presidente = '1'
						AND p.iscrizione = '2'
						ORDER BY c.tipo_meeting DESC, p.data_conferma";
$result = mysql_query($query, $db);
while($row = mysql_fetch_array( $result )) 
{
	?>
	<tr>
	<td colspan="5" align="center" style="font size:90%" bgcolor="#ffffff"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><?= $row[nome_comitato] ?></td>
	<td colspan="4" align="center" style="font size:90%" bgcolor="#ffffff"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><?= $row[nome_volontario]." ".$row[cognome_volontario] ?></td>
	<td colspan="2" align="center" style="font size:90%" bgcolor="#ffffff"><div style="height:18px; overflow-y:hidden; overflow-x:hidden;"><?= $row[data_conferma] ?></td>
	<?php
}
	
mysql_close($db); 
?>
</table>

<br /><br />
<p align="left">
	
<a href="#" onClick ="$('#stato_iscrizione').tableExport({type:'excel',escape:'false',fileName: 'Stato Iscrizione'});">Esporta in MS Excel</a>

<br /><br />


</div>
</div>
</div>


<!-- Page Content END -->

	</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
