<?php

include_once ("auth.php");

include_once ("authconfig.php");

include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico

if (!($check['team'] == 'backoffice') && !($check['team'] == 'capitani'))

{

	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";

	print "<b>Accesso non consentito</b>";

	print "</font><br>";

	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";

	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato al Back Office.</b></font>";

	exit;	// Stop script execution

}
?>


	<!--IE 7 quirks mode please-->

	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">

	<head>

		<title>Meeting 2016 - Comitato Regionale del Piemonte - Homepage</title>

	

		<!-- Contents -->

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

		<meta http-equiv="Content-Language" content="it" />

		<meta http-equiv="last-modified" content="07/01/2009 10.37.23" />

		<meta http-equiv="Content-Type-Script" content="text/javascript" />

		<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />

		<meta name="keywords" content="" />

	

		<!-- Others -->

		<meta name="Author" content="Paolo di Toma" />

		<meta http-equiv="ImageToolbar" content="False" />

		<meta name="MSSmartTagsPreventParsing" content="True" />

		<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	

		<!-- Parent -->

		<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

	

		<!-- Res -->

		<script type="text/javascript" src="res/x5engine.js"></script>

		<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />

		<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />

		<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />

		<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->

		<link rel="stylesheet" type="text/css" href="res/home.css" media="screen, print" />

		<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />

		<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	

		<!-- Robots -->

		<meta http-equiv="Expires" content="0" />

		<meta name="Resource-Type" content="document" />

		<meta name="Distribution" content="global" />

		<meta name="Robots" content="index, follow" />

		<meta name="Revisit-After" content="21 days" />

		<meta name="Rating" content="general" />

	</head>

	<body>

	<div id="imSite">

	<div id="imHeader">

<?php

include ("config.inc.php");

echo "<font color=#2B3856 size='2' face='Tahoma'>";

include ("apri_db.php");

$ora_attuale = strftime('%H:%M', time());

?>

<?php

		$id_comitato = $_GET['id_comitato'];

		

		include ("config.inc.php");

		include ("apri_db.php");

		

		

		$query = "SELECT p.F_SEL1 AS f_sel

						FROM preiscrizioni AS p

						WHERE id_comitato = $id_comitato";

		

		$result = mysql_query($query, $db);

		$row = mysql_fetch_array($result);

		

		?>

<form name="form1" enctype="multipart/form-data" method="post" action="salva_questionario_f.php?domanda=1&id_comitato=<?= $id_comitato ?>">

		

		<fieldset data-role="controlgroup">

			<legend>La disabilità è:</legend>

				<input type="radio" name="risposta" id="radio-choice-1" value="3" <?php if($row[f_sel] == 3) echo "checked= 'checked' "; ?>>

				<label for="radio-choice-1">Una caratteristica di un individuo.</label><br /><br />

				<input type="radio" name="risposta" id="radio-choice-2" value="2" <?php if($row[f_sel] == 2) echo " checked= 'checked' "; ?>>

				<label for="radio-choice-2">Una condizione di differenza che rende inferiori le persone che ne sono affette.</label><br /><br />

				<input type="radio" name="risposta" id="radio-choice-3" value="1" <?php if($row[f_sel] == 1) echo " checked= 'checked' "; ?>>

				<label for="radio-choice-3">Una complessa interazione di condizioni, molte delle quali sono create dall’ambiente sociale.</label><br /><br />

				<input type="radio" name="risposta" id="radio-choice-4" value="4" <?php if($row[f_sel] == 4) echo " checked= 'checked' "; ?>>

				<label for="radio-choice-4">Tutte le precedenti.</label><br /><br />

				<input type="radio" name="risposta" id="radio-choice-5" value="5" <?php if($row[f_sel] == 5) echo " checked= 'checked' "; ?>>

				<label for="radio-choice-5">Non lo so.</label><br /><br />

		</fieldset>

        <br /> <br /> <br />

		

		<input type="submit" value="Conferma" />

		</form>

		

		<?php

		mysql_close($db); 

		?>

</div>

</div>



</body>

</html>		