<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice') && !($check['team'] == 'tutor'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
 
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Registrazione Squadra Osservatori</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/tableExport.js"></script>
	<script type="text/javascript" src="js/jquery.base64.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Registrazione Squadra Osservatori</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Registrazione Squadra Osservatori</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include ("config.inc.php");
echo "<font color=#2B3856 size='2' face='Tahoma'>";

$id_comitato=$_REQUEST['id_comitato'];
include ("apri_db.php");

//Comitato di cui si inserisci la passwd
$query = "SELECT nome_comitato FROM comitati WHERE id = '$id_comitato'";
$result = mysql_query($query, $db);
$row = mysql_fetch_array( $result );

?>
<form name="form1" enctype="multipart/form-data" method="post" action="salva_registrazione_squadra_osservatori.php">

<table border='0' width=100%>

<tr>
<th align=center colspan='9' style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Provenienti dal <?php echo $row[nome_comitato] ?></th> </tr>
</table>
<table id='stato_iscrizione' border='0' width=100%>

<thead>
<tr>
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Ritira</th>
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Ruolo</th>
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Nome</th> 
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Cognome</th>
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>E-mail</th>
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Presente</th>
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Assente</th>
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Modifica</th>
</thead>	

<?php
//Faccio la query sulla tabella iscrizioni per estrarre i dati da visualizzare
$query_iscrizioni = "SELECT	*
								FROM iscrizioni
								WHERE id_comitato = '$id_comitato'
								AND (ruolo = 'capitano' OR ruolo ='partecipante' OR ruolo ='adulto' OR ruolo = 'osservatore') 
								ORDER BY FIND_IN_SET (ruolo, 'capitano,partecipante,adulto,osservatore')";
$result_iscrizioni = mysql_query($query_iscrizioni, $db);
$i = 0;
while($row_iscrizioni = mysql_fetch_array( $result_iscrizioni )) 
{
	$i++;
	?>
	<tr>
    <td align=center style="font size:90%" bgcolor="#ffffff"><div style="width:60px; height:18px; overflow-y:hidden; overflow-x:hidden;"><input type="radio" name="id_ritira" value="<?= $row_iscrizioni[id] ?>" /></td>
	<td align=center style="font size:90%" bgcolor="#ffffff"><div style="width:110px; height:18px; overflow-y:hidden; overflow-x:hidden;"><b><font color="#2B3856"><?php echo $row_iscrizioni[ruolo] ?></b></font></td>
	<td align=center style="font size:90%" bgcolor="#ffffff"><div style="width:100px; height:18px; overflow-y:hidden; overflow-x:hidden;"><?php echo $row_iscrizioni[nome] ?></td>
	<td align=center style="font size:90%" bgcolor="#ffffff"><div style="width:100px; height:18px; overflow-y:hidden; overflow-x:hidden;"><?php echo $row_iscrizioni[cognome] ?></td>
	<td align=center style="font size:90%" bgcolor="#ffffff"><div style=" <?php if($row_iscrizioni[mail_confermata] != 1) echo "color:red;"; ?> width:200px; height:18px; overflow-y:hidden; overflow-x:hidden;"><?php echo $row_iscrizioni[mail] ?></td>
 
    <?php
    //Faccio un IF per capire se già registrato
    if ($row_iscrizioni[registrazione] == '1' || $row_iscrizioni[registrazione] == '2')
    {
		?>
        <td align=center style='font size:90%' bgcolor=#ffffff><div style='height:18px; overflow-y:hidden; overflow-x:hidden;'><img src=images/spunta.jpg width='20' height='20' align='middle' border='' title='Già registrato'><td>
        <?php
    }
    elseif ($row_iscrizioni[registrazione] == '9')
		{
			?>
            <td align=center style='font size:90%' bgcolor=#ffffff><div style='height:18px; overflow-y:hidden; overflow-x:hidden;'>assente<td>
            <?php
		}
		else
    	{
			?>
			<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:60px; height:18px; overflow-y:hidden; overflow-x:hidden;'>
        	<input type="checkbox" name="presenze[]" value="<?php echo "$row_iscrizioni[id]"?>"/>
			</td>
			<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:60px; height:18px; overflow-y:hidden; overflow-x:hidden;'>
        	<input type="checkbox" name="assenze[]" value="<?php echo "$row_iscrizioni[id]"?>"/>
			</td>
			<?php
    	}
    ?>
    
	<td align=center style='font size:90%' bgcolor=#ffffff>
		<div style=' width:110px; height:18px; overflow-y:hidden; overflow-x:hidden;'>
			<a href="modifica_iscritto.php?id=<?= $row_iscrizioni[id] ?>&id_comitato=<?= $id_comitato ?>">
				<img src=images/ominostaff.jpeg width='20' height='20' align='middle' border='' title='Modifica dati iscritto'>
			</a>
		</div>
	</td>
    <?php
}

mysql_close($db); 
?>
</table>
<br /><br />

<fieldset>
	<legend><b>L'adulto ha compiuto 31 anni (nascita prima del 24-10-1985)</b></legend>
	<br />
	SI <input type="radio" name="penalita_over" value="1" checked="checked"/>
	&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
	NO <input type="radio" name="penalita_over" value="0.95"/><br />
</fieldset><br />


<br /><br />
<b>Totale da registrare: <?= $i ?></b><br />


<br /><br />
<input type="hidden" name="id_comitato" value="<?php echo $id_comitato; ?>" />
<input type="submit" value="Registra" />
</form>


</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
