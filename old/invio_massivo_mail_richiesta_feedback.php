<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato all'organizzazione od al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Invio Massivo Mail Richiesta Feedback</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Invio Massivo Mail Pubblicazione Classifica</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Invio Massivo Mail Richiesta Feedback</h2>
<p id="imPathTitle">Post-Meeting</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include ("config.inc.php");
include('PHPMailer/class.phpmailer.php');
echo "<font color=#2B3856 size='2' face='Tahoma'>";
$fp = fopen ("log_iscrizioni.txt",a);

include ("apri_db.php");

//Cerco le info del volontario tramite id
$query_id = "SELECT c.nome_comitato AS nome_comitato,
					c.tipo_meeting AS tipo_meeting,
					i.nome AS nome,
					i.cognome AS cognome,
					i.ruolo AS ruolo,
					i.id AS id,
					i.telefono AS telefono,
					i.mail AS mail,
					i.token AS token,
					i.mail_reminder AS mail_reminder
					FROM iscrizioni AS i
					INNER JOIN comitati AS c
					ON i.id_comitato = c.id
					WHERE i.mail_confermata IS NOT NULL
					AND (i.registrazione = 1 OR i.registrazione = 2)
					AND i.mail_richiesta_feedback IS NULL
					AND i.id < 800";
$result_id = mysql_query($query_id, $db);
while($row_id = mysql_fetch_array( $result_id )) 
{
	if ($row_id[tipo_meeting] == 'N')
	{
		echo "link finale $link_finale <br>";
		echo "$row_id[ruolo]";
		//Nazionali
		switch ($row_id[ruolo]) 
		{
			case 'partecipante':
				$link_finale = 'feedback_nazionale.php';
				break;
			case 'osservatore':
				$link_finale = 'feedback_nazionale.php';
				break;
			case 'capitano':
				$link_finale = 'feedback_nazionale.php';
				break;				
			case 'simulatore':
				$link_finale = 'feedback_simulatori_n.php';
				break;
			case 'giudice':
				$link_finale = 'feedback_giudici_n.php';
				break;
			case 'ospite':
				$link_finale = 'feedback_generale_ospitante_n.php';
				break;
			case 'coordinamento':
				$link_finale = 'feedback_generale_ospitante_n.php';
				break;
			default:
				$link_finale = 'feedback_generale_ospitante_n.php';
		}
				echo "link finale $link_finale <br>";
				echo "$row_id[ruolo]";
	}
	else
	{
		//Provinciale e Regionale
		switch ($row_id[ruolo]) 
		{
			case 'partecipante':
				$link_finale = 'feedback_quota_tutor.php';
				break;
			case 'osservatore':
				$link_finale = 'feedback_quota_tutor.php';
				break;
			case 'capitano':
				$link_finale = 'feedback_quota_tutor.php';
				break;				
			case 'simulatore':
				$link_finale = 'feedback_simulatori.php';
				break;
			case 'giudice':
				$link_finale = 'feedback_giudici.php';
				break;
			case 'staff':
				$link_finale = 'feedback_staff.php';
				break;
			case 'ospite':
				$link_finale = 'feedback_generale_ospitante.php';
				break;
			case 'coordinamento':
				$link_finale = 'feedback_generale_ospitante.php';
				break;				
			default:
				$link_finale = 'feedback_generale_ospitante.php';
		}
	}
	
	
	//Invio la mail di conferma all'iscritto
	$mittente = "meeting.2015@cri.it";
	$nomemittente = "Back Office Meeting 2015";
	$destinatario = "$row_id[mail]";
//	$destinatario = "paolo.ditoma@libero.it";
	$ServerSMTP = "mailbox.cri.it"; //server SMTP
	$oggetto = "Richiesta Feedback Meeting 2015";
	if ($row_id[tipo_meeting] == 'N')
	{
		//Messaggio Meeting Nazionale
		$corpo_messaggio = "Ciao $row_id[nome],\ndal 9 all\'11 ottobre hai preso parte al Meeting 2015 come $row_id[ruolo] del $row_id[nome_comitato] e tutto lo Staff si augura sia stata per te una bella esperienza. Ma adesso abbiamo bisogno di un ultimo tuo contributo per aiutarci a migliorare le prossime edizioni.\n\nTramite il portale dedicato puoi inserire le tue valutazioni (entro e non oltre il 30 novembre). Ti chiediamo di inserire per ogni voce che ti verrà proposta, un giudizio da 0 a 10 (puoi inserire anche i decimali). Il Back Office garantisce l'anonimato delle valutazioni inserite.\n\nClicca sul link riportato di seguito per la compilazione del questionario\n\n      http://www.itempa.com/meeting2015/mobile/".$link_finale."?id=".$row_id[token]."&ruolo=".$row_id[ruolo]."\n\n\n";

		$corpo_messaggio .= "Non utilizzare i tasti indietro/avanti del browser, nel caso fosse necessario, riparti sempre da questo link.\nTi ricordiamo, qualora tu non la abbia ancora consultata, che sul portale è disponibile la classifica finale con i relativi punteggi. Una volta rilasciato il feedback sarà per te possibile accedere alla pagina contenente i punteggi parziali di ogni prova svolta dalla squadra del tuo comitato cliccando sul link\n\n      http://www.itempa.com/meeting2015/classifica_finale_squadra.php?token=".$row_id[token]."&type=".$row_id[tipo_meeting];

		$corpo_messaggio .= "\n\n\nGrazie in anticipo per la tua collaborazione.\n\nCordiali Saluti,\nBack Office\nMeeting 2015\n\n(inviata automaticamente dal Portale Web)";
	}
	else
	{
		//Messaggio Meeting Regonale Provinciale
		$corpo_messaggio = "Ciao $row_id[nome],\nlo scorso 11 ottobre hai preso parte al Meeting Regionale-Provinciale 2015 come $row_id[ruolo] del $row_id[nome_comitato] e tutto lo Staff si augura sia stata per te una bella esperienza. Ma adesso abbiamo bisogno di un ultimo tuo contributo per aiutarci a migliorare le prossime edizioni.\n\nTramite il portale dedicato puoi inserire le tue valutazioni (entro e non oltre il 30 novembre). Ti chiediamo di inserire per ogni voce che ti verrà proposta, un giudizio da 0 a 10 (puoi inserire anche i decimali). Il Back Office garantisce l'anonimato delle valutazioni inserite.\n\nClicca sul link riportato di seguito per la compilazione del questionario\n\n      http://www.itempa.com/meeting2015/mobile/".$link_finale."?id=".$row_id[token]."&ruolo=".$row_id[ruolo]."\n\n\n";

		$corpo_messaggio .= "Non utilizzare i tasti indietro/avanti del browser, nel caso fosse necessario, riparti sempre da questo link.\nTi ricordiamo, qualora tu non la abbia ancora consultata, che sul portale è disponibile la classifica finale con i relativi punteggi. Una volta rilasciato il feedback sarà per te possibile accedere alla pagina contenente i punteggi parziali di ogni prova svolta dalla squadra del tuo comitato cliccando sul link\n\n      http://www.itempa.com/meeting2015/classifica_finale_squadra.php?token=".$row_id[token]."&type=".$row_id[tipo_meeting];

		$corpo_messaggio .= "\n\n\nGrazie in anticipo per la tua collaborazione.\n\nCordiali Saluti,\nBack Office\nMeeting 2015\n\n(inviata automaticamente dal Portale Web)";
		
	}


	
	$msg = new PHPMailer;
	$msg->CharSet = "UTF-8";
	$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
	//$msg->IsHTML(true);
	$msg->SMTPAuth = true; // Autenticazione SMTP
	$msg->SMTPKeepAlive = "true";
	$msg->Host = $ServerSMTP;
	$msg->Username = "meeting.2015@cri.it"; // Nome utente SMTP autenticato
	$msg->Password = "Spaghett!44"; // Password account email con SMTP autenticato
	
	$msg->From = $mittente;
	$msg->FromName = $nomemittente;
	$msg->AddAddress($destinatario); 
//	$msg->AddBCC("paolo.ditoma@libero.it");
	$msg->Subject = $oggetto; 
	$msg->Body = $corpo_messaggio;
	
	
	if(!$msg->Send())
	{
		echo "Si è verificato un errore nell'invio della mail pubblicazione classifica per l'iscritto $row_id[nome] $row_id[cognome] all'indirizzo $row_id[mail] :".$msg->ErrorInfo;
		echo "<br><br>";
		fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Pubblicazione classifica - Invio mail pubblicazione classifica per l'iscritto $row_id[nome] $row_id[cognome] all'indrizzo $row_id[mail] con TOKEN $row_id[token] : ".$msg->ErrorInfo."\r\n");
	}
	else
	{
		echo "Abbiamo inviato una mail di pubblicazione classifica all'iscritto $row_id[nome] $row_id[cognome] all'indirizzo $row_id[mail] con TOKEN $row_id[token].<br><br>";
		fwrite($fp,date('d/m/Y H:i:s').' - '."Pubblicazione classifica massivo - Invio mail all'iscritto $row_id[nome] $row_id[cognome] con TOKEN $row_id[token] correttamente effettuato all'indirizzo $row_id[mail]."."\r\n");

		$query = "UPDATE iscrizioni SET mail_richiesta_feedback = 1 WHERE id = $row_id[id]";
		$result_update=mysql_query($query, $db);
		
		//Se l'update è andata bene, scrivo nel log
		if ($result_update)
		{
			fwrite($fp,date('d/m/Y H:i:s').' - '."Sollecito conferma mail massivo - Update del mail_classifica a 1 per $row_id[nome] $row_id[cognome] correttamente inserito nel DB."."\r\n");
			echo "Update del mail_classificareminder a 1 per $row_id[nome] $row_id[cognome] correttamente inserito nel DB.<br><br>";

		}
		else
		{
			echo "Update del mail_classifica a 1 per $row_id[nome] $row_id[cognome] in errore:".$msg->ErrorInfo;
			fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Update mail_classifica - Update del mail_classifica a 1 per $row_id[nome] $row_id[cognome]: ".$msg->ErrorInfo."\r\n");
		}
	}
}



mysql_close($db);
fclose($fp);

?>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
