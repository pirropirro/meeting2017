<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato all'organizzazione od al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Invio massivo per conferma mail</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Invio massivo per programma partecipanti, osservatori, staff, ospiti, coordinamento</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Invio singolo notifica per conferma mail</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include ("config.inc.php");
include('PHPMailer/class.phpmailer.php');
echo "<font color=#2B3856 size='2' face='Tahoma'>";


 
$mail = new PHPMailer();  
 
$mail->IsSMTP();  // telling the class to use SMTP
$mail->Mailer = "smtp";
$mail->Host = "ssl://smtp.gmail.com";
$mail->Port = 465;
$mail->SMTPAuth = true; // turn on SMTP authentication
$mail->Username = "ditoma.paolo@gmail.com"; // SMTP username
$mail->Password = "VFR$56yhn"; // SMTP password 
 
$mail->From     = "ditoma.paolo@gmail.com";
$mail->AddAddress("paolo.ditoma@libero.it");  
 
$mail->Subject  = "First PHPMailer Message";
$mail->Body     = "Hi! \n\n This is my first e-mail sent through PHPMailer.";
$mail->WordWrap = 50;  
 
if(!$mail->Send()) {
echo 'Message was not sent.';
echo 'Mailer error: ' . $mail->ErrorInfo;
} else {
echo 'Message has been sent.';
}





//	$mittente = "meeting.2017@piemonte.cri.it";
//	$nomemittente = "Back Office Meeting 2017";
//	$destinatario = "$row_id[mail]";
////	$destinatario = "paolo.ditoma@libero.it";
//	$ServerSMTP = "ssl://smtp.gmail.com"; //server SMTP
//	$oggetto = "Meeting 2017 - Test";
//	$apertura_messaggio = "Caro $row_id[nome],\nquesto è un test.\n\n";
//	
//	$firma_messaggio = "\n\n\nCordiali Saluti,\nBack Office\nMeeting 2017\n\n(inviata automaticamente dal Portale Web)";
//	
//	$msg = new PHPMailer;
//	$msg->CharSet = "UTF-8";
//	$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
//	//$msg->IsHTML(true);
//	
//	$msg->SMTPAuth = true; // Autenticazione SMTP
//	$msg->SMTPKeepAlive = "true";
//	$msg->Host = $ServerSMTP;
//	$msg->Port = 465;
//	$msg->Username = "meeting.2017@piemonte.cri.it"; // Nome utente SMTP autenticato
//	$msg->Password = "Spaghett!17"; // Password account email con SMTP autenticato
//	
//	$msg->From = $mittente;
//	$msg->FromName = $nomemittente;
//	$msg->AddAddress($destinatario); 
//	$msg->AddBCC("paolo.ditoma@piemonte.cri.it");
//	$msg->Subject = $oggetto; 
//	$msg->Body = $apertura_messaggio;
//	$msg->Body .= $firma_messaggio;	
//	
//	if(!$msg->Send())
//	{
//		echo "Si è verificato un errore nell'invio della mail del saluto rappresentante per l'iscritto $row_id[nome] $row_id[cognome] all'indirizzo $row_id[mail] :".$msg->ErrorInfo;
//		echo "<br><br>";
//		fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE saluto rappresentante - Invio mail del programma per simulatori per l'iscritto $row_id[nome] $row_id[cognome] all'indrizzo $row_id[mail] : ".$msg->ErrorInfo."\r\n");
//	}
//	else
//	{
//		echo "Abbiamo inviato una mail del programma per simulatori all'iscritto $row_id[nome] $row_id[cognome] all'indirizzo $row_id[mail].<br><br>";
//		fwrite($fp,date('d/m/Y H:i:s').' - '."saluto rappresentante mail massivo - Invio mail del programma per simulatori all'iscritto $row_id[nome] $row_id[cognome] correttamente effettuato all'indirizzo $row_id[mail]."."\r\n");
//


?>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
