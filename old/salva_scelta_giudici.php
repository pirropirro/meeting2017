<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice') && !($check['team'] == 'organigramma'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato all'organizzazione od al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Scelta Giudici</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Scelta Giudici</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Scelta Giudici</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include ("config.inc.php");
echo "<font color=#2B3856 size='3' face='Tahoma'>";
$fp = fopen ("log_iscrizioni.txt",a);

$operatore=$check['uname'];

//Leggo i dati in input
$id_scelto=$_REQUEST['id_scelto'];
//echo "$id_scelto<br>";

include ("apri_db.php");

//Cerco le info del volontario tramite id
$query_id = "SELECT c.nome_comitato AS nome_comitato,
					c.nome_presidente AS nome_presidente,
					c.cognome_presidente AS cognome_presidente,
					c.mail_comitato AS mail_comitato,
					p.nome_delegato AS nome_volontario,
					p.cognome_delegato AS cognome_volontario,
					p.mail_delegato AS mail_volontario,
					i.nome AS nome,
					i.cognome AS cognome,
					i.ruolo AS ruolo,
					i.id AS id,
					i.telefono AS telefono,
					i.mail AS mail,
					i.truccatore AS truccatore
					FROM iscrizioni AS i
					INNER JOIN preiscrizioni AS p
					ON i.id_comitato = p.id_comitato
					INNER JOIN comitati AS c
					ON i.id_comitato = c.id
					WHERE i.id = $id_scelto";
$result_id = mysql_query($query_id, $db);
$row_id = mysql_fetch_array($result_id);

if ($result_id)
{
	//lo inserisco come giudice
	$ruolo_nuovo = "giudice";

	$query = "UPDATE iscrizioni SET ruolo = '$ruolo_nuovo' WHERE id = $id_scelto";
	$result_update=mysql_query($query, $db);
	
	//Se l'update è andata bene, invio la mail di avviso
	if ($result_update)
	{
    	fwrite($fp,date('d/m/Y H:i:s').' - '."Reclutamento dell'osservatore $row_id[nome] $row_id[cognome] come $ruolo_nuovo effettuata da $operatore correttamente inserita nel DB."."\r\n");
		echo "Il reclutamento come $ruolo_nuovo dell'osservatore $row_id[nome] $row_id[cognome] afferente al $row_id[nome_comitato] è avvenuto correttamente.<br><br>";
		//Invio la mail di conferma al delagato che ha inserito l'osservatore
		include('PHPMailer/class.phpmailer.php');
		//require_once('PHPMailer/class.smtp.php');  
	
		$mittente = "meeting.2016@piemonte.cri.it";
		$nomemittente = "Back Office Meeting 2016";
		$destinatario = "$row_id[mail]";
//		$destinatario = "paolo.ditoma@libero.it";
		$ServerSMTP = "mailbox.cri.it"; //server SMTP
		$oggetto = "Reclutamento $ruolo_nuovo Meeting 2016";
		$corpo_messaggio = "Ciao $row_id[nome],\ncon la presente ti notifichiamo che sei stato selezionato per partecipare al Meeting 2016 con il ruolo di $ruolo_nuovo, ti ringraziamo fin da adesso per la tua disponibilità. Come da regolamento, facendo parte dello staff, sei esonerato dal pagamento della quota prevista per gli ossservatori.\n\nPotrai essere ricontattato nei prossimi giorni dal referente per i giudici e/o dal Back Office per tutti i dettagli del caso. Per completezza di informazioni abbiamo inserito in copia il tuo consigliere giovane o il volontario $row_id[nome_volontario] $row_id[cognome_volontario] che si è occupato delle iscrizioni ed il presidente del $row_id[nome_comitato] ($row_id[nome_presidente] $row_id[cognome_presidente]).\n\nPer qualsiasi dubbio, angoscia e/o perplessità, non esitare a contattarci.\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
	
		$msg = new PHPMailer;
		$msg->CharSet = "UTF-8";
		$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
		//$msg->IsHTML(true);
		$msg->SMTPAuth = true; // Autenticazione SMTP
		$msg->SMTPKeepAlive = "true";
		$msg->Host = $ServerSMTP;
		$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
		$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
	
		$msg->From = $mittente;
		$msg->FromName = $nomemittente;
		$msg->AddAddress($destinatario); 
		$msg->AddCC("$row_id[mail_comitato]");
		$msg->AddCC("$row_id[mail_volontario]");
		$msg->AddBCC("paolo.ditoma@libero.it");
		$msg->AddBCC("backoffice.meeting@gmail.com");
		$msg->Subject = $oggetto; 
		$msg->Body = $corpo_messaggio;
	
	
		if(!$msg->Send())
		{
			echo "Si è verificato un errore nell'invio della mail di notifica reclutamento come $ruolo_nuovo dell'osservatore $row_id[nome] $row_id[cognome]:".$msg->ErrorInfo;
			fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Invio mail notifica reclutamento come $ruolo_nuovo dell'osservatore $row_id[nome] $row_id[cognome] effettuata da $operatore con in cc $row_id[nome_volontario] $row_id[cognome_volontario] (indirizzo $row_comitato[mail_volontario]) e presidente $row_id[nome_presidente] $row_id[cognome_presidente] (indirizzo $row_id[mail_comitato]: ".$msg->ErrorInfo."\r\n");
		}
		else
		{
			echo "Abbiamo inviato una mail di notifica a $row_id[nome] $row_id[cognome] (indirizzo $row_id[mail]) con in copia $row_id[nome_volontario] $row_id[cognome_volontario] (indirizzo $row_id[mail_volontario]) ed il presidente $row_id[nome_presidente] $row_id[cognome_presidente] (indirizzo $row_id[mail_comitato]).";
			fwrite($fp,date('d/m/Y H:i:s').' - '."Invio mail notifica avvenuta iscrizione osservatore $nome $cognome effettuata da $row_comitato[nome_volontario] $row_comitato[cognome_volontario] correttamente effettuato all'indirizzo $row_comitato[mail_volontario]."."\r\n");
		}
	}
	else
	{
		echo "Si è verificato un errore nell'update reclutamento come $ruolo_nuovo dell'osservatore $row_id[nome] $row_id[cognome]:".$msg->ErrorInfo;
		fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Update reclutamento come $ruolo_nuovo dell'osservatore $row_id[nome] $row_id[cognome] effettuata da $operatore: ".$msg->ErrorInfo."\r\n");
	}
}
else
{
	echo "Non trovo l'id del volontario, rivolgersi all'amministratore del sistema.<br><br>";
}



mysql_close($db);
fclose($fp);

?>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
