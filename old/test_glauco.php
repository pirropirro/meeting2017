<html>
<head>
	<title>Consulta iscrizione</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />

</head>
<body>

<?php
include ("config.inc.php");
include ("apri_db.php");

//Estraggo in un array le preiscrizioni non confermate
$query ="SELECT 	c.nome_comitato AS nome_comitato,
					c.nome_presidente AS nome_presidente,
					c.cognome_presidente AS cognome_presidente,
					c.mail_comitato AS mail_comitato,
					c.passwd AS passwd_comitato,
					p.id AS id_preiscrizione,
					p.nome_delegato AS nome_volontario,
					p.cognome_delegato AS cognome_volontario,
					p.telefono_delegato AS telefono_volontario,
					p.mail_delegato AS mail_volontario,
					DATE_FORMAT(p.data_inserimento, '%d-%m') AS data_inserimento,
					DATE_FORMAT(p.data_inserimento, '%H:%i') AS ora_inserimento
					FROM preiscrizioni AS p
					INNER JOIN comitati AS c
					ON p.id_comitato = c.id
					WHERE p.conferma_presidente = '1'
					AND p.mail_apertura_iscrizioni = '1'
					AND p.iscrizione IS NULL
					ORDER BY p.data_conferma DESC";


//$query = "SELECT * FROM preiscrizioni WHERE conferma_presidente = '0' ORDER BY data_inserimento";
$result = mysql_query($query, $db);
while($row = mysql_fetch_array( $result )) 
{
echo "$row[telefono_volontario];Ciao $row[nome_volontario] $row[cognome_volontario], stiamo testando il servizio di instant messaging per il Meeting 2015. Ti scriviamo in quanto volontario che si è occupato della preiscrzione della squadra del $row[nome_comitato]. Ti preghiamo di non segnalare questo numero come spam e di non rispondere a questo messaggio, il numero non è abilitato alla ricezione. Grazie per la tua collaborazione. Back Office Meeting 2015;<br>";
}
	
mysql_close($db); 
?>

</body>
</html>
