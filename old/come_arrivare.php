<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Come arrivare</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Parent -->
	<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>

<script type="text/javascript" src="tabber.js"></script>
<link rel="stylesheet" href="example.css" TYPE="text/css" MEDIA="screen">
<link rel="stylesheet" href="example-print.css" TYPE="text/css" MEDIA="print">

<script type="text/javascript">

/* Optional: Temporarily hide the "tabber" class so it does not "flash"
   on the page as plain HTML. After tabber runs, the class is changed
   to "tabberlive" and it will appear. */

document.write('<style type="text/css">.tabber{display:none;}<\/style>');
</script>

<script type="text/javascript" src="scripts/wysiwyg.js"></script>
<script type="text/javascript" src="scripts/wysiwyg-settings.js"></script> 
<script type="text/javascript">
WYSIWYG.attach('textarea2', small); // full featured setup 
</script> 

<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Come arrivare</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">
  
<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Come arrivare</h2>
<p id="imPathTitle">Contatti</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<br />
<table style="border: 0 !important">
    <tr>
        <td width="50%" align="center" style="border:0; border-style:outset; font size:100%; color:#FFFF99"><div style="height:400px;">
		
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2821.42680820065!2d7.663205815772361!3d44.995953972585255!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478812656f84d02d%3A0x891c2109f0125b3b!2sVia+Bertero%2C+2%2C+10024+Moncalieri+TO!5e0!3m2!1sit!2sit!4v1476694395827" width="600" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
		</td>
        <td width="50%" align="left" style="border:0; border-style:outset; font size:100%; color:#FFFF99"><div style="height:400px;">
       		<font color=#2B3856 size='2' face='Tahoma'>
            <b>Con mezzo proprio:</b> raggiungere la destinazione <b>Scuola Principessa Maria Clotilde (Istituto comprensivo Santa Maria), Via Bertero, 2, Moncalieri (TO)</b><br /><br /><br />

			<b>In treno:</b> raggiungere la stazione FS di <b>Porta Nuova</b> o <b>Porta Susa</b><br /><br /><br />
			
			<b>In aereo:</b> raggiungere l'<b>Aeroporto internazionale di Torino-Caselle "Sandro Pertini"</b><br /><br /><br /><br /><br /><br />
			
			Lo staff del Meeting si occuperà di garantire il servizio navetta da e verso le stazioni e l'aeroporto per le squadre provenienti dalle altre Regioni.<br />
			<b>N.B.</b> Si raccomanda di comunicare gli orari di partenza/arrivo all'indirizzo <a href="mailto:meeting.2016@piemonte.cri.it?subject=Indicazioni arrivo/partenza da Portale" title="">meeting.2016@piemonte.cri.it</a>
       </td>
   </tr>
</table>
<br /><br />

</div>
</div>
</div>

</div>
<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
