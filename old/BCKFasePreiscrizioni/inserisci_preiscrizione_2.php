<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Inserisci preiscrizione</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Parent -->
	<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<script type="text/javascript">
        function unlock(el1, el2) {
            if(el1.checked) {
                document.getElementById(el2).disabled = false;
            } else {
                document.getElementById(el2).disabled = 'disabled';
            }
        }
    </script>

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Inserisci preiscrizione</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Inserisci preiscrizione</h2>
<p id="imPathTitle">Preiscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?
include ("config.inc.php");
echo "<font color=#2B3856 size='3' face='Tahoma'>";
?>

<form method="post" action="salva_inserisci_preiscrizione.php">

<br />
<label>Squadra del<br>
 
<select name="id_comitato" size="8" >
<!--
   <optgroup label="Provincia di Torino">
   <option value='1'>Comitato Locale di Torino</option>
   <option value='2'>Comitato Locale di Agliè</option>
   <option value='3'>Comitato Locale di Airasca</option>
   <option value='4'>Comitato Locale di Bardonecchia</option>
   <option value='5'>Comitato Locale di Beinasco</option>
   <option value='6'>Comitato Locale di Carignano</option>
   <option value='7'>Comitato Locale di Carmagnola</option>
   <option value='8'>Comitato Locale di Mappano</option>
   <option value='9'>Comitato Locale di Castellamonte</option>
   <option value='10'>Comitato Locale di Chieri</option>
   <option value='11'>Comitato Locale di Chivasso</option>
   <option value='12'>Comitato Locale di Valli di Lanzo</option>
   <option value='13'>Comitato Locale di Cuorgné</option>
   <option value='14'>Comitato Locale di Druento</option>
   <option value='15'>Comitato Locale di Fiano</option>
   <option value='16'>Comitato Locale di Gassino Torinese</option>
   <option value='17'>Comitato Locale di Giaveno</option>
   <option value='124'>Delegazione di Grugliasco</option>
   <option value='18'>Comitato Locale di Ivrea</option>
   <option value='19'>Comitato Locale di Lauriano</option>
   <option value='20'>Comitato Locale di Leinì</option>
   <option value='123'>Delegazione di Mathi</option>
   <option value='21'>Comitato Locale di Moncalieri</option>
   <option value='22'>Comitato Locale di Montanaro</option>
   <option value='23'>Comitato Locale di Nichelino</option>
   <option value='120'>Delegazione di Pianezza</option>
   <option value='121'>Delegazione di Pinerolo</option>
   <option value='26'>Comitato Locale di Piossasco</option>
   <option value='27'>Comitato Locale di Poirino</option>
   <option value='28'>Comitato Locale di Pont Canavese</option>
   <option value='29'>Comitato Locale di Rivarolo Canavese</option>
   <option value='30'>Comitato Locale di Rivoli</option>
   <option value='31'>Comitato Locale di San Francesco al Campo</option>
   <option value='32'>Comitato Locale di San Giorgio Canavese</option>
   <option value='33'>Comitato Locale di Santena</option>
   <option value='34'>Comitato Locale di Settimo Torinese</option>
   <option value='35'>Comitato Locale di Settimo Vittone</option>
   <option value='36'>Comitato Locale di Strambino</option>
   <option value='37'>Comitato Locale di Susa</option>
   <option value='38'>Comitato Locale di Torre Pellice</option>
   <option value='39'>Comitato Locale di Trofarello</option>
   <option value='122'>Delegazione di Val Della Torre</option>   
   <option value='40'>Comitato Locale di Vigone</option>
   <option value='41'>Comitato Locale di Villar Dora</option>
   </optgroup>
   <optgroup label="Provincia di Alessandria">
   <option value='42'>Comitato Locale di Alessandria</option>
   <option value='43'>Comitato Locale di Acqui Terme</option>
   <option value='44'>Comitato Locale di Monferrato</option>
   <option value='45'>Comitato Locale di Cassine</option> 
   <option value='46'>Comitato Locale di Gavi Ligure</option> 
   <option value='47'>Comitato Locale di Novi Ligure</option> 
   <option value='48'>Comitato Locale di Serravalle Scrivia</option> 
   <option value='49'>Comitato Locale di Tortona</option> 
   <option value='50'>Comitato Locale di Vignole Borbera</option>
   </optgroup> 
   <optgroup label="Provincia di Asti">
   <option value='51'>Comitato Locale di Asti</option> 
   <option value='52'>Comitato Locale di Canelli</option>
   <option value='53'>Comitato Locale di Castelnuovo Don Bosco</option>   
   <option value='54'>Comitato Locale di Cocconato</option> 
   <option value='55'>Comitato Locale di Isola D'Asti </option> 
   <option value='56'>Comitato Locale di Monale</option> 
   <option value='57'>Comitato Locale di Montegrosso D'Asti</option> 
   <option value='58'>Comitato Locale di San Damiano D'Asti</option> 
   <option value='59'>Comitato Locale di Villanova D'Asti</option>
   </optgroup> 
   <optgroup label="Provincia di Cuneo"> 
   <option value='63'>Comitato Locale Provincia Granda</option> 
   <option value='64'>Comitato Locale di Alba</option> 
   <option value='65'>Comitato Locale di Borgo San Dalmazzo</option> 
   <option value='66'>Comitato Locale di Bra</option> 
   <option value='67'>Comitato Locale di Busca</option> 
   <option value='68'>Comitato Locale di Caraglio</option> 
   <option value='69'>Comitato Locale di Centallo</option> 
   <option value='70'>Comitato Locale di Cuneo</option>
   <option value='71'>Comitato Locale di Dronero</option> 
   <option value='72'>Comitato Locale di Limone Piemonte</option> 
   <option value='73'>Comitato Locale di Melle</option> 
   <option value='74'>Comitato Locale di Mondovì</option> 
   <option value='75'>Comitato Locale di Monesiglio</option> 
   <option value='76'>Comitato Locale di Moretta</option> 
   <option value='77'>Comitato Locale di Peveragno</option> 
   <option value='78'>Comitato Locale di Racconigi</option>
   <option value='79'>Comitato Locale di Sanpeyre</option>   
   <option value='80'>Comitato Locale di Savigliano</option>
   <option value='81'>Comitato Locale di Sommariva Bosco</option>
   <option value='82'>Comitato Locale di Valle Stura</option>
   </optgroup> 
   <optgroup label="Provincia di Verbania">  
   <option value='90'>Comitato Locale di Verbania</option> 
   <option value='91'>Comitato Locale di Baveno</option>
   <option value='92'>Comitato Locale di Cannobio</option> 
   <option value='93'>Comitato Locale di Domodossola</option> 
   <option value='94'>Comitato Locale di Stresa</option>
   </optgroup>

   <optgroup label="Provincia di VerNoBi">  
   <option value='95'>Comitato Locale di Vercelli</option>  
   <option value='96'>Comitato Locale di Borgosesia</option> 
   <option value='97'>Comitato Locale di Crescentino</option> 
   <option value='98'>Comitato Locale di Gattinara</option> 
   <option value='84'>Comitato Locale di Arona</option> 
   <option value='85'>Comitato Locale di Borgomanero</option>
   <option value='86'>Comitato Locale di Galliate</option> 
   <option value='87'>Comitato Locale di Novara</option> 
   <option value='88'>Comitato Locale di Oleggio</option> 
   <option value='89'>Comitato Locale di Trecate</option>
   <option value='60'>Comitato Locale di Biella</option> 
   <option value='61'>Comitato Locale di Cavaglià</option> 
   <option value='62'>Comitato Locale di Cossato</option>
   </optgroup> 
-->
   <optgroup label="Comitati Regionali"> 
   <option value='106'>Comitato Regionale della Liguria</option>
   <option value='107'>Comitato Regionale della Lombardia</option>
   <option value='118'>Comitato Regionale della Valle D'Aosta</option>
   <option value='119'>Comitato Regionale del Veneto</option>
   <option value='127'>Comitato Locale di Vado Ligure Quiliano</option>
   <option value='129'>Comitato Municipio 2-3 di Roma</option>
   </optgroup>
  </select>
</label>

<br /><br /><br />

<b>Dati del Delegato Area V </b> &nbsp&nbsp
<br /><br />
Nome&nbsp&nbsp
<textarea style="overflow:hidden" cols="30" rows="1" name="nome_delegato"></textarea><br />
<br />

Cognome&nbsp&nbsp
<textarea style="overflow:hidden" cols="30" rows="1" name="cognome_delegato"></textarea><br />
<br />

Recapito telefonico&nbsp&nbsp
<textarea style="overflow:hidden" cols="11" rows="1" name="telefono_delegato"></textarea><br />
<br />

E-mail&nbsp&nbsp
<textarea style="overflow:hidden" cols="25" rows="1" name="mail_delegato"></textarea><br />
<br />
<br />

<p><input type="checkbox" name="checkbox" value="1" onclick="unlock(this, 'btn1')" /> Confermo di aver preso visione del Regolamento per i Comitati Locali al Meeting Regionale (disponibile al <a class="ImLink" href="http://www.itempa.com/meeting2016/RegolamentoMeeting2016_Regionale.pdf" title="Regolamento Meeting 2016 Regionale">link</a>)</p>
<br />
<br />

<p><input type="submit" name="submit" id="btn1" value="Registra Preiscrizione" disabled="disabled" /></p>
<br />
<br />
</form>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
