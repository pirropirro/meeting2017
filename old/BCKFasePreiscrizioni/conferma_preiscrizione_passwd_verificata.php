<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Conferma preiscrizione</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Parent -->
	<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Meeting 2016 - Conferma preiscrizione</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<h2>Conferma Preiscrizione</h2>
<p id="imPathTitle">Preiscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?
include("config.inc.php");
echo "<font color=#2B3856 size='3' face='Tahoma'>";

$id_preiscrizione=$_GET['id'];
$passwd=$_REQUEST['passwd'];
$passwd=trim($passwd);

$db = mysql_connect($db_host, $db_user, $db_password);
if ($db == FALSE)
die ("Errore nella connessione. Verificare i parametri nel file config.inc.php");
mysql_select_db($db_name, $db)
or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");

//Recupero i dati della preiscrizione
$query = "SELECT * FROM preiscrizioni WHERE id = '$id_preiscrizione'";
$result = mysql_query($query, $db);
$row = mysql_fetch_array( $result );
$nome_comitato = $row[comitato];
$id_comitato = $row[id_comitato];
$nome_delegato = $row[nome_delegato];
$cognome_delegato = $row[cognome_delegato];
$mail_delegato = $row[mail_delegato];

//recupero i dati del comitato
$query = "SELECT * FROM comitati WHERE id = '$id_comitato'";
$result = mysql_query($query, $db);
$row = mysql_fetch_array( $result );
$nome_comitato = $row[nome_comitato];
$mail_comitato = $row[mail_comitato];
$nome_presidente = $row[nome_presidente];
$cognome_presidente = $row[cognome_presidente];
$passwd_comitato = $row[passwd];

if ($passwd_comitato == $passwd)
{
	//Faccio l'update sulla tabella preiscrizioni
	mysql_select_db($db_name, $db)
	or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");
	$query = "UPDATE preiscrizioni SET conferma_presidente = '1' WHERE id=$id_preiscrizione";
	if (mysql_query($query, $db))
	{
		echo "Hai confermato la partecipazione della squadra del $nome_comitato.<br>";

		//Invio la mail di conferma
		include('PHPMailer/class.phpmailer.php');
		//require_once('PHPMailer/class.smtp.php');  

		$mittente = "meeting.2016@piemonte.cri.it";
		$nomemittente = "Back Office Meeting 2016";
		$destinatario = "$mail_comitato";
		$ServerSMTP = "mailbox.cri.it"; //server SMTP
		$oggetto = "Preiscrizione confermata per il Meeting 2016";
		$corpo_messaggio = "Buongiorno Presidente $nome_presidente $cognome_presidente,\n\nla presente per notificare che la preiscrizione della squadra del $nome_comitato è stata confermata.\nIl volontario $nome_delegato $cognome_delegato che ha effettuato la preiscrizione è inserito in copia alla mail.\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016";


		$msg = new PHPMailer;
		$msg->CharSet = "UTF-8";
		$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
		//$msg->IsHTML(true);
		$msg->SMTPAuth = true; // Autenticazione SMTP
		$msg->SMTPKeepAlive = "true";
		$msg->Host = $ServerSMTP;
		$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
		$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato

		$msg->From = $mittente;
		$msg->FromName = $nomemittente;
		$msg->AddAddress($destinatario); 
		$msg->AddCC("$mail_delegato");
		$msg->AddBCC("paolo.ditoma@libero.it");
		$msg->Subject = $oggetto; 
		$msg->Body = $corpo_messaggio;

				
		if(!$msg->Send())
			{
			echo "Si è verificato un errore nell'invio della mail per la conferma da parte del tuo presidente:".$msg->ErrorInfo;
			}
		else
			{
			echo "Abbiamo inviato una mail di notifica al presidente ed in copia al consigliere giovane che ha effettuato la preiscrizione.";
			}
		}
else
		{
    	echo "&nbsp&nbsp&nbspSi è verificato un errore durante la conferma, contatta l'amministratore";
		}
//endif;  
mysql_close($db);


}
else
{
echo "Password errata";
}





$db = mysql_connect($db_host, $db_user, $db_password);
  if ($db == FALSE)
    die ("Errore nella connessione. Verificare i parametri nel file config.inc.php");

//ricavo l'id
mysql_select_db($db_name, $db)
or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");
$query = "SELECT * FROM preiscrizioni WHERE id='$id_preiscrizione'";
$result = mysql_query($query, $db);
$row = mysql_fetch_array($result);
$nome_comitato= $row[nome_comitato];
//echo "$comitato";



?>

<br /><br />
<form method="post">
<input type="button" value="Chiudi"
onclick="window.close()">
</form>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
