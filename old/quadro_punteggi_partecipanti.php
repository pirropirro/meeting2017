<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Tabellone iscrizioni</title>

	<!-- Contents xxxxx-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Parent -->
	<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/tableExport.js"></script>
	<script type="text/javascript" src="js/jquery.base64.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p018.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Tabellone iscrizioni</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Tabellone iscrizioni</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?
include("config.inc.php");
include ("apri_db.php");
?>
<font color=#2B3856 size='2' face='Tahoma'>

<img src=images/sleep-icon.gif width='30' height='30' align='middle' border='' title='Punteggio parzialmente pervenuto'>
Punteggio parzialmente pervenuto&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
<img src=images/confusion-icon.gif width='30' height='30' align='middle' border='' title='Punteggio incongruente da validare'>
Punteggio incongruente da validare&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
<img src=images/repair-icon.gif width='30' height='30' align='middle' border='' title='Punteggio congruente da validare'>
Punteggio congruente da validare&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
<img src=images/ok-icon.gif width='30' height='30' align='middle' border='' title='Punteggio valido'>
Punteggio valido&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

<br /><br />

<font color=#2B3856 size='2' face='Tahoma'>
<table id='stato_iscrizione' border='0' width=100%>

<tr>
<th align=center colspan='8' style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>QUADRO PUNTEGGI</th>
<tr>

<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style=' width:170px; height:25px; '>Squadra</th> 
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style=' width:102px; height:25px; '>Prova Comune</th> 
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style=' width:108px; height:25px; '>Prova 1</th> 
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style=' width:108px; height:25px; '>Prova 2</th>
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style=' width:108px; height:25px; '>Prova 3</th> 
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style=' width:108px; height:25px; '>Prova 4</th>
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style=' width:108px; height:25px; '>Prova 5</th>
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style=' width:108px; height:25px; '>Prova 6</th>
<tr>


<?php
$query = "SELECT	c.nome_comitato AS nome_comitato,
								p.0_c AS 0_c,
								p.0_v AS 0_v,
								p.1_g AS 1_g,
								p.1_c AS 1_c,
								p.1_v AS 1_v,
								p.2_g AS 2_g,
								p.2_c AS 2_c,
								p.2_v AS 2_v,
								p.3_g AS 3_g,
								p.3_c AS 3_c,
								p.3_v AS 3_v,
								p.4_g AS 4_g,
								p.4_c AS 4_c,
								p.4_v AS 4_v,
								p.5_g AS 5_g,
								p.5_c AS 5_c,
								p.5_v AS 5_v,
								p.6_g AS 6_g,
								p.6_c AS 6_c,
								p.6_v AS 6_v
								FROM preiscrizioni AS p
								INNER JOIN comitati AS c
								ON p.id_comitato = c.id
								WHERE p.iscrizione='1'
								ORDER BY c.nome_comitato";
$result = mysql_query($query, $db);
while($row = mysql_fetch_array( $result )) 
{
	$re = "/(\\s*comitato locale di\\s*|\\s*comitato provinciale di\\s*|\\s*comitato regionale\\s*|\\s*delegazione di\\s*|\\s*a valenza regionale\\s*|\\s*comitato\\s*)/i"; 
	$subst = ""; 	 
	$comitato_trunc = preg_replace($re, $subst, $row[nome_comitato]);
	?>
	
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:200px; height:18px; overflow-y:hidden; overflow-x:hidden;'><?php echo "$comitato_trunc"?></td>
	<?php
	if ($row['0_v'] != NULL)
	{
		echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:72px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/ok-icon.gif width='24' height='24' align='middle' border='' title='Punteggio valido'></td>";
	}
	elseif ($row['0_c'] == NULL)
		{
			echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:72px; height:25px; overflow-y:hidden; overflow-x:hidden;'> </td>";
		}
		else
		{
			echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:72px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/repair-icon.gif width='24' height='24' align='middle' border='' title='Punteggio congruente da validare'></td>";
		}
	
	
	
	if ($row['1_v'] != NULL)
	{
		echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/ok-icon.gif width='24' height='24' align='middle' border='' title='Punteggio valido'></td>";
	}
	elseif ($row['1_g'] == NULL && $row['1_c'] == NULL)
		{
			echo "<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'> </td>";
		}
		elseif ($row['1_g'] == NULL || $row['1_c'] == NULL)
			{
				echo "<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/sleep-icon.gif width='24' height='24' align='middle' border='' title='Punteggio parzialmente pervenuto'></td>";
			}
			elseif ($row['1_g'] == $row['1_c'])
				{
					echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/repair-icon.gif width='24' height='24' align='middle' border='' title='Punteggio congruente da validare'></td>";
				}
				else
				{
					echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/confusion-icon.gif width='24' height='24' align='middle' border='' title='Punteggio incongruente da validare'></td>";
				}	
	
	if ($row['2_v'] != NULL)
	{
		echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/ok-icon.gif width='24' height='24' align='middle' border='' title='Punteggio valido'></td>";
	}
	elseif ($row['2_g'] == NULL && $row['2_c'] == NULL)
		{
			echo "<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'> </td>";
		}
		elseif ($row['2_g'] == NULL || $row['2_c'] == NULL)
			{
				echo "<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/sleep-icon.gif width='24' height='24' align='middle' border='' title='Punteggio parzialmente pervenuto'></td>";
			}
			elseif ($row['2_g'] == $row['2_c'])
				{
					echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/repair-icon.gif width='24' height='24' align='middle' border='' title='Punteggio congruente da validare'></td>";
				}
				else
				{
					echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/confusion-icon.gif width='24' height='24' align='middle' border='' title='Punteggio incongruente da validare'></td>";
				}	
	
	if ($row['3_v'] != NULL)
	{
		echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/ok-icon.gif width='24' height='24' align='middle' border='' title='Punteggio valido'></td>";
	}
	elseif ($row['3_g'] == NULL && $row['3_c'] == NULL)
		{
			echo "<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'> </td>";
		}
		elseif ($row['3_g'] == NULL || $row['3_c'] == NULL)
			{
				echo "<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/sleep-icon.gif width='24' height='24' align='middle' border='' title='Punteggio parzialmente pervenuto'></td>";
			}
			elseif ($row['3_g'] == $row['3_c'])
				{
					echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/repair-icon.gif width='24' height='24' align='middle' border='' title='Punteggio congruente da validare'></td>";
				}
				else
				{
					echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/confusion-icon.gif width='24' height='24' align='middle' border='' title='Punteggio incongruente da validare'></td>";
				}	
	
	if ($row['4_v'] != NULL)
	{
		echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/ok-icon.gif width='24' height='24' align='middle' border='' title='Punteggio valido'></td>";
	}
	elseif ($row['4_g'] == NULL && $row['4_c'] == NULL)
		{
			echo "<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'> </td>";
		}
		elseif ($row['4_g'] == NULL || $row['4_c'] == NULL)
			{
				echo "<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/sleep-icon.gif width='24' height='24' align='middle' border='' title='Punteggio parzialmente pervenuto'></td>";
			}
			elseif ($row['4_g'] == $row['4_c'])
				{
					echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/repair-icon.gif width='24' height='24' align='middle' border='' title='Punteggio congruente da validare'></td>";
				}
				else
				{
					echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/confusion-icon.gif width='24' height='24' align='middle' border='' title='Punteggio incongruente da validare'></td>";
				}	
	
	if ($row['5_v'] != NULL)
	{
		echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/ok-icon.gif width='24' height='24' align='middle' border='' title='Punteggio valido'></td>";
	}
	elseif ($row['5_g'] == NULL && $row['5_c'] == NULL)
		{
			echo "<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'> </td>";
		}
		elseif ($row['5_g'] == NULL || $row['5_c'] == NULL)
			{
				echo "<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/sleep-icon.gif width='24' height='24' align='middle' border='' title='Punteggio parzialmente pervenuto'></td>";
			}
			elseif ($row['5_g'] == $row['5_c'])
				{
					echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/repair-icon.gif width='24' height='24' align='middle' border='' title='Punteggio congruente da validare'></td>";
				}
				else
				{
					echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/confusion-icon.gif width='24' height='24' align='middle' border='' title='Punteggio incongruente da validare'></td>";
				}	
	
	if ($row['6_v'] != NULL)
	{
		echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/ok-icon.gif width='24' height='24' align='middle' border='' title='Punteggio valido'></td>";
	}
	elseif ($row['6_g'] == NULL && $row['6_c'] == NULL)
		{
			echo "<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'> </td>";
		}
		elseif ($row['6_g'] == NULL || $row['6_c'] == NULL)
			{
				echo "<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/sleep-icon.gif width='24' height='24' align='middle' border='' title='Punteggio parzialmente pervenuto'></td>";
			}
			elseif ($row['6_g'] == $row['6_c'])
				{
					echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/repair-icon.gif width='24' height='24' align='middle' border='' title='Punteggio congruente da validare'></td>";
				}
				else
				{
					echo "<td align=center style='font size:100%' bgcolor=#ffffff><div style=' width:108px; height:25px; overflow-y:hidden; overflow-x:hidden;'><img src=images/confusion-icon.gif width='24' height='24' align='middle' border='' title='Punteggio incongruente da validare'></td>";
				}	
	
	?>
	<tr>
	<?php
}
mysql_close($db);
?>
</table>

<br /><br />


</div>
</div>
</div>


<!-- Page Content END -->

	</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
