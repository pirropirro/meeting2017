<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Iscrizione squadra</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Iscrizione squadra</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Iscrizione squadra</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include ("config.inc.php");
echo "<font color=#2B3856 size='3' face='Tahoma'>";

$id_comitato=$_REQUEST['id_comitato'];
$passwd=$_REQUEST['passwd'];
//echo $id_comitato;
//echo $passwd;

include ("apri_db.php");

//Comitato di cui si inserisci la passwd
$query = "SELECT nome_comitato, passwd FROM comitati WHERE id = '$id_comitato'";
$result = mysql_query($query, $db);
$row = mysql_fetch_array( $result );
mysql_close($db); 

//echo $row[nome_comitato];


if ($row[passwd] == $passwd  && !($id_comitato == ''))
{
	?>
	<form name="form1" enctype="multipart/form-data" method="post" action="salva_inserisci_iscrizione_squadra.php">

	<table border='0' width=100%>

	<tr>
	<th align=center colspan='9' style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>SQUADRA del <?php echo $row[nome_comitato] ?></th> </tr>

	<tr>

	<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Ruolo</th>
	<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Nome</th> 
	<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Cognome</th>
	<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Telefono</th> 
	<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>E-mail</th>
	<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Esigenze alimentari</th>
	<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99 ><div style='height:25px; '>Eventuali disabilità</th>
	
	</tr>


	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:110px; height:18px; overflow-y:hidden; overflow-x:hidden;'><b><font color="#2B3856">Capitano</b></font></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:130px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="nome_1" size="12" maxlength="40" required ></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:130px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="cognome_1" size="12" maxlength="40" required></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:80px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="tel" name="telefono_1" size="8" required title="prefisso e numero senza spazi o altri caratteri (max 10 cifre)" pattern="[0-9]{10}"></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:210px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="email" name="mail_1" size="28" required></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:210px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="esigenze_1" size="36" maxlength="140" placeholder="nessuna indicazione"></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:210px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="disabilita_1" size="36" maxlength="140" placeholder="nessuna indicazione"></td>
    <tr>
    
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:110px; height:18px; overflow-y:hidden; overflow-x:hidden;'><b><font color="#2B3856">Partecipante</b></font></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:130px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="nome_2" size="12" maxlength="40" required ></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:130px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="cognome_2" size="12" maxlength="40" required></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:80px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="tel" name="telefono_2" size="8" required title="prefisso e numero senza spazi o altri caratteri (max 10 cifre)" pattern="[0-9]{10}"></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:210px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="email" name="mail_2" size="28" required></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:210px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="esigenze_2" size="36" maxlength="140" placeholder="nessuna indicazione"></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:210px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="disabilita_2" size="36" maxlength="140" placeholder="nessuna indicazione"></td>
    <tr>
    
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:110px; height:18px; overflow-y:hidden; overflow-x:hidden;'><b><font color="#2B3856">Partecipante</b></font></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:130px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="nome_3" size="12" maxlength="40" required ></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:130px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="cognome_3" size="12" maxlength="40" required></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:80px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="tel" name="telefono_3" size="8" required title="prefisso e numero senza spazi o altri caratteri (max 10 cifre)" pattern="[0-9]{10}"></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:210px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="email" name="mail_3" size="28" required></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:210px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="esigenze_3" size="36" maxlength="140" placeholder="nessuna indicazione"></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:210px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="disabilita_3" size="36" maxlength="140" placeholder="nessuna indicazione"></td>
    <tr>
    
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:110px; height:18px; overflow-y:hidden; overflow-x:hidden;'><b><font color="#2B3856">Partecipante</b></font></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:130px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="nome_4" size="12" maxlength="40" required ></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:130px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="cognome_4" size="12" maxlength="40" required></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:80px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="tel" name="telefono_4" size="8" required title="prefisso e numero senza spazi o altri caratteri (max 10 cifre)" pattern="[0-9]{10}"></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:210px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="email" name="mail_4" size="28" required></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:210px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="esigenze_4" size="36" maxlength="140" placeholder="nessuna indicazione"></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:210px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="disabilita_4" size="36" maxlength="140" placeholder="nessuna indicazione"></td>
    <tr>
    
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:110px; height:18px; overflow-y:hidden; overflow-x:hidden;'><b><font color="#2B3856">Partecipante</b></font></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:130px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="nome_5" size="12" maxlength="40" required ></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:130px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="cognome_5" size="12" maxlength="40" required></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:80px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="tel" name="telefono_5" size="8" required title="prefisso e numero senza spazi o altri caratteri (max 10 cifre)" pattern="[0-9]{10}"></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:210px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="email" name="mail_5" size="28" required></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:210px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="esigenze_5" size="36" maxlength="140" placeholder="nessuna indicazione"></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:210px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="disabilita_5" size="36" maxlength="140" placeholder="nessuna indicazione"></td>
    <tr>
    
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:110px; height:18px; overflow-y:hidden; overflow-x:hidden;'><b><font color="#2B3856">Adulto</b></font></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:130px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="nome_6" size="12" maxlength="40" required ></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:130px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="cognome_6" size="12" maxlength="40" required></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:80px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="tel" name="telefono_6" size="8" required title="prefisso e numero senza spazi o altri caratteri (max 10 cifre)" pattern="[0-9]{10}"></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:210px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="email" name="mail_6" size="28" required></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:210px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="esigenze_6" size="36" maxlength="140" placeholder="nessuna indicazione"></td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' width:210px; height:18px; overflow-y:hidden; overflow-x:hidden;'><input type="text" name="disabilita_6" size="36" maxlength="140" placeholder="nessuna indicazione"></td>
    <tr>
 
</table>


<br /><br />


<br />
<p align="right">
<input type="hidden" name="id_comitato" value="<?php echo $id_comitato; ?>" />
<input type="hidden" name="passwd" value="<?php echo $passwd; ?>" />
&nbsp&nbsp <input type="submit" value="Inserisci" />
</form>

<?php

} else {

echo "Password errata";

?>

<br />
<br />

<form method="post" action="index.php">
<input type='submit' value='Torna alla Home'></form>

<?php
}

?>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
