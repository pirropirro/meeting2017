<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Iscrizione osservatore senza squadra</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Iscrizione osservatore senza squadra</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Iscrizione osservatore senza squadra</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include ("config.inc.php");
echo "<font color=#2B3856 size='3' face='Tahoma'>";

$id_comitato=$_REQUEST['id_comitato'];
$passwd=$_REQUEST['passwd'];
//echo $id_comitato;
//echo $passwd;

include ("apri_db.php");

//Comitato di cui si inserisci la passwd
$query = "SELECT passwd, tipo_meeting FROM comitati WHERE id = '$id_comitato'";
$result = mysql_query($query, $db);
$row = mysql_fetch_array( $result );
mysql_close($db); 

if ($row[passwd] == $passwd  && !($id_comitato == ''))
{
	?>
	<form name="form1" enctype="multipart/form-data" method="post" action="salva_inserisci_iscrizione_osservatore_senza_squadra.php">

    <fieldset>
        <legend><b>Dati del volontario</b></legend>
        <br />
        Nome&nbsp&nbsp
        <input type="text" name="nome" size="20" maxlength="40" required >
        
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        
        Cognome&nbsp&nbsp
        <input type="text" name="cognome" size="20" maxlength="40" required><br />
        <br />
        
        Recapito telefonico&nbsp&nbsp
        <input type="tel" name="telefono" size="8" required title="prefisso e numero senza spazi o altri caratteri (max 10 cifre)" pattern="[0-9]{10}">
        
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        
        E-mail&nbsp&nbsp
        <input type="email" name="mail" size="28" required><br />
        <br />
    
        Esigenze alimentari&nbsp&nbsp
        <input type="text" name="esigenze" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario"><br />
        <br />

        Eventuali disabilità&nbsp&nbsp
        <input type="text" name="disabilita" size="60" maxlength="140" placeholder="nessuna indicazione" title="Compilare solo se necessario"><br />
        <br />
    
    <?php
	if ($row[tipo_meeting] == 'PR'  || $row[tipo_meeting] == 'R')
	{
		?>
        <fieldset>
			<legend><b>Disponibilità come simulatore</b></legend>
			<br />
			Qualificato <input type="radio" name="simulatore" value="2"/>
			&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
			Aspirante simulatore <input type="radio" name="simulatore" value="1"/>
			&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
			Non disponibile <input type="radio" name="simulatore" value="0" checked="checked"/><br />
		</fieldset><br />
		<br />

        <fieldset>
			<legend><b>Disponibilità come truccatore</b></legend>
			<br />
			Qualificato <input type="radio" name="truccatore" value="2"/>
			&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
			Non disponibile <input type="radio" name="truccatore" value="0" checked="checked"/><br />
		</fieldset><br />
		<br />
    <?php
    }
	?>
    <fieldset>
        <legend><b>Disponibilità come giudice</b></legend>
        <br />
        <input type="checkbox" name="giudice_1" value="1"/>Istruttore di Educazione alla Sessualità e Prevenzione dalle MST<br/><br/>
        <input type="checkbox" name="giudice_2" value="1"/>Istruttore di Educazione Alimentare<br/><br/>
        <input type="checkbox" name="giudice_3" value="1"/>Monitore PS<br/><br/>
        <input type="checkbox" name="giudice_4" value="1"/>Operatore Sociale Generico<br/><br/>
        <input type="checkbox" name="giudice_5" value="1"/>OPEM<br/><br/>
        <input type="checkbox" name="giudice_6" value="1"/>Operatore DRRCCA<br/><br/>
        <input type="checkbox" name="giudice_7" value="1"/>Istruttore DIU<br/><br/>
        <input type="checkbox" name="giudice_8" value="1"/>Operatore di Cooperazione Internazionale<br/><br/>
        <input type="checkbox" name="giudice_9" value="1"/>Operatore Giovani in Azione<br/><br/>
        <input type="checkbox" name="giudice_10" value="1"/>Operatore Attività per i Giovani<br/><br/>
        <input type="checkbox" name="giudice_11" value="1"/>Istruttore di EducAzione alla Pace<br/><br/>
        <input type="checkbox" name="giudice_12" value="1"/>Operatore Sviluppo<br/><br/>
    </fieldset><br />
    <br />

	<br />
	<p align="left">
	<input type="hidden" name="id_comitato" value="<?php echo $id_comitato; ?>" />
	<input type="hidden" name="passwd" value="<?php echo $passwd; ?>" />
	<input type="submit" value="Inserisci" />
	</form>

	<?php
}
else
{
	echo "Password errata";
	?>
	<br /><br />
	<form method="post" action="index.php">
	<input type='submit' value='Torna alla Home'></form>
	<?php
}
?>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
