<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Iscrizione squadra</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Iscrizione squadra</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Iscrizione squadra</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include ("config.inc.php");
echo "<font color=#2B3856 size='3' face='Tahoma'>";
$fp = fopen ("log_iscrizioni.txt",a);

//Leggo i dati in input
$id_comitato=$_REQUEST['id_comitato'];
$passwd=$_REQUEST['passwd'];
//echo $id_comitato;
//echo $passwd;

$nome_1=$_REQUEST['nome_1'];
$nome_1_ins = addslashes(stripslashes($nome_1)); 
$nome_1_ins = str_replace("<", "&lt;", $nome_1_ins);
$nome_1_ins = str_replace(">", "&gt;", $nome_1_ins);
$cognome_1=$_REQUEST['cognome_1'];
$cognome_1_ins = addslashes(stripslashes($cognome_1)); 
$cognome_1_ins = str_replace("<", "&lt;", $cognome_1_ins);
$cognome_1_ins = str_replace(">", "&gt;", $cognome_1_ins);
$telefono_1=$_REQUEST['telefono_1'];
$mail_1=strtolower($_REQUEST['mail_1']);
$esigenze_1=$_REQUEST['esigenze_1'];
$esigenze_1_ins = addslashes(stripslashes($esigenze_1)); 
$esigenze_1_ins = str_replace("<", "&lt;", $esigenze_1_ins);
$esigenze_1_ins = str_replace(">", "&gt;", $esigenze_1_ins);
$disabilita_1=$_REQUEST['disabilita_1'];
$disabilita_1_ins = addslashes(stripslashes($disabilita_1)); 
$disabilita_1_ins = str_replace("<", "&lt;", $disabilita_1_ins);
$disabilita_1_ins = str_replace(">", "&gt;", $disabilita_1_ins);

$nome_2=$_REQUEST['nome_2'];
$nome_2_ins = addslashes(stripslashes($nome_2)); 
$nome_2_ins = str_replace("<", "&lt;", $nome_2_ins);
$nome_2_ins = str_replace(">", "&gt;", $nome_2_ins);
$cognome_2=$_REQUEST['cognome_2'];
$cognome_2_ins = addslashes(stripslashes($cognome_2)); 
$cognome_2_ins = str_replace("<", "&lt;", $cognome_2_ins);
$cognome_2_ins = str_replace(">", "&gt;", $cognome_2_ins);
$telefono_2=$_REQUEST['telefono_2'];
$mail_2=strtolower($_REQUEST['mail_2']);
$esigenze_2=$_REQUEST['esigenze_2'];
$esigenze_2_ins = addslashes(stripslashes($esigenze_2)); 
$esigenze_2_ins = str_replace("<", "&lt;", $esigenze_2_ins);
$esigenze_2_ins = str_replace(">", "&gt;", $esigenze_2_ins);
$disabilita_2=$_REQUEST['disabilita_2'];
$disabilita_2_ins = addslashes(stripslashes($disabilita_2)); 
$disabilita_2_ins = str_replace("<", "&lt;", $disabilita_2_ins);
$disabilita_2_ins = str_replace(">", "&gt;", $disabilita_2_ins);

$nome_3=$_REQUEST['nome_3'];
$nome_3_ins = addslashes(stripslashes($nome_3)); 
$nome_3_ins = str_replace("<", "&lt;", $nome_3_ins);
$nome_3_ins = str_replace(">", "&gt;", $nome_3_ins);
$cognome_3=$_REQUEST['cognome_3'];
$cognome_3_ins = addslashes(stripslashes($cognome_3)); 
$cognome_3_ins = str_replace("<", "&lt;", $cognome_3_ins);
$cognome_3_ins = str_replace(">", "&gt;", $cognome_3_ins);
$telefono_3=$_REQUEST['telefono_3'];
$mail_3=strtolower($_REQUEST['mail_3']);
$esigenze_3=$_REQUEST['esigenze_3'];
$esigenze_3_ins = addslashes(stripslashes($esigenze_3)); 
$esigenze_3_ins = str_replace("<", "&lt;", $esigenze_3_ins);
$esigenze_3_ins = str_replace(">", "&gt;", $esigenze_3_ins);
$disabilita_3=$_REQUEST['disabilita_3'];
$disabilita_3_ins = addslashes(stripslashes($disabilita_3)); 
$disabilita_3_ins = str_replace("<", "&lt;", $disabilita_3_ins);
$disabilita_3_ins = str_replace(">", "&gt;", $disabilita_3_ins);

$nome_4=$_REQUEST['nome_4'];
$nome_4_ins = addslashes(stripslashes($nome_4)); 
$nome_4_ins = str_replace("<", "&lt;", $nome_4_ins);
$nome_4_ins = str_replace(">", "&gt;", $nome_4_ins);
$cognome_4=$_REQUEST['cognome_4'];
$cognome_4_ins = addslashes(stripslashes($cognome_4)); 
$cognome_4_ins = str_replace("<", "&lt;", $cognome_4_ins);
$cognome_4_ins = str_replace(">", "&gt;", $cognome_4_ins);
$telefono_4=$_REQUEST['telefono_4'];
$mail_4=strtolower($_REQUEST['mail_4']);
$esigenze_4=$_REQUEST['esigenze_4'];
$esigenze_4_ins = addslashes(stripslashes($esigenze_4)); 
$esigenze_4_ins = str_replace("<", "&lt;", $esigenze_4_ins);
$esigenze_4_ins = str_replace(">", "&gt;", $esigenze_4_ins);
$disabilita_4=$_REQUEST['disabilita_4'];
$disabilita_4_ins = addslashes(stripslashes($disabilita_4)); 
$disabilita_4_ins = str_replace("<", "&lt;", $disabilita_4_ins);
$disabilita_4_ins = str_replace(">", "&gt;", $disabilita_4_ins);

$nome_5=$_REQUEST['nome_5'];
$nome_5_ins = addslashes(stripslashes($nome_5)); 
$nome_5_ins = str_replace("<", "&lt;", $nome_5_ins);
$nome_5_ins = str_replace(">", "&gt;", $nome_5_ins);
$cognome_5=$_REQUEST['cognome_5'];
$cognome_5_ins = addslashes(stripslashes($cognome_5)); 
$cognome_5_ins = str_replace("<", "&lt;", $cognome_5_ins);
$cognome_5_ins = str_replace(">", "&gt;", $cognome_5_ins);
$telefono_5=$_REQUEST['telefono_5'];
$mail_5=strtolower($_REQUEST['mail_5']);
$esigenze_5=$_REQUEST['esigenze_5'];
$esigenze_5_ins = addslashes(stripslashes($esigenze_5)); 
$esigenze_5_ins = str_replace("<", "&lt;", $esigenze_5_ins);
$esigenze_5_ins = str_replace(">", "&gt;", $esigenze_5_ins);
$disabilita_5=$_REQUEST['disabilita_5'];
$disabilita_5_ins = addslashes(stripslashes($disabilita_5)); 
$disabilita_5_ins = str_replace("<", "&lt;", $disabilita_5_ins);
$disabilita_5_ins = str_replace(">", "&gt;", $disabilita_5_ins);

$nome_6=$_REQUEST['nome_6'];
$nome_6_ins = addslashes(stripslashes($nome_6)); 
$nome_6_ins = str_replace("<", "&lt;", $nome_6_ins);
$nome_6_ins = str_replace(">", "&gt;", $nome_6_ins);
$cognome_6=$_REQUEST['cognome_6'];
$cognome_6_ins = addslashes(stripslashes($cognome_6)); 
$cognome_6_ins = str_replace("<", "&lt;", $cognome_6_ins);
$cognome_6_ins = str_replace(">", "&gt;", $cognome_6_ins);
$telefono_6=$_REQUEST['telefono_6'];
$mail_6=strtolower($_REQUEST['mail_6']);
$esigenze_6=$_REQUEST['esigenze_6'];
$esigenze_6_ins = addslashes(stripslashes($esigenze_6)); 
$esigenze_6_ins = str_replace("<", "&lt;", $esigenze_6_ins);
$esigenze_6_ins = str_replace(">", "&gt;", $esigenze_6_ins);
$disabilita_6=$_REQUEST['disabilita_6'];
$disabilita_6_ins = addslashes(stripslashes($disabilita_6)); 
$disabilita_6_ins = str_replace("<", "&lt;", $disabilita_6_ins);
$disabilita_6_ins = str_replace(">", "&gt;", $disabilita_6_ins);



if (($mail_1 == $mail_2) || ($mail_1 == $mail_3) || ($mail_1 == $mail_4) || ($mail_1 == $mail_5) || ($mail_1 == $mail_6)) :
	echo "Hai inserito due inidirizzi mail identici. Ogni membro della squadra deve avere un indirizzo mail valido e distinto!";
elseif (($mail_2 == $mail_3) || ($mail_2 == $mail_4) || ($mail_2 == $mail_5) || ($mail_2 == $mail_6)) :
    	echo "Hai inserito due inidirizzi mail identici. Ogni membro della squadra deve avere un indirizzo mail valido e distinto!";
    elseif (($mail_3 == $mail_4) || ($mail_3 == $mail_5) || ($mail_3 == $mail_6)) :
	    	echo "Hai inserito due inidirizzi mail identici. Ogni membro della squadra deve avere un indirizzo mail valido e distinto!";
	  elseif (($mail_4 == $mail_5) || ($mail_4 == $mail_6)) :
		      echo "Hai inserito due inidirizzi mail identici. Ogni membro della squadra deve avere un indirizzo mail valido e distinto!";
		elseif ($mail_5 == $mail_6) :
		    	echo "Hai inserito due inidirizzi mail identici. Ogni membro della squadra deve avere un indirizzo mail valido e distinto!";
			else:
				include ("apri_db.php");
				
				//Aggiorno la tabella preiscrizioni settando ad 1 il valore di 'iscrizione'
				$query = "UPDATE preiscrizioni SET iscrizione = '1' WHERE id_comitato = $id_comitato";
				if (mysql_query($query, $db))
				{
    				//Faccio le 5 insert dei membri della squadra (pericoloso perchè non le controllo)
					$query_inserimento = "INSERT INTO iscrizioni (id_comitato, ruolo, nome, cognome, telefono, mail, esigenze, disabilita) VALUES ('$id_comitato', 'capitano', '$nome_1_ins', '$cognome_1_ins', '$telefono_1', '$mail_1', '$esigenze_1_ins', '$disabilita_1_ins')";
    				mysql_query($query_inserimento, $db);					
					$query_inserimento = "INSERT INTO iscrizioni (id_comitato, ruolo, nome, cognome, telefono, mail, esigenze, disabilita) VALUES ('$id_comitato', 'partecipante', '$nome_2_ins', '$cognome_2_ins', '$telefono_2', '$mail_2', '$esigenze_2_ins', '$disabilita_2_ins')";
    				mysql_query($query_inserimento, $db);					
					$query_inserimento = "INSERT INTO iscrizioni (id_comitato, ruolo, nome, cognome, telefono, mail, esigenze, disabilita) VALUES ('$id_comitato', 'partecipante', '$nome_3_ins', '$cognome_3_ins', '$telefono_3', '$mail_3', '$esigenze_3_ins', '$disabilita_3_ins')";
    				mysql_query($query_inserimento, $db);					
					$query_inserimento = "INSERT INTO iscrizioni (id_comitato, ruolo, nome, cognome, telefono, mail, esigenze, disabilita) VALUES ('$id_comitato', 'partecipante', '$nome_4_ins', '$cognome_4_ins', '$telefono_4', '$mail_4', '$esigenze_4_ins', '$disabilita_4_ins')";
    				mysql_query($query_inserimento, $db);					
					$query_inserimento = "INSERT INTO iscrizioni (id_comitato, ruolo, nome, cognome, telefono, mail, esigenze, disabilita) VALUES ('$id_comitato', 'partecipante', '$nome_5_ins', '$cognome_5_ins', '$telefono_5', '$mail_5', '$esigenze_5_ins', '$disabilita_5_ins')";
    				mysql_query($query_inserimento, $db);					
					$query_inserimento = "INSERT INTO iscrizioni (id_comitato, ruolo, nome, cognome, telefono, mail, esigenze, disabilita) VALUES ('$id_comitato', 'adulto', '$nome_6_ins', '$cognome_6_ins', '$telefono_6', '$mail_6', '$esigenze_6_ins', '$disabilita_6_ins')";
    				mysql_query($query_inserimento, $db);					
					echo "L'inserimento della squadra è avvenuto correttamente. <br><br> In bocca al lupo! ;-) <br><br>";
				
					//Invio la mail di riepilogo dell'iscrizione appena avvenuta al delegato con in copia il presidente
					$query_estrazione = "SELECT     c.nome_comitato AS nome_comitato,
								                    c.nome_presidente AS nome_presidente,
                    								c.cognome_presidente AS cognome_presidente,
													c.mail_comitato AS mail_comitato,
                    								p.nome_delegato AS nome_volontario,
                    								p.cognome_delegato AS cognome_volontario,
													p.mail_delegato AS mail_volontario
                    								FROM preiscrizioni AS p
                    								INNER JOIN comitati AS c
                    								ON p.id_comitato = c.id
                    								WHERE p.id_comitato = $id_comitato";
					$result = mysql_query($query_estrazione, $db);
					$row = mysql_fetch_array($result);
					
					include('PHPMailer/class.phpmailer.php');
					//require_once('PHPMailer/class.smtp.php');  
	
					$mittente = "meeting.2016@piemonte.cri.it";
					$nomemittente = "Back Office Meeting 2016";
					$destinatario = "$row[mail_volontario]";
					$ServerSMTP = "mailbox.cri.it"; //server SMTP
					$oggetto = "Iscrizione Squadra Meeting 2016";
					$corpo_messaggio = "Ciao $row[nome_volontario],\nla presente per notificare l'avvenuta iscrizione della squadra del $row[nome_comitato], di seguito, il riepilogo dei dati:\n\n - $nome_1 $cognome_1 (capitano)\n - $nome_2 $cognome_2 (partecipante)\n - $nome_3 $cognome_3 (partecipante)\n - $nome_4 $cognome_4 (partecipante)\n - $nome_5 $cognome_5 (partecipante)\n - $nome_6 $cognome_6 (partecipante)\n\nIn copia alla presente abbiamo inserito il presidente del tuo comitato ($row[nome_presidente] $row[cognome_presidente]). Ti ricordiamo che non è possibile apportare modifiche alla squadra tramite il portale, qualora tu ne avessi comunque bisogno, ti preghiamo di motivarcele via mail.\n\nDa questo momento è possibile per te inserire gli eventuali osservatori utilizzando il portale dalla sezione Iscrizioni (http://www.itempa.com/meeting2016). Nella stessa sezione potrai consultare il riepilogo dei dati immessi relativi al tuo comitato concernenti sia la squadra che gli eventuali osservatori.\n\nPer qualsiasi dubbio e/o consiglio, non esitare a contattarci.\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
	
					$msg = new PHPMailer;
					$msg->CharSet = "UTF-8";
					$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
					//$msg->IsHTML(true);
					$msg->SMTPAuth = true; // Autenticazione SMTP
					$msg->SMTPKeepAlive = "true";
					$msg->Host = $ServerSMTP;
					$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
					$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
	
					$msg->From = $mittente;
					$msg->FromName = $nomemittente;
					$msg->AddAddress($destinatario); 
					$msg->AddCC("$row[mail_comitato]");
					$msg->AddBCC("paolo.ditoma@libero.it");
					$msg->AddBCC("backoffice.meeting@gmail.com");
					$msg->Subject = $oggetto; 
					$msg->Body = $corpo_messaggio;
	
	
					if(!$msg->Send())
					{
						echo "Si è verificato un errore nell'invio della mail di notifica iscrizione squadra:".$msg->ErrorInfo;
					}
					else
					{
						echo "Ti abbiamo inviato una mail di riepilogo dell'avvenuta iscrizione della squadra, inserendo in copia il tuo presidente.";
						fwrite($fp,date('d/m/Y H:i:s').' - '."Iscrizione della squadra del $row[nome_comitato] effettuata da $row[nome_volontario] $row[cognome_volontario]. Inviata mail di notifica all'indirizzo $row[mail_volontario] ed in copia al presidente $row[nome_presidente] $row[cognome_presidente] all'indirizzo mail $row[mail_comitato]."."\r\n");
					}

					//Invio le 6 mail per la conferma dell'indirizzo ad ognuno dei membri (usare MD5 per il codice di conferma)

					//Calcolo il Token, lo immetto nel DB ed invio la mail per la conferma
					$code = array();
					$query_id = "SELECT * FROM iscrizioni WHERE id_comitato = '$id_comitato' AND nome = '$nome_1_ins' AND cognome = '$cognome_1_ins' AND telefono = '$telefono_1' AND mail = '$mail_1'";
					$result_id = mysql_query($query_id, $db);
					$row_id = mysql_fetch_array($result_id);
					$md5 = strtoupper(md5($row_id[id] . $cognome_1 . $telefono_1 . $mail_1));
					$code[] = substr ($md5, 0, 5);
					$code[] = substr ($md5, 5, 5);
					$code[] = substr ($md5, 10, 5);
					$code[] = substr ($md5, 15, 5);
					$token = implode ("-", $code); 
					$query = "UPDATE iscrizioni SET token = '$token' WHERE id = '$row_id[id]'";
					mysql_query($query, $db);
					$mittente = "meeting.2016@piemonte.cri.it";
					$nomemittente = "Back Office Meeting 2016";
					$destinatario = "$mail_1";
					$ServerSMTP = "mailbox.cri.it"; //server SMTP
					$oggetto = "Iscrizione Squadra Meeting 2016 - Conferma indirizzo mail";
					$corpo_messaggio = "Ciao $nome_1,\nsei appena stato iscritto come capitano della squadra del $row[nome_comitato] per la partecipazione al Meeting 2016 dei Giovani della croce Rossa Italiana. Al fine di verificare la veridicità delle informazioni inserite ti chiediamo di confermare il tuo indirizzo mail cliccando sul link seguente\n\n  http://www.itempa.com/meeting2016/verifica_mail_iscrizione.php?token=$token\n\nOgni altra notifica riguardante il Meeting ti sarà recapitata all'indirizzo mail confermato.\n\nN.B.Qualora tu intenda utilizzare un altro indirizzo, ti preghiamo di indicarcelo rispondendo a questa mail senza procedere quindi alla conferma.\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
		
					$msg = new PHPMailer;
					$msg->CharSet = "UTF-8";
					$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
					//$msg->IsHTML(true);
					$msg->SMTPAuth = true; // Autenticazione SMTP
					$msg->SMTPKeepAlive = "true";
					$msg->Host = $ServerSMTP;
					$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
					$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
		
					$msg->From = $mittente;
					$msg->FromName = $nomemittente;
					$msg->AddAddress($destinatario); 
	//				$msg->AddCC("$row[mail_comitato]");
					$msg->AddBCC("paolo.ditoma@libero.it");
					$msg->Subject = $oggetto; 
					$msg->Body = $corpo_messaggio;
					
					if(!$msg->Send())
					{
						fwrite($fp,date('d/m/Y H:i:s').' - '."Si è verificato un errore nell'invio della mail di verifica a $nome_1 $cognome_1 all'indirizzo $mail_1 con Token $token".$msg->ErrorInfo."\r\n");
					}
					else
					{
						$query = "UPDATE iscrizioni SET mail_reminder ='1' WHERE token = '$token'";
						mysql_query($query, $db);
						fwrite($fp,date('d/m/Y H:i:s').' - '."Inviata mail a $nome_1 $cognome_1 per conferma mail $mail_1 con Token $token"."\r\n");
					}


					$code = array();
					$query_id = "SELECT * FROM iscrizioni WHERE id_comitato = '$id_comitato' AND nome = '$nome_2_ins' AND cognome = '$cognome_2_ins' AND telefono = '$telefono_2' AND mail = '$mail_2'";
					$result_id = mysql_query($query_id, $db);
					$row_id = mysql_fetch_array($result_id);
					$md5 = strtoupper(md5($row_id[id] . $cognome_2 . $telefono_2 . $mail_2));
					$code[] = substr ($md5, 0, 5);
					$code[] = substr ($md5, 5, 5);
					$code[] = substr ($md5, 10, 5);
					$code[] = substr ($md5, 15, 5);
					$token = implode ("-", $code); 
					$query = "UPDATE iscrizioni SET token = '$token' WHERE id = '$row_id[id]'";
					mysql_query($query, $db);
					$mittente = "meeting.2016@piemonte.cri.it";
					$nomemittente = "Back Office Meeting 2016";
					$destinatario = "$mail_2";
					$ServerSMTP = "mailbox.cri.it"; //server SMTP
					$oggetto = "Iscrizione Squadra Meeting 2016 - Conferma indirizzo mail";
					$corpo_messaggio = "Ciao $nome_2,\nsei appena stato iscritto come partecipante della squadra del $row[nome_comitato] per la partecipazione al Meeting 2016 dei Giovani della croce Rossa Italiana. Al fine di verificare la veridicità delle informazioni inserite ti chiediamo di confermare il tuo indirizzo mail cliccando sul link seguente\n\n  http://www.itempa.com/meeting2016/verifica_mail_iscrizione.php?token=$token\n\nOgni altra notifica riguardante il Meeting ti sarà recapitata all'indirizzo mail confermato.\n\nN.B.Qualora tu intenda utilizzare un altro indirizzo, ti preghiamo di indicarcelo rispondendo a questa mail senza procedere quindi alla conferma.\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
		
					$msg = new PHPMailer;
					$msg->CharSet = "UTF-8";
					$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
					//$msg->IsHTML(true);
					$msg->SMTPAuth = true; // Autenticazione SMTP
					$msg->SMTPKeepAlive = "true";
					$msg->Host = $ServerSMTP;
					$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
					$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
		
					$msg->From = $mittente;
					$msg->FromName = $nomemittente;
					$msg->AddAddress($destinatario); 
	//				$msg->AddCC("$row[mail_comitato]");
					$msg->AddBCC("paolo.ditoma@libero.it");
					$msg->Subject = $oggetto; 
					$msg->Body = $corpo_messaggio;
					
					if(!$msg->Send())
					{
						fwrite($fp,date('d/m/Y H:i:s').' - '."Si è verificato un errore nell'invio della mail di verifica a $nome_2 $cognome_2 all'indirizzo $mail_2 con Token $token".$msg->ErrorInfo."\r\n");
					}
					else
					{
						$query = "UPDATE iscrizioni SET mail_reminder ='1' WHERE token = '$token'";
						mysql_query($query, $db);
						fwrite($fp,date('d/m/Y H:i:s').' - '."Inviata mail a $nome_2 $cognome_2 per conferma mail $mail_2 con Token $token"."\r\n");
					}


					$code = array();
					$query_id = "SELECT * FROM iscrizioni WHERE id_comitato = '$id_comitato' AND nome = '$nome_3_ins' AND cognome = '$cognome_3_ins' AND telefono = '$telefono_3' AND mail = '$mail_3'";
					$result_id = mysql_query($query_id, $db);
					$row_id = mysql_fetch_array($result_id);
					$md5 = strtoupper(md5($row_id[id] . $cognome_3 . $telefono_3 . $mail_3));
					$code[] = substr ($md5, 0, 5);
					$code[] = substr ($md5, 5, 5);
					$code[] = substr ($md5, 10, 5);
					$code[] = substr ($md5, 15, 5);
					$token = implode ("-", $code); 
					$query = "UPDATE iscrizioni SET token = '$token' WHERE id = '$row_id[id]'";
					mysql_query($query, $db);
					$mittente = "meeting.2016@piemonte.cri.it";
					$nomemittente = "Back Office Meeting 2016";
					$destinatario = "$mail_3";
					$ServerSMTP = "mailbox.cri.it"; //server SMTP
					$oggetto = "Iscrizione Squadra Meeting 2016 - Conferma indirizzo mail";
					$corpo_messaggio = "Ciao $nome_3,\nsei appena stato iscritto come partecipante della squadra del $row[nome_comitato] per la partecipazione al Meeting 2016 dei Giovani della croce Rossa Italiana. Al fine di verificare la veridicità delle informazioni inserite ti chiediamo di confermare il tuo indirizzo mail cliccando sul link seguente\n\n  http://www.itempa.com/meeting2016/verifica_mail_iscrizione.php?token=$token\n\nOgni altra notifica riguardante il Meeting ti sarà recapitata all'indirizzo mail confermato.\n\nN.B.Qualora tu intenda utilizzare un altro indirizzo, ti preghiamo di indicarcelo rispondendo a questa mail senza procedere quindi alla conferma.\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
		
					$msg = new PHPMailer;
					$msg->CharSet = "UTF-8";
					$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
					//$msg->IsHTML(true);
					$msg->SMTPAuth = true; // Autenticazione SMTP
					$msg->SMTPKeepAlive = "true";
					$msg->Host = $ServerSMTP;
					$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
					$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
		
					$msg->From = $mittente;
					$msg->FromName = $nomemittente;
					$msg->AddAddress($destinatario); 
	//				$msg->AddCC("$row[mail_comitato]");
					$msg->AddBCC("paolo.ditoma@libero.it");
					$msg->Subject = $oggetto; 
					$msg->Body = $corpo_messaggio;
					
					if(!$msg->Send())
					{
						fwrite($fp,date('d/m/Y H:i:s').' - '."Si è verificato un errore nell'invio della mail di verifica a $nome_3 $cognome_3 all'indirizzo $mail_3 con Token $token".$msg->ErrorInfo."\r\n");
					}
					else
					{
						$query = "UPDATE iscrizioni SET mail_reminder ='1' WHERE token = '$token'";
						mysql_query($query, $db);
						fwrite($fp,date('d/m/Y H:i:s').' - '."Inviata mail a $nome_3 $cognome_3 per conferma mail $mail_3 con Token $token"."\r\n");
					}


					$code = array();
					$query_id = "SELECT * FROM iscrizioni WHERE id_comitato = '$id_comitato' AND nome = '$nome_4_ins' AND cognome = '$cognome_4_ins' AND telefono = '$telefono_4' AND mail = '$mail_4'";
					$result_id = mysql_query($query_id, $db);
					$row_id = mysql_fetch_array($result_id);
					$md5 = strtoupper(md5($row_id[id] . $cognome_4 . $telefono_4 . $mail_4));
					$code[] = substr ($md5, 0, 5);
					$code[] = substr ($md5, 5, 5);
					$code[] = substr ($md5, 10, 5);
					$code[] = substr ($md5, 15, 5);
					$token = implode ("-", $code); 
					$query = "UPDATE iscrizioni SET token = '$token' WHERE id = '$row_id[id]'";
					mysql_query($query, $db);
					$mittente = "meeting.2016@piemonte.cri.it";
					$nomemittente = "Back Office Meeting 2016";
					$destinatario = "$mail_4";
					$ServerSMTP = "mailbox.cri.it"; //server SMTP
					$oggetto = "Iscrizione Squadra Meeting 2016 - Conferma indirizzo mail";
					$corpo_messaggio = "Ciao $nome_4,\nsei appena stato iscritto come partecipante della squadra del $row[nome_comitato] per la partecipazione al Meeting 2016 dei Giovani della croce Rossa Italiana. Al fine di verificare la veridicità delle informazioni inserite ti chiediamo di confermare il tuo indirizzo mail cliccando sul link seguente\n\n  http://www.itempa.com/meeting2016/verifica_mail_iscrizione.php?token=$token\n\nOgni altra notifica riguardante il Meeting ti sarà recapitata all'indirizzo mail confermato.\n\nN.B.Qualora tu intenda utilizzare un altro indirizzo, ti preghiamo di indicarcelo rispondendo a questa mail senza procedere quindi alla conferma.\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
		
					$msg = new PHPMailer;
					$msg->CharSet = "UTF-8";
					$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
					//$msg->IsHTML(true);
					$msg->SMTPAuth = true; // Autenticazione SMTP
					$msg->SMTPKeepAlive = "true";
					$msg->Host = $ServerSMTP;
					$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
					$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
		
					$msg->From = $mittente;
					$msg->FromName = $nomemittente;
					$msg->AddAddress($destinatario); 
	//				$msg->AddCC("$row[mail_comitato]");
					$msg->AddBCC("paolo.ditoma@libero.it");
					$msg->Subject = $oggetto; 
					$msg->Body = $corpo_messaggio;
					
					if(!$msg->Send())
					{
						fwrite($fp,date('d/m/Y H:i:s').' - '."Si è verificato un errore nell'invio della mail di verifica a $nome_4 $cognome_4 all'indirizzo $mail_4 con Token $token".$msg->ErrorInfo."\r\n");
					}
					else
					{
						$query = "UPDATE iscrizioni SET mail_reminder ='1' WHERE token = '$token'";
						mysql_query($query, $db);
						fwrite($fp,date('d/m/Y H:i:s').' - '."Inviata mail a $nome_4 $cognome_4 per conferma mail $mail_4 con Token $token"."\r\n");
					}


					$code = array();
					$query_id = "SELECT * FROM iscrizioni WHERE id_comitato = '$id_comitato' AND nome = '$nome_5_ins' AND cognome = '$cognome_5_ins' AND telefono = '$telefono_5' AND mail = '$mail_5'";
					$result_id = mysql_query($query_id, $db);
					$row_id = mysql_fetch_array($result_id);
					$md5 = strtoupper(md5($row_id[id] . $cognome_5 . $telefono_5 . $mail_5));
					$code[] = substr ($md5, 0, 5);
					$code[] = substr ($md5, 5, 5);
					$code[] = substr ($md5, 10, 5);
					$code[] = substr ($md5, 15, 5);
					$token = implode ("-", $code); 
					$query = "UPDATE iscrizioni SET token = '$token' WHERE id = '$row_id[id]'";
					mysql_query($query, $db);
					$mittente = "meeting.2016@piemonte.cri.it";
					$nomemittente = "Back Office Meeting 2016";
					$destinatario = "$mail_5";
					$ServerSMTP = "mailbox.cri.it"; //server SMTP
					$oggetto = "Iscrizione Squadra Meeting 2016 - Conferma indirizzo mail";
					$corpo_messaggio = "Ciao $nome_5,\nsei appena stato iscritto come partecipante della squadra del $row[nome_comitato] per la partecipazione al Meeting 2016 dei Giovani della croce Rossa Italiana. Al fine di verificare la veridicità delle informazioni inserite ti chiediamo di confermare il tuo indirizzo mail cliccando sul link seguente\n\n  http://www.itempa.com/meeting2016/verifica_mail_iscrizione.php?token=$token\n\nOgni altra notifica riguardante il Meeting ti sarà recapitata all'indirizzo mail confermato.\n\nN.B.Qualora tu intenda utilizzare un altro indirizzo, ti preghiamo di indicarcelo rispondendo a questa mail senza procedere quindi alla conferma.\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
		
					$msg = new PHPMailer;
					$msg->CharSet = "UTF-8";
					$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
					//$msg->IsHTML(true);
					$msg->SMTPAuth = true; // Autenticazione SMTP
					$msg->SMTPKeepAlive = "true";
					$msg->Host = $ServerSMTP;
					$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
					$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
		
					$msg->From = $mittente;
					$msg->FromName = $nomemittente;
					$msg->AddAddress($destinatario); 
	//				$msg->AddCC("$row[mail_comitato]");
					$msg->AddBCC("paolo.ditoma@libero.it");
					$msg->Subject = $oggetto; 
					$msg->Body = $corpo_messaggio;
					
					if(!$msg->Send())
					{
						fwrite($fp,date('d/m/Y H:i:s').' - '."Si è verificato un errore nell'invio della mail di verifica a $nome_5 $cognome_5 all'indirizzo $mail_5 con Token $token".$msg->ErrorInfo."\r\n");
					}
					else
					{
						$query = "UPDATE iscrizioni SET mail_reminder ='1' WHERE token = '$token'";
						mysql_query($query, $db);
						fwrite($fp,date('d/m/Y H:i:s').' - '."Inviata mail a $nome_5 $cognome_5 per conferma mail $mail_5 con Token $token"."\r\n");
					}

					$code = array();
					$query_id = "SELECT * FROM iscrizioni WHERE id_comitato = '$id_comitato' AND nome = '$nome_6_ins' AND cognome = '$cognome_6_ins' AND telefono = '$telefono_6' AND mail = '$mail_6'";
					$result_id = mysql_query($query_id, $db);
					$row_id = mysql_fetch_array($result_id);
					$md5 = strtoupper(md5($row_id[id] . $cognome_6 . $telefono_6 . $mail_6));
					$code[] = substr ($md5, 0, 5);
					$code[] = substr ($md5, 5, 5);
					$code[] = substr ($md5, 10, 5);
					$code[] = substr ($md5, 15, 5);
					$token = implode ("-", $code); 
					$query = "UPDATE iscrizioni SET token = '$token' WHERE id = '$row_id[id]'";
					mysql_query($query, $db);
					$mittente = "meeting.2016@piemonte.cri.it";
					$nomemittente = "Back Office Meeting 2016";
					$destinatario = "$mail_6";
					$ServerSMTP = "mailbox.cri.it"; //server SMTP
					$oggetto = "Iscrizione Squadra Meeting 2016 - Conferma indirizzo mail";
					$corpo_messaggio = "Ciao $nome_6,\nsei appena stato iscritto come partecipante della squadra del $row[nome_comitato] per la partecipazione al Meeting 2016 dei Giovani della croce Rossa Italiana. Al fine di verificare la veridicità delle informazioni inserite ti chiediamo di confermare il tuo indirizzo mail cliccando sul link seguente\n\n  http://www.itempa.com/meeting2016/verifica_mail_iscrizione.php?token=$token\n\nOgni altra notifica riguardante il Meeting ti sarà recapitata all'indirizzo mail confermato.\n\nN.B.Qualora tu intenda utilizzare un altro indirizzo, ti preghiamo di indicarcelo rispondendo a questa mail senza procedere quindi alla conferma.\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
		
					$msg = new PHPMailer;
					$msg->CharSet = "UTF-8";
					$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
					//$msg->IsHTML(true);
					$msg->SMTPAuth = true; // Autenticazione SMTP
					$msg->SMTPKeepAlive = "true";
					$msg->Host = $ServerSMTP;
					$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
					$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
		
					$msg->From = $mittente;
					$msg->FromName = $nomemittente;
					$msg->AddAddress($destinatario); 
	//				$msg->AddCC("$row[mail_comitato]");
					$msg->AddBCC("paolo.ditoma@libero.it");
					$msg->Subject = $oggetto; 
					$msg->Body = $corpo_messaggio;
					
					if(!$msg->Send())
					{
						fwrite($fp,date('d/m/Y H:i:s').' - '."Si è verificato un errore nell'invio della mail di verifica a $nome_6 $cognome_6 all'indirizzo $mail_6 con Token $token".$msg->ErrorInfo."\r\n");
					}
					else
					{
						$query = "UPDATE iscrizioni SET mail_reminder ='1' WHERE token = '$token'";
						mysql_query($query, $db);
						fwrite($fp,date('d/m/Y H:i:s').' - '."Inviata mail a $nome_6 $cognome_6 per conferma mail $mail_6 con Token $token"."\r\n");
					}
				}
				else
				{
					echo "Errore grave - Rivolgersi all'amministratore - Si è verificato un errore nell'iscrizione della squadra:".$msg->ErrorInfo;
					fwrite($fp,date('d/m/Y H:i:s').' - '."Errore grave sulla query di update $query"."\r\n");			
				}
				mysql_close($db);
endif;
fclose($fp);

?>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
