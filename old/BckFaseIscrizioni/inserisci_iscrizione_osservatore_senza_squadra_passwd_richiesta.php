<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Iscrizione osservatore senza squadra</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Parent -->
	<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Iscrizione osservatore senza squadra</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Iscrizione osservatore senza squadra</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">


<?php
include ("config.inc.php");
echo "<font color=#2B3856 size='3' face='Tahoma'>";
include ("apri_db.php");
?>

<form method="post" action="inserisci_iscrizione_osservatore_senza_squadra_passwd_verificata.php">

<br />
<label>Squadra del<br>
 
<select name="id_comitato" size="8" >

<?php
$query = "SELECT 	c.nome_comitato AS nome_comitato,
					c.id AS id,
					c.nome_presidente AS nome_presidente,
					c.cognome_presidente AS cognome_presidente,
					c.mail_comitato AS mail_comitato,
					c.nome_consigliere AS nome_delegato,
					c.cognome_consigliere AS cognome_delegato,
					c.mail_consigliere AS mail_delegato
					FROM comitati AS c
					WHERE c.id NOT IN (
										SELECT p.id_comitato
										FROM preiscrizioni AS p
										WHERE p.id_comitato = c.id
										AND (p.iscrizione = '1' OR p.iscrizione IS NULL)
									  )
					AND c.tipo_meeting!='N'
					AND c.mail_consigliere!=''";

$result = mysql_query($query, $db);
while($row = mysql_fetch_array( $result )) 
{
	?>
	<option value='<?php echo $row[id]; ?>'><?php echo $row[nome_comitato]; ?></option>
	<?php
}
mysql_close($db);
?>
</select>
</label>

<br /><br /><br />

<b>Password</b> &nbsp&nbsp
&nbsp&nbsp
<input type="text" name="passwd" size="8" maxlength="15" title="Inserisci la password ricevuta via mail" required><br />
<br /><br />

<input type="submit" value="Verifica" />
<br />
<br />
</form>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
