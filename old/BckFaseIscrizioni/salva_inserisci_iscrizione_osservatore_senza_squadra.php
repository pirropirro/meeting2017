<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Iscrizione osservatore senza squadra</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Iscrizione osservatore senza squadra</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Iscrizione osservatore senza squadra</h2>
<p id="imPathTitle">Iscrizioni</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?php
include ("config.inc.php");
echo "<font color=#2B3856 size='3' face='Tahoma'>";
$fp = fopen ("log_iscrizioni.txt",a);

//Leggo i dati in input
$id_comitato=$_REQUEST['id_comitato'];
$passwd=$_REQUEST['passwd'];

$nome=$_REQUEST['nome'];
$nome_ins = addslashes(stripslashes($nome)); 
$nome_ins = str_replace("<", "&lt;", $nome_ins);
$nome_ins = str_replace(">", "&gt;", $nome_ins);
$cognome=$_REQUEST['cognome'];
$cognome_ins = addslashes(stripslashes($cognome)); 
$cognome_ins = str_replace("<", "&lt;", $cognome_ins);
$cognome_ins = str_replace(">", "&gt;", $cognome_ins);
$telefono=$_REQUEST['telefono'];
$mail=strtolower($_REQUEST['mail']);
$esigenze=$_REQUEST['esigenze'];
$esigenze_ins = addslashes(stripslashes($esigenze)); 
$esigenze_ins = str_replace("<", "&lt;", $esigenze_ins);
$esigenze_ins = str_replace(">", "&gt;", $esigenze_ins);
$disabilita=$_REQUEST['disabilita'];
$disabilita_ins = addslashes(stripslashes($disabilita)); 
$disabilita_ins = str_replace("<", "&lt;", $disabilita_ins);
$disabilita_ins = str_replace(">", "&gt;", $disabilita_ins);
$simulatore=$_REQUEST['simulatore'];
$truccatore=$_REQUEST['truccatore'];
//echo "Simulatore =".$simulatore."\n";
//echo "Truccatore =".$truccatore."\n";
$giudice_1=$_REQUEST['giudice_1'];
$giudice_2=$_REQUEST['giudice_2'];
$giudice_3=$_REQUEST['giudice_3'];
$giudice_4=$_REQUEST['giudice_4'];
$giudice_5=$_REQUEST['giudice_5'];
$giudice_6=$_REQUEST['giudice_6'];
$giudice_7=$_REQUEST['giudice_7'];
$giudice_8=$_REQUEST['giudice_8'];
$giudice_9=$_REQUEST['giudice_9'];
$giudice_10=$_REQUEST['giudice_10'];
$giudice_11=$_REQUEST['giudice_11'];
$giudice_12=$_REQUEST['giudice_12'];

include ("apri_db.php");

//Estraggo i dati del comitato
$query_comitato = "SELECT		c.nome_comitato AS nome_comitato,
								c.tipo_meeting AS tipo_meeting,
								c.nome_presidente AS nome_presidente,
                    			c.cognome_presidente AS cognome_presidente,
								c.mail_comitato AS mail_comitato,
                    			c.nome_consigliere AS nome_consigliere,
                    			c.cognome_consigliere AS cognome_consigliere,
								c.mail_consigliere AS mail_volontario
                    			FROM comitati AS c
                    			WHERE c.id = $id_comitato";
$result_comitato = mysql_query($query_comitato, $db);
$row_comitato = mysql_fetch_array($result_comitato);

//Controllo che non ci sia già una mail registrata uguale
$query_estrazione = "SELECT id FROM iscrizioni WHERE mail = '$mail'";
$result = mysql_query($query_estrazione, $db);

if (mysql_num_rows($result) == '1')
{
	echo "Hai inserito un indirizzo mail già presente nelle iscrizioni. Ogni volontario iscritto al Meeting deve avere un indirizzo mail valido e distinto!<br><br>";
}
else
{
	//Faccio la insert con i dati dell'osservatore facendo differenza se è PR,R o N
	if ($row_comitato[tipo_meeting] == 'PR' || $row_comitato[tipo_meeting] == 'R')
	{
		$query_inserimento = "INSERT INTO iscrizioni (id_comitato, ruolo, nome, cognome, telefono, mail, esigenze, disabilita, simulatore, truccatore, giudice_1, giudice_2, giudice_3, giudice_4, giudice_5, giudice_6, giudice_7, giudice_8, giudice_9, giudice_10, giudice_11, giudice_12) VALUES ('$id_comitato', 'osservatore', '$nome_ins', '$cognome_ins', '$telefono', '$mail', '$esigenze_ins', '$disabilita_ins', '$simulatore', '$truccatore', '$giudice_1', '$giudice_2', '$giudice_3', '$giudice_4', '$giudice_5', '$giudice_6', '$giudice_7', '$giudice_8', '$giudice_9', '$giudice_10', '$giudice_11', '$giudice_12')";
	}
	else
	{
		$query_inserimento = "INSERT INTO iscrizioni (id_comitato, ruolo, nome, cognome, telefono, mail, esigenze, disabilita, giudice_1, giudice_2, giudice_3, giudice_4, giudice_5, giudice_6, giudice_7, giudice_8, giudice_9, giudice_10, giudice_11, giudice_12) VALUES ('$id_comitato', 'osservatore', '$nome_ins', '$cognome_ins', '$telefono', '$mail', '$esigenze_ins', '$disabilita_ins', '$giudice_1', '$giudice_2', '$giudice_3', '$giudice_4', '$giudice_5', '$giudice_6', '$giudice_7', '$giudice_8', '$giudice_9', '$giudice_10', '$giudice_11', '$giudice_12')";
	}
	
	$result_inserimento =  mysql_query($query_inserimento, $db);
	//Controllo che sia andata a buon fine la insert
	
	if ($result_inserimento)
	{
    	fwrite($fp,date('d/m/Y H:i:s').' - '."Iscrizione osservatore $nome $cognome per il $row_comitato[nome_comitato] effettuata da $row_comitato[nome_consigliere] $row_comitato[cognome_consigliere] correttamente inserita nel DB."."\r\n");
		echo "L'inserimento dell'osservatore $nome $cognome afferente al $row_comitato[nome_comitato] è avvenuto corretamente.<br><br>";
		//Invio la mail di conferma al delagato che ha inserito l'osservatore
		include('PHPMailer/class.phpmailer.php');
		//require_once('PHPMailer/class.smtp.php');  
	
		$mittente = "meeting.2016@piemonte.cri.it";
		$nomemittente = "Back Office Meeting 2016";
		$destinatario = "$row_comitato[mail_volontario]";
		$ServerSMTP = "mailbox.cri.it"; //server SMTP
		$oggetto = "Iscrizione Osservatore Meeting 2016";
		$corpo_messaggio = "Ciao $row_comitato[nome_consigliere],\nla presente per notificare l'avvenuta iscrizione dell'osservatore $nome $cognome afferente al $row_comitato[nome_comitato].\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
	
		$msg = new PHPMailer;
		$msg->CharSet = "UTF-8";
		$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
		//$msg->IsHTML(true);
		$msg->SMTPAuth = true; // Autenticazione SMTP
		$msg->SMTPKeepAlive = "true";
		$msg->Host = $ServerSMTP;
		$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
		$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato
	
		$msg->From = $mittente;
		$msg->FromName = $nomemittente;
		$msg->AddAddress($destinatario); 
		$msg->AddCC("$row_comitato[mail_comitato]");
		$msg->AddBCC("paolo.ditoma@libero.it");
		$msg->AddBCC("backoffice.meeting@gmail.com");
		$msg->Subject = $oggetto; 
		$msg->Body = $corpo_messaggio;
	
	
		if(!$msg->Send())
		{
			echo "Si è verificato un errore nell'invio della mail di notifica iscrizione osservatore:".$msg->ErrorInfo;
			fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE Invio mail notifica iscrizione osservatore $nome $cognome effettuata da $row_comitato[nome_consigliere] $row_comitato[cognome_consigliere] con indirizzo $row_comitato[mail_volontario]: ".$msg->ErrorInfo."\r\n");
		}
		else
		{
			echo "Ti abbiamo inviato una mail di notifica.";
			fwrite($fp,date('d/m/Y H:i:s').' - '."Invio mail notifica avvenuta iscrizione osservatore $nome $cognome effettuata da $row_comitato[nome_consigliere] $row_comitato[cognome_consigliere] correttamente effettuato all'indirizzo $row_comitato[mail_volontario]."."\r\n");
		}
		
		//Calcolo il Token, lo immetto nel DB ed invio la mail per la conferma
		$code = array();
		$query_id = "SELECT * FROM iscrizioni WHERE id_comitato = '$id_comitato' AND nome = '$nome_ins' AND cognome = '$cognome_ins' AND telefono = '$telefono' AND mail = '$mail'";
		$result_id = mysql_query($query_id, $db);
		$row_id = mysql_fetch_array($result_id);
		$md5 = strtoupper(md5($row_id[id] . $cognome . $telefono . $mail));
		$code[] = substr ($md5, 0, 5);
		$code[] = substr ($md5, 5, 5);
		$code[] = substr ($md5, 10, 5);
		$code[] = substr ($md5, 15, 5);
		$token = implode ("-", $code); 
		$query = "UPDATE iscrizioni SET token = '$token' WHERE id = '$row_id[id]'";
		mysql_query($query, $db);

		$mittente = "meeting.2016@piemonte.cri.it";
		$nomemittente = "Back Office Meeting 2016";
		$destinatario = "$mail";
		$ServerSMTP = "mailbox.cri.it"; //server SMTP
		$oggetto = "Iscrizione Osservatore Meeting 2016 - Conferma indirizzo mail";
		$corpo_messaggio = "Ciao $nome,\nsei appena stato iscritto come osservatore del $row_comitato[nome_comitato] per la partecipazione al Meeting 2016 dei Giovani della Croce Rossa Italiana. Al fine di verificare la veridicità delle informazioni inserite ti chiediamo di confermare il tuo indirizzo mail cliccando sul link seguente\n\n  http://www.itempa.com/meeting2016/verifica_mail_iscrizione.php?token=$token\n\nOgni altra notifica riguardante il Meeting ti sarà recapitata all'indirizzo mail confermato.\n\nN.B.Qualora tu intenda utilizzare un altro indirizzo, ti preghiamo di indicarcelo rispondendo a questa mail senza procedere quindi alla conferma.\n\n\nCordiali Saluti,\nBack Office\nMeeting 2016\n\n(inviata automaticamente dal Portale Web)";
	
		$msg = new PHPMailer;
		$msg->CharSet = "UTF-8";
		$msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
		//$msg->IsHTML(true);
		$msg->SMTPAuth = true; // Autenticazione SMTP
		$msg->SMTPKeepAlive = "true";
		$msg->Host = $ServerSMTP;
		$msg->Username = "meeting.2016@piemonte.cri.it"; // Nome utente SMTP autenticato
		$msg->Password = "XSW\"34rfv"; // Password account email con SMTP autenticato

		$msg->From = $mittente;
		$msg->FromName = $nomemittente;
		$msg->AddAddress($destinatario); 
//		$msg->AddCC("$row[mail_comitato]");
		$msg->AddBCC("paolo.ditoma@libero.it");
		$msg->Subject = $oggetto; 
		$msg->Body = $corpo_messaggio;
		
		if(!$msg->Send())
		{
			fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE invio della mail di verifica indirizzo mail a $nome $cognome all'indirizzo $mail con Token $token".$msg->ErrorInfo."\r\n");
		}
		else
		{
			$query = "UPDATE iscrizioni SET mail_reminder ='1' WHERE token = '$token'";
			mysql_query($query, $db);
			fwrite($fp,date('d/m/Y H:i:s').' - '."Inviata mail di verifica indirizzo mail a $nome $cognome per conferma mail $mail con Token $token"."\r\n");
		}
	}
	else
	{
		echo "Si è verificato un errore nell'inserimento dell'iscrizione, contattare l'amministratore del sistema.<br><br>";
		fwrite($fp,date('d/m/Y H:i:s').' - '."ERRORE query insert ( $query_inserimento ) dell'osservatore $nome $cognome per il $row_comitato[nome_comitato] : ".$result_inserimento."\r\n");
	}
}

mysql_close($db);
fclose($fp);

?>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
