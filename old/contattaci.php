<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Contattaci</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2016 - Comitato Regionale del Piemonte" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Parent -->
	<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<script type="text/javascript">
        function unlock(el1, el2) {
            if(el1.checked) {
                document.getElementById(el2).disabled = false;
            } else {
                document.getElementById(el2).disabled = 'disabled';
            }
        }
    </script>

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>
<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Contattaci</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">

<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Contattaci</h2>
<p id="imPathTitle">Contatti</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<?
include ("config.inc.php");
echo "<font color=#2B3856 size='3' face='Tahoma'>";
?>

<form method="post" action="salva_contattaci.php">



<b>Indicaci i tuoi dati</b> &nbsp&nbsp
<br /><br />
Nome&nbsp&nbsp
<textarea style="overflow:hidden" cols="30" rows="1" name="nome"></textarea><br />
<br />

E-mail&nbsp&nbsp
<textarea style="overflow:hidden" cols="30" rows="1" name="mail"></textarea><br />
<br />

<br />
Oggetto&nbsp&nbsp
 
<select name="oggetto" >
   <option value=''></option>
   <option value='Preiscrizioni'>Preiscrizioni</option>
   <option value='Iscrizioni'>Iscrizioni</option>
   <option value='Orari'>Orari</option>
   <option value='Costi'>Costi</option>
   <option value='Location'>Location</option>
   <option value='Vitto'>Vitto</option>
   <option value='Altro...'>Altro...</option>
</select>


<br /><br /><br />


Richiesta<br />
<textarea style="overflow:hidden" cols="40" rows="4" name="richiesta"></textarea><br />
<br />

<input type="submit" name="submit" value="Invia" />
<br />
<br />
</form>

</div>
</div>
</div>

<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
