<?php
include_once ("auth.php");
include_once ("authconfig.php");
include_once ("check.php");

// Controllo l'autorizzazione a segreteria o tecnico
if (!($check['team'] == 'backoffice') && !($check['team'] == 'organigramma') && !($check['team'] == 'nazionale'))
{
	print "<font face=\"Arial\" size=\"5\" color=\"#FF0000\">";
	print "<b>Accesso non consentito</b>";
	print "</font><br>";
	print "<font face=\"Verdana\" size=\"2\" color=\"#000000\">";
	print "<b>Tu non hai i permessi per accedere a questa sezione, è un compito riservato all'organizzazione od al Back Office.</b></font>";
	exit;	// Stop script execution
}
?>
<!--IE 7 quirks mode please-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" dir="ltr">
<head>
	<title>Riepilogo materiale</title>

	<!-- Contents -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Language" content="it" />
	<meta http-equiv="last-modified" content="07/01/2009 11.02.56" />
	<meta http-equiv="Content-Type-Script" content="text/javascript" />
	<meta name="description" content="Meeting 2015 - Comitato Provinciale di Torino" />
	<meta name="keywords" content="" />

	<!-- Others -->
	<meta name="Author" content="Paolo di Toma" />
	<meta http-equiv="ImageToolbar" content="False" />
	<meta name="MSSmartTagsPreventParsing" content="True" />
	<link rel="Shortcut Icon" href="res/favicon.ico" type="image/x-icon" />

	<!-- Parent -->
	<link rel="sitemap" href="imsitemap.html" title="Mappa generale del sito" />

	<!-- Res -->
	<script type="text/javascript" src="res/x5engine.js"></script>
	<link rel="stylesheet" type="text/css" href="res/styles.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/template.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="res/print.css" media="print" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="res/iebehavior.css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="res/p019.css" media="screen, print" />
	<link rel="stylesheet" type="text/css" href="res/handheld.css" media="handheld" />
	<link rel="alternate stylesheet" title="Alto contrasto - Accessibilita" type="text/css" href="res/accessibility.css" media="screen" />

	<!-- Robots -->
	<meta http-equiv="Expires" content="0" />
	<meta name="Resource-Type" content="document" />
	<meta name="Distribution" content="global" />
	<meta name="Robots" content="index, follow" />
	<meta name="Revisit-After" content="21 days" />
	<meta name="Rating" content="general" />
</head>

<script type="text/javascript" src="tabber.js"></script>
<link rel="stylesheet" href="example.css" TYPE="text/css" MEDIA="screen">
<link rel="stylesheet" href="example-print.css" TYPE="text/css" MEDIA="print">

<script type="text/javascript">

/* Optional: Temporarily hide the "tabber" class so it does not "flash"
   on the page as plain HTML. After tabber runs, the class is changed
   to "tabberlive" and it will appear. */

document.write('<style type="text/css">.tabber{display:none;}<\/style>');
</script>

<script type="text/javascript" src="scripts/wysiwyg.js"></script>
<script type="text/javascript" src="scripts/wysiwyg-settings.js"></script> 
<script type="text/javascript">
WYSIWYG.attach('textarea2', small); // full featured setup 
</script> 

<body>
<div id="imSite">
<div id="imHeader">
	
	<h1>Riepilogo materiale</h1>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Salta il menu di navigazione">Vai ai contenuti</a>
<a name="imGoToMenu"></a>
</div>
<div id="imBody">
	<div id="imMenuMain">
  
<!-- Menu Content START -->
<p class="imInvisible">Menu principale:</p>
<div id="imMnMn">

<?php 
include ("main_menu.php");
?>

</div>
<!-- Menu Content END -->

	</div>
<hr class="imInvisible" />
<a name="imGoToCont"></a>
	<div id="imContent">

<!-- Page Content START -->
<div id="imPageSub">
<br />
<h2>Riepilogo materiale</h2>
<p id="imPathTitle">Service</p>
<div id="imToolTip"></div>
<div id="imBody">
<div id="imContent">

<form method="post" action="inserisci_materiale.php">
<p align="center">
  <input type="submit" value="Nuova richiesta" />
</p>
</form>
<br />

<?php
include("config.inc.php");

echo "<font color=#2B3856 size='2' face='Tahoma'>";
echo "<table border='0' width=100%>";


$db = mysql_connect($db_host, $db_user, $db_password);
if ($db == FALSE)
die ("Errore nella connessione. Verificare i parametri nel file config.inc.php");
mysql_select_db($db_name, $db)
or die ("Errore nella selezione del database. Verificare i parametri nel file config.inc.php");



echo "<tr>

<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99><div style='width:40px; height:24px; '>DATA</th> 
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99><div style='width:60px; height:24px; '>RICHIEDENTE</th> 
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99><div style='width:250px; height:24px; '>DESCRIZIONE</th>
<th align=center style=border-style:outset style='font size:100%' style=color:#FFFF99><div style='width:80px; height:24px; '>AZIONI</th> 
</tr>";


//Richieste aperte
$query = "SELECT id, DATE_FORMAT(data_richiesta,'%d-%m-%Y') as data_richiesta, descrizione, richiedente FROM shopping WHERE tipo_chiusura IS NULL ORDER BY data_richiesta";
$result = mysql_query($query, $db);
while($row = mysql_fetch_array( $result )) 
{
	echo "<tr>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'>$row[data_richiesta]</td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'>$row[richiedente]</td>
	<td align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'>$row[descrizione]</td>
	<td align=center style=color:#FFFF99 bgcolor=#ffffff>";
	//
	echo "<a href=visualizza_materiale.php?id=$row[id]><img src=images/visualizza.jpg width='20' height='20' align='middle' border='' title='Dettagli richiesta materiale'></a>&nbsp&nbsp";
	echo "<a href=modifica_materiale.php?id=$row[id]><img src=images/modifica.jpg width='20' height='20' align='middle' border='' title='Modifica richiesta materiale'></a>&nbsp&nbsp";
	echo "<a href=elimina_materiale.php?id=$row[id]><img src=images/elimina.jpg width='20' height='20' align='middle' border='' title='Elimina richiesta materiale'></a>&nbsp&nbsp";
	echo "<a href=chiudi_materiale.php?id=$row[id]><img src=images/chiudi.jpg width='20' height='20' align='middle' border='' title='Chiudi richiesta materiale'></a>&nbsp&nbsp";
}

//Richieste gi� chiuse
echo "<tr>	<td colspan='4' align=center style='font size:90%' bgcolor=#ffffff><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'> &nbsp; </td>";
echo "<tr>	<td colspan='4' align=center style='font size:90%' bgcolor=#90EE90><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'> Richieste chiuse </td>";

$query = "SELECT id, DATE_FORMAT(data_richiesta,'%d-%m-%Y') as data_richiesta, descrizione, richiedente, DATE_FORMAT(data_chiusura,'%d-%m-%Y') as data_chiusura FROM shopping WHERE tipo_chiusura IS NOT NULL ORDER BY data_chiusura";
$result = mysql_query($query, $db);
while($row = mysql_fetch_array( $result )) 
{
	echo "<tr>
	<td align=center style='font size:90%' bgcolor=#90EE90><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'>$row[data_richiesta]</td>
	<td align=center style='font size:90%' bgcolor=#90EE90><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'>$row[richiedente]</td>
	<td align=center style='font size:90%' bgcolor=#90EE90><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'>$row[descrizione]</td>
	<td align=center style='font size:90%' bgcolor=#90EE90><div style=' height:18px; overflow-y:hidden; overflow-x:hidden;'><a href=visualizza_materiale.php?id=$row[id]>chiusa il $row[data_chiusura]</a>
	<td align=center style=color:#FFFF99 bgcolor=#ffffff>";  
}



echo "</table>";



mysql_close($db); 
?>

<br /><br />

</div>
</div>
</div>

</div>
<!-- Page Content END -->

		</div>
	<div id="imFooter">
		<?php 
        include ("footer.php");
        ?>
	</div>
</div>
</div>
<div class="imInvisible">
<hr />
<a href="#imGoToCont" title="Rileggi i contenuti della pagina">Torna ai contenuti</a> | <a href="#imGoToMenu" title="Naviga ancora nella pagina">Torna al menu</a>
</div>

<div id="imZIBackg" onclick="imZIHide()" onkeypress="imZIHide()"></div>
</body>
</html>
