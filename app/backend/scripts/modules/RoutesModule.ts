import { IModule } from "../bootstrap/IModule";
import { interfaces } from "inversify";
import { IController } from "../engine/IController";
import { CommitteRoute } from "../routes/committees/CommitteRoute";
import { MailRoute } from "../routes/mail/MailRoute";
import { UserRoute } from "../routes/users/UserRoute";

export class RoutesModule implements IModule {
    modules(container: interfaces.Container): void {
        container.bind<IController>("IController").to(UserRoute);
        container.bind<IController>("IController").to(MailRoute);
        container.bind<IController>("IController").to(CommitteRoute);
    }
}