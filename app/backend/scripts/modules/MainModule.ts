import * as AWS from "aws-sdk";
import { interfaces } from "inversify";
import { ManagementClient } from "auth0";
import { SmtpOptions } from "nodemailer-smtp-transport";
import { DocumentClient } from "aws-sdk/clients/dynamodb";

import { Express } from "../engine/Express";
import { IExpress } from "../engine/IExpress";
import { IModule } from "../bootstrap/IModule";
import { MailClient } from "../mail/MailClient";
import { IMailClient } from "../mail/IMailClient";
import { IAuth0Config } from "../auth0/IAuth0Config";
import { TokenRetriever } from "../auth0/TokenRetriever";
import { IExpressConfig } from "../engine/IExpressConfig";
import { DynamoClient } from "../dynamo/DynamoClient";
import { ITokenRetriever } from "../auth0/ITokenRetriever";
import { TemplateLoader } from "../template/TemplateReader";
import { ITemplateLoader } from "../template/ITemplateLoader";
import { IClientRetriever } from "../auth0/IClientRetriever";

export class MainModule implements IModule {
    modules(container: interfaces.Container): void {
        let config = require("../../settings/config.json");

        config.express.port = process.env.PORT || config.express.port;
        config.express.ipaddress = process.env.IP || config.express.ipaddress;

        container.bind<IExpress>("IExpress").to(Express).inSingletonScope();
        container.bind<IExpressConfig>("IExpressConfig").toConstantValue(config.express);

        container.bind<IAuth0Config>("IAuth0Config").toConstantValue(config.auth0);
        container.bind<ITokenRetriever>("ITokenRetriever").to(TokenRetriever);

        container.bind<IClientRetriever>("IClientRetriever").toConstantValue({
            retrieve: async () => {
                let c = container.get<IAuth0Config>("IAuth0Config");
                let token = await container.get<ITokenRetriever>("ITokenRetriever").retrieve();

                return new ManagementClient({ token: token, domain: c.domain });
            }
        });

        container.bind<SmtpOptions>("SmtpOptions").toConstantValue(config.smtp);
        container.bind<IMailClient>("IMailClient").to(MailClient).inSingletonScope();

        container.bind<DocumentClient>("DocumentClient").toDynamicValue(() => new AWS.DynamoDB.DocumentClient(config.aws));
        container.bind<DynamoClient>("DynamoClient").to(DynamoClient).inSingletonScope();

        container.bind<ITemplateLoader>("ITemplateLoader").to(TemplateLoader).inSingletonScope();
    }
}