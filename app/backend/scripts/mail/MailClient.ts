import * as nodemailer from "nodemailer";
import { inject, injectable } from "inversify";
import { SmtpOptions } from "nodemailer-smtp-transport";
import { Transporter, SendMailOptions } from "nodemailer";

import { IMailClient } from "./IMailClient";

@injectable()
export class MailClient implements IMailClient {
    private transporter: Transporter;
    constructor( @inject("SmtpOptions") options: SmtpOptions) {
        this.transporter = nodemailer.createTransport(options);
    }

    send(options: SendMailOptions): Promise<any> {
        return this.transporter.sendMail(options);
    }
}
