import { SendMailOptions } from "nodemailer";

export interface IMailClient {
    send(options: SendMailOptions): Promise<any>;
};
