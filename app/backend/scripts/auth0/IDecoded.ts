export interface IDecoded {
    "email": string;
    "email_verified": boolean;
    "iss": string;
    "sub": string;
    "aud": string;
    "exp": number;
    "iat": number;
}
