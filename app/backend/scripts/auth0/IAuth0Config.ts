export interface IAuth0Config {
    domain: string;
    connection: string;
    server_secret: string;
    endpoint: string;
    client_id: string;
    client_secret: string;
    audience: string;
}
