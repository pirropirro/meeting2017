export interface ITokenRetriever {
    retrieve(): Promise<string>;
}