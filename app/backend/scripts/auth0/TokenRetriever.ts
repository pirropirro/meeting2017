import * as request from "request";
import { injectable, inject } from "inversify";
import { ITokenRetriever } from "./ITokenRetriever";
import { IAuth0Config } from "./IAuth0Config";

@injectable()
export class TokenRetriever implements ITokenRetriever {
    constructor( @inject("IAuth0Config") private config: IAuth0Config) { }

    retrieve(): Promise<string> {
        let body = { "grant_type": "client_credentials", "client_id": this.config.client_id, "client_secret": this.config.client_secret, "audience": this.config.audience }
        return new Promise<string>((res, rej) => {
            request.post(this.config.endpoint, { json: body }, (err, r) => err ? rej(err) : res(r.body.access_token));
        });
    };
}
