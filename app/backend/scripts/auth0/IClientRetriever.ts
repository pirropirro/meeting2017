import { ManagementClient } from "auth0";

export interface IClientRetriever {
    retrieve(): Promise<ManagementClient>;
}
