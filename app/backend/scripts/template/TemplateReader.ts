import * as fs from "fs";
import * as path from "path";
import * as mustache from "mustache";
import { injectable } from "inversify";
import { ITemplateLoader } from "./ITemplateLoader";

@injectable()
export class TemplateLoader implements ITemplateLoader {
    load(location: string, data?: any): string {
        let template = fs.readFileSync(path.resolve(path.join(__dirname, "../../templates"), `${location}.html`)).toString();
        return data ? mustache.render(template, data) : template;
    }
}