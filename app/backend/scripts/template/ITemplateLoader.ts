export interface ITemplateLoader {
    load(path: string, data?: any): string;
}