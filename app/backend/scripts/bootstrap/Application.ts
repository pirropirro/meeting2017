import * as _ from "lodash";
import { Container } from "inversify";
import { Response } from "express";
import * as bodyParser from "body-parser";
import getDecorators from "inversify-inject-decorators";

import { IModule } from "./IModule";
import { IController } from "../engine/IController";
import { IExpress } from "../engine/IExpress";
import { MainModule } from "../modules/MainModule";
import { ControllerUtil } from "../routes/Route";

let container = new Container();
export let { lazyInject, lazyMultiInject } = getDecorators(container);

export class Application {

    protected container = container;

    constructor() {
        this.register(new MainModule());
    }

    register(module: IModule): boolean {
        module.modules(this.container);
        return true;
    }

    run(overrides?: any) {
        this.boot(overrides);
    }

    boot(overrides?: any) {
        let controllers = this.container.getAll<IController>("IController");
        let exp: IExpress = this.container.get<IExpress>("IExpress");

        controllers.forEach(c => {
            let { base, routes } = ControllerUtil.getMetadata(c.constructor);
            _.forEach(routes, (({ location, type, name }) => {
                let endpoint = `/api${base}${location}`;
                console.log("[REGISTERED]", type, endpoint);
                exp.app[type](endpoint, bodyParser.json(), (req, res: Response) => {
                    c[name](req).then(data => res.send(data)).catch(err => {
                        console.log(`[ERROR] [${type} - ${endpoint}]\n\n${JSON.stringify(err)}`);
                        res.status((err ? err.statusCode : null) || 400).send(err);
                    });
                });
            }));
        });
        exp.start();
    }
}