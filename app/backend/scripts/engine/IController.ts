import { Request } from "express";

export interface IController { }
export type IRoute = (request: Request) => Promise<any>;