export interface IExpressConfig {
    port: number;
    ipaddress: string;
}