import * as fs from "fs";
import * as path from "path";
import * as express from "express";
import * as moment from "moment";
import { IExpress } from "./IExpress";
import { inject, injectable } from "inversify";
import { IExpressConfig } from "./IExpressConfig";

@injectable()
export class Express implements IExpress {
    public app: express.Application;

    constructor( @inject("IExpressConfig") private config: IExpressConfig) {
        this.app = express();
        this.app.use("/", express.static(path.join(__dirname, "../../../frontend/dist/main")));

        this.app.use((req, res, next) => {
            res.setHeader("Access-Control-Allow-Origin", "*");
            res.setHeader("Content-Type", "application/json");
            res.setHeader("Access-Control-Allow-Methods", "OPTIONS,POST,GET,DELETE,PUT");
            res.setHeader("Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, Content-Type, Accept");
            return next();
        });

        this.setupTerminationHandlers();
    }

    public start() {
        this.app.use((req, res, next) => {
            res.setHeader("Content-Type", "text/html");
            return next();
        })
        this.app.get("*", (req, res) => res.sendFile(path.resolve(path.join(__dirname, "../../../frontend/dist/main"), "index.html")));

        this.app.listen(this.config.port, this.config.ipaddress, () => {
            console.log("[%s] Listen on port %d ...", moment().format("HH:mm:ss"), this.config.port);
        });
    }

    private terminator(sig?: string): void {
        if (typeof sig === "string") {
            console.log("[%s] Received %s - terminating sample app ...", moment().format("HH:mm:ss"), sig);
            process.exit(1);
        }
        console.log("[%s] Node server stopped.", moment().format("HH:mm:ss"));
    }

    private setupTerminationHandlers() {
        process.on("exit", () => this.terminator());

        ["SIGHUP", "SIGINT", "SIGQUIT", "SIGILL", "SIGTRAP", "SIGABRT",
            "SIGBUS", "SIGFPE", "SIGUSR1", "SIGSEGV", "SIGUSR2", "SIGTERM"
        ].forEach((element: any, index, array) => {
            process.on(element, () => { this.terminator(element); });
        });
    };
}
