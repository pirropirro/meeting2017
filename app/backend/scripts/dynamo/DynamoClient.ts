import { inject, injectable } from "inversify";
import { DocumentClient } from "aws-sdk/clients/dynamodb";

@injectable()
export class DynamoClient {
    constructor( @inject("DocumentClient") private document: DocumentClient) { }

    scan<T = any>(table: string): Promise<any> {
        return new Promise<any>((res, rej) => this.document
            .scan({ TableName: table }, (error, results) => !error ? res(results.Items || []) : rej(error)));
    }

    query<T = any>(options: DocumentClient.QueryInput): Promise<any> {
        return new Promise<any>((res, rej) => this.document
            .query(options, (error, results) => !error ? res(results.Items || []) : rej(error)));
    }

    put(table: string, item: any): Promise<any> {
        return new Promise<any>((res, rej) => this.document
            .put({ TableName: table, Item: item }, (error, results) => !error ? res(results) : rej(error)));
    }

    update(options: DocumentClient.UpdateItemInput): Promise<any> {
        return new Promise<any>((res, rej) => this.document
            .update(options, (error, results) => !error ? res(results) : rej(error)));
    }

    delete(table: string, key: any): Promise<any> {
        return new Promise<any>((res, rej) => this.document
            .delete({ TableName: table, Key: key }, (error, results) => !error ? res(results) : rej(error)));
    }
}