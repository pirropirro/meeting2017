import "reflect-metadata";
import { Application } from "./bootstrap/Application";
import { RoutesModule } from "./modules/RoutesModule";

const app = new Application()
app.register(new RoutesModule());
app.run();