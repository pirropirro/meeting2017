import * as uuid from "uuid";
import { merge, filter } from "lodash";
import { Request } from "express";
import { inject } from "inversify";

import { Controller, RouteType, Route } from "../Route";
import { IController } from "../../engine/IController";
import { DynamoClient } from "../../dynamo/DynamoClient";
import { ICommittee } from "../../../../Interface";
import { Tables } from "../../dynamo/Tables";

@Controller("/committee")
export class CommitteRoute implements IController {
    constructor( @inject("DynamoClient") private client: DynamoClient) { }

    @Route(RouteType.GET, "/list")
    listCommittee(req: Request): Promise<any> {
        return this.client.scan(Tables.COMMITTEES);
    }

    @Route(RouteType.GET, "/notPreregistered")
    listNotPreregisteredCommittee(req: Request): Promise<any> {
        return this.client.scan(Tables.COMMITTEES).then(committees => filter(committees, (c: ICommittee) => !c.preRegistered));
    }

    @Route(RouteType.DELETE, "/delete/:committeeId")
    deleteCommittee(req: Request): Promise<any> {
        let { committeeId } = req.params;
        return this.client.delete(Tables.COMMITTEES, { committeeId: committeeId });
    }

    @Route(RouteType.POST, "/update")
    updateComittee(req: Request): Promise<any> {
        let committee: ICommittee = req.body;
        return (!committee.committeeId
            ? this.client.put(Tables.COMMITTEES, merge(committee, { committeeId: uuid.v4() }))
            : this.client.update({
                TableName: Tables.COMMITTEES,
                Key: { "committeeId": committee.committeeId },
                UpdateExpression: "set email = :e, committeeName = :n, councilior.email = :y",
                ExpressionAttributeValues: {
                    ":e": committee.email,
                    ":n": committee.committeeName,
                    ":y": committee.councilior.email
                }
            })).then(data => committee);
    }

    @Route(RouteType.POST, "/preregister")
    async preRegisterCommittee(req: Request): Promise<any> {
        let { committeeId } = req.body;
        let [committee]: [ICommittee] = await this.client.query({
            TableName: Tables.COMMITTEES, KeyConditionExpression: "committeeId = :c",
            ExpressionAttributeValues: { ":c": committeeId }
        }).catch(() => ({ message: `Password errata!` }));
        if (committee.preRegisteredAccepted) throw ({ message: `${committee.committeeName} già preiscritto` });

        return this.client.update({
            TableName: Tables.COMMITTEES, Key: { "committeeId": committeeId },
            UpdateExpression: "set preRegisteredAccepted = :v", ExpressionAttributeValues: { ":v": true }
        }).then(() => committee);
    }
};
