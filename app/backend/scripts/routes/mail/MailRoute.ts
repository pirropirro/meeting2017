import { merge } from "lodash";
import { Request } from "express";
import { inject } from "inversify";
import { SendMailOptions } from "nodemailer";

import { IMailClient } from "../../mail/IMailClient";
import { IController } from "../../engine/IController";
import { Controller, RouteType, Route } from "../Route";
import { ITemplateLoader } from "../../template/ITemplateLoader";
import { DynamoClient } from "../../dynamo/DynamoClient";
import { encrypt } from "../../Crypt";
import { ICommittee, IPerson } from "../../../../Interface";
import { Tables } from "../../dynamo/Tables";

const fromAdmin = {
    "from": "Meeting 2017 | Back Office <meeting.2017@piemonte.cri.it>",
    "replyTo": "meeting.2017@piemonte.cri.it",
};

const toAdmin = (replyTo) => ({ "from": `Meeting 2017 | Supporto <${replyTo}>`, to: "meeting.2017@piemonte.cri.it", bcc: "sviluppo.meeting@gmail.com" })

@Controller("/mail")
export class MailRoute implements IController {

    constructor( @inject("IMailClient") private client: IMailClient,
        @inject("DynamoClient") private dynamo: DynamoClient,
        @inject("ITemplateLoader") private loader: ITemplateLoader) { }

    @Route(RouteType.POST, "/send")
    sendEmail(req: Request): Promise<any> {
        let mail: SendMailOptions = req.body;
        return this.client.send(merge(mail, fromAdmin));
    };

    @Route(RouteType.POST, "/support")
    sendSupportEmail(req: Request): Promise<any> {
        let mail: SendMailOptions = req.body;
        return this.client.send(merge(mail, toAdmin(mail.replyTo)));
    };

    @Route(RouteType.POST, "/preinscription")
    async sendPreinscription(req: Request): Promise<any> {
        let committee = await this.updateComittee(req.body);

        let mail: SendMailOptions = {};
        mail.html = this.loader.load("preinscription/confirm", merge(committee, { password: committee.committeeId }));
        mail.to = committee.email;
        mail.subject = "Preiscrizione Meeting 2017";

        return this.client.send(merge(mail, fromAdmin));
    };

    @Route(RouteType.POST, "/inviteUser")
    async inviteUser(req: Request): Promise<any> {
        let user: IPerson = req.body;
        let options: SendMailOptions = {};

        options.html = this.loader.load("inviteuser", { user: merge(user, { grant: encrypt(user.grant || "MEMBER") }) });
        options.to = user.email;
        options.subject = "Registrazione Portale Meeting 2017";

        return this.client.send(merge(options, fromAdmin));
    };

    private updateComittee(committee: ICommittee): Promise<ICommittee> {
        return this.dynamo.update({
            TableName: Tables.COMMITTEES,
            Key: { "committeeId": committee.committeeId },
            UpdateExpression: "set delegate = :d, preRegistered = :p",
            ExpressionAttributeValues: {
                ":d": committee.delegate,
                ":p": true
            }
        }).then(data => committee);
    }
}
