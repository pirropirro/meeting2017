import * as uuid from "uuid";
import { Request } from "express";
import { inject } from "inversify";
import * as jwt from "jsonwebtoken";
import { IUser } from "../../../../Interface";
import { IDecoded } from "../../auth0/IDecoded";
import { IAuth0Config } from "../../auth0/IAuth0Config";
import { Controller, RouteType, Route } from "../Route";
import { IController } from "../../engine/IController";
import { DynamoClient } from "../../dynamo/DynamoClient";
import { decrypt } from "../../Crypt";
import { IClientRetriever } from "../../auth0/IClientRetriever";
import { Tables } from "../../dynamo/Tables";

@Controller("/user")
export class UserRoute implements IController {
    constructor( @inject("IAuth0Config") private config: IAuth0Config,
        @inject("IClientRetriever") private retriever: IClientRetriever,
        @inject("DynamoClient") private dynamo: DynamoClient) { }

    @Route(RouteType.POST, "/register")
    async registerUser(req: Request): Promise<any> {
        let user: IUser = req.body;
        let man = await this.retriever.retrieve();
        let res = await man.createUser({ email: user.email, connection: this.config.connection, password: user.password, email_verified: true, verify_email: false });
        user.principalId = res.user_id;
        user.userId = uuid.v4();
        delete user.password;
        delete user.confirmPassword;
        return await this.dynamo.put(Tables.USERS, user);
    };

    @Route(RouteType.GET, "/check/:email")
    async checkUser(req: Request): Promise<any> {
        let { email } = req.params;
        return await this.dynamo.query({
            TableName: Tables.USERS, KeyConditionExpression: "email = :email",
            ExpressionAttributeValues: {
                ":email": email
            }
        }).then(data => data[0] ? true : false);
    };

    @Route(RouteType.GET, "/byToken/:token")
    byToken(req: Request): Promise<any> {
        let { token } = req.params;
        return new Promise<IDecoded>((res, rej) => jwt.verify(token, Buffer.from(this.config.server_secret),
            (err: Error, decoded: IDecoded) => err ? rej({ message: err.message, statusCode: 401 }) : res(decoded)))
            .then(({ email }) => this.dynamo.query({
                TableName: Tables.USERS, KeyConditionExpression: "email = :email",
                ExpressionAttributeValues: { ":email": email }
            })).then(data => data[0] || {});
    };

    @Route(RouteType.GET, "/decrypt/:grant")
    async decryptGrant(req: Request): Promise<any> {
        return { grant: decrypt(req.params.grant) };
    };
}
