import * as React from "react";

export class ErrorCheck extends React.Component<{ message?: string }> {
    render() {
        return <div className="mt-error">
            <svg className="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                <circle className="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                <path className="checkmark__check" d="m17.7,34.2125l16.7,-16.8" fill="none" />
                <path className="checkmark__check" transform="rotate(90)" d="m17.7,34.2125l16.7,-16.8" fill="none" />
            </svg>
            {this.props.message ? <div className="mt-confirm__message">{this.props.message}</div> : null}
        </div>;
    }
}