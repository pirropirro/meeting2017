import * as React from "react";
import * as classnames from "classnames";

export class BurgerIcon extends React.Component<{ onOpen: () => void, onClose: () => void, isOpen: boolean }> {
    render() {
        return <div className="burger-icon" onClick={() => this.props.isOpen ? this.props.onClose() : this.props.onOpen()} >
            <button className={classnames("lines-button", this.props.isOpen ? "x close" : "")} type="button">
                <span className="lines"></span>
            </button>
        </div>;
    }
}