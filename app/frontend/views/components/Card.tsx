import * as React from "react";
import { isFunction } from "lodash";

export class Card extends React.Component<{ title?: string, icon?: string | (() => JSX.Element) | (() => JSX.Element[]) }> {
    render() {
        let { title, icon } = this.props;
        let iconTemplate;
        if (icon) iconTemplate = isFunction(icon) ? icon() : <i className="material-icons">{icon}</i>;

        return <div className="mt-card">
            <div className="mt-card__header">
                <div className="mt-card__title">
                    <div className="truncate">{title}</div>
                </div>
                {icon ? <div className="mt-card__right-icon">{iconTemplate}</div> : null}
            </div>
            <div className="mt-card__content">
                {this.props.children}
            </div>
        </div>;
    }
}