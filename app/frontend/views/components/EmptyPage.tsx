import * as React from "react";

export class EmptyPage extends React.Component<{icon?: string, message?: string}> {
    render() {
        return <div className="mt-empty">
            {this.props.children ? this.props.children :
                [<i className="material-icons">{this.props.icon || "inbox"}</i>, <span>{this.props.message || "Nessun Elemento"}</span>]}
        </div>;
    }
}