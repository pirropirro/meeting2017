import * as React from "react";
import { isFunction } from "lodash";
import * as classnames from "classnames";
const onClickOutside = require("react-onclickoutside").default;

interface IFlyingButtonProps {
    icon?: string | (() => JSX.Element);
    description?: string;
    onToggle?: () => void;
    isOpen?: boolean;
    disabled?: boolean;
}

class FlyingButton extends React.Component<IFlyingButtonProps> {
    static defaultProps: IFlyingButtonProps = {
        onToggle: () => true
    };


    render() {
        return <div className={classnames("flying-button", this.props.isOpen ? "open" : "", this.props.disabled ? "disabled" : null)} onClick={() => this.props.onToggle()} >
            <div className="flying-button__content">
                {isFunction(this.props.icon) ? this.props.icon() : <i className="material-icons">{this.props.icon}</i>}
            </div>
            {this.props.description ? <div className="flying-button__description"><div className="truncate">{this.props.description}</div></div> : null}
            {this.props.children ? <div className="flying-button__children">{this.props.children}</div> : null}
            <div className="flying-button__overlay"></div>
        </div>;
    }

    handleClickOutside(evt) {
        if (this.props.isOpen) this.props.onToggle();
    }
}

export default onClickOutside(FlyingButton);