import * as React from "react";
import * as classnames from "classnames";

export class Button extends React.Component<{ bottom?: boolean, disabled?: boolean, onToggle: () => void }> {
    render() {
        return <div className={classnames("mt-button", this.props.bottom ? "fixed-bottom" : null, this.props.disabled ? "disabled" : null)} >
            <div className="mt-button__content" onClick={() => this.props.onToggle()} >{this.props.children}</div>
        </div>;
    }
}