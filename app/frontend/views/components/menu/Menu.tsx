import * as React from "react";

const onClickOutside = require("react-onclickoutside").default;

class Menu extends React.Component<{ onClickOutside?: () => void }> {
    render() {
        return <div className="mt-menu">
            {this.props.children}
        </div>;
    }

    handleClickOutside(evt) {
        if (this.props.onClickOutside) this.props.onClickOutside();
    }
}

export default onClickOutside(Menu);