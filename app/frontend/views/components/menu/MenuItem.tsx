import * as React from "react";
import { isFunction } from "lodash";
import * as classnames from "classnames";

interface IMenuItemProps {
    label: string | (() => string);
    icon?: string | (() => JSX.Element);
    onToggle?: () => void;
    areChildrenVisible?: boolean;
}

export class MenuItem extends React.Component<IMenuItemProps> {

    static defaultProps: IMenuItemProps = {
        label: "",
        onToggle: () => true
    };

    render() {
        let { icon, label, onToggle, areChildrenVisible } = this.props;
        icon = !this.props.children ? icon
            : areChildrenVisible
                ? "keyboard_arrow_up"
                : "keyboard_arrow_down";

        return <div className={classnames("mt-menu__item", areChildrenVisible ? "open" : "")}>
            <div className="mt-menu__item-content" onClick={() => onToggle()}>
                <div className="mt-menu__item-label truncate">
                    {isFunction(label) ? label() : label}
                </div>
                <div className="mt-menu__item-icon">
                    {isFunction(icon) ? icon() : <i className="material-icons">{icon}</i>}
                </div>
            </div>
            {this.props.children && areChildrenVisible ? (<div className="mt-menu__item-children">{this.props.children}</div>) : null}
        </div>;
    }
}
