import { View } from "ninjagoat";
import IndexViewModel from "../scripts/viewmodels/IndexViewModel";
import * as React from "react";
import { Card } from "./components/Card";

export default class Index extends View<IndexViewModel> {

    render() {
        return (
            <div className="page">
                <div className="page__content">
                    <Card title="Benvenuto" >
                        <p>Siamo giunti alla X edizione Regionale. Abbiamo creato questo portale per gestire le iscrizioni, le registrazioni,
                            le votazioni, la consultazione dei punteggi e quant'altro è funzionale al Meeting.</p>
                        <p>Le preiscrizioni si sono chiuse lo scorso 31 agosto.</p>
                        <p>Le iscrizioni saranno disponibili dal 12 settembre al 2 ottobre.</p>
                    </Card>
                </div>
            </div>
        );
    }
}
