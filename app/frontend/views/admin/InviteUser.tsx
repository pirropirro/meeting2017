import * as React from "react";
import { View } from "ninjagoat";
import { TextField, SelectField, MenuItem } from "material-ui";

import { InviteUserViewModel } from "../../scripts/viewmodels/admin/InviteUserViewModel";
import { EmptyPage } from "../components/EmptyPage";
import { Card } from "../components/Card";
import { Button } from "../components/buttons/Button";
import { Grant } from "../../scripts/grants/Grant";

export default class InviteUser extends View<InviteUserViewModel> {

    render() {
        let { user, committees } = this.viewModel;
        return (
            <div className="page">
                {committees && user ? <div className="page__content">
                    <Card title="Inserisci i dati"><div className="row">
                        <div className="col-lg-6 col-md-12">
                            <SelectField floatingLabelText="Comitato" fullWidth={true}
                                value={user.committeeId}
                                onChange={(event, index, value) => this.viewModel.onCommitteeChange(value)}>
                                {committees.map(c => <MenuItem value={c.committeeId} primaryText={c.committeeName} />)}
                            </SelectField>
                        </div>
                        <div className="col-lg-6 col-md-12">
                            <TextField fullWidth={true} floatingLabelText="Nome" value={user.name}
                                onChange={(v: any) => this.viewModel.onNameChange(v.target.value)} />
                        </div>
                        <div className="col-lg-6 col-md-12">
                            <TextField fullWidth={true} type="email" value={user.email} floatingLabelText="Email"
                                onChange={(v: any) => this.viewModel.onEmailChange(v.target.value)} />
                        </div>
                        <div className="col-lg-6 col-md-12">
                            <SelectField floatingLabelText="Ruolo" fullWidth={true}
                                value={user.grant}
                                onChange={(event, index, value) => this.viewModel.onGrantChange(value)}>
                                <MenuItem value={Grant.ADMIN} primaryText="Admin" />
                                <MenuItem value={Grant.BACK_OFFICE} primaryText="Back Office" />
                                <MenuItem value={Grant.CAPTAIN} primaryText="Capitano" />
                                <MenuItem value={Grant.JUDGE} primaryText="Giudice" />
                                <MenuItem value={Grant.SUPERVISOR} primaryText="Supervisore" />
                                <MenuItem value={Grant.MEMBER} primaryText="Membro" />
                                <MenuItem value={Grant.OBSERVER} primaryText="Osservatore" />
                            </SelectField>
                        </div>
                    </div></Card> <Button disabled={!this.viewModel.canInvite()} onToggle={() => this.viewModel.inviteUser()}>Invita</Button>
                </div> : <EmptyPage />}
            </div>
        );
    }
} 
