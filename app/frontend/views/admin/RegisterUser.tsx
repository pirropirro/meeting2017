import * as React from "react";
import { View } from "ninjagoat";
import { TextField, SelectField, MenuItem } from "material-ui";

import { RegisterUserViewModel } from "../../scripts/viewmodels/admin/RegisterUserViewModel";
import { EmptyPage } from "../components/EmptyPage";
import { Card } from "../components/Card";
import { Button } from "../components/buttons/Button";
import { Grant } from "../../scripts/grants/Grant";

export default class RegisterUser extends View<RegisterUserViewModel> {

    render() {
        let { user, committees } = this.viewModel;
        return (
            <div className="page">
                {committees && user ? <div className="page__content">
                    <Card title="Inserisci i dati"><div className="row">
                        <div className="col-lg-6 col-md-12">
                            <TextField fullWidth={true} floatingLabelText="Username" value={user.username}
                                onChange={(v: any) => this.viewModel.onUserameChange(v.target.value)} />
                        </div>
                        <div className="col-lg-6 col-md-12">
                            <TextField fullWidth={true} type="email" value={user.email} floatingLabelText="Email"
                                onChange={(v: any) => this.viewModel.onEmailChange(v.target.value)} />
                        </div>
                        <div className="col-lg-6 col-md-12">
                            <TextField fullWidth={true} type="password" floatingLabelText="Password" value={user.password}
                                onChange={(v: any) => this.viewModel.onPasswordChange(v.target.value)} />
                        </div>
                        <div className="col-lg-6 col-md-12">
                            <TextField fullWidth={true} type="password" floatingLabelText="Conferma Password"
                                onChange={(v: any) => this.viewModel.onConfirmPasswordChange(v.target.value)}
                                value={user.confirmPassword}
                                errorText={user.confirmPassword && user.confirmPassword !== user.password ? "Le password devono essere uguali" : null} />
                        </div>
                        <div className="col-lg-6 col-md-12">
                            <SelectField floatingLabelText="Ruolo" fullWidth={true}
                                value={user.grant}
                                onChange={(event, index, value) => this.viewModel.onGrantChange(value)}>
                                <MenuItem value={Grant.ADMIN} primaryText="Admin" />
                                <MenuItem value={Grant.BACK_OFFICE} primaryText="Back Office" />
                                <MenuItem value={Grant.CAPTAIN} primaryText="Capitano" />
                                <MenuItem value={Grant.JUDGE} primaryText="Giudice" />
                                <MenuItem value={Grant.SUPERVISOR} primaryText="Supervisore" />
                                <MenuItem value={Grant.MEMBER} primaryText="Membro" />
                                <MenuItem value={Grant.OBSERVER} primaryText="Osservatore" />
                            </SelectField>
                        </div>
                        <div className="col-lg-6 col-md-12">
                            <SelectField floatingLabelText="Comitato" fullWidth={true}
                                value={user.committeeId}
                                onChange={(event, index, value) => this.viewModel.onCommitteeChange(value)}>
                                {committees.map(c => <MenuItem value={c.committeeId} primaryText={c.committeeName} />)}
                            </SelectField>
                        </div>
                    </div></Card> <Button disabled={!this.viewModel.canSave()} onToggle={() => this.viewModel.createUser()}>Registra</Button>
                </div> : <EmptyPage />}
            </div>
        );
    }
} 
