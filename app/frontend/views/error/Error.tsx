import * as React from "react";
import { View } from "ninjagoat";

import { ErrorCheck } from "../components/ErrorCheck";
import { ErrorViewModel } from "../../scripts/viewmodels/error/ErrorViewModel";

export default class Error extends View<ErrorViewModel> {
    render() {
        return (
            <div className="page">
                <ErrorCheck message={this.viewModel.message} />
            </div>
        );
    }
}
