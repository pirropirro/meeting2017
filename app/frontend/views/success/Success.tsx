import * as React from "react";
import { View } from "ninjagoat";

import { ConfirmCheck } from "../components/ConfirmCheck";
import { SuccessViewModel } from "../../scripts/viewmodels/success/SuccessViewModel";

export default class Success extends View<SuccessViewModel> {
    render() {
        return (
            <div className="page">
                <ConfirmCheck message={this.viewModel.message} />
            </div>
        );
    }
}
