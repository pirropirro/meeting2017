import * as React from "react";
import { View } from "ninjagoat";
import { TextField, Checkbox } from "material-ui";

import { Card } from "../components/Card";
import { EmptyPage } from "../components/EmptyPage";
import FlyingButton from "../components/buttons/FlyingButtons";
import ListViewModel from "../../scripts/viewmodels/committee/ListViewModel";

export default class List extends View<ListViewModel> {
    render() {
        return (
            <div className="page">
                {this.viewModel.committees.length ? (<div className="page__content">
                    <div className="row no-gutters">
                        {this.viewModel.committees.map(c =>
                            <div className="col-lg-4 col-md-6 col-sm-12 pl-2 pr-2">
                                <Card title={c.committeeName} key={c.committeeId} icon={() => [(c.dirty
                                    ? (this.viewModel.canSave(c)
                                        ? <i className="material-icons" onClick={() => this.viewModel.updateCommittee(c)}>save</i>
                                        : <i className="material-icons">warning</i>)
                                    : null), <i className="material-icons" onClick={() => this.viewModel.removeCommittee(c)}>delete</i>]}>
                                    <div className="row">
                                        <div className="col-12" >
                                            <TextField floatingLabelText="Nome" value={c.committeeName} errorText={c.committeeName ? "" : "Il nome non può essere vuoto"}
                                                fullWidth={true} onChange={(v: any) => this.viewModel.onNameChange(c, v.target.value)} />
                                        </div>
                                        <div className="col-lg-6 col-md-12" >
                                            <TextField floatingLabelText="Email" type="email" value={c.email} errorText={c.email ? "" : "La email non può essere vuota"}
                                                fullWidth={true} onChange={(v: any) => this.viewModel.onMailChange(c, v.target.value)} />
                                        </div>
                                        <div className="col-lg-6 col-md-12" >
                                            <TextField floatingLabelText="Email consigliere giovane" type="email"
                                                value={c.councilior ? c.councilior.email : null}
                                                errorText={c.councilior && c.councilior.email ? "" : "La email non può essere vuota"}
                                                fullWidth={true} onChange={(v: any) => this.viewModel.onCounciliorEmail(c, v.target.value)} />
                                        </div>
                                        <br />
                                        <div className="col-6 mt-3">
                                            <Checkbox label="Preiscritto" checked={c.preRegistered} disabled={true} />
                                        </div>
                                        <div className="col-6 mt-3">
                                            <Checkbox label="Accettato" checked={c.preRegisteredAccepted} disabled={true} />
                                        </div>
                                    </div>
                                </Card>
                            </div>)}
                    </div>
                </div>) : <EmptyPage />}
                <FlyingButton onToggle={() => this.viewModel.toggleActions()} isOpen={this.viewModel.actionsVisible} icon="add" >
                    <FlyingButton onToggle={() => this.viewModel.addCommittee()} icon="add" description="Aggiungi" />
                    {this.viewModel.canSaveAll() ? <FlyingButton onToggle={() => this.viewModel.updateAll()} icon="save" description="Salva Tutti" /> : null}
                </FlyingButton>
            </div>
        );
    }
}
