import { View, lazyInject, INavigationManager } from "ninjagoat";
import * as React from "react";
import * as classnames from "classnames";
import MasterViewModel from "../scripts/viewmodels/MasterViewModel";
import { BurgerIcon } from "./components/BurgerIcon";
import Menu from "./components/menu/Menu";
import { MenuItem } from "./components/menu/MenuItem";
import * as Swipeable from "react-swipeable";
import { MuiThemeProvider } from "material-ui/styles";
import getMuiTheme from "material-ui/styles/getMuiTheme";
import { NotificationBar } from "../scripts/notifications/NotificationBar";
import { Grant } from "../scripts/grants/Grant";
import { Loader } from "../scripts/loader/Loader";
import onClickOutside from "./components/menu/Menu";

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: "#cc0000",
        primary2Color: "#cc0000",
        primary3Color: "#cc0000",
        accent1Color: "#cc0000",
        accent2Color: "#cc0000"
    }
});


export default class Master extends View<MasterViewModel> {
    render() {
        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                <Swipeable onSwipingRight={() => this.viewModel.openMenu()} onSwipingLeft={() => this.viewModel.closeMenu()}>
                    <div className={classnames("main-app", this.viewModel.isMenuOpen ? "menu-opened" : "")}>
                        {getMenu(this.viewModel)}
                        <section className="body">
                            <nav className="navbar main-navbar">
                                <BurgerIcon onOpen={() => this.viewModel.openMenu()}
                                    onClose={() => this.viewModel.closeMenu()}
                                    isOpen={this.viewModel.isMenuOpen} />
                                <div className="navbar__title">
                                    <span onClick={() => this.viewModel.navigate("Index", "Index")}><b>MEET</b>ING <span className="navbar__subtitle">20<b>17</b></span></span>
                                </div>
                            </nav>
                            <section className="content">
                                <div className="content__overlay" />
                                {this.props.children}
                            </section>
                        </section>
                    </div>
                    <NotificationBar />
                    <Loader />
                </Swipeable>
            </MuiThemeProvider>
        );
    }
}

function getMenu(vm: MasterViewModel): JSX.Element {
    let user = vm.user;
    const avatar = (<div className="mt-menu__avatar">
        <i className="material-icons">tag_faces</i>
        <div className="welcome-text">Benvenuto {user ? <div className="truncate">{`, ${user.username}`}</div> : null}</div>
    </div>);

    let always = [<MenuItem key="PREISCRIZIONI" label="PRE ISCRIZIONI" icon="label_outline" onToggle={() => vm.navigate("preinscription", "summary")} />,
    <MenuItem key="ISCRIZIONI" label="ISCRIZIONI" icon="label" />,
    <MenuItem key="REGOLAMENTO" label="REGOLAMENTO" icon="description" />,
    <MenuItem key="CONTATTACI" label="CONTATTACI" icon="phone_in_talk" onToggle={() => vm.navigate("support", "email")} />,
    <MenuItem key="COME ARRIVARE" label="COME ARRIVARE" icon="directions" />];

    const anonymusMenu = (<Menu onClickOutside={() => vm.closeMenu()}>
        {avatar}
        <MenuItem label="LOGIN" icon="person" onToggle={() => vm.login()} />
        {always}
    </Menu>);

    const loggedMenu = (grant: Grant) => (<Menu onClickOutside={() => vm.closeMenu()}>
        {avatar}
        {grant === Grant.ADMIN || grant === Grant.BACK_OFFICE
            ? <MenuItem key="BACK_OFFICE" label="BACK OFFICE" areChildrenVisible={vm.isItemOpen("BACK_OFFICE")} onToggle={() => vm.toggleMenuItem("BACK_OFFICE")} >
                <MenuItem key="COMITATI" label="COMITATI" icon="account_balance" onToggle={() => vm.navigate("committee", "list")} />
                <MenuItem key="INVITA" label="INVITA UTENTE" icon="send" onToggle={() => vm.navigate("admin", "inviteUser")} />
                <MenuItem key="REGISTRA" label="REGISTRA UTENTE" icon="person_add" onToggle={() => vm.navigate("admin", "registeruser")} />
                <MenuItem key="INVIA_MAIL" label="INVIA MAIL" icon="email" onToggle={() => vm.navigate("admin", "sendemail")} />
            </MenuItem> : null}
        {always}
        <MenuItem label="LOGOUT" icon="person_outline" onToggle={() => vm.logout()} />
    </Menu>);

    return user ? loggedMenu(user.grant) : anonymusMenu;
}
