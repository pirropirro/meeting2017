import * as React from "react";
import { View } from "ninjagoat";
import { Card } from "../components/Card";
import { Button } from "../components/buttons/Button";
import SummaryViewModel from "../../scripts/viewmodels/preinscription/SummaryViewModel";

export default class Summary extends View<SummaryViewModel> {

    render() {
        return (
            <div className="page">
                <div className="page__content">
                    <Card title="Preiscrizioni" >
                        <p>La preiscrizione può essere effettuata dal Consigliere Giovane o da un volontario che ne fa le veci.</p>
                        <p>È la procedura attraverso la quale viene notificato al Presidente Regionale o Locale, tramite mail automatica, l'intenzione della
                            squadra di partecipare al Meeting, previa consultazione del rispettivo regolamento (pubblicato nel Portale nella sezione 'Regolamento').</p>
                        <p> Nella mail di notifica è riportato il link e la password attraverso la quale poter convalidare la preiscrizione.Quando il Presidente
                            o chi ne fa le veci, effettua la convalida, viene inviata da Portale una mail di notifica sia al Presidente che al Consigliere Giovane
                            dell'avvenuta operazione.
                        </p>
                        <p>Tale operazione è propedeutica nonché necessaria per poter successivamente effettuare la registrazione dei partecipanti e/o osservatori.</p>
                    </Card>
                    <Button onToggle={() => this.viewModel.goNext()}>Continua</Button>
                </div>
            </div>
        );
    }
}
