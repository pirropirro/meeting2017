import * as React from "react";
import { View } from "ninjagoat";
import { TextField, SelectField, MenuItem } from "material-ui";

import { InsertViewModel } from "../../scripts/viewmodels/preinscription/InsertViewModel";
import { EmptyPage } from "../components/EmptyPage";
import { Card } from "../components/Card";
import { Button } from "../components/buttons/Button";

export default class Insert extends View<InsertViewModel> {

    render() {
        let { committeeId, committees, delegate, committee } = this.viewModel;
        return (
            <div className="page">
                {committees ? <div className="page__content">
                    <Card title="Dati del Delegato"><div className="row">
                        <div className="col-12">
                            <SelectField floatingLabelText="Comitato" fullWidth={true}
                                value={committeeId}
                                onChange={(event, index, value) => this.viewModel.onCommitteeChange(value)}>
                                {committees.map(c => <MenuItem value={c.committeeId} primaryText={c.committeeName} />)}
                            </SelectField>
                        </div>
                        <div className="col-lg-6 col-md-12">
                            <TextField disabled={!committee} fullWidth={true} floatingLabelText="Nome" value={delegate.name}
                                onChange={(v: any) => this.viewModel.onNameChange(v.target.value)} />
                        </div>
                        <div className="col-lg-6 col-md-12">
                            <TextField disabled={!committee} fullWidth={true} floatingLabelText="Cognome" value={delegate.surname}
                                onChange={(v: any) => this.viewModel.onSurnameChange(v.target.value)} />
                        </div>
                        <div className="col-lg-6 col-md-12">
                            <TextField disabled={!committee} fullWidth={true} type="email" value={delegate.email} floatingLabelText="Email"
                                onChange={(v: any) => this.viewModel.onEmailChange(v.target.value)} />
                        </div>
                        <div className="col-lg-6 col-md-12">
                            <TextField disabled={!committee} fullWidth={true} type="number" value={delegate.phone} floatingLabelText="Recapito Telefonico"
                                onChange={(v: any) => this.viewModel.onPhoneChange(v.target.value)} />
                        </div>
                    </div></Card>
                    <Button disabled={!this.viewModel.canGoNext()} onToggle={() => this.viewModel.goNext()}>Registra</Button>
                </div> : <EmptyPage />}
            </div>
        );
    }
}