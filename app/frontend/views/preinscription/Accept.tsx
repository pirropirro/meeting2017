import * as React from "react";
import { View } from "ninjagoat";
import { TextField } from "material-ui";

import { AcceptViewModel } from "../../scripts/viewmodels/preinscription/AcceptViewModel";
import { Button } from "../components/buttons/Button";
import { Card } from "../components/Card";

export default class Accept extends View<AcceptViewModel> {
    render() {
        return (
            <div className="page">
                <div className="page__content">
                    <Card title="Accetta Preiscrizione" >
                        <TextField fullWidth={true} floatingLabelText="Inserisci la password" value={this.viewModel.committeeId}
                            onChange={(v: any) => this.viewModel.onCommitteeChange(v.target.value)} />
                    </Card>
                    <Button disabled={!this.viewModel.canGoNext()} onToggle={() => this.viewModel.goNext()}>Accetta</Button>
                </div>
            </div>
        );
    }
}
