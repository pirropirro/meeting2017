import { View } from "ninjagoat";
import { NotFoundViewModel } from "../scripts/viewmodels/NotFoundViewModel";
import * as React from "react";
import { EmptyPage } from "./components/EmptyPage";

export default class NotFound extends View<NotFoundViewModel> {

    render() {
        return (
            <div className="page">
                <EmptyPage icon="mood_bad" message="Pagina non trovata" />
            </div>
        );
    }
}
