import * as React from "react";
import { View } from "ninjagoat";
import { TextField } from "material-ui";

import { Card } from "../components/Card";
import { Button } from "../components/buttons/Button";
import EmailViewModel from "../../scripts/viewmodels/support/EmailViewModel";

export default class Email extends View<EmailViewModel> {
    render() {
        let { mail } = this.viewModel;
        return (
            <div className="page">
                <div className="page__content">
                    <Card title="Inserisci il messaggio"><div className="row">
                        <div className="col-lg-6 col-md-12">
                            <TextField fullWidth={true} floatingLabelText="Tua email" value={mail.replyTo}
                                onChange={(v: any) => this.viewModel.onSenderChange(v.target.value)} />
                        </div>
                        <div className="col-lg-6 col-md-12">
                            <TextField fullWidth={true} value={mail.subject} floatingLabelText="Oggetto"
                                onChange={(v: any) => this.viewModel.onSubjectChange(v.target.value)} />
                        </div>
                        <div className="col-12">
                            <TextField fullWidth={true} floatingLabelText="Testo" value={mail.text}
                                multiLine={true} rows={4} onChange={(v: any) => this.viewModel.onTextChange(v.target.value)} />
                        </div>
                    </div></Card>
                    <Button disabled={!this.viewModel.canSend()} onToggle={() => this.viewModel.sendEmail()}>Invia</Button>
                </div>
            </div>
        );
    }
}