import * as React from "react";
import { View } from "ninjagoat";
import { TextField } from "material-ui";

import { RegisterViewModel } from "../../scripts/viewmodels/user/RegisterViewModel";
import { EmptyPage } from "../components/EmptyPage";
import { Card } from "../components/Card";
import { Button } from "../components/buttons/Button";

export default class Register extends View<RegisterViewModel> {

    render() {
        let { user, alreadyExist } = this.viewModel;
        return (
            <div className="page">
                {!alreadyExist && user ? <div className="page__content">
                    <Card title="Inserisci i tuoi dati"><div className="row">
                        <div className="col-lg-6 col-md-12">
                            <TextField fullWidth={true} floatingLabelText="Username" value={user.username}
                                onChange={(v: any) => this.viewModel.onUserameChange(v.target.value)} />
                        </div>
                        <div className="col-lg-6 col-md-12">
                            <TextField fullWidth={true} disabled value={user.email} floatingLabelText="Email" />
                        </div>
                        <div className="col-lg-6 col-md-12">
                            <TextField fullWidth={true} type="password" floatingLabelText="Password" value={user.password}
                                onChange={(v: any) => this.viewModel.onPasswordChange(v.target.value)} />
                        </div>
                        <div className="col-lg-6 col-md-12">
                            <TextField fullWidth={true} type="password" floatingLabelText="Conferma Password"
                                onChange={(v: any) => this.viewModel.onConfirmPasswordChange(v.target.value)}
                                value={user.confirmPassword}
                                errorText={user.confirmPassword && user.confirmPassword !== user.password ? "Le password devono essere uguali" : null} />
                        </div>
                    </div></Card>
                    <Button disabled={!this.viewModel.canSave()} onToggle={() => this.viewModel.createUser()}>Registrati</Button>
                </div> : user ? <EmptyPage>
                    <i className="material-icons" style={{ color: "#cc0000" }}>error</i>
                    <span className="little">L'utente {user.email} esiste già</span>
                </EmptyPage> : null}
            </div>
        );
    }
}