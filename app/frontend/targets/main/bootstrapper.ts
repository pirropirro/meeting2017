window.fetch = null;
import "babel-polyfill";
import "whatwg-fetch";
import "jquery";
import "tether";
import "bootstrap";
import "reflect-metadata";
import { Application } from "ninjagoat";
import { AuthModule } from "../../lib/ninjagoat-auth/ninjagoat-auth";

import AppModule from "../../scripts/modules/AppModule";
import { UserModule } from "../../scripts/modules/UserModule";
import { AdminModule } from "../../scripts/modules/AdminModule";
import { SupportModule } from "../../scripts/modules/SupportModule";
import { CommitteeModule } from "../../scripts/modules/CommitteeModule";
import { PreinscriptionModule } from "../../scripts/modules/PreinscriptionModule";

const config = require("../../settings/config.json");
config.backend.host = process.env.BACKEND_HOST || config.backend.host;

let app = new Application();
app.register(new AuthModule());
app.register(new AppModule(config));
app.register(new UserModule);
app.register(new AdminModule);
app.register(new SupportModule);
app.register(new CommitteeModule);
app.register(new PreinscriptionModule);
app.run();
