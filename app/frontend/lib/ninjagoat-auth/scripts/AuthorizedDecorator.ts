import { Grant } from "../../../scripts/grants/Grant";
function Authorized(...grants: Grant[]) {
    return function (target: any) {
        Reflect.defineMetadata("ninjagoat:authorized", true, target);
        if (grants.length > 0) Reflect.defineMetadata("page:grant", grants, target);
        return target;
    };
}

export default Authorized