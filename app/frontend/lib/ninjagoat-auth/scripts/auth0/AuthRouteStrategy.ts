import { IRouteStrategy, INavigationManager } from "ninjagoat";
import { RegistryEntry } from "ninjagoat";
import { RouterState } from "react-router";
import { inject, injectable } from "inversify";
import IAuthProvider from "../interfaces/IAuthProvider";
import IAuthDataRetriever from "../interfaces/IAuthDataRetriever";
import ILocationNavigator from "../interfaces/ILocationNavigator";
import IAuthConfig from "../interfaces/IAuthConfig";
import { IUserGrantProvider } from "../../../../scripts/grants/IUserGrantProvider";

@injectable()
class AuthRouteStrategy implements IRouteStrategy {

    constructor( @inject("IAuthProvider") private authProvider: IAuthProvider,
        @inject("IAuthProvider") private authDataRetriever: IAuthDataRetriever,
        @inject("ILocationNavigator") private locationNavigator: ILocationNavigator,
        @inject("INavigationManager") private navigator: INavigationManager,
        @inject("IAuthConfig") private config: IAuthConfig,
        @inject("IUserGrantProvider") private grant: IUserGrantProvider) {

    }

    enter(entry: RegistryEntry<any>, nextState: RouterState): Promise<string> {
        let needsAuthorization = <boolean>Reflect.getMetadata("ninjagoat:authorized", entry.construct);
        let grants = Reflect.getMetadata("page:grant", entry.construct);
        if (!needsAuthorization) return Promise.resolve("");
        return Promise.resolve(this.authDataRetriever.getIDToken())
            .then(idToken => idToken ? null : this.authProvider.requestSSOData())
            .then(data => !grants || grants.indexOf(this.grant.get()) > -1
                ? data
                : this.navigator.navigate("Error", "Error", { message: "Ruolo non valido" }))
            .then((data: any) => {
                if (!data) return "";
                return this.authProvider.login(this.locationNavigator.getCurrentLocation(), data.sso ? this.config.connection : null);
            });
    }

}

export default AuthRouteStrategy