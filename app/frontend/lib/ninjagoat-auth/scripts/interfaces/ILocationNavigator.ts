interface ILocationNavigator {
    navigate(url:string);
    getCurrentLocation():string;
}

export default ILocationNavigator