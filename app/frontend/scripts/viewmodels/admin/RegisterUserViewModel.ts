import { ObservableViewModel, ViewModel, IHttpClient, Refresh } from "ninjagoat";
import { inject } from "inversify";
import { Grant } from "../../grants/Grant";
import { INotificationsService } from "../../notifications/INotificationsService";
import { Authorized } from "../../../lib/ninjagoat-auth/ninjagoat-auth";
import { IUser, ICommittee } from "../../../../Interface";

@Authorized(Grant.ADMIN, Grant.BACK_OFFICE)
@ViewModel("RegisterUser")
export class RegisterUserViewModel extends ObservableViewModel<any> {
    public user: IUser;
    public committees: ICommittee[] = [];

    constructor( @inject("IHttpClient") private client: IHttpClient,
        @inject("INotificationsService") private notification: INotificationsService) {
        super();
    }

    protected onData(committees: ICommittee[]): void {
        this.committees = committees;
        this.user = { email: "", grant: Grant.MEMBER };
    }

    public createUser() {
        if (!this.canSave()) return;

        this.client.post("/user/register", this.user).subscribe(() =>
            this.notification.notify("Utente registrato correttamente"));
    }

    @Refresh
    public onEmailChange(email: string): void {
        this.user.email = email;
    }

    @Refresh
    public onGrantChange(grant: Grant): void {
        this.user.grant = grant;
    }

    @Refresh
    public onUserameChange(username: string): void {
        this.user.username = username;
    }

    @Refresh
    public onPasswordChange(password: string): void {
        this.user.password = password;
    }

    @Refresh
    public onConfirmPasswordChange(confirmPassword: string): void {
        this.user.confirmPassword = confirmPassword;
    }

    @Refresh
    public onCommitteeChange(committeeId: string): void {
        this.user.committeeId = committeeId;
    }

    public canSave(): boolean {
        return this.user && this.user.email && this.user.grant && this.user.username && this.user.password && this.user.password === this.user.confirmPassword;
    }
}
