import { inject } from "inversify";
import { ObservableViewModel, ViewModel, IHttpClient, Refresh, INavigationManager } from "ninjagoat";
import Authorized from "../../../lib/ninjagoat-auth/scripts/AuthorizedDecorator";
import { Grant } from "../../grants/Grant";
interface IEmail {
    to?: string;
    subject?: string;
    text?: string;
}

@ViewModel("SendEmail")
@Authorized(Grant.ADMIN, Grant.BACK_OFFICE)
class SendEmailViewModel extends ObservableViewModel<any> {
    public mail: IEmail = {};

    protected onData(item: any): void { }

    constructor( @inject("IHttpClient") private client: IHttpClient,
        @inject("INavigationManager") private navigator: INavigationManager) {
        super();
    }

    @Refresh
    public onToChange(replyTo: string): void {
        this.mail.to = replyTo;
    }

    @Refresh
    public onSubjectChange(subject: string): void {
        this.mail.subject = subject;
    }

    @Refresh
    public onTextChange(text: string): void {
        this.mail.text = text;
    }

    public canSend(): boolean {
        return !!this.mail.to && !!this.mail.subject && !!this.mail.text;
    }

    public sendEmail(): void {
        if (!this.canSend()) return;

        this.client.post("/mail/send", this.mail).subscribe(() =>
            this.navigator.navigate("Success", "Success", { message: "Messaggio inviato!" }));
    }
}

export default SendEmailViewModel;
