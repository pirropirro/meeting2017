import { ObservableViewModel, ViewModel, IHttpClient, Refresh, INavigationManager } from "ninjagoat";
import { inject } from "inversify";
import { Grant } from "../../grants/Grant";
import { Authorized } from "../../../lib/ninjagoat-auth/ninjagoat-auth";
import { ICommittee, IPerson } from "../../../../Interface";

@Authorized(Grant.ADMIN, Grant.BACK_OFFICE)
@ViewModel("InviteUser")
export class InviteUserViewModel extends ObservableViewModel<any> {
    public user: IPerson;
    public committees: ICommittee[] = [];

    constructor( @inject("IHttpClient") private client: IHttpClient,
        @inject("INavigationManager") private navigator: INavigationManager) {
        super();
    }

    protected onData(committees: ICommittee[]): void {
        this.committees = committees;
        this.user = { email: "", grant: Grant.MEMBER };
    }

    public inviteUser() {
        if (!this.canInvite()) return;

        this.client.post("/mail/inviteUser", this.user).subscribe(() =>
            this.navigator.navigate("Success", "Success", { message: "Utente invitato correttamente" }));
    }

    @Refresh
    public onEmailChange(email: string): void {
        this.user.email = email;
    }

    @Refresh
    public onGrantChange(grant: Grant): void {
        this.user.grant = grant;
    }

    @Refresh
    public onNameChange(name: string): void {
        this.user.name = name;
    }

    @Refresh
    public onCommitteeChange(committeeId: string): void {
        this.user.committeeId = committeeId;
    }

    public canInvite(): boolean {
        return !!this.user && !!this.user.email && !!this.user.grant && !!this.user.name;
    }
}
