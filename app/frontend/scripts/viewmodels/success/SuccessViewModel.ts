import { inject } from "inversify";
import { ObservableViewModel, ViewModel, INavigationManager } from "ninjagoat";
import { IDisposable, Observable } from "rx";

@ViewModel("Success")
export class SuccessViewModel extends ObservableViewModel<any> {

    public message: string;
    private notifications: IDisposable;

    constructor( @inject("INavigationManager") private navigator: INavigationManager) {
        super();
        this.notifications = Observable.just(null).delay(2500).subscribe(() =>
            this.navigator.navigate("Index", "Index"));
    }

    protected onData(message: string): void {
        this.message = message;
    }

    dispose() {
        super.dispose();
        this.notifications.dispose();
    }
}
