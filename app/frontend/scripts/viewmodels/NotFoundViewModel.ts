import { ObservableViewModel, ViewModel } from "ninjagoat";

@ViewModel("NotFound")
export class NotFoundViewModel extends ObservableViewModel<any> {

    protected onData(item: any): void {
    }
}
