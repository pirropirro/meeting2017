import { ObservableViewModel, ViewModel, IHttpClient, Refresh, INavigationManager } from "ninjagoat";
import { IUser } from "../../../../Interface";
import { inject } from "inversify";
import { Grant } from "../../grants/Grant";

export interface IRegisterState {
    alreadyExist?: boolean;
    email?: string;
    grant?: Grant;
    committeeId?: string;
}

@ViewModel("Register")
export class RegisterViewModel extends ObservableViewModel<IRegisterState> {
    public user: IUser;
    public alreadyExist: boolean;

    constructor( @inject("IHttpClient") private client: IHttpClient,
        @inject("INavigationManager") private navigator: INavigationManager) {
        super();
    }

    protected onData(data: IRegisterState): void {
        this.alreadyExist = data.alreadyExist;
        this.user = { email: data.email, grant: data.grant };
    }

    public createUser() {
        if (this.canSave()) this.client.post("/user/register", this.user)
            .subscribe(() => this.navigator.navigate("Success", "Success", { message: "Utente registrato correttamente" }));
    }

    @Refresh
    public onUserameChange(username: string): void {
        this.user.username = username;
    }

    @Refresh
    public onPasswordChange(password: string): void {
        this.user.password = password;
    }

    @Refresh
    public onConfirmPasswordChange(confirmPassword: string): void {
        this.user.confirmPassword = confirmPassword;
    }

    public canSave(): boolean {
        return this.user && this.user.username && this.user.password && this.user.password === this.user.confirmPassword;
    }
}
