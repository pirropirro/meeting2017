import { ObservableViewModel, ViewModel, Refresh, INavigationManager } from "ninjagoat";
import IAuthProvider from "../../lib/ninjagoat-auth/scripts/interfaces/IAuthProvider";
import { inject } from "inversify";
import { IUser } from "../../../Interface";


@ViewModel("Master")
class MasterViewModel extends ObservableViewModel<IUser> {
    public isMenuOpen: boolean;
    public menuItems = new Map<string, boolean>();
    public user: IUser;

    constructor( @inject("IAuthProvider") private auth: IAuthProvider,
        @inject("INavigationManager") private navigator: INavigationManager) {
        super();
    }
    protected onData(data: IUser): void {
        this.user = data;
    }

    @Refresh
    public openMenu(): void {
        this.isMenuOpen = true;
    }

    @Refresh
    public closeMenu(): void {
        this.isMenuOpen = false;
    }

    @Refresh
    public toggleMenuItem(key: string): void {
        this.menuItems.set(key, !this.menuItems.get(key));
    }

    public isItemOpen(key: string): boolean {
        return this.menuItems.get(key);
    }

    public navigate(area: string, viewmodel: string) {
        this.navigator.navigate(area, viewmodel);
        this.closeMenu();
    }

    public login() {
        this.auth.login("/");
    }

    public logout() {
        this.auth.logout();
    }
}

export default MasterViewModel;
