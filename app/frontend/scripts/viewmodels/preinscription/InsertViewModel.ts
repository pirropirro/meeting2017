import { inject } from "inversify";
import { find, cloneDeep } from "lodash";
import { ObservableViewModel, ViewModel, INavigationManager, Refresh, IHttpClient } from "ninjagoat";
import { ICommittee, IDelegate } from "../../../../Interface";

@ViewModel("Insert")
export class InsertViewModel extends ObservableViewModel<any> {
    public committeeId: string;
    public committee: ICommittee;
    public committees: ICommittee[];

    public get delegate(): IDelegate {
        return this.committee ? this.committee.delegate : {};
    }

    constructor( @inject("IHttpClient") private client: IHttpClient,
        @inject("INavigationManager") private navigator: INavigationManager) {
        super();
    }

    protected onData(committees: ICommittee[]): void {
        this.committees = committees;
    }

    public goNext(): void {
        if (!this.canGoNext()) return;
        this.client.post("/mail/preinscription", this.committee)
            .subscribe(data => this.navigator.navigate("Success", "Success", { message: "Preiscrizione inviata!" }));
    }

    public canGoNext(): boolean {
        return !!this.committeeId && !!this.delegate.name && !!this.delegate.surname && !!this.delegate.phone && !!this.delegate.email;
    }

    @Refresh
    public onCommitteeChange(committeeId: string): void {
        this.committeeId = committeeId;
        this.committee = cloneDeep(find(this.committees, { committeeId: this.committeeId })) || null;
    }

    @Refresh
    public onNameChange(name: string): void {
        this.delegate.name = name;
    }

    @Refresh
    public onSurnameChange(surname: string): void {
        this.delegate.surname = surname;
    }

    @Refresh
    public onEmailChange(email: string): void {
        this.delegate.email = email;
    }

    @Refresh
    public onPhoneChange(phone: string): void {
        this.delegate.phone = phone;
    }
}
