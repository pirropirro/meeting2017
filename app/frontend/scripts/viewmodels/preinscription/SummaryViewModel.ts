import { inject } from "inversify";
import { ObservableViewModel, ViewModel, INavigationManager } from "ninjagoat";

@ViewModel("Summary")
class SummaryViewModel extends ObservableViewModel<any> {

    constructor( @inject("INavigationManager") private navigator: INavigationManager) {
        super();
    }

    protected onData(item: any): void { }

    public goNext(): void {
        this.navigator.navigate("preinscription", "insert");
    }
}

export default SummaryViewModel;
