import { inject } from "inversify";
import { ObservableViewModel, ViewModel, INavigationManager, Refresh, IHttpClient } from "ninjagoat";
import { ICommittee } from "../../../../Interface";

@ViewModel("Accept")
export class AcceptViewModel extends ObservableViewModel<any> {
    public committeeId: string;

    constructor( @inject("IHttpClient") private client: IHttpClient,
        @inject("INavigationManager") private navigator: INavigationManager) {
        super();
    }

    protected onData(): void { }

    public goNext(): void {
        if (!this.canGoNext()) return;

        this.client.post("/committee/preregister", { committeeId: this.committeeId })
            .map(data => data.response)
            .subscribe((c: ICommittee) => this.navigator.navigate("Success", "Success", { message: `${c.committeeName} preiscritto correttamente!` }));
    }

    public canGoNext(): boolean {
        return !!this.committeeId;
    }

    @Refresh
    public onCommitteeChange(committeeId: string): void {
        this.committeeId = committeeId;
    }

}
