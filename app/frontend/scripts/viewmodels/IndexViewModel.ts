import { ObservableViewModel, ViewModel } from "ninjagoat";

@ViewModel("Index")
class IndexViewModel extends ObservableViewModel<any> {

    protected onData(item: any): void {
    }
}

export default IndexViewModel;
