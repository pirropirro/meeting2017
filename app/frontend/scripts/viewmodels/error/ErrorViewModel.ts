import { inject } from "inversify";
import { ObservableViewModel, ViewModel, INavigationManager } from "ninjagoat";

@ViewModel("Error")
export class ErrorViewModel extends ObservableViewModel<any> {

    public message;

    constructor( @inject("INavigationManager") private navigator: INavigationManager) {
        super();
        // setTimeout(() => this.navigator.navigate("Index", "Index"), 2500);
    }

    protected onData(message: string): void {
        this.message = message;
    }
}
