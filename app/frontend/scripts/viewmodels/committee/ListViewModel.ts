import { ObservableViewModel, ViewModel, Refresh, IHttpClient } from "ninjagoat";
import { Authorized } from "../../../lib/ninjagoat-auth/ninjagoat-auth";
import { inject } from "inversify";
import { remove, merge, sortBy } from "lodash";
import { INotificationsService } from "../../notifications/INotificationsService";
import { ICommittee } from "../../../../Interface";

@Authorized()
@ViewModel("List")
class ListViewModel extends ObservableViewModel<any> {
    public committees: ICommittee[] = [];
    public actionsVisible: boolean;

    constructor( @inject("IHttpClient") private client: IHttpClient,
        @inject("INotificationsService") private notification: INotificationsService) {
        super();
    }

    protected onData(committees: ICommittee[]): void {
        this.committees = sortBy(committees, c => c.committeeName.toLowerCase());
        this.committees.forEach(c => c.dirty = false);
    }

    @Refresh
    public toggleActions(): void {
        this.actionsVisible = !this.actionsVisible;
    }

    @Refresh
    public onNameChange(committee: ICommittee, name: string): void {
        committee.committeeName = name;
        committee.dirty = true;
    }

    @Refresh
    public onMailChange(committee: ICommittee, email: string): void {
        committee.email = email;
        committee.dirty = true;
    }

    @Refresh
    public onCounciliorEmail(committee: ICommittee, counciliorEmail: string): void {
        committee.councilior = committee.councilior || {};
        committee.councilior.email = counciliorEmail;
        committee.dirty = true;
    }

    @Refresh
    public addCommittee(): void {
        this.committees.push({ committeeName: "Nuovo comitato", dirty: true, councilior: {}, delegate: {} });
    }

    public removeCommittee(c: ICommittee): void {
        if (!c.committeeId) this._remove(c);
        else this.client.delete(`/committee/delete/${c.committeeId}`)
            .subscribe(() => this._remove(c));
    }

    public updateCommittee(committee: ICommittee): void {
        delete committee.dirty;
        this.client.post("/committee/update", committee)
            .map(r => r.response)
            .subscribe(c => this.updateAfterSave(committee, c));
    }

    public updateAll(): void {
        this.committees.filter(c => c.dirty && this.canSave(c)).forEach(c => this.updateCommittee(c));
    }

    public canSave(c: ICommittee): boolean {
        return !!c.committeeName && !!c.email && !!c.councilior && !!c.councilior.email;
    }

    public canSaveAll(): boolean {
        return this.committees.filter(c => c.dirty && this.canSave(c)).length > 0;
    }

    @Refresh
    private _remove(c: ICommittee): void {
        remove(this.committees, i => i === c);
        this.notification.notify("Comitato eliminato correttamente");
    }

    @Refresh
    private updateAfterSave(o: ICommittee, n: ICommittee): void {
        merge(o, n);
        this.notification.notify("Comitato salvato correttamente");
    }
}

export default ListViewModel;
