import { inject } from "inversify";
import { ObservableViewModel, ViewModel, IHttpClient, Refresh, INavigationManager } from "ninjagoat";

interface IEmail {
    replyTo?: string;
    subject?: string;
    text?: string;
}

@ViewModel("Email")
class EmailViewModel extends ObservableViewModel<any> {
    public mail: IEmail = {};

    protected onData(item: any): void { }

    constructor( @inject("IHttpClient") private client: IHttpClient,
        @inject("INavigationManager") private navigator: INavigationManager) {
        super();
    }

    @Refresh
    public onSenderChange(replyTo: string): void {
        this.mail.replyTo = replyTo;
    }

    @Refresh
    public onSubjectChange(subject: string): void {
        this.mail.subject = subject;
    }

    @Refresh
    public onTextChange(text: string): void {
        this.mail.text = text;
    }

    public canSend(): boolean {
        return !!this.mail.replyTo && !!this.mail.subject && !!this.mail.text;
    }

    public sendEmail(): void {
        if (!this.canSend()) return;

        this.client.post("/mail/support", { replyTo: this.mail.replyTo, subject: `[RICHIESTA SUPPORTO] ${this.mail.subject}`, text: this.mail.text })
            .subscribe(() => this.navigator.navigate("Success", "Success", { message: "Messaggio inviato!" }));
    }
}

export default EmailViewModel;
