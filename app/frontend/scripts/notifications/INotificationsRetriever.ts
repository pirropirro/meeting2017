import { Observable } from "rx";
export interface INotificationsRetriever {
    notifications: Observable<string>;
    onClose(): void;
}
