import { INotificationsService } from "./INotificationsService";
import { INotificationsRetriever } from "./INotificationsRetriever";
import { Subject } from "rx";
import { injectable, inject } from "inversify";
import { INavigationManager } from "ninjagoat";

@injectable()
export class NotificationsService implements INotificationsService, INotificationsRetriever {
    public messages: string[] = [];
    public notifications = new Subject<string>();

    constructor( @inject("INavigationManager") private navigator: INavigationManager) { }

    notify(message: string): void {
        this.messages.push(message);
        if (this.messages.length === 1) this.sendMessage(message);
    };

    error(error: string): void {
        this.navigator.navigate("Error", "Error", { message: error });
    };

    onClose(): void {
        this.messages.shift();
        this.sendMessage(this.messages[0]);
    };

    private sendMessage(message: string) {
        this.notifications.onNext(message);
    }
}
