import * as React from "react";
import { IDisposable } from "rx";
import Snackbar from "material-ui/Snackbar";
import { lazyInject } from "ninjagoat";
import { INotificationsRetriever } from "./INotificationsRetriever";

export class NotificationBar extends React.Component<{}, { message?: string }> {
    @lazyInject("INotificationsRetriever")
    private retriever: INotificationsRetriever;
    private subscription: IDisposable;

    constructor(props) {
        super(props);
        this.state = { message: null };
    }

    componentWillMount(): void {
        this.subscription = this.retriever.notifications.subscribe(message => this.setState({ message: message }));
    }

    componentWillUnmount(): void {
        if (this.subscription) this.subscription.dispose();
    }

    render() {

        return <Snackbar open={!!this.state.message}
            message={this.state.message || ""}
            autoHideDuration={4000}
            onRequestClose={() => this.retriever.onClose()} />;
    }
}