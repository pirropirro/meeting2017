export interface INotificationsService {
    notify(message: string): void;
    error(message: string): void;
}
