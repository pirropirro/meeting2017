import { Grant } from "./Grant";

export interface IUserGrantProvider {
    set(grant: Grant): void;
    get(): Grant;
}
