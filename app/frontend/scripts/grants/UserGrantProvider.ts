import { Grant } from "./Grant";
import { IUserGrantProvider } from "./IUserGrantProvider";
import { injectable, inject } from "inversify";
import { ISettingsManager } from "ninjagoat";

@injectable()
export class UserGrantProvider implements IUserGrantProvider {
    private grant: Grant;
    constructor( @inject("ISettingsManager") private settings: ISettingsManager) { }

    set(grant: Grant): void {
        this.grant = grant;
        this.settings.setValue("userGrant", grant);
    };

    get(): Grant {
        return this.grant || this.settings.getValue("userGrant");
    };
}
