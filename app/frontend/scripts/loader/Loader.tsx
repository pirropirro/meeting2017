import * as React from "react";
import { IDisposable } from "rx";
import { lazyInject } from "ninjagoat";
import * as classnames from "classnames";
import { ILoaderService } from "./ILoaderService";

export class Loader extends React.Component<{}, { visible?: boolean }> {
    @lazyInject("ILoaderService")
    private service: ILoaderService;
    private subscription: IDisposable;

    constructor(props) {
        super(props);
        this.state = { visible: false };
    }

    componentWillMount(): void {
        this.subscription = this.service.notifications.subscribe(visible => this.setState({ visible: visible }));
    }

    componentWillUnmount(): void {
        if (this.subscription) this.subscription.dispose();
    }

    render() {
        return <div className={classnames("mt-loader", this.state.visible ? "visible" : null)}>
            <div className="spinner">
                <div className="double-bounce1"></div>
                <div className="double-bounce2"></div>
            </div>
            <span className="mt-loader__label">Caricamento</span>
        </div>;
    }
}