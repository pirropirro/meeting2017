import { Subject } from "rx";
import { ILoaderService } from "./ILoaderService";
import { injectable } from "inversify";

@injectable()
export class LoaderService implements ILoaderService {
    public notifications = new Subject<boolean>();
    show(): void {
        this.notifications.onNext(true);
    };
    hide(): void {
        this.notifications.onNext(false);
    };
}
