import { Observable } from "rx";
export interface ILoaderService {
    notifications: Observable<boolean>;
    show(): void;
    hide(): void;
}
