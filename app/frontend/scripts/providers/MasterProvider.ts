import { Observable } from "rx";
import { injectable, inject } from "inversify";
import { IHttpClient, ISettingsManager } from "ninjagoat";
import { IUser } from "../../../Interface";
import { IUserGrantProvider } from "../grants/IUserGrantProvider";

@injectable()
export class MasterProvider {
    constructor( @inject("IHttpClient") private client: IHttpClient,
        @inject("ISettingsManager") private settings: ISettingsManager,
        @inject("IUserGrantProvider") private provider: IUserGrantProvider) { }

    provide(): Observable<IUser> {
        let token = this.settings.getValue("auth_id_token");
        return !token
            ? Observable.empty()
            : this.client.get(`/user/byToken/${token}`)
                .map(res => res ? res.response || res : null)
                .do((user: IUser) => this.provider.set(user ? user.grant : null));
    }
}