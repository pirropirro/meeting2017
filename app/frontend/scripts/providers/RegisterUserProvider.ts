import { Observable } from "rx";
import { injectable, inject } from "inversify";
import { IHttpClient, ViewModelContext } from "ninjagoat";
import { IRegisterState } from "../viewmodels/user/RegisterViewModel";

@injectable()
export class RegisterUserProvider {
    constructor( @inject("IHttpClient") private client: IHttpClient) { }

    provide(context: ViewModelContext): Observable<IRegisterState> {
        let { email, grant, committeeId } = context.parameters;
        return Observable.combineLatest(this.client.get(`/user/decrypt/${grant}`), this.client.get(`/user/check/${email}`),
            (g, { response }) => !response ? g.response.grant : null).map(decrypt =>
                ({ alreadyExist: !decrypt, email: email, grant: decrypt, committeeId: committeeId }));
    }
}   