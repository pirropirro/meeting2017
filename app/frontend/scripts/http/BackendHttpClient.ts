import { IHttpClient } from "ninjagoat";
import { HttpResponse } from "ninjagoat";
import { Dictionary } from "ninjagoat";
import { inject, injectable } from "inversify";
import { Observable } from "rx";
import { IBackendConfig } from "./IBackendConfig";
import { INotificationsService } from "../notifications/INotificationsService";
import IAuthProvider from "../../lib/ninjagoat-auth/scripts/interfaces/IAuthProvider";
import { ILoaderService } from "../loader/ILoaderService";

@injectable()
export class BackendHttpClient implements IHttpClient {

    constructor( @inject("HttpClient") private httpClient: IHttpClient,
        @inject("IBackendConfig") private config: IBackendConfig,
        @inject("INotificationsService") private notification: INotificationsService,
        @inject("ILoaderService") private loader: ILoaderService,
        @inject("IAuthProvider") private auth: IAuthProvider) { }

    get(url: string, headers?: Dictionary<string>): Observable<HttpResponse> {
        return this.handleResponse(this.httpClient.get(this.concat(url), headers));
    }

    post(url: string, body: any, headers?: Dictionary<string>): Observable<HttpResponse> {
        return this.handleResponse(this.httpClient.post(this.concat(url), body, headers));
    }

    put(url: string, body: any, headers?: Dictionary<string>): Observable<HttpResponse> {
        return this.handleResponse(this.httpClient.put(this.concat(url), body, headers));
    }

    delete(url: string, headers?: Dictionary<string>): Observable<HttpResponse> {
        return this.handleResponse(this.httpClient.delete(this.concat(url), headers));
    }

    private concat(url: string): string {
        return `${this.config.host}${url}`;
    }

    private handleResponse(observable: Observable<HttpResponse>): Observable<HttpResponse> {
        this.loader.show();
        observable.subscribe(data => data, err => this.onError(err), () => this.loader.hide());
        return observable;
    }

    private onError(error: any): void {
        let message = error.message || (error.response ? error.response.message : null) || "ERRORE";
        if (error.statusCode === 401 || error.status === 401) this.auth.logout();
        else this.notification.error(message);
        this.loader.hide();
    }
}

