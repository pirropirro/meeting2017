import * as Cookies from "js-cookie";
import { ISettingsManager } from "ninjagoat";
import { injectable } from "inversify";

@injectable()
export class CookieSettingsManager implements ISettingsManager {
    getValue<T>(key: string, fallback?: T): T {
        return Cookies.get(key) as any;
    };
    setValue<T>(key: string, value: T): void {
        if (value) Cookies.set(key, value);
        else Cookies.remove(key);
    };
}