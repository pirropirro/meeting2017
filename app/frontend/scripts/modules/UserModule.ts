import { interfaces } from "inversify";
import { IModule, IViewModelRegistry, IServiceLocator } from "ninjagoat";
import { RegisterViewModel } from "../viewmodels/user/RegisterViewModel";
import { RegisterUserProvider } from "../providers/RegisterUserProvider";

export class UserModule implements IModule {
    modules = (container: interfaces.Container) => {
        container.bind<RegisterUserProvider>("RegisterUserProvider").to(RegisterUserProvider).inSingletonScope();
    };

    register(registry: IViewModelRegistry, serviceLocator?: IServiceLocator, overrides?: any): void {
        registry.add(RegisterViewModel, c => serviceLocator.get<RegisterUserProvider>("RegisterUserProvider").provide(c))
            .forArea("user");
    }
}
