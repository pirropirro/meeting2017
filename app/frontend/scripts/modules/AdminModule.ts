import { Observable } from "rx";
import { interfaces } from "inversify";
import { IModule, IViewModelRegistry, IServiceLocator, IHttpClient } from "ninjagoat";

import SendEmailViewModel from "../viewmodels/admin/SendEmailViewModel";
import { InviteUserViewModel } from "../viewmodels/admin/InviteUserViewModel";
import { RegisterUserViewModel } from "../viewmodels/admin/RegisterUserViewModel";

export class AdminModule implements IModule {
    modules = (container: interfaces.Container) => { };

    register(registry: IViewModelRegistry, serviceLocator?: IServiceLocator, overrides?: any): void {
        let client = serviceLocator.get<IHttpClient>("IHttpClient");

        registry
            .add(RegisterUserViewModel, () => client.get("/committee/list").map(res => res.response))
            .add(InviteUserViewModel, () => client.get("/committee/list").map(res => res.response))
            .add(SendEmailViewModel, () => Observable.empty())
            .forArea("admin");
    }
}
