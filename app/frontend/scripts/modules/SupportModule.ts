import { Observable } from "rx";
import { interfaces } from "inversify";
import { IModule, IViewModelRegistry, IServiceLocator } from "ninjagoat";

import EmailViewModel from "../viewmodels/support/EmailViewModel";

export class SupportModule implements IModule {
    modules = (container: interfaces.Container) => { };

    register(registry: IViewModelRegistry, serviceLocator?: IServiceLocator, overrides?: any): void {
        registry.add(EmailViewModel, () => Observable.empty()).forArea("support");
    }
}
