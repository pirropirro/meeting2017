import { interfaces } from "inversify";
import { IModule, IViewModelRegistry, IServiceLocator, IHttpClient } from "ninjagoat";

import ListViewModel from "../viewmodels/committee/ListViewModel";

export class CommitteeModule implements IModule {
    modules = (container: interfaces.Container) => { };

    register(registry: IViewModelRegistry, serviceLocator?: IServiceLocator, overrides?: any): void {
        let client = serviceLocator.get<IHttpClient>("IHttpClient");

        registry.add(ListViewModel, () => client.get(`/committee/list`).map(res => res.response))
            .forArea("committee");
    }
}
