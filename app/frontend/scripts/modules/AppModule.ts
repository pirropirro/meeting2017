import { IModule, IViewModelRegistry, IServiceLocator, IHttpClient, HttpClient, ISettingsManager } from "ninjagoat";
import { interfaces } from "inversify";
import { Observable } from "rx";
import IndexViewModel from "../viewmodels/IndexViewModel";
import MasterViewModel from "../viewmodels/MasterViewModel";
import { NotFoundViewModel } from "../viewmodels/NotFoundViewModel";
import { IAuthConfig } from "../../lib/ninjagoat-auth/ninjagoat-auth";
import { MasterProvider } from "../providers/MasterProvider";
import { BackendHttpClient } from "../http/BackendHttpClient";
import { IBackendConfig } from "../http/IBackendConfig";
import { INotificationsRetriever } from "../notifications/INotificationsRetriever";
import { NotificationsService } from "../notifications/NotificationsService";
import { INotificationsService } from "../notifications/INotificationsService";
import { CookieSettingsManager } from "../settings/CookieSettingsManager";
import { LoaderService } from "../loader/LoaderService";
import { ILoaderService } from "../loader/ILoaderService";
import { SuccessViewModel } from "../viewmodels/success/SuccessViewModel";
import { ErrorViewModel } from "../viewmodels/error/ErrorViewModel";
import { IUserGrantProvider } from "../grants/IUserGrantProvider";
import { UserGrantProvider } from "../grants/UserGrantProvider";

class AppModule implements IModule {
    constructor(private config) { }

    modules = (container: interfaces.Container) => {
        this.config.auth.loginCallbackUrl = location.origin + this.config.auth.loginCallbackUrl;
        this.config.auth.logoutCallbackUrl = location.origin + this.config.auth.logoutCallbackUrl;
        this.config.auth.renewCallbackUrl = location.origin + this.config.auth.renewCallbackUrl;

        container.bind<any>("Views").toConstantValue(require("../../views/export"));
        container.bind<IAuthConfig>("IAuthConfig").toConstantValue(this.config.auth);
        container.bind<IBackendConfig>("IBackendConfig").toConstantValue(this.config.backend);

        container.unbind("IHttpClient");
        container.bind<HttpClient>("HttpClient").to(HttpClient);
        container.bind<IHttpClient>("IHttpClient").to(BackendHttpClient)

        container.unbind("ISettingsManager");
        container.bind<ISettingsManager>("ISettingsManager").to(CookieSettingsManager);

        container.bind<MasterProvider>("MasterProvider").to(MasterProvider).inSingletonScope();

        container.bind<INotificationsRetriever>("INotificationsRetriever").to(NotificationsService).inSingletonScope();
        container.bind<INotificationsService>("INotificationsService").toDynamicValue(() => container.get<NotificationsService>("INotificationsRetriever"));

        container.bind<ILoaderService>("ILoaderService").to(LoaderService).inSingletonScope();
        container.bind<IUserGrantProvider>("IUserGrantProvider").to(UserGrantProvider).inSingletonScope();
    };

    register(registry: IViewModelRegistry, serviceLocator?: IServiceLocator, overrides?: any): void {
        let masterProvider = serviceLocator.get<MasterProvider>("MasterProvider");

        registry.master(MasterViewModel, () => masterProvider.provide());
        registry.index(IndexViewModel, () => Observable.empty());
        registry.notFound(NotFoundViewModel, () => Observable.empty());

        registry.add(SuccessViewModel, c => Observable.just(c.parameters.message), ":message").forArea("success");
        registry.add(ErrorViewModel, c => Observable.just(c.parameters.message), ":message").forArea("error");
    }
}

export default AppModule;
