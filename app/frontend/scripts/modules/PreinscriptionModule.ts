import { Observable } from "rx";
import { interfaces } from "inversify";
import { IModule, IViewModelRegistry, IServiceLocator, IHttpClient } from "ninjagoat";

import SummaryViewModel from "../viewmodels/preinscription/SummaryViewModel";
import { InsertViewModel } from "../viewmodels/preinscription/InsertViewModel";
import { AcceptViewModel } from "../viewmodels/preinscription/AcceptViewModel";

export class PreinscriptionModule implements IModule {
    modules = (container: interfaces.Container) => { };

    register(registry: IViewModelRegistry, serviceLocator?: IServiceLocator, overrides?: any): void {
        let client = serviceLocator.get<IHttpClient>("IHttpClient");

        registry
            .add(SummaryViewModel, () => Observable.empty())
            .add(InsertViewModel, () => client.get("/committee/notPreregistered").map(res => res.response))
            .add(AcceptViewModel, () => Observable.empty())
            .forArea("preinscription");
    }
}
