export interface ICommittee {
    committeeId?: string;
    committeeName?: string;
    email?: string;
    councilior?: ICouncilior;
    delegate?: IDelegate;
    dirty?: boolean;
    preRegistered?: boolean;
    preRegisteredAccepted?: boolean;
    registered?: boolean;
}

export enum Grant {
    ADMIN = "ADMIN",
    BACK_OFFICE = "BACK_OFFICE",
    JUDGE = "JUDGE",
    SUPERVISOR = "SUPERVISOR",
    CAPTAIN = "CAPTAIN",
    MEMBER = "MEMBER",
    OBSERVER = "OBSERVER"
}

export interface IUser {
    principalId?: string;
    userId?: string;
    grant?: Grant;
    email: string;
    username?: string;
    password?: string;
    confirmPassword?: string;
    committeeId?: string;
}

export interface IPerson {
    name?: string;
    surname?: string;
    phone?: string;
    email?: string;
    committeeId?: string;
    grant?: Grant;
}

export interface IDelegate extends IPerson { }
export interface ICouncilior extends IPerson { }