
echo "Activate environment $1"
BASEDIR=$(dirname -- "$0")
source $BASEDIR/$1/env.sh
echo "[START] Pulling latest version"
git reset --hard --quiet
git pull --quiet
echo "[END] Pulling latest version"
echo "[START] Build frontend"
cd $BASEDIR/../app/frontend 
npm i
rm -rf dist
smild build main
echo "[END] Build frontend"
echo "[START] Build backend"
cd ../backend
npm i 
rm -rf dist
tsc -p .
echo "[END] Build backend"
echo "[START] Start backend"
killall -9 node
npm run start
